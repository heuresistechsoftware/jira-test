/**
 * Smd - Safety micro daemon 
 *  
 * This program acts as a high level software interface to the 
 * safety micro board.
 *  
 * The primary communication to the hardware is performed via 
 * the SPI bus through the spidev driver. 
 *  
 * This program has two modes: command line and daemon. 
 *  
 * The command line mode serves to provide a unit test interface 
 * for the commands. The same command processor is used
 * for both. 
 *  
 * This program naturally progressed from a simple spidev unit 
 * test program to a much more elegant implementation.
 *  As a result some code artifacts are left in that are
 * useful for testing. 
 */




#include <stdio.h>
#include <cassert>
#include <cstring>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <unistd.h>
#include <pthread.h>

#include "spidev.h"
#include "jni.h"
#include "fcntl.h"
#include "gpio.h"

#include "android/log.h"
#define LOG_TAG_SM "SM-jni"

#ifdef SM_DEBUG
#define SM_LOG(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG_SM, __VA_ARGS__)
#else
#define SM_LOG(...)
#endif

static int fd_;

#define SM_STATUS_MASK  0x07
#define SM_STATUS_SHIFT 5

#define MOTOR_ENABLE 37 // Motor enable I/O is GPIO2_IO5 NANDF_D5
#define SM_RESET 35  //GPIO2_IO3 NANDF_D3
#define GREEN_LED 174
#define RED_LED 171

static bool byPass;
static bool momentaryTrigger;
static long triggerTimeOut;
static long surfaceTimeOut;
pthread_mutex_t smLock;

enum CommandId
{
    ciNull                  = 0,
  ciNoOp                    = 0x01, ///< No-op; simple comm check
  ciGetVersion              = 0x02, ///< Get version information
  ciGetSystemStatus         = 0x03, ///< Get system status
  ciGetFaultStatus          = 0x04, ///< Get current fault status
  ciGetConfigParms          = 0x05, ///< Get current config options
  ciSetConfigParms          = 0x06, ///< Set config options
  ciGetShutterDriveTime     = 0x07, ///< Get shutter drive time
  ciSetShutterDriveTime     = 0x08, ///< Set shutter drive time
  ciGetShutterVarHoldTime   = 0x09, ///< Get shutter variable hold time
  ciSetShutterVarHoldTime   = 0x0A, ///< Set shutter variable hold time
  ciGetShutterFixedHoldTime = 0x0B, ///< Get shutter fixed hold time
  ciSetShutterFixedHoldTime = 0x0C, ///< Set shutter fixed hold time
  ciArmShutter              = 0x0D, ///< Arm the shutter for measurement
  ciSoftwareTrigger         = 0x0E, ///< Send a software trigger event
  ciCloseShutter            = 0x0F, ///< Close shutter immediately
  ciOpenShutterNoInterlocks = 0x10, ///< Open shutter, ignore interlocks
  ciEnableInterlockOverride = 0x11, ///< Enable interlock override
  ciDisArmShutter			= 0x12, ///< Disarms the shutter
  cigetSurfaceBypass        = 0x13,
  cisetSurfaceBypass        = 0x14,
  ciseDumy        = 0x15, ///< True enables momentary trigger
    cisetSafetyTimeout        = 0x16,
    Unsued_Leave_It         = 0x17,
    cisetMomentaryTrig         =0x18,
  ciEndMarker
};


void dumpstat(const char *name, int fd)
{
	__u8 mode, lsb, bits;
	__u32 speed;

	if (ioctl(fd, SPI_IOC_RD_MODE, &mode) < 0) {
		perror("SPI rd_mode");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
		perror("SPI rd_lsb_fist");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) {
		perror("SPI bits_per_word");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
		perror("SPI max_speed_hz");
		return;
	}

	printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",
		 name, mode, bits, lsb ? "(lsb first) " : "", speed);
}



/** Perform an SPI transaction.  The Safety Micro protocol specifies a
 * half-duplex operation - for simplicity on the SM side, even though the SPI
 * interface is inherently full-duplex.  So we have at least three different
 * options for how we implement this:
 *   1. A series of ioctl calls, as Chris' first attempt went
 *   2. A write() call, followed by a read() call
 *   3. A single, full-duplex transaction, as was intended.
 *
 * For the full-duplex approach, the Master sends NC bytes, followed by NR null
 * bytes; here, NC equals the number of "command" bytes, and NR equals the
 * number of "response" bytes - these are both fixed for any particular command.
 * For the Slave's part, it responds with NC null bytes, then NR data bytes.
 *
 * Because client functions provide the command opcode and all argument bytes,
 * NC equals the supplied nx parameter; however, because we process the ack
 * byte, NR is equal to the supplied parameter nr + 1.  So the total number of
 * bytes we will transfer is (nx + 1 + nr).
 *
 * Because the commands are small and fixed in size, we simply create small
 * buffers on the stack for both transmit and receive, and we copy the argument
 * bytes into and response bytes out of these buffers from or to the client
 * buffers.
 *
 * \param [in]  nx     Number of command bytes to send
 * \param [in]  xbuff  Pointer to buffer containing bytes to send
 * \param [in]  nr     Number of response bytes to receive
 * \param [out] rbuff  Pointer to buffer to receive response bytes
 *
 * \note The null bytes, while nominally 0, may actually be any value at
 * all.  In particular, the Slave's first transmitted byte of a transaction may
 * be what it sent last; this depends entirely on the hardware and firmware
 * details.  For simplicity, null bytes are not defined, and receivers of them
 * must not process them in any way.
 */
char doSpiTransaction( int fd,
					   uint8_t nx,
					   uint8_t* xbuff,
					   uint8_t nr,
					   uint8_t* rbuff )
{
	#define MAX_SIZE 8
    char retVal;
    pthread_mutex_lock(&smLock);

        struct spi_ioc_transfer xfer;
        uint8_t lxbuff[MAX_SIZE];
        uint8_t lrbuff[MAX_SIZE];

        memset(&xfer, 0, sizeof(struct spi_ioc_transfer));

        //SM_LOG("doSpiTransaction - xfer zero'd");
        xfer.len = nx + nr + 2;
        memset((void*)lxbuff, (int)0, (int)xfer.len);
        memset(lrbuff, 0, xfer.len);
        memcpy((void*)lxbuff, (const void*)xbuff, nx);

        xfer.tx_buf = (unsigned long)lxbuff;
        xfer.rx_buf = (unsigned long)lrbuff;
        int ret;
        //SM_LOG("Xfer intialized - sending msg to SM");

        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &xfer);
        //SM_LOG("SPI transfer result: %d", ret);

        if (nr > 0) {
            memcpy((void*)rbuff, (const void*)&lrbuff[nx + 2], (int)nr);
            //SM_LOG("Rbuff %x ", *rbuff);
        }

        retVal = (char) ((lrbuff[nx + 2] >> SM_STATUS_SHIFT) & SM_STATUS_MASK);

    pthread_mutex_unlock(&smLock);

	return retVal;
}

extern "C" {

int InitializeSMD(  )
{
    const char *spidev = "/dev/spidev0.0";
    static uint32_t speed = 1000000; /** 1MHz */
    char mode = SPI_MODE_1;
    int ret;

    fd_ = open(spidev, O_RDWR);
    if (fd_ < 0) {
    	SM_LOG("SM failed to open spi");
      perror(spidev);
      return -1;
    }
    /** set the mode and speed */
    ret = ioctl(fd_, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
    {
    	SM_LOG("SM write mode failed");
        perror(spidev);
        return -2;
    }

    ret = ioctl(fd_, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
    {
    	SM_LOG("SM spi max speed failed");
        perror(spidev);
        return -3;
    }

    // In order for the shutter to open the motor needs to be enabled by the
    // application.
    gpio_set_dir(MOTOR_ENABLE, (unsigned int) true); //set I/O as output
    gpio_set_value(MOTOR_ENABLE, (unsigned int) true); // Turn on motor enable

    gpio_set_dir(SM_RESET, (unsigned int) true); //set I/O as output
    gpio_set_value(SM_RESET, (unsigned int) false); // Put SM in reset;
    usleep(500000);
    gpio_set_value(SM_RESET, (unsigned int) true); // Take SM out of reset

    // Control for LED LCD used to indicate prox state/xray status.
    gpio_set_dir(GREEN_LED, (unsigned int) true); //set I/O as output
    gpio_set_dir(RED_LED, (unsigned int) true); //set I/O as output

    SM_LOG("SM Initialized!");
	return ret;
}

int closeSMD( )
{
	gpio_set_dir(MOTOR_ENABLE, (unsigned int) true); //set I/O as output
    int val = gpio_set_value(MOTOR_ENABLE, (unsigned int) false); // Turn off motor enable
    if ( val < 0)
    	return -2;

	if(fd_)
    {
    	close(fd_);
		return 0;
	}
	else
		return -1;
}

int noOp( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciNoOp) };
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

JNIEXPORT jbyteArray JNICALL getVersion( JNIEnv *env, jobject obj )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetVersion) };
	jbyte resp[2];
	//jbyteArray resp_byteArray[2];

	jbyteArray versionByteArray;

	versionByteArray = env->NewByteArray(2);

	uint8_t result = (uint8_t) doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(resp), (uint8_t*)resp);

	if (result == 0)
	{
		env->SetByteArrayRegion(versionByteArray, 0, 2, resp);
	}
	return versionByteArray;
}


/********
 * Bit config for status byte. This track the state of these items.
 * A 1 mean it is on/active and a 0 indicates it is off/disabled
 *
    ShutterOpen      0x01
    ShutterClosed    0x02
    HardTrigger      0x04
    Surface          0x08
    Fault            0x10
    WarningLED       0x20
    ConfigSts        0x40
    SS7       		 0x80
 *
 */
int getSystemStatus()
{
  uint8_t status = 0;
  uint8_t cmd[1] = { static_cast<uint8_t>(ciGetSystemStatus) };

  doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(status), &status);

  return status;
}


/*****
 *  Fault bits. A one indicates a fault/problem with the item. A 0 indicates its working properly.
 *
    MotorSupply      0x01
    LEDsFault        0x02
    TriggerSwitch    0x04
    ShutterDrive     0x08
    Heartbeat        0x10
    FirmwareFault    0x20
    SurfaceSensor    0x40
    ShutterSensor    0x80
 *
 */
int getFaultStatus( )
{
	uint8_t faults;

	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetFaultStatus) };

	doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(faults), &faults);

	return faults;
}

int getConfigParms( )
{
	uint8_t parms;

	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetConfigParms) };

	doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(parms), &parms);

	return parms;
}

int setConfigParms( uint8_t parms )
{
	uint8_t cmd[2] = { static_cast<uint8_t>(ciSetConfigParms), parms };

	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int getShutterDriveTime()
{
	uint8_t dtime;

	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetShutterDriveTime) };

	doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(dtime), &dtime);

	return dtime;
}

int setShutterDriveTime( uint8_t dtime )
{
	uint8_t cmd[2] = { static_cast<uint8_t>(ciSetShutterDriveTime), dtime };

	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int getShutterVarHoldTime( )
{
	uint16_t vhtime = 0;

	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetShutterVarHoldTime) };
	uint8_t resp[2];

	uint8_t result = (uint8_t) doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(resp), resp);

	if (result == 0) {
		vhtime = (static_cast<uint16_t>(resp[0]) << 8) | resp[1];
	}

	return vhtime;
}

int setShutterVarHoldTime( uint16_t vhtime )
{
  uint8_t cmd[3] = { static_cast<uint8_t>(ciSetShutterVarHoldTime),
                     static_cast<uint8_t>(vhtime >> 8),
                     static_cast<uint8_t>(vhtime) };
  return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int getShutterFixedHoldTime( )
{
	uint16_t fhtime = 0;

	uint8_t cmd[1] = { static_cast<uint8_t>(ciGetShutterFixedHoldTime) };
	uint8_t resp[2];

	uint8_t result = (uint8_t) doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(resp), resp);

	if (result == 0) {
		fhtime = (static_cast<uint16_t>(resp[0]) << 8) | resp[1];
	}

	return fhtime;
}

int setShutterFixedHoldTime( uint16_t fhtime )
{
	uint8_t cmd[3] = { static_cast<uint8_t>(ciSetShutterFixedHoldTime),
					 static_cast<uint8_t>(fhtime >> 8),
					 static_cast<uint8_t>(fhtime) };

	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);

}

int armShutter( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciArmShutter) };
	//SM_LOG( "ArmShutter Called");
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);

}

int disarmShutter( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(0x12) };
	//SM_LOG( "DisarmShutter Called");
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int softwareTrigger( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciSoftwareTrigger) };
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int closeShutter( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciCloseShutter) };
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int openShutterNoInterlocks( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciOpenShutterNoInterlocks) };
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

int enableInterlockOverride( )
{
	uint8_t cmd[1] = { static_cast<uint8_t>(ciEnableInterlockOverride) };
	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
}

bool setGrnLed( bool on )
{
//	gpio_set_value( RED_LED, !on );
	return (bool) gpio_set_value(GREEN_LED, (unsigned int) on);
}

bool setRedLed( bool on )
{
//	gpio_set_value( GREEN_LED, !on);
	return (bool) gpio_set_value(RED_LED, (unsigned int) on);
}

bool setYellowLed( bool on )
{
	bool sts = 0;

	sts = (bool) gpio_set_value(GREEN_LED, (unsigned int) on);
	sts |= gpio_set_value(RED_LED, (unsigned int) on);

	return sts;
}

bool setOffLed(  )
{
	bool sts = 0;

	sts = (bool) gpio_set_value(GREEN_LED, (unsigned int) false);
	sts |= gpio_set_value(RED_LED, (unsigned int) false);

	return sts;
}

bool getSurfaceBypass( void )
{
 	bool bypass = 0;

    uint8_t cmd[1] = { static_cast<uint8_t>(cigetSurfaceBypass) };
    uint8_t resp[1];

    uint8_t result = (uint8_t) doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(resp), resp);

    if (result == 0) {
        bypass = (bool)(resp[0] & 0x01);
    }

    return bypass;
}

bool setSurfaceBypass( bool bypass )
{
    uint8_t byteBypass = (uint8_t)bypass;

    SM_LOG("setSurfaceBypass byteBypass = %d", byteBypass );

    uint8_t cmd[] = { static_cast<uint8_t>(cisetSurfaceBypass),
					   static_cast<uint8_t>(byteBypass) };

	return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);

}


bool setMomentaryTrig( bool momentaryTrig )
{
    uint8_t echo = 0;
    uint8_t byteMomentaryTrig = (uint8_t)momentaryTrig;

    SM_LOG("setMomentaryTrig  = %d", momentaryTrig );

    uint8_t cmd[] = { static_cast<uint8_t>(cisetMomentaryTrig)
                      ,static_cast<uint8_t>(byteMomentaryTrig) };
    //SM_LOG("Send SM CMD %d %d ", cmd[0], cmd[1] );
    char retVal = doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);
    if( retVal != 0)
        SM_LOG("Failed to set momentary trig! %x", retVal);
    //uint8_t cfg = getConfigParms();
    //SM_LOG("Recvd SM %x", echo);
    return retVal;

}

bool setTimeout (long triggerTimeout )
{
    SM_LOG("Trigger timeout value = %d", (int)triggerTimeout );
    triggerTimeOut = triggerTimeout; // save the state to be restored after reset

    uint8_t cmd[] = { static_cast<uint8_t>(cisetSafetyTimeout),
                      static_cast<uint8_t>(( triggerTimeout >> 24) & 0xff),
                      static_cast<uint8_t>((triggerTimeout >> 16) & 0xff),
                      static_cast<uint8_t>((triggerTimeout >> 8) & 0xff),
                      static_cast<uint8_t>(triggerTimeout & 0xff),
    };

    //SM_LOG("Send SM CMD %d %d %d %d %d", cmd[0], cmd[1], cmd[2], cmd[3], cmd[4]);
    return doSpiTransaction(fd_, sizeof(cmd), cmd, 0, NULL);

}

unsigned short getFirmwareVer(){

    uint8_t version[] = {0,0,0};

    uint8_t cmd[]= {static_cast<uint8_t>(ciGetVersion)};

    doSpiTransaction(fd_, sizeof(cmd), cmd, sizeof(version), version);
    short ver = version[1] | ( version[0] << 8);
    SM_LOG("SM: Get Version %x", ver );
    return (unsigned short) ver;
}


//-------------------------------------------------------------------------------------------------
//
//  JNI Wrapper CALLS Need to add a line for each Java APP that is calling these common functions
//
//-------------------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_InitializeSMD(JNIEnv *env, jobject obj)
{
	return InitializeSMD();
}

JNIEXPORT jint JNICALL Java_com_heuresistech_pb200i_TestActivity_InitializeSMD(JNIEnv *env, jobject obj)
{
    return InitializeSMD();
}
//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_closeSMD(JNIEnv *env, jobject obj)
{
	return closeSMD();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_getSystemStatus(JNIEnv *env, jobject obj)
{
	return getSystemStatus();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_getFaultStatus(JNIEnv *env, jobject obj)
{
	return getFaultStatus();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setShutterVarHoldTime(JNIEnv *env, jobject obj, uint16_t vhtime)
{
	return setShutterVarHoldTime( vhtime );
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_armShutter(JNIEnv *env, jobject obj)
{
	return armShutter();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_disarmShutter(JNIEnv *env, jobject obj)
{
	return disarmShutter();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_openShutterNoInterlocks(JNIEnv *env, jobject obj)
{
	return openShutterNoInterlocks();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_enableInterlockOverride(JNIEnv *env, jobject obj)
{
	return enableInterlockOverride();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jint JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_closeShutter(JNIEnv *env, jobject obj)
{
	return closeShutter();
}

//----------------------------------------------------------------------------------------

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setGrnLed( JNIEnv *env, jobject obj, bool on )
{
	return (jboolean) setGrnLed(on );
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setRedLed( JNIEnv *env, jobject obj, bool on )
{
	return (jboolean) setRedLed(on );
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setYellowLed( JNIEnv *env, jobject obj, bool on )
{
	return (jboolean) setYellowLed(on );
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setOffLed( JNIEnv *env, jobject obj )
{
	return (jboolean) setOffLed(  );
}


//////////   Get/Set for surface bypass /////////////////////////


JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setSurfaceBypass( JNIEnv *env, jobject obj, bool bypass )
{
    SM_LOG("BYpass value = %d", (int)bypass );
    byPass = bypass;
    return (jboolean) setSurfaceBypass(bypass );
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_commsReset( JNIEnv *env, jobject obj )
{
    volatile int i = 0;

    SM_LOG("Comms Reset " );
    gpio_set_dir(SM_RESET, (unsigned int) true); // make suer pin is an output
    gpio_set_value(SM_RESET, (unsigned int) false); // Put SM in reset;
    //for ( i=0; i < 5000000; i++ );  //spin for a brief time.
    usleep(500000);
    gpio_set_value(SM_RESET, (unsigned int) true); // Take SM out of reset
    usleep(100000);

    setMomentaryTrig( momentaryTrigger );
    setSurfaceBypass( byPass );

//    setTrigTimeout( triggerTimeOut );
//    setSurfaceTimeout( surfaceTimeOut );
    return (jboolean) true;
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setMomentaryTrig( JNIEnv *env, jobject obj, bool momentaryTrig )
{
    //SM_LOG("Momentary Trigger value = %d", momentaryTrig );
    momentaryTrigger = momentaryTrig;  // save the state to be restored after reset
    return (jboolean) setMomentaryTrig(momentaryTrig );
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setTimeout( JNIEnv *env, jobject obj, long timeout )
{
    return (jboolean) setTimeout(timeout );
}


// NO LONGER USED - Old apk code will still try and call it so leave it here
JNIEXPORT jboolean JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_setSurfaceTimeout( JNIEnv *env, jobject obj, long surfaceTimeout )
{

    // the SM runs a 2ms loop so the ms timeout is divided by 2 to get the right answer.
    return (jboolean) true;
}

JNIEXPORT jshort JNICALL Java_com_heuresistech_smicro_SafetyMicroIsotope_getFirmwareVer( JNIEnv *env, jobject obj, long surfaceTimeout )
{
    unsigned short firmVer = getFirmwareVer(  );
    return (short)firmVer;
}


} //extern "C"


