LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Smd-jni

LOCAL_C_INCLUDES := \
	$(TOP_PATH)/utils
	
LOCAL_SRC_FILES := \
	Smd.cpp  


LOCAL_CPPFLAGS := -std=c++11 -Ofast -mfpu=vfpv3-fp16 \
            -mfloat-abi=softfp -DSM_DEBUG -g

#LOCAL_CPPFLAGS := -g -std=c++11 -DSM_DEBUG -O2	-DSM_DEBUG -g -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9

LOCAL_SHARED_LIBRARIES := utils-jni 

LOCAL_LDLIBS := -llog 

include $(BUILD_SHARED_LIBRARY)
