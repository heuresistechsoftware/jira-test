#include <string.h>
#include <jni.h>

extern "C" {

	JNIEXPORT jstring JNICALL
	Java_com_heuresistech_radsafety_MainActivity_stringFromJNI
	(JNIEnv *env, jobject obj)
	{
		return env->NewStringUTF ( "Hello from C++ over JNI");
	}

	JNIEXPORT jint JNICALL
	Java_com_heuresistech_radsafety_MainActivity_intFromJNI
	(JNIEnv *env, jobject obj)
	{
		//return (*env)->NewStringUTF (env, 123);
		return 56954;
	}

	JNIEXPORT jintArray JNICALL
	Java_com_heuresistech_radsafety_MainActivity_intArrayFromJNI
	(JNIEnv *env, jobject obj, int size)
	{
	 jintArray result;
	 result = env->NewIntArray(size);
	 if (result == NULL) {
	     return NULL; /* out of memory error thrown */
	 }

	 int i;

	 // fill a temp structure to use to populate the java int array
	 jint fill[256];
	 for (i = 0; i < size; i++) {
	     fill[i] = i+1; // put whatever logic you want to populate the values here.
	 }

	 // move from the temp structure to the java structure
	 env->SetIntArrayRegion(result, 0, size, fill);
	 return result;

	}

}
