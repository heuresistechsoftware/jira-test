#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <cstdint>

#include "DppSpectrum.hpp"
#include "PeakFitPrototypes.hpp"
#include "Element.hpp"


// define spectrum objects
class Spectrum {
public:

	int  DataSmoothSize;          // Kernal size for smoothing input data (+/- DataSmoothSize) [2]
	int  PCalibElementID ;        // Primary Element to calibrate on (for gain determination) [W]
	float DetResLoError;          // Lower error level for measured detector resolution (percent @100keV)
	float DetResHiError;              // Upper error level for measured detector resolution (percent @100keV)
	float MaxGainDrift;           // max percentage gain drift from factory default setting for locating calibration peaks [3.0]
	float GainDriftNull;          // max percentage gain drift from factory default setting for gain from ecal to be used
	float DefaultEnergyRes;       // Default percent energy resolution at 100keV (0.0123)
	int   NumFitIterFac;
	bool performEnergyCal;        // Does an energyCalibration if true. On for Pb200i off for HBI

	char name[50];
    int nIncrement;
    int numBins;
    int totCnts;
    int fastCorrCnts;
    int slowCnts;
    int subType;                // substrate type
    int tempType;                // background template type with best match
	float tempNormFac;          // normalization factor for scaling background template
	float tempFitAdjust;        // adjustment factor for scaling background template (mostly comes from errors in src assay strength)
    float tempGain;             // gain used to fit background template
	float rtgFarFac;			// RTA factor for calculating surface concentration
	float rtgLoRate;			// max count rate in Ag ROI for RTA NOT to be present
	float rtgHiRate;			// min count rate in Ag ROI for RTA to be present
	bool rtgSurfMode;			// turns surface concentration mode on or off
	float maxCntRateSF;			// max count rate for styrofoam
	float crudeScaleFac;		// max crude scale factor for template fitting
    int serialNumber;           // serial number of instrument producing spectrum
    int calibSamp;              // for calibration mode, this stores the calibration sample number
    int calibSubType;           // for calibration mode, this stores the calibration sample substrate type
    int fitMode;                // 1: fit only templates with same substrate as calib sample 2: fit all background templates
    int TECTemp;                // detector temperature
    int boardTemp;              // temperature of DPP
	char srcType[25];           // source type (e.g. Co57 or Cd109)
    char srcAssayDate[25];      // assay date for instrument source
    int srcAssayDay;
    int srcAssayMonth;
    int srcAssayYear;
    float srcAssayMCi;          // source assay strength
    float srcMCi;               // current source strength
	float srcHalfLife;			// system source half life (days)
    float srcAgeDays;           // current source age since assay (days)
    float elapsedDays;          // elapsed days since calibration
    float tempChisq;            // chisq of template match
    float ecalChisq;            // chisq for energy cal peak fitting
    float ecalFWHM;             // FWHM from energy cal peak fitting
    char ename[50];                // element of interest
    float liveTime;                // total live acquisition time for spectrum
    float accTime;                // total acquisition (accumulation) time for spectrum
    float nomSecs;                // nominal acquisition time (in seconds with nominal source strength)
    float conc;                    // concentration of element of interest (e.g. lead)
    float l_offset;
    float l_gain;
    float k_offset;
    float k_gain;
    float defKGain;
    float defKOffset;

	float defPbSNR;             // default SNR for lead detection
	float minPbSNR;             // this is the minimum SNR as set by GUI slider (0.5 - 2.0 times the default value)
	int PbLevel;                // detection level (0 if SNR less than minPbSNR, 1 if less than defPbSNR, 2 otherwise)


	bool ecalStatus;                        // status of ecal (bad ecal is false, good ecal is true)
    float kLineCnts[NumElements];
    float kBgndCnts[NumElements];
    int bgndPeakLoBin;
    int bgndPeakHiBin;

    int prevTotCnts;                        // total counts before last change to accumulated spectrum
    int prevSlowCnts;                       // total slow counts before last change to accumulated spectrum
    int prevFastCorrCnts;                   // total corrected fast counts before last change to accumulated spectrum
    float prevAccTime;                      // total accumulation time before last change to accumulated spectrum

    int prevTempType;            // template type for previous spectrum
    int prevSpecTime;            // global time previous spectrum was acquired

    int compCnts1;                // counts between SoilCompKeVLo1 and SoilCompKeVHi1
    int compCnts2;                // counts between SoilCompKeVLo2 and SoilCompKeVHi2

    float calibFitPars[MaxCalibPeaks][MaxFitParams];
    float bgndPeakCentBin;                    // peak centroid (bins)
    float bgndPeakFWHMBin;                    // peak FWHM (bins)
    float bgndPeakCentKeV;                    // peak centroid (keV)
    float bgndPeakFWHMKeV;                    // peak FWHM (keV)
    float bgndPeakSepKeV;                // separation of Compton peaks in spectrum and best matched template
    float bgndOffset;
    float bgndFitCnts;                        // fitted background under analysis peaks
    float bgndRawCnts;                        // raw background under analysis peaks
    float bgndPeakRawCnts;                    // raw counts under background peak (per second)
    float bgnd[MaxNumBins];                    // fitted bgnd under fitted curve in each bin
    bool includeBgnd[MaxNumBins];            // flag that determines whether bin should be used to fit the background
    const uint32_t *counts;                    // total raw counts in each bin
	uint32_t prevCounts[MaxNumBins];            // previous raw counts before last incremental acquisition
    uint32_t cumCounts[MaxNumBins];            // cumulative counts over previous NumCumSpec spectra

    int normCounts[OutSpecNumBins];         // counts normalized for output
    int normCountsBSub[OutSpecNumBins];     // background subtracted counts normalized for output
    float normEnergy[OutSpecNumBins];       // energy of each bin for normalized output

	float soilCalCoeff1;
	float soilCalCoeff2;
	float soilCalOffset;
	float soilInsitCoeff1;
	float soilInsitCoeff2;
	float soilInsitOffset;
	float dustCalCoeff1;
	float dustCalCoeff2;
	float dustCalOffset;

	// obsolete (for backward compatibility)
	float dWipeZeroAdjust;
	float coefficient2;  // Formally soilCalOffset
	float coefficient1;  // Formally soilCalSlope
    float dustPullUpFac;

	int maxAirCntRate;                    // max air cnt rate (no sample)
	int maxAirCntRateRTG;                 // max air cnt rate with RTG (no sample)


	// instantiate elements within spectrum
	Element element[NumElements];

public:
	Spectrum (){
		int n;
		for (n = 0; n < MaxNumBins; n++) {
			bgnd[n] = 0;

		}
		setSystemTypeConsts();
	}

    bool reset() { return false; }
    void initializeElements();
	int  findComptonPeak();
	void setBinStatus();
	bool checkForStyroFm();
	void initSpectrum( const DppSpectrum &dppSpec );
	void writeSpectrum( float gain, float offset);
	int  writeLogFile( int ID, int elemID);
	float fitPeaksBgnd(int elementID1, int line_ID1, int elementID2, int line_ID2, int mode);
	float fitPeaksBgnd(int elementID1, int line_ID1, int elementID2, int line_ID2);
    float fitTwinPeaks(int elementID, int line_ID);
    void reCalibSpec(float gain);
    bool energyCalSpec(int nElem);
	void printFits();
    void plotFits();
	int  createMessage(int messCode, int mode, float val1, float val2, float val3, float val4);

	float nomSeconds(float seconds);
	float realSeconds(float nomSecs);
	int  sourceCorrectMS(int nomMilliSecs);
	int  sourceCorrectSeconds(int realMilliseconds);
	int  remainingTime( float minNomSecs);
	void writeSpectrumNorm(float gain, float offset);
	int  writeConfigFile( int calibMode);
	int  fitBackground(int elemID, int lineID);
	float calcSoilPPM(int elemID, int lineID);

	virtual void setSystemTypeConsts();
};

#endif // SPECTRUM_H
