/** \file PeakFitResults.hpp
 *  \brief Peak Fit Results Format
 */
#ifndef PEAKFITRESULTS_HPP
#define PEAKFITRESULTS_HPP

#include <ctime>

const uint32_t CResultsMagic = 0x15171ead;  // Is it lead?
const uint32_t CResultsVersion = 1;

enum Determination {
    dInProcess,
    dNegative,
    dPositive,
    dInconclusive,
    dComplete,
    dDLTimedOut,
    dAirScan,
    dNull,
    dError
};

// assign labels to measurement modes
// Action level mode; DL mode with preset detection limit; DL mode with preset time; DL mode with trigger operation up to max time
enum meas_mode {
    AL, DLStopAtSetLevel, DLFixedTime, DLUnlimitedTime, DLStopAtStatistics
};

/** Peak-fit algorithm determination.  */
struct PeakFitResults {
    // Header
    uint32_t magic;        // Must equal CResultsMagic
    uint32_t version;      // Must equal CResultsVersion

    meas_mode measMode;    // Measurement mode used to obtain result
    float negConfLev;      // Confidence level for negative result
    float posConfLev;      // Confidence level for positive result

    // Results
    Determination result;  // Current best-guess
    float SNR;              // SNR for lead detection
    int tempType;          // Background template with best match
    float mgCm2;           // Measured lead level (mg/cm2) from full template fit
    float mgCm2_quick;     // Measured lead level (mg/cm2) from quick template fit
    float confLev;         // Confidence level in determination
    float stdDev;          // Standard deviation of measured lead level
    float alphaBetaRatio;  // Alpha-beta ratio

    float tempChisq;
    float tempNormFactor;

    float mgCm2_NoRTG;     // Last measured lead level without the RTG present
    float mgCm2_Near;      // Calculated near side concentration if read through is present
    bool readThrough;      // Read Through flag
    bool RTGPresent;       // Read Through Gauge detected
    int nullCode;          // indicates why null code was returned.

    // results for AL mode when operating in extended mode
    Determination ALResult;
    float ALmgCm2;
    float ALerror;
    bool ALDone;

    bool reset() { return false; }
};

#endif // PEAKFITRESULTS_HPP
