//
// Created by AndyHanks on 2/1/2019.
//

#include "SpectrumHBI.hpp"

SpectrumHBI::SpectrumHBI(){
    setSystemTypeConsts();
}

void SpectrumHBI::setSystemTypeConsts(){

    ALOG("HBI- Consts");
    DataSmoothSize = 2;             // Kernal size for smoothing input data (+/- DataSmoothSize) [2]
    PCalibElementID = Ba;           // Primary Element to calibrate on (for gain determination) [W]
    DetResLoError = 2.0f;           // Lower error level for measured detector resolution (percent @100keV)
    DetResHiError = 10.0f;          // Upper error level for measured detector resolution (percent @100keV)
    MaxGainDrift =  5.0f;           // max percentage gain drift from factory default setting for locating calibration peaks [3.0]
    GainDriftNull = 5.0f;           // max percentage gain drift from factory default setting for gain from ecal to be used
    DefaultEnergyRes = 3.5f;        // Default percent energy resolution at 100keV (0.0123)
    NumFitIterFac = 5000;           // Number of fit iterations per parameter [2500]
    performEnergyCal = 0;           // Should be off for HBI

}