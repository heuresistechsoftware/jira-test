#ifndef LINE_H
#define LINE_H

#include "PeakFitPrototypes.hpp"
//#include "PeakFit.hpp"

// define data types for fluorescence lines
enum shell_id {
    l, k
};
enum line_id {
    la1, lb1, ka2, ka1, kb3, kb1, kb2, na
};  // lines are loaded in this order

// define lines to calibrate energy on ("calibration lines")
// (lines actually used set by NumCalibPeaks)
static line_id calibPeakID[MaxCalibPeaks] = {ka1, kb1};

// assign a shell to each fluorescence line
static shell_id lineShell[MaxLinesPerElement] = {l, l, k, k, k, k, k};

// assign a name to each fluorescence line
static char lineName[NumLinesPerElement][5] = {"la1", "lb1", "ka2", "ka1", "kb3", "kb1", "kb2"};


// define subset of peaks to analyze for each analysis element for Co57 source
static line_id analyzePeakCo57[MaxNumAnalyzeElements][MaxNumAnalyzePeaks] = {
        ka1, kb1,  na,  na,   // Pb
        ka2,  na,  na,  na,   // Cd
        kb3,  na,  na,  na,   // Au
        ka2, ka1, kb3,  kb1,  // W
        ka2, ka1, kb3,  kb1   // Ba
};

// define subset of peaks to analyze for each analysis element for Cd109 source
static line_id analyzePeakCd109[MaxNumAnalyzeElements][MaxNumAnalyzePeaks] = {
        kb1,  na,  na,  na,   // Pb
        ka2,  na,  na,  na,   // Cd
        kb3,  na,  na,  na,   // Au
        ka2, ka1, kb3,  kb1,  // W
        ka2, ka1, kb3,  kb1   // Ba
};

// number of peaks to exclude from background template fitting for each analysis element
// For soils use this option (tungsten peaks EXCLUDED in template fitting)
//static int numExcludePeaks[MaxNumAnalyzeElements] = {4, 0, 0, 4, 0};

// For lead paint and dust use this option (tungsten peaks INCLUDED in template fitting (DEFAULT))
static int numExcludePeaks[MaxNumAnalyzeElements] = {4, 0, 0, 0, 0};

// define peaks of analysis element to exclude from background template fitting
static line_id excludePeakID[MaxNumAnalyzeElements][MaxNumAnalyzePeaks] = {
        ka1, ka2, kb1, kb2,   // Pb
        ka1, ka2, kb1, kb2,   // Cd
        ka1, ka2, kb1, kb2,   // Au
        ka1, ka2, kb1, kb2,   // W
        ka1, ka2, kb1, kb2    // Ba
};

// energies in keV of fluorescence lines for each element
static double lineKeV[NumElements][NumLinesPerElement] = {
        10.549, 12.611,72.804, 74.969, 84.450, 84.936, 87.367, // Pb
        3.133,  3.316, 22.985, 23.173, 26.061, 26.095, 26.653, // Cd
        9.711, 11.439, 66.990, 68.804, 77.580, 77.985, 80.182, // Au
        8.396,  9.670, 57.982, 59.318, 66.951, 67.244, 69.100, //  W
        4.470,  4.827, 31.817, 32.194, 36.303, 36.378, 37.270  // Ba
};

// relative intensities of fluorescence lines for each element
static float lineInt[NumElements][NumLinesPerElement] = {
        1.0, 1.0, 0.65, 1.0, 0.1, 0.2, 0.050,
        1.0, 1.0, 0.52, 1.0, 0.1, 0.2, 0.050,
        1.0, 1.0, 0.58, 1.0, 0.1, 0.2, 0.050,
        1.0, 1.0, 0.58, 1.0, 0.1, 0.2, 0.055,
        1.0, 1.0, 0.58, 1.0, 0.1, 0.2, 0.050
};

// lower limit of ROI used by analyzeCounts() for each line
static double lineKeVLo[NumElements][NumLinesPerElement] = {
        10.549, 12.611, 71.0  , 71.0  , 83.0  , 83.0,   83.0  , // Pb
        3.133,  3.316, 22.985, 23.173, 26.061, 26.095, 26.653, // Cd
        9.711, 11.439, 66.990, 68.804, 77.580, 77.985, 80.182, // Au
        8.396,  9.670, 57.982, 59.318, 66.951, 67.244, 69.100, //  W
        4.470,  4.827, 29.317, 29.694, 33.803, 33.878, 34.770  // Ba
};

// upper limit of ROI used by analyzeCounts() for each line
static double lineKeVHi[NumElements][NumLinesPerElement] = {
        10.549, 12.611, 76.0  , 76.0  , 85.8,   85.8,   85.8  , // Pb
        3.133,  3.316, 22.985, 23.173, 26.061, 26.095, 26.653, // Cd
        9.711, 11.439, 66.990, 68.804, 77.580, 77.985, 80.182, // Au
        8.396,  9.670, 57.982, 59.318, 66.951, 67.244, 69.100,  //  W
        4.470,  4.827, 34.317, 34.694, 38.803, 38.878, 39.770  // Ba
};

static int numLines = NumLinesPerElement;
static int numLLines = NumLLinesPerElement;
static int numKLines = NumKLinesPerElement;
static int maxLines = MaxLinesPerElement;

// define fluorescence line objects
class Line {
public:

    shell_id shell;                // K or L Shell
    line_id id;                // peak ID (e.g. k-Alpha-1 etc)
    bool fitStatus;                // set to true when the peak has been fit
    int bgndMode;                // indicates which background mode was used during peak fitting
    int centroidCnts;            // counts in centroid bin
    float centroidBin;            // centroid bin expected from calibration
    float centroidBgnd;            // background counts under centroid
    int lo_bin;                // lower bin limit for curve fit
    int hi_bin;                // upper bin limit for curve fit
    float lo_lim;                // lower bin limit for integrating counts
    float hi_lim;                // upper bin limit for integrating counts
    float keV;                // energy in keV
    float intensity;            // relative intensity compared with K-Alpha 1 peak
    float chisq;                // chisq of fit
    float FWHM_bins;            // fitted FWHM (bins)
    float FWHM_keV;                // fitted FWHM (keV)
    float rawCnts;                // raw integrated counts in peak
    float fitCnts;                // integrated counts in fitted peak
    float bgndCnts;                // background counts under fitted peak
    float lineCnts;                // counts in line (fitCnts - bgndCnts)
    float fitPars[MaxFitParams];            // peak fit parameters

    void calibrate(float, float);

    void calcFWHM(float, float);

    void approxFWHM(float, float);

    Line(void) {
        int n;
        fitStatus = false;
        for (n = 0; n < MaxFitParams; n++) fitPars[n] = 0;
    }

};

#endif // LINE_H
