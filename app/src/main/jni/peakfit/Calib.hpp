#ifndef CALIB_HPP
#define CALIB_HPP

#include "time.h"

#define CONFIG_INCLUDE_CAL

// calibration mode
#define SetCalibMode 2                                  // 1: Service calibration mode  2: Factory calibration mode

// source parameters
#define Co57NomSrcMCi 5.0f                               // nominal source strength (used to calculate nominal source strength) [mCi]
#define Cd109NomSrcMCi 20.0f                               // nominal source strength (used to calculate nominal source strength) [mCi]
#define NomLiveTime 0.6f                                // average live time for all substrates with fresh source of strength NomSourceMCi

// SD parameters
#define SDCorrFactor 1.5f                               // SD determined by calibration is multiplied by this factor before being written to sysCalib file
#define DefaultMgcm2SD 0.15f                            // if SD is zero this value will be used

// calibration parameters
#define DoOutlierCorr true                              // replace outlier calibration factors with the average
#define NumElements 5                                   // Total number of elements to create arrays for (includes analysis and calibration elements)
#define NumSubstrates 200                               // Number of substrates used in calibration samples
#define MaxNumMeasure  100                              // Max number of measurements per calibration sample

// Soils Change value was 6 for PbPaint, 19 for soils
#define MaxConcLevels 25                                 // Number of element levels used in calibration samples

#define MinCalibConc 0.1                                // Min concentration of calibration samples for them to be included in calculating average calibration factors
#define MaxCalibConc 4.0                                // Max concentration of calibration samples for them to be included in calculating average calibration factors
//#define SourceHalfLife 270.9f                           // Half-life of source (days)
#define Co57HalfLife 270.9f                             // Half-life of Co57 source (days)
#define Cd109HalfLife 462.6f                            // Half-life of Cd109 source (days)
#define MaxCalibSamples 500                             // Max number of input calibration samples
#define MaxSampPerSubstrate 25                          // Max number of samples per element per substrate
#define NumAltNames 5                                   // Number of alternative names for each substrate

// soil calibration parameters
#define SoilCalOffset -0.1125f
#define SoilCalSlope 0.1852f
#define SoilCompKeVLo1  91.0f
#define SoilCompKeVHi1 104.0f
#define SoilCompKeVLo2 104.0f
#define SoilCompKeVHi2 130.0f

#define DustCalCoeff1 28.023
#define DustCalCoeff2 -307.23
#define DustCalOffset 0.0336
#define DWipeZeroAdjust 0.0                             // Zero the scale for dust wipes. (ug/1000)
#define DWipePullUpFac 2.5f

// quick energy calibration parameters
#define ECalCalibSample "3-6-Styrofm-A_001.csv"         // Calibration spectrum used to calculate initial gain and offset
#define ECalSearchRange 10.0f                           // search range used to look for peaks around expected centroid energy [keV]

// calibration check parameters
#define QuickConcCalcMode 2                             // Mode=1: Use raw counts scaled by liveTime and source strength Mode=2: Use counts/bgnd ratio (no scaling req.)

// calibration logging parameters
#define MinCalibRawFac  500                             // minimum calibration factor for generating entry in log file indicating a problem
#define MaxCalibRawFac 1200                             // maximum calibration factor for generating entry in log file indicating a problem


// autodetection of calibration samples
#define AutoDetectSample false                          // determines whether calibration sample is automatically identified
#define AutoReportSample 1                              // number of measurements taken before reporting detected sample ID

// background template parameters
#define FormattedTemplates true                         // select formatted or binary templates [true]
#define FitAllBgndTemp false                            // determines whether only blank background templates are fitted [false]
#define TempFitCheck true                               // checks for template flipping (requires two consecutive incremental spectra with same template match for result in AL mode)
#define BlkAirSubType 161                               // substrate type for performing two-element energy cal (to determine offset)
#define MaxBlankConc 0.025f                             // max concentration of blank calibration samples
#define MaxNumTemp 500
#define MaxTempBins 512
#define CrudeScaleIter 50                               // number of iterations for crude intensity scaling for template matching [50]
#define MaxCrudeScaleFac 0.20f                          // crude intensity scaling range (0.15 = +/- 15%) for template matching [0.20]
#define FineScaleIter 200                               // number of iterations for fine intensity scaling for template matching [200]
#define MaxFineScaleFac 0.01f                           // fine intensity scaling range (0.01 = +/- 1%) for template matching [0.01]

#define TempEcalIter 1                                  // number of iterations for energy calibration scaling for template matching [1]
#define MaxTempEcalFac 0.0f                             // energy calibration scaling range (0.01 = +/- 1%) for template matching [0.0]
#define TempEcalIterLP 1                               // number of iterations for energy calibration scaling for template matching [1]
#define MaxTempEcalFacLP 0.0f                          // energy calibration scaling range (0.01 = +/- 1%) for template matching [0.0]
#define TempEcalIterSL 1                                // number of iterations for energy calibration scaling for template matching [1]
#define MaxTempEcalFacSL 0.0f                           // energy calibration scaling range (0.01 = +/- 1%) for template matching [0.0]
#define TempEcalIterDW 150                              // number of iterations for energy calibration scaling for template matching [150]
#define MaxTempEcalFacDW 0.02f                          // energy calibration scaling range (0.01 = +/- 1%) for template matching [0.02]

#define TempEnergyWeightOn true                         // turn on perform energy weighting around peaks of interest when fitting templates [true]
#define TempEnergyWeightOff false                       // turn off perform energy weighting around peaks of interest when fitting templates [false]
#define MinEnergyWeightLo 0.13f                         // minimum weighting for template bins furthest below peak of interest (peak bins have weight=1.0) [0.13]
#define MinEnergyWeightLoNonLp 0.0f                     // minimum weighting for template bins furthest below peak of interest (peak bins have weight=1.0) [0.0]

#define MinEnergyWeightHi 0.0f                         // minimum weighting for template bins furthest above peak of interest (peak bins have weight=1.0) [0.0]
#define EnergyWeightPowerLo 1.0f                        // weight factor below peak will be raised to this power (2 means weight factor will be squared) [1.0]
#define EnergyWeightPowerHi 2.0f                        // weight factor above peak will be raised to this power (2 means weight factor will be squared) [2.0]
#define EnergyWeightPowerHiNonLp 1.0f                   // weight factor above peak will be raised to this power (2 means weight factor will be squared) [1.0]
#define EnergyWeightPowerHiRTG 1.0f                     // weight factor above peak will be raised to this power (2 means weight factor will be squared) [1.0]

#define CalibFacDiffWarn 10.0
#define TempFitPeakCheck false                           // check counts match in Compton peaks in spectrum and template before attempting to fit [false]
#define MaxBgndPeakCntDiff 0.25f                        // maximum fractional difference in integrated counts in bgnd peak to try matching [0.25]
#define BgndTempLoKeV 27.0f                             //  [27.0]
#define BgndTempLoKeVNonLp 50.0f                        //  [50.0]

#define BgndTempHiKeV 115.0f                           //  [115.0] (92.0 may be good to avoid paint overlay effects)

#define TempSmoothSize 2                                // Not used [2]
#define TempChisqWarn  50.0f                             // Max chisquare value for good template match (otherwise a warning is generated)

// file names
#define DefaultSDFile "SDValues.txt"                    // file that contains default SD values for each substrate type

// calibration cross section parameters
#define NumCSBins 150
#define WFluxNormFac 1000                               // Divide the weighted flux by this factor
#define CalibCSFile "CSFile.txt"

// Read through parameters
#define DoSurfaceMode false                             // turn surface concentration mode on or off
#define RTATemplateNumber 45                            // min template number (default value only) for read through adaptor [43]
//#define MinRTGCntRate 50                              // min count rate at centroid of Ag Ka1 peak when RTG is present
#define MinRTGCntRate 250                               // min count rate at centroid of W Ka1 peak when RTG is present
#define RTGLoRate 3.5f                                  //
#define RTGHiRate 3.5f                                  //
#define RTGFarFac 0.33f                                 // for latest internal RTA
// styrofoam detection
#define MaxCntRateStyroFm 800.0f

// global variables
extern float avMgcm2SD[NumElements][MaxNumTemp][MaxConcLevels];
extern float calibRatio[NumElements][MaxNumTemp][MaxConcLevels];
extern float avCalibRaw[NumElements][MaxNumTemp];
extern float kGainCum[MaxCalibSamples];
extern float kOffsetCum[MaxCalibSamples];

// calibration substrate names
extern char subName[NumSubstrates][NumAltNames][25];

class Calib {
public:
    int deviceType;
    int calibMode;
    time_t calibTime;              // Global time in seconds when calibration was performed
    int nSamples;                  // total number of samples
    int nElements;                 // number of sample elements
    int nSubstrates;               // number of sample substrates
    int nTemplates;                // number of background templates created from blank calibration samples
    int nNonRTA;                   // total number of non-RTA templates
    int nInsitu;                   // total number of insitu soil templates
    int nSTemplates;               // number of sample templates created from all calibration samples
    int nConcLevels;               // number of concentration levels in all samples
    int totMeasure;                // total number of measurements over all calibration samples
    int srcAssayDay;               // day of source assay
    int srcAssayMonth;             // month of source assay
    int srcAssayYear;              // year of source assay
    int specDay;                   // day of spectra acquisition
    int specMonth;                 // month of spectra acquisition
    int specYear;                  // year of spectra acquisition
    float livetime;                // live time/sample for spectrum acquisition
    float gain;                    // gain determined from energy calibration of  calibration samples
    float offset;                  // offset determined from energy calibration of  calibration samples
    float srcAssayMCi;             // source strength from original assay (mCi)
    float srcMCi;                  // calibration source strength (mCi) at time of calibration
    float srcAgeDays;              // age of source since assay date (days)
    char srcType[25];              // source type (e.g. Co57 or Cd109)
    char specDate[25];             // calibration spectra acquisition date
    char srcAssayDate[25];         // source assay date

    char name[MaxCalibSamples][50];   // sample name
    int nMeasure[MaxCalibSamples];    // total number of measurements for each sample
    int nGood[MaxCalibSamples];       // number of good measurements for each sample
    int elemID[MaxCalibSamples];
    int subType[MaxCalibSamples];
    int tempType[MaxCalibSamples];
    int concID[MaxCalibSamples];      // this is a list of the concentration ID for each sample (concID=0 for 0.0mg; 1 for 0.3mg, etc)
    float concLevels[MaxConcLevels];  // this is a list of the concentrations found in all the samples
    float conc[MaxCalibSamples];      // this records the concentration found in each sample
    float ecalFWHM[MaxCalibSamples];

    bool reset();        // method to clear all values to zero.
};

class SampTemplate {
public:
    int numTemplates;                        // total number of templates read in
    int nBins[MaxNumTemp];                   // number of bins in template file
    int subType[MaxNumTemp];
    int buildTime[MaxNumTemp];
    int tLoBin[MaxNumTemp];                  // low bin in original spectrum (or spectra) template was built from
    int totCnts[MaxNumTemp];                 // total raw counts in un-normalized template
    int rCounts[MaxNumTemp][MaxTempBins];    // counts in each bin as stored in template file
    int bgndPeakRawCnts[MaxNumTemp];         // raw counts in Compton peak per liveTime second
    float calibFac[MaxNumTemp];
    float gain[MaxNumTemp];
    float offset[MaxNumTemp];
    float lo_keV[MaxNumTemp];
    float hi_keV[MaxNumTemp];
    float conc[MaxNumTemp];
    float liveTime[MaxNumTemp];
    float nomSecs[MaxNumTemp];               // nominal source seconds corresponding to live-time
    float bgndPeakCentKeV[MaxNumTemp];       // energy of Compton peak centroid
    float normFac[MaxNumTemp];
    float tCounts[MaxNumTemp][MaxTempBins];  // counts in each bin as stored in template file,
    // but normalized for live-time and source decay to match spectrum

    int sNumBins[MaxNumTemp];                // number of bins in re-binned spectrum (used to match current spectrum)
    int sLoBin[MaxNumTemp];                  // low bin of template corresponding to current spectrum
    float sCounts[MaxNumTemp][MaxTempBins];  // counts re-binned to match spectrum (req. due to possible gain differences)

    bool reset() { return false; }        // method to clear all values to zero.
};

class CrossSect {
public:
    int nElements;
    int nBins[NumElements];
    float edgeEnergy[NumElements];
    float dEnergy[NumElements];
    float loEnergy[NumElements];
    float hiEnergy[NumElements];
    //float energy[NumElements][NumCSBins];
    float cs[NumElements][NumCSBins];

};

int readCalibParams(Calib &calib, int mode, int analyticMode);

void readSysCalibEnc(Calib &calib, int analyticMode);

void readSysCalibCorrectionFactors(Calib &calib, int analyticMode);


#endif // CALIB_HPP
