LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Peakfit-jni

LOCAL_C_INCLUDES := \
	$(TOP_PATH)/Dppd \
	$(TOP_PATH)/utils \
	$(TOP_PATH)/include 
	
LOCAL_SRC_FILES := \
	Calib.cpp \
	Element.cpp  \
	hac.cpp  \
	hcc.cpp  \
	Line.cpp  \
	PeakFit.cpp \
	Spectrum.cpp \
	SpectrumHBI.cpp

#LOCAL_CPPFLAGS := -std=c++11 -O2 -g -DPF_DEBUG -Ofast -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9
#LOCAL_CPPFLAGS := -std=c++11 -Ofast  -mfpu=vfpv3-fp16 \
#                    -mfloat-abi=softfp -g -DPF_DEBUG
LOCAL_CPPFLAGS := -std=c++11 -O2 -Ofast -mfpu=vfpv3-fp16 \
                    -mfloat-abi=softfp -g -DPF_DEBUG
LOCAL_LDLIBS := -llog 

LOCAL_SHARED_LIBRARIES := utils-jni
	
include $(BUILD_SHARED_LIBRARY)	
