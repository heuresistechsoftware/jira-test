//
// Created by AndyHanks on 1/29/2019.
//

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <random>
#include <chrono>
#include <c++/v1/random>
#include <list>
#include <c++/v1/list>

#include "Element.hpp"
#include "Spectrum.hpp"
#include "MessCode.hpp"

extern PeakFitResults results;

void Spectrum::setSystemTypeConsts(){

    ALOG("PB Consts");
    DataSmoothSize = 0;            // Kernal size for smoothing input data (+/- DataSmoothSize) [0]
    PCalibElementID = W;         // Primary Element to calibrate on (for gain determination) [W]
    DetResLoError = 0.8f;          // Lower error level for measured detector resolution (percent @100keV)
    DetResHiError = 5.0f;          // Upper error level for measured detector resolution (percent @100keV)
    MaxGainDrift = 3.0f;           // max percentage gain drift from factory default setting for locating calibration peaks [3.0]
    GainDriftNull = 2.5f;          // max percentage gain drift from factory default setting for gain from ecal to be used
    DefaultEnergyRes = 2.5f;       // Default percent energy resolution at 100keV (0.0123)
    NumFitIterFac = 5000;
    performEnergyCal = 1;           // Should be on for Pb200i
}

void Spectrum::initializeElements() {

    // initialize element objects
    int i, j, n;

    // use default energy resolution if not yet set from energy calibration
    if(ecalFWHM==0) ecalFWHM = DefaultEnergyRes;

    for (i = 0; i < NumElements; i++) {

        // set element name
        element[i].setName(i);

        // assign number of analysis peaks
        for (n = 0; n < MaxNumAnalyzeElements; n++) {

            if (analyzeElementID[n] == i) {
                element[i].numAnalyzePeaks = numAnalyzePeaks[n];
                element[i].analyzePoint = n;
                break;
            }
        }

        // load line information
        element[i].numLines = NumLinesPerElement;
        element[i].numLLines = NumLLinesPerElement;
        element[i].numKLines = NumKLinesPerElement;
        for (j = 0; j < NumLinesPerElement; j++) {

            element[i].line[j].shell = k;
            element[i].line[j].keV = (float) lineKeV[i][j];
            element[i].line[j].intensity = lineInt[i][j];
            element[i].line[j].id = (line_id) j;
            element[i].line[j].calibrate(k_offset, k_gain);
            element[i].line[j].centroidCnts = counts[int(element[i].line[j].centroidBin + 0.5)];
            element[i].line[j].approxFWHM(ecalFWHM, k_gain);
        }
    }

    // find peak of Compton background
    bgndPeakCentBin = (float) findComptonPeak();

    // initialize template fit chisquare value to zero
    tempChisq = 0.0f;

    // set background status of bins in spectrum
    setBinStatus();

}

int Spectrum::findComptonPeak() {
    // Returns centroid bin, FWHM, and offset counts for Compton background peak

    int n, m, q, loBin, hiBin, centroid, cnt, sum, sumMax = 0, halfWidth, FWHM;
    int loLim, hiLim;
    bool loFnd = false;
    float yFWHM, avVal, sigma, offset;
    float ln2 = 0.69315f;

    // calculate search window
    if (k_gain > 0) {
        loBin = int((BgndPeakFitLoKeV - k_offset) / k_gain + 0.5);
        hiBin = int((BgndPeakFitHiKeV - k_offset) / k_gain + 0.5);
        halfWidth = (NumSumBgndBins - 1) / 2;
    } else {
        loBin = 0;
        hiBin = 0;
        halfWidth = 0;
    }

    if (loBin < 0) loBin = 0;
    if (hiBin >= MaxNumBins) hiBin = MaxNumBins - 1;

    //ALOGI(" findComptonPeak: loBin=%d hiBin=%d halfWidth=%d", loBin, hiBin, halfWidth);

    centroid = 0;
    for (m = loBin + halfWidth; m <= hiBin - halfWidth; m++) {
        sum = 0;
        for (n = -halfWidth; n <= halfWidth; n++) {
            q = m + n;
            if (q < 0) q = 0;
            if (q >= MaxNumBins) q = MaxNumBins - 1;
            sum += counts[q];
            //ALOGI(" sum=%d", sum);
        }
        if (sum > sumMax) {
            sumMax = sum;
            centroid = m;
        }
    }

    yFWHM = (float) counts[centroid] / 2;
    //ALOGI(" yFWHM=%f sumMax=%d centroid=%d", yFWHM, sumMax, centroid);

    // find FWHM
    loLim = loBin;
    hiLim = hiBin;
    for (m = loBin + halfWidth; m <= hiBin - halfWidth; m++) {
        sum = 0;
        for (n = -halfWidth; n <= halfWidth; n++) {
            q = m + n;
            if (q < 0) q = 0;
            if (q >= MaxNumBins) q = MaxNumBins - 1;
            sum += counts[q];
        }
        avVal = (float) sum / NumSumBgndBins;
        //ALOGI(" avVal=%f", avVal);
        if (!loFnd && avVal > yFWHM) {
            loLim = m;
            loFnd = true;
        }
        if (loFnd && sum < yFWHM) {
            hiLim = m;
            break;
        }
    }
    FWHM = hiLim - loLim;
    sigma = FWHM / (2 * sqrt(2 * ln2));

    // integrated counts under Compton peak
    sum = 0;
    for (m = loBin; m <= hiBin; m++) {
        sum += counts[m];
    }

    bgndPeakRawCnts = (float) sum / nomSecs;

    // find flat background under peak (look at flat region between loBin and peak)
    hiBin = int(centroid - BgndOffsetSigmaFac * sigma);
    if (hiBin >= MaxNumBins) hiBin = MaxNumBins - 1;
    sum = 0;
    cnt = 0;
    for (m = loBin; m <= hiBin; m++) {
        if (includeBgnd[m]) {
            sum += counts[m];
            cnt++;
        }
    }
    if (cnt > 0) {
        offset = (float) sum / cnt;
    } else {
        offset = 0;
    }
    //ALOGI(" findComptonPeak: loBin=%d hiBin=%d, centroid=%d FWHM=%d offset=%f", loBin, hiBin, centroid, FWHM, offset);

    bgndPeakCentBin = (float) centroid;
    bgndPeakFWHMBin = (float) FWHM;
    bgndPeakCentKeV = k_offset + k_gain * (float) centroid;
    bgndPeakFWHMKeV = k_gain * FWHM;
    bgndOffset = offset;

    return centroid;
}

void Spectrum::setBinStatus() {
    // sets the status of a bin depending on whether it should be included in bgnd fit

    int i, j, m, eid, id, lo_bin, hi_bin;

    // default is to include bins
    for (m = 0; m < MaxNumBins; m++) includeBgnd[m] = true;

    // do not include bins lying under calibration peaks
    eid = PCalibElementID;
    for (j = 0; j < numExcludePeaks[eid]; j++) {
        //id = calibPeakID[j];
        id = excludePeakID[eid][j];
        lo_bin = element[eid].line[id].lo_bin;
        hi_bin = element[eid].line[id].hi_bin;

        //ALOGI(" setBinStatus calibPeaks: j=%d eid=%d id=%d: lo_bin=%d hi_bin=%d", j, eid, id, lo_bin, hi_bin);
        //if(fabs(centroidBin-spectrum.bgndPeakCentBin)>MinDistBgndPeak*FWHM_bins) {
        //  for(m=lo_bin; m<=hi_bin; m++) spectrum.includeBgnd[m]=false;
        //}
        for (m = lo_bin; m <= hi_bin; m++) {
            if (m >= 0 and m < MaxNumBins) includeBgnd[m] = false;
        }
    }

    // do not include bins lying under analysis peaks
    for (i = 0; i < MaxNumAnalyzeElements; i++) {
        eid = analyzeElementID[i];
        for (j = 0; j < numExcludePeaks[eid]; j++) {

            id = excludePeakID[eid][j];
            lo_bin = element[eid].line[id].lo_bin;
            hi_bin = element[eid].line[id].hi_bin;

            //if(fabs(centroidBin-spectrum.bgndPeakCentBin)>MinDistBgndPeak*FWHM_bins) {
            //  for(m=lo_bin; m<=hi_bin; m++) spectrum.includeBgnd[m]=false;
            //}

            for (m = lo_bin; m <= hi_bin; m++) {
                if (m >= 0 and m < MaxNumBins) includeBgnd[m] = false;
            }

        }
    }

    // PJR 9/11/15 test!!
    // exclude region where lead affects underlying template
    //lo_bin = int((45.0 - spectrum.k_offset)/spectrum.k_gain + 0.5);
    //hi_bin = int((56.5 - spectrum.k_offset)/spectrum.k_gain + 0.5);
    //for(m=lo_bin; m<=hi_bin; m++) spectrum.includeBgnd[m]=false;

}



bool Spectrum::checkForStyroFm() {
    int i, loBin, hiBin;
    int sumCompCnts = 0;
    float maxCntRate;
    bool status=false;

    // Returns true for styrofoam substrate

    // look at counts in Compton peak
    loBin = int((50. - k_offset) / k_gain);
    hiBin = int((55. - k_offset) / k_gain);

    // find sum of counts in Compton ROI
    sumCompCnts = 0;
    for (i = loBin; i <= hiBin; i++) {
        sumCompCnts += counts[i];
    }

    maxCntRate = (float)sumCompCnts / nomSecs;
    ALOG("checkForStyroFm: maxCntRate = %f", maxCntRate);

    if (maxCntRate < maxCntRateSF) status = true;

    return (status);

}

void Spectrum::initSpectrum(const DppSpectrum &dppSpec ) {
    //
    // Initialize variables in spectrum structure from the dppSpec structure
    //

    int i;

    // live time reported by DPP
    liveTime = dppSpec.liveTime_;

    // calculate nominal measurement live time (equivalent if source was nominal strength)
    if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
        nomSecs = srcMCi / Co57NomSrcMCi * liveTime;
    } else {
        nomSecs = srcMCi / Cd109NomSrcMCi * liveTime;
    }

    // accumulation time
    accTime = dppSpec.accTime_;

    // fast and slow counts
    fastCorrCnts = dppSpec.fastCntCorr_;
    slowCnts = dppSpec.slowCount_;

    // board and detector temperatures
    TECTemp = dppSpec.TECTemp_;
    boardTemp = dppSpec.boardTemp_;

    if (dppSpec.numBins_ > MaxNumBins) {
        ALOGI(" initSpectrum: Too many bins for Spectrum structure - increase size of MaxNumBins");
    }

    // assign number of bins
    if (dppSpec.numBins_ <= MaxNumBins) {
        numBins = dppSpec.numBins_;
    } else {
        numBins = MaxNumBins;
    }

    // Set the pointer to the input counts
    counts = dppSpec.spectrum_;

    // total counts in spectrum
    for (i = 0; i < numBins; i++) {
        totCnts += counts[i];
    }

    ALOG(" initSpectrum: totCnts=%d", totCnts);
}

float Spectrum::nomSeconds(float seconds) {

    // Converts seconds to nominal source seconds

    float nomSecs;

    if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
        nomSecs = srcMCi / Co57NomSrcMCi * seconds;
    } else {
        nomSecs = srcMCi / Cd109NomSrcMCi * seconds;
    }

    return (nomSecs);
}


float Spectrum::realSeconds(float nomSecs) {

    // Converts nominal source seconds to real seconds

    float realSecs;

    if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
        realSecs = Co57NomSrcMCi / srcMCi * nomSecs;
    } else {
        realSecs = Cd109NomSrcMCi / srcMCi * nomSecs;
    }
    return (realSecs);
}

int Spectrum::sourceCorrectMS(int nomMilliSecs) {

    // Converts nominal milliseconds to actual accumulation time required
    // Corrects for both source decay and simultaneous live time increase

    int accMilliSecs;
    float decayFac;

    if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
        decayFac = Co57NomSrcMCi / srcMCi;
    } else {
        decayFac = Cd109NomSrcMCi / srcMCi;
    }
    accMilliSecs = int(decayFac * decayFac / (decayFac + NomLiveTime - 1.0) * nomMilliSecs);

    ALOG("NomSrc %f, actualSrc %f, nomMilliSecs %d accMilliSecs %d", Co57NomSrcMCi, srcMCi,
         nomMilliSecs, accMilliSecs);

    return (accMilliSecs);
}

int Spectrum::sourceCorrectSeconds(int realMilliseconds) {

    // Converts real seconds to source corrected seconds, get longer as source decays

    int srcCorSec;
    ALOG("NomSrc %f, actualSrc %f, realSecs %d", Co57NomSrcMCi, srcMCi, realMilliseconds);

    if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
        srcCorSec = (int) (Co57NomSrcMCi / srcMCi * realMilliseconds);
    } else {
        srcCorSec = (int) (Cd109NomSrcMCi / srcMCi * realMilliseconds);
    }
    ALOG("SourceCorrectSecs = %d", srcCorSec);

    return (srcCorSec);
}

int Spectrum::remainingTime(float minNomSecs) {

    // Calculates the approximate remaining accumulation time needed to have a measurement that exceeds
    // a live time of minNomSecs
    //
    //  Inputs:
    //     minNomSecs: the minimum required source time for the measurement (seconds)
    //
    //  Return Value:
    //     Remaining accumulation time needed for the measurement
    //

    int secs;
    float deltaNomSecs;

    deltaNomSecs = (minNomSecs - nomSecs);

    // remaining accumulation time required
    secs = (int) (deltaNomSecs * accTime / liveTime);

    return (secs);

}

int Spectrum::writeConfigFile( int calibMode) {

    // write out system configuration file
    //
    // Inputs:
    //    calibMode=1: Create service configuration file
    //    calibMode=2: Create factory configuration file
    //

    bool status = false;
    FILE *ofp;

    if (calibMode == 1) {
        ofp = fopen(SysConfigSvc, "w");
    } else {
        ofp = fopen(SysConfigFac, "w");
    }

    if (ofp != NULL) {
        // write source assay information to file
        fprintf(ofp, "SrcAssayDate %s", srcAssayDate);
        fprintf(ofp, "\nSrcAssayMCi %5.2f", srcAssayMCi);

        // write gain and offset
        fprintf(ofp, "\nGain %6.4f", k_gain);
        fprintf(ofp, "\nOffset %6.4f", k_offset);

    } else {
        status = false;
    }
    fclose(ofp);

    return (status);
}

void Spectrum::writeSpectrum( float gain, float offset) {
    // Just writes raw spectrum with no energy normalization

//    int i;
//
//    for (i = 0; i < numBins; i++) {
//        normCounts[i] = counts[i];
//    }
    writeSpectrumNorm(gain, offset);

}

void Spectrum::writeSpectrumNorm(float gain, float offset) {

    //
    //  Fills normalized spectrum arrays
    //

    int i, specBinInt;
    int elemID, lineID;
    float spec_bin;
    float energy, value, subValue;
    float specBinFrac, outSpecGain;

    // gain of current spectrum may be different from default output file gain, so first need to rebin spectrum
    outSpecGain = (OutSpecHiKeV - OutSpecLoKeV) / (OutSpecNumBins - 1);

    if (gain > 0) {

        // interpolate between spectrum bins to create rebinned output spectrum
        for (i = 0; i < OutSpecNumBins; i++) {

            // initialize background subtracted spectrum to zero
            normCountsBSub[i] = 0;

            // energy of output spectrum bin
            energy = OutSpecLoKeV + outSpecGain * i;

            // corresponding bin in current spectrum
            spec_bin = (energy - offset) / gain;
            if (spec_bin < 0) spec_bin = 0;
            if (spec_bin >= MaxNumBins) spec_bin = MaxNumBins - 1;

            // integer and bin fraction for interpolation
            specBinInt = (int) spec_bin;
            specBinFrac = spec_bin - specBinInt;

            // interpolate value between current spectrum bins
            if ((specBinInt + 1) < MaxNumBins) {
                value = (float) (((1.0 - specBinFrac) * counts[specBinInt] +
                                  specBinFrac * counts[specBinInt + 1]) * outSpecGain / gain);
                subValue = (float) (((1.0 - specBinFrac) *
                                     (counts[specBinInt] - bgnd[specBinInt]) +
                                     specBinFrac *
                                     (counts[specBinInt + 1] - bgnd[specBinInt + 1])) *
                                    outSpecGain / gain);
            } else {
                value = 0;
                subValue = 0;
            }

            //ALOGI(" specBin=%d tempBin=%f specEnergy=%f specGain=%f tempGain=%f value=%f", i, temp_bin, spec_energy, spectrum.k_gain, sampTemp.gain[n], value);
            normCounts[i] = int(value + 0.5);
            normEnergy[i] = energy;

            // display background subtracted spectrum for entire energy range
            if (OutSpecBgndSubMode == 1) {
                if (energy >= BgndTempLoKeV and energy <= BgndTempHiKeV) {
                    normCountsBSub[i] = int(subValue + 0.5);
                }
                // display background subtracted spectrum only in peak regions of interest
            } else {

                if (energy >= BgndTempLoKeV and energy <= BgndTempHiKeV) {

                    // don't allow negative counts for this display mode
                    if (subValue < 0) subValue = 0;

                    for (elemID = 0; elemID < MaxNumAnalyzeElements; elemID++) {
                        if (numAnalyzePeaks[elemID] > 0) {
                            for (lineID = 0; lineID < NumLinesPerElement; lineID++) {
                                if (spec_bin >= element[elemID].line[lineID].lo_bin and
                                    spec_bin <= element[elemID].line[lineID].hi_bin) {
                                    normCountsBSub[i] = int(subValue + 0.5);
                                }
                            }
                        }
                    }

                }
                //ALOG("bin %d: Counts=%d", i+1, spectrum.normCountsBSub[i]);
            }
        }
    }

}

int Spectrum::writeLogFile( int ID, int elemID) {

    FILE *ofp;

    // open log file
    ofp = fopen(LogFileName, "a");

    // write to file
    fprintf(ofp,
            "\n %2d %2d %2d %4.3f %2d  %6.3f %6.3f %5.3f %4.1f   %d %d %d %d %d  %5.3f %6.1f %6.3f %7.4f %6.3f %6.3f %4.3f  %6.3f %6.4f %d %d",
            ID,
            calibSubType + 1,
            results.tempType + 1,
            tempNormFac,
            results.result,

            results.mgCm2,
            element[elemID].mgcm2_quick,
            results.stdDev,
            results.confLev,

            (int) element[elemID].kAlphaRaw,
            (int) element[elemID].kAlphaBgnd,
            (int) element[elemID].kAlphaCnts,
            (int) element[elemID].kBetaBgnd,
            (int) element[elemID].kBetaCnts,

            element[elemID].alphaBetaRatio,
            element[elemID].kCalibRawFac,
            element[elemID].kCalibRatioFac[0],
            ecalChisq,
            tempChisq,
            ecalFWHM,
            bgndPeakSepKeV,

            liveTime,
            k_gain,
            TECTemp,
            boardTemp);

    fclose(ofp);

    return 0;

}

int Spectrum::fitBackground(int elemID, int lineID) {

    // fit a smooth quadratic curve through up to five points on the spectrum to find the background

    int i, j, n, m, ibin;
    int avSize = 3; //[3]
    int bin[5];
    float cnts[5], avCnts[5];
    int numPnts = 3;
    int numIter_x1 = 3000; // [3000]
    int numIter_x2 = 2500; // [2500]
    int allCnts, lo_bin, hi_bin;
    //float pntEnergy[5]={38.7f, 62.9f, 71.2f, 78.0f, 0.0}; // [#5]
    float pntEnergy[5] = {62.9f, 71.2f, 78.0f, 0.0, 0.0}; // [#1] Default
    //float pntEnergy[5]={38.7f, 62.9f, 78.0f, 0.0, 0.0};  // [#2]
    //float pntEnergy[5]={38.7f, 71.2f, 78.0f, 0.0, 0.0}; // [#4]
    //float pntEnergy[5]={71.2f, 78.0f, 0.0, 0.0, 0.0}; // [#3]
    float x0, x1, x2, x1_min, x1_max, x2_min, x2_max;
    float dx1, dx2, val, chisq, scaleFac = 1000.f;
    float x1_fit = 0.0f, x2_fit = 0.0f, chisq_min = 1.E8;
    float bgnd, keV_lo, keV_hi;
    bool debug = false;

    FILE *ofp;

    // x0 = counts at first point
    // 0.0 < x1 < 25.0
    // 0.0 < x2 <  0.5

    // find the bins corresponding to the points
    for (i = 0; i < numPnts; i++) {
        bin[i] = int((pntEnergy[i] - k_offset) / k_gain + 0.5f);
    }

    // find the average counts around the points
    for (i = 0; i < numPnts; i++) {
        avCnts[i] = 0;
        for (j = -avSize; j <= avSize; j++) {
            ibin = bin[i] + j;
            if (ibin >= 0 and ibin < MaxNumBins) {
                avCnts[i] += counts[ibin];
            }
        }
        avCnts[i] /= (avSize * 2 + 1);
    }

    // normalize counts relative to first point (mult by scaleFac)
    for (i = 0; i < numPnts; i++) {
        cnts[i] = avCnts[i] / avCnts[0] * scaleFac;
    }

    x0 = cnts[0];

    // initial slope is slope between 1st pair of points
    x1 = (cnts[1] - cnts[0]) / (bin[1] - bin[0]);

    x1_min = (float) (0.90 * x1); // [0]
    x1_max = (float) (1.10 * x1); // [20]
    x2_min = 0;     // [0]
    x2_max = 0.15f; // [0.15]

    dx1 = (x1_max - x1_min) / numIter_x1;
    dx2 = (x2_max - x2_min) / numIter_x2;

    x1 = x1_min;
    for (n = 0; n < numIter_x1; n++) {

        x2 = x2_min;
        for (m = 0; m < numIter_x2; m++) {

            chisq = 0;
            for (i = 0; i < numPnts; i++) {

                ibin = bin[i] - bin[0];
                val = x0 + x1 * ibin + x2 * ibin * ibin;
                chisq += (cnts[i] - val) * (cnts[i] - val);

            }
            chisq = sqrt(chisq / numPnts);

            if (chisq < chisq_min) {
                chisq_min = chisq;
                x1_fit = x1;
                x2_fit = x2;
            }
            x2 += dx2;

        }

        x1 += dx1;

    }

    // first find bin limits for ROI under peak
    keV_lo = (float) lineKeVLo[elemID][lineID];
    keV_hi = (float) lineKeVHi[elemID][lineID];
    lo_bin = int((keV_lo - k_offset) / k_gain + 0.5);
    hi_bin = int((keV_hi - k_offset) / k_gain + 0.5);
    if (lo_bin < 0) lo_bin = 0;
    if (hi_bin > MaxNumBins) hi_bin = MaxNumBins - 1;

    // calculate counts in peak and background counts using fit
    allCnts = 0;
    bgnd = 0;
    for (i = lo_bin; i < +hi_bin; i++) {
        allCnts += counts[i];
        //bgnd += (x0 + x1_fit * (i-bin[0]) + x2_fit * (i-bin[0])*(i-bin[0])) * spectrum.counts[bin[0]]/scaleFac;
        bgnd += (x0 + x1_fit * (i - bin[0]) + x2_fit * (i - bin[0]) * (i - bin[0])) * avCnts[0] /
                scaleFac;
    }

    if (lineID == ka1) {
        element[elemID].kAlphaRaw_fit = allCnts;
        element[elemID].kAlphaCnts_fit = allCnts - bgnd;
        element[elemID].kAlphaBgnd_fit = bgnd;
    }
    ALOGI(" #### fitBackground: PbCnts=%d  Bgnd=%d RawCnts= %d (x0=%f x1=%f x2=%f)", int(allCnts
            -bgnd), (int) bgnd, allCnts, x0 * avCnts[0] / scaleFac, x1_fit, x2_fit);


    if (debug) {
        printf("\n **** Fit Values ****");
        for (i = 0; i < numPnts; i++) {
            printf("\n   bin %d = %d  avCnts=%d", i + 1, bin[i], (int) avCnts[i]);
        }
        printf("\n  x0 = %6.0f   x1 = %6.4f   x2 = %6.4f", x0, x1_fit, x2_fit);

        ofp = fopen("fitBgnd.txt", "a+");
        for (i = bin[0] - 50; i <= bin[numPnts - 1] + 50; i++) {
            //val = (x0 + x1_fit * (i-bin[0]) + x2_fit * (i-bin[0])*(i-bin[0])) * spectrum.counts[bin[0]]/scaleFac;
            val = (x0 + x1_fit * (i - bin[0]) + x2_fit * (i - bin[0]) * (i - bin[0])) * avCnts[0] /
                  scaleFac;
            fprintf(ofp, "\n %3d %6d %f", i, counts[i], val);
        }
        fclose(ofp);
    }

    return (0);
}

float Spectrum::calcSoilPPM(int elemID, int lineID)
{

    // calculates the soil concentration using a density correction based on the Compton peak

    int i;
    int bin_lo, bin_hi;
    int compCnts1, compCnts2;
    float calFac, calcPPM;
    float keV_lo;
    float keV_hi;

    // calculate the counts in the 1st Compton ROI
    keV_lo = SoilCompKeVLo1;
    keV_hi = SoilCompKeVHi1;
    bin_lo = int((keV_lo - k_offset) / k_gain + 0.5);
    bin_hi = int((keV_hi - k_offset) / k_gain + 0.5);
    compCnts1 = 0;
    for (i = bin_lo; i <= bin_hi; i++) {
        compCnts1 += counts[i];
    }

    // calculate the counts in the 2nd Compton ROI
    keV_lo = SoilCompKeVLo2;
    keV_hi = SoilCompKeVHi2;
    bin_lo = int((keV_lo - k_offset) / k_gain + 0.5);
    bin_hi = int((keV_hi - k_offset) / k_gain + 0.5);
    compCnts2 = 0;
    for (i = bin_lo; i <= bin_hi; i++) {
        compCnts2 += counts[i];
    }

    // calculate calibration factor
    calFac = coefficient2 + coefficient1 * (float)compCnts1 / (compCnts1 + compCnts2);

    // calculate concentration
    if (lineID == ka1 || lineID == ka2) {
        calcPPM = (element[elemID].kAlphaCnts / (compCnts1 + compCnts2)) / calFac;
    } else {
        calcPPM = 0;
    }

    ALOG(" compCnts1=%d compCnts2=%d calFac=%f C1/CSum=%f calcPPM=%f", compCnts1, compCnts2, calFac,
         (float) compCnts1 / (compCnts1 + compCnts2), calcPPM);

    compCnts1 = compCnts1;
    compCnts2 = compCnts2;

    return (calcPPM);
}

float Spectrum::fitPeaksBgnd(int elementID1, int line_ID1, int elementID2, int line_ID2, int mode) {
    //
    //   *** Version used for HBI-Pb ***
    //
    //
    // Fits two (twin) peaks with single (dual) Gaussians using the PJR random phase space approach and fits a quartic
    //   background curve under the peaks
    //
    // If lineID=ka1, secondary peak is ka2 (and vice versa)
    // If lineID=kb1, secondary peak is kb3 (and vice versa)
    // If lineID=kb2, secondary peak is kb1 (and vice versa)
    //
    // Fit parameters:
    // p0: Peak 1 amplitude
    // p1: Peak 1 Centroid (bin number)
    // p2: Peak sigma (in bins)
    // p3: Peak tailing amplitude
    // p4: Peak tailing sigma (in bins)
    // p5: Background offset
    // p6: Background slope
    // p7: Background Curvature (square)
    // p8: Background Curvature (cube)
    // p9: Background Curvature (quartic)

    int bin_lo, bin_hi, bin;
    int totPeaks, rndmMode = 1, lineID[2][2], elementID[2], numIterations;
    int i, j, id, n, np, m, npar, nbins, totCnts = 0, line_ID;
    int numPeaks, lo_bin, hi_bin, loBin[2][2], hiBin[2][2];
    int rawCounts;
    int m2[MaxFitPoints], m3[MaxFitPoints], m4[MaxFitPoints];
    float x0[2][2], y0[2][2];
    double FWHM_bins[2][2];
    float x[MaxFitPoints], y[MaxFitPoints], w[MaxFitPoints];
    double h[2][MaxFitParams];
    double p[2][MaxFitParams];
    double pnew[2][MaxFitParams];
    static int numFit=0;
    double y_hat[MaxFitPoints], y_hat_f[MaxFitPoints];
    double y_bgnd[MaxFitPoints], y_bgnd_f[MaxFitPoints];
    float chisq, chisq_f, min_chisq_f;
    double initSigmaSq[2];
    double bgndCnts, initStepMag[MaxFitParams], maxStepMag[MaxFitParams];
    float ln2 = 0.69315f;
    double sqrt_2ln2 = 1.1774l;
    float cent1, cent2, keV[2], binSep[2], iFac[2], SNR;
    float ECalKeV, keV_lo, keV_hi, fitCounts, bgndCounts;

    double fexp(double);
    float pjr_rand(void);

    // initial tail amplitude factor (fraction of height of gaussian)
    float tailAmpFac = 0.45f;

    // initial tail width (bins)
    float tailWidth = 6.2f;

    // increase the bin range on each side of the peaks by this factor times the peak FWHM
    float rangePadFactor = 2.25f;  // 2.25f;

    bool printFit =  false;
    bool AddTailExp = false;

    FILE *ofp;

    // fit two twin peaks
    totPeaks = 2;

    // energy of primary calibration peak
    ECalKeV = element[element_id(PCalibElementID)].line[calibPeakID[0]].keV;
    ALOG(" Energy of primary calibration peak: %f ", ECalKeV);

    lineID[0][0] = line_ID1;
    lineID[1][0] = line_ID2;
    elementID[0] = elementID1;
    elementID[1] = elementID2;

    // fit twin peaks
    numPeaks = 2;

    // number of fit parameters
    npar = 10;

    lo_bin = numBins;
    hi_bin = 0;
    for (np = 0; np < totPeaks; np++) {

        // set peak ID's
        if (np == 0) {
            line_ID = line_ID1;
        } else {
            line_ID = line_ID2;
        }

        // if main peak is ka1/kb1 assign other peak to ka2/kb2 and vice versa
        // (higher energy peak is always first)
        if (line_ID == ka1 || line_ID == ka2) {
            lineID[np][0] = ka1;
            lineID[np][1] = ka2;
        } else if (line_ID == kb1 || line_ID == kb3) {
            lineID[np][0] = kb1;
            lineID[np][1] = kb3;
        } else if (line_ID == kb2) {
            lineID[np][0] = kb2;
            lineID[np][1] = kb1;
        }

        // line energies (higher energy peak of each twin peak)
        keV[np] = element[elementID[np]].line[lineID[np][0]].keV;

        // expected bin separation between peaks
        cent1 = (element[elementID[np]].line[lineID[np][0]].keV - k_offset) /
                k_gain;
        cent2 = (element[elementID[np]].line[lineID[np][1]].keV - k_offset) /
                k_gain;

        binSep[np] = cent1 - cent2;

        // relative intensity of peaks within twin peak
        iFac[np] = element[elementID[np]].line[lineID[np][1]].intensity /
                   element[elementID[np]].line[lineID[np][0]].intensity;
        ALOG(" fitPeaksBgnd: %s (%s and %s): keV=%f binSep=%f iFac=%f", elementSymb[elementID[np]], lineName[lineID[np][0]], lineName[lineID[np][1]], keV[np], binSep[np], iFac[np]);

        // find bin range for fit that includes both peaks
        for (i = 0; i < numPeaks; i++) {
            id = lineID[np][i];
            x0[np][i] = element[elementID[np]].line[id].centroidBin;
            y0[np][i] = (float) counts[(int) x0[np][i]];
            //FWHM_bins[np][i] = sqrt(keV[np]/ECalKeV) * ECalFWHM;
            FWHM_bins[np][i] = element[elementID[np]].line[id].FWHM_bins;
            loBin[np][i] = element[elementID[np]].line[id].lo_bin;
            hiBin[np][i] = element[elementID[np]].line[id].hi_bin;

            // allow for one FWHM on each side of the two twin peaks
            loBin[np][i] -= rangePadFactor * (int) FWHM_bins[np][i];
            hiBin[np][i] += rangePadFactor * (int) FWHM_bins[np][i];

            if (loBin[np][i] < lo_bin) {
                lo_bin = loBin[np][i];
            }

            if (hiBin[np][i] > hi_bin) {
                hi_bin = hiBin[np][i];
            }
        }

    } // end looping over total peaks

    nbins = hi_bin - lo_bin + 1;
    ALOG(" fitPeaksBgnd: lo_bin=%d hi_bin=%d", lo_bin, hi_bin);

    // check for array overload
    if (nbins >= MaxFitPoints) {
        ALOG(" Too many points in fit to be stored in arrays. Increase the size of defined parameter MaxFitPoints");
        return nbins;
    }

    if (lo_bin + nbins > numBins) {
        ALOG(" fitPeaksBgnd: Overloading counts array");
        return (lo_bin + nbins);
    }

    // load counts depending on whether incremental or cumulative spectra selected
    // load counts depending on mode
    for (m = 0; m < nbins; m++) {
        x[m] = (float)m;                                        // x data point
        if(mode==1) {                                           // y data point
            y[m] = (float) counts[lo_bin + m] - (float) prevCounts[lo_bin + m];
        } else if (mode==2){
            y[m] = (float) counts[lo_bin + m];
        } else {
            y[m] = (float) cumCounts[lo_bin + m];
        }
        totCnts += counts[lo_bin + m];
    }

    // fill y0 array depending on mode
    for (np = 0; np < totPeaks; np++) {
        for (i = 0; i < numPeaks; i++) {
            if (mode == 1) {
                y0[np][i] = (float) (counts[(int) x0[np][i]] - prevCounts[(int) x0[np][i]]);
            } else if (mode==2) {
                y0[np][i] = (float) counts[(int) x0[np][i]];
            } else {
                y0[np][i] = (float) cumCounts[(int) x0[np][i]];
            }
        }
    }


    // background offset is just counts at lower bin limit
    p[0][5] = y[0];

    // initial background slope and curvature
    p[0][6] = (y[nbins-1] - y[0]) / nbins;
    p[0][7] = -0.03f;
    p[0][8] = 0.0002f;
    p[0][9] = 0.0000003f;

    //ALOG(" fitPeaksBgnd: Cnts(lo)=%d Cnts(hi)=%d p[5]=%f p[6]=%f", spectrum.counts[lo_bin], spectrum.counts[hi_bin], p[0][5], p[0][6]);

    // initial Gaussian fit parameters
    for(np=0; np<totPeaks; np++) {

        // find background under peaks and subtract this from the initial peak intensities
        bgndCnts = p[0][5] + ((double)x0[np][0] - lo_bin) * p[0][6];

        // initial best guess at fit parameters (twin Gaussians with fixed separation and equal widths)
        p[np][0] = (1.0f - tailAmpFac) * (y0[np][0] - bgndCnts);
        p[np][1] = x0[np][0] - lo_bin;

        // note: p[np][2] is sigma squared
        initSigmaSq[np] = FWHM_bins[np][0] * FWHM_bins[np][0] / (8 * ln2);
        p[np][2] = initSigmaSq[np];

        // initial tailing fit parameters
        p[np][3] = tailAmpFac; // amplitude (fraction of Gaussian amplitude)
        p[np][4] = tailWidth;  // width (bins)

    }

    // max absolute step sizes for each parameter for main Gaussian and background
    initStepMag[0] = p[0][0] * 0.25f;    // max change in amplitude
    initStepMag[1] = 3.0f;               // max change in centroid bin
    initStepMag[2] = p[0][2] * 0.25f;    // max change in sigma2
    initStepMag[3] = 0.5f;               // max change in tailing amplitude
    initStepMag[4] = 4.0f;               // max change in tailing width
    initStepMag[5] = p[0][5] * 0.25f;    // max change in background
    initStepMag[6] = p[0][6] * 0.10f;    // max change in background slope [0.10... 50.0 before that]
    initStepMag[7] = p[0][7] * 0.10f;    // max change in quadratic term
    initStepMag[8] = p[0][8] * 0.10f;    // max change in cubic term
    initStepMag[9] = p[0][9] * 0.10f;    // max change in quartic term

    // precalculate values
    for (m=0; m<nbins; m++) {
        m2[m] = m * m;
        m3[m] = m2[m] * m;
        m4[m] = m3[m] * m;
    }

    // initial fit values
    chisq = 0;
    for(m=0; m<nbins; m++) {

        y_bgnd[m] = p[0][5] + p[0][6]*m + p[0][7]*m2[m] + p[0][8]*m3[m] + p[0][9]*m4[m];            // fit a variable background along with other fit parameters
        //w[m] = y[m]/totCnts;                                                                      // weighting for each point
        w[m] = 1.0f;

        y_hat[m] = y_bgnd[m];
        for (np = 0; np < totPeaks; np++) {
            y_hat[m] += p[np][0] * fexp(-(x[m] - p[np][1]) * (x[m] - p[np][1]) / (2 * p[np][2]));        // initial fitted value for peak 1
            y_hat[m] += iFac[np] * p[np][0] * fexp(-(x[m] - (p[np][1] - binSep[np])) * (x[m] - (p[np][1] - binSep[np])) / (2 * p[np][2]));     // initial fitted value for peak 2

            // add exponential tailing
            if (AddTailExp) {
                if (m < p[np][1]) {
                    y_hat[m] += p[np][3] * p[np][0] * fexp(-(p[np][1] - x[m]) / p[np][4]);
                }
                if (m < (p[np][1] - binSep[np])) {
                    y_hat[m] += p[np][3] * iFac[np] * p[np][0] * fexp(-(p[np][1] - binSep[np] - x[m]) / p[np][4]);
                }
            }
            chisq += w[m] * (y[m] - y_hat[m]) * (y[m] - y_hat[m]);      // chisq for initial parameter values)
        }

    }
    chisq /= nbins;

    for (np = 0; np < totPeaks; np++) {
        ALOG(" fitPeaksBgnd: totCnts=%d chisq=%f Initial Parameters: %f %f %f %f %f", totCnts, chisq, p[np][0], p[np][1]+lo_bin, p[np][2], p[np][3], p[np][4]);
    }

    // initialize min chisq to start chisq
    min_chisq_f = chisq;

    numIterations = NumFitIterFac*npar;

    for (j = 0; j < numIterations; j++) {

        //ALOG("Rand=%f", pjr_rand());

        for (n = 0; n < npar; n++) maxStepMag[n] = (1 - float(j) / numIterations) * initStepMag[n];

        // randomly switch between modes 1 (change all parameters) and 2 (change only one parameter)
        if (pjr_rand() < 0.1) {  // 0.25
            rndmMode = 1;
        } else {
            rndmMode = 2;
        }

        // randomly determine step size for all variables
        if (rndmMode == 1) {
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    h[np][n] = 2 * (pjr_rand() - 0.5f) * maxStepMag[n];
                }
            }

        } else {
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    h[np][n] = 0;
                }
            }
            n = int(pjr_rand() * npar);
            np = int(pjr_rand() * totPeaks);
            if (n >= npar) n--;
            if (np >= totPeaks) np--;
            h[np][n] = 2 * (pjr_rand() - 0.5f) * maxStepMag[n];
            //ALOGI("\n Change in parameter (np=%d n=%d) = %f", np+1, n, h[np][n]);
        }

        // do not allow peak widths to become too small (added 7/26/19)
        //for (np = 0; np < totPeaks; np++) {
        //    if((p[np][2]+h[np][2])<initSigmaSq[np]*MinFitWidthFac) {
        //        h[np][2] = initSigmaSq[np]*MinFitWidthFac - p[np][2];
        //    }
        //}

        // do not allow peak widths to become too large
        for (np = 0; np < totPeaks; np++) {
            if((p[np][2]+h[np][2])>initSigmaSq[np]*MaxFitWidthFac) {
                h[np][2] = initSigmaSq[np]*MaxFitWidthFac - p[np][2];
            }
        }

        // do not vary centroid bins
        //h[0][1] = 0;
        //h[1][1] = 0;

        // do not allow centroids to vary too far from initial values
        for (np = 0; np < totPeaks; np++) {
            if ((p[np][1] + h[np][1]) > ((x0[np][0]-lo_bin) + MaxBinFitDelta)) h[np][1] = ((x0[np][0]-lo_bin) + MaxBinFitDelta) - p[np][1];
            if ((p[np][1] + h[np][1]) < ((x0[np][0]-lo_bin) - MaxBinFitDelta)) h[np][1] = ((x0[np][0]-lo_bin) - MaxBinFitDelta) - p[np][1];
        }

        // determine centroid of 2nd peak from the 1st peak (default is commented  out)
        //h[1][1] = (keV[1] - k_offset) / (keV[0]-k_offset) * (p[0][1] + h[0][1] + lo_bin) - p[1][1] - lo_bin;

        // force 2nd peak height relative to first
        h[1][0] = element[elementID[np]].line[lineID[1][0]].intensity / element[elementID[np]].line[lineID[0][0]].intensity * (p[0][0] + h[0][0]) - p[1][0];

        // scale width of 2nd peak using fitted width of 1st peak (default is comment out)
        h[1][2] = (keV[1] / keV[0]) * (p[0][2] + h[0][2]) - p[1][2];

        if (AddTailExp) {
            // determine tailing parameters of 2nd peak from 1st peak
            //h[1][3] = (p[0][3] + h[0][3]) - p[1][3];
            //h[1][4] = (p[0][4] + h[0][4]) - p[1][4];

            // check that tailing amplitude is not less than 0.4 and greater than 0.6
            for (np = 0; np < totPeaks; np++) {
                if ((p[np][3] + h[np][3]) < 0.4 || (p[np][3] + h[np][3]) > 0.6) {
                    h[np][3] = 0;
                }
            }

            // check that tailing width is not less than 5 and greater than 10
            for (np = 0; np < totPeaks; np++) {
                if ((p[np][4] + h[np][4]) < 5.0 || (p[np][4] + h[np][4]) > 10.0) {
                    h[np][4] = 0;
                }
            }
        }

        // don't change background scaling
        //h[0][5] = 0;

        // PJR 5/11/16 check that bin centroid is within range (otherwise don't change)
        // ...this is to prevent the calibration "walk" that resulted in some crazy wrong measurements in the field!
        for (np = 0; np < totPeaks; np++) {
            if ((p[np][1] + h[np][1]) < 0 || (p[np][1] + h[np][1] + lo_bin) > hi_bin) {
                h[np][1] = 0;
            }
        }

        // ensure peak and background height parameters (first 6 params) are positive
        for (n = 0; n <= 5; n++) {
            for (np = 0; np < totPeaks; np++) {
                if ((p[np][n] + h[np][n]) <= 0) {
                    h[np][n] = -p[np][n];
                }
            }
        }

        // new parameters to test after changes
        for (n = 0; n <npar; n++) {
            for (np = 0; np < totPeaks; np++) {
                pnew[np][n] = p[np][n] + h[np][n];
            }
        }

        chisq_f = 0;
        for (m = 0; m < nbins; m++) {

            // fitted value after change in parameters
            y_bgnd[m] = pnew[0][5] + pnew[0][6]*m + pnew[0][7]*m2[m] + pnew[0][8]*m3[m] + pnew[0][9]*m4[m];


            // fit starts with background then adds Gaussian peaks
            y_hat[m] = y_bgnd[m];

            for(np=0; np<totPeaks; np++) {
                y_hat[m]  += pnew[np][0]*fexp(-(x[m]-pnew[np][1])*(x[m]-pnew[np][1])/(2*pnew[np][2]));
                y_hat[m]  += iFac[np]*pnew[np][0]*fexp(-(x[m]-pnew[np][1]-binSep[np])*(x[m]-pnew[np][1]-binSep[np])/(2*pnew[np][2]));

                if(AddTailExp) {
                    if(m<pnew[np][1]) {
                        y_hat[m] += pnew[np][3]*pnew[np][0]*fexp(-(pnew[np][1]-x[m])/pnew[np][4]);
                    }
                    if(m<(pnew[np][1]-binSep[np])) {
                        y_hat[m]  += pnew[np][3]*iFac[np]*pnew[np][0]*fexp(-(pnew[np][1]-binSep[np]-x[m])/pnew[np][4]);
                    }
                }
            }

            // chisq after parameters have been updated
            chisq_f += w[m]*(y[m]-y_hat[m])*(y[m]-y_hat[m]);

        }
        chisq_f /= nbins;

        if (chisq_f < min_chisq_f) {
            min_chisq_f = chisq_f;
            // update parameters
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    p[np][n] += h[np][n];
                }
            }
            for(m=0; m<nbins; m++) {
                y_hat_f[m] = y_hat[m];
                y_bgnd_f[m] = y_bgnd[m];
            }
            //ALOG(" fitPeaksBgnd: Inside Loop: best_chisq=%d Parameters: %d %f %f %f %f", (int)chisq_f, (int)p[0][0], p[0][1], p[0][2]+lo_bin, p[0][3], p[0][4]);
        }

    }  // end looping over fit iterations

    // calculate counts in peak and background counts using fit
    rawCounts = 0;
    fitCounts = 0;
    bgndCounts = 0;

    ALOG("Entire fit region: bin_lo=%d bin_hi=%d", lo_bin, hi_bin);

    // look at counts under each peak (centroid +/- 2*sigma)
    for (np = 0; np < totPeaks; np++) {

        // find bin limits for ROI under peak (ID=0 is higher-energy peak of each twin peak)
        keV_lo = (float) lineKeV[elementID[np]][lineID[np][1]];
        keV_hi = (float) lineKeV[elementID[np]][lineID[np][0]];
        bin_lo = int((keV_lo - k_offset) / k_gain + 0.5) - 0.75 * element[elementID[np]].line[lineID[np][1]].FWHM_bins;
        bin_hi = int((keV_hi - k_offset) / k_gain + 0.5) + 0.75 * element[elementID[np]].line[lineID[np][0]].FWHM_bins;
        if (bin_lo < 0) bin_lo = 0;
        if (bin_hi > MaxNumBins) bin_hi = MaxNumBins - 1;

        // set bin limits for ROI to be entire fit region
        //bin_lo = lo_bin;
        //bin_hi = hi_bin;

        ALOG("Region under peak %d: keV_lo=%f(bin_lo=%d) keV_hi=%f (bin_hi=%d)", np+1, (float)bin_lo*k_gain+k_offset, bin_lo, (float)bin_hi*k_gain+k_offset, bin_hi);

        // sum counts in ROI under peak
        for (m = 0; m < nbins; m++) {
            bin = m + lo_bin;
            if (bin >= bin_lo and bin <= bin_hi) {
                rawCounts += y[m];
                if (y_hat_f[m] > 0) fitCounts += y_hat_f[m];
                if (y_bgnd_f[m] > 0) bgndCounts += y_bgnd_f[m];
            }
        }
    }

    if (printFit==true) {
        for (m = 0; m < nbins; m++) {
            ALOG(" %d %6.2f %6.1f %6.1f %6.1f", m + lo_bin, (m + lo_bin) * k_gain + k_offset, y[m], y_hat_f[m], y_bgnd_f[m]);
        }
    }

    ALOG("Cent1:  Fit=%f Initial=%f", (float)p[0][1], x0[0][0]-lo_bin);
    ALOG("Cent2:  Fit=%f Initial=%f", (float)p[1][1], x0[1][0]-lo_bin);
    ALOG("Width1: Fit=%f Initial=%f", (float)p[0][2], (float)initSigmaSq[0]);
    ALOG("Width2: Fit=%f Initial=%f", (float)p[1][2], (float)initSigmaSq[1]);

    // assign fit values to spectrum arrays
    element[elementID2].kAlphaRaw_fit = rawCounts;
    element[elementID2].kAlphaCnts_fit = fitCounts - bgndCounts;
    element[elementID2].kAlphaBgnd_fit = bgndCounts;

    for (np = 0; np < totPeaks; np++) {

        // return bin centroid relative to entire spectrum
        //p[np][1] += lo_bin;

        // update FWHM of peaks after fitting (p[2] is sigma squared)
        for (i = 0; i < numPeaks; i++) FWHM_bins[np][i] = 2 * sqrt_2ln2 * sqrt(p[np][2]);

        ALOG(" fitPeaksBgnd: #%d chisq=%d Peak: %d %lf %lf  Tail: %lf %lf  Bgnd: %lf %lf %lf %lf %lf", np+1, (int)sqrt(min_chisq_f),   (int)p[np][0], p[np][1]+lo_bin, p[np][2],
             p[np][3], p[np][4],   p[0][5], p[0][6], p[0][7], p[0][8], p[0][9]);
    }


    // SNR ratio
    SNR = (fitCounts-bgndCounts)/sqrt(bgndCounts);

    ALOG(" fitPeaksBgnd: mode=%d bin_lo=%d bin_hi=%d PbCnts=%d rawCounts=%d fitCounts=%d bgnd=%d SNR=%4.2f", mode, bin_lo, bin_hi, (int)(fitCounts-bgndCounts), rawCounts, (int)fitCounts, (int)bgndCounts, SNR);

    // reset SNR if raw counts in fit region are too low
    if(rawCounts<MinSpecCounts) SNR = 0;

    // reset SNR if Pb counts are too low
    if((fitCounts-bgndCounts)<MinPbCounts) SNR = 0;

    // update previous counts array
    for(i=0; i<MaxNumBins; i++) prevCounts[i] = counts[i];

    // increment number of times fit has been performed
    numFit++;

    // return detection SNR
    return (SNR);

}


float Spectrum::fitPeaksBgnd(int elementID1, int line_ID1, int elementID2, int line_ID2) {
    // Fits two (twin) peaks with single (dual) Gaussians using the PJR random phase space approach and fits a quadratic
    //   background curve under the peaks
    //
    // If lineID=ka1, secondary peak is ka2 (and vice versa)
    // If lineID=kb1, secondary peak is kb3 (and vice versa)
    // If lineID=kb2, secondary peak is kb1 (and vice versa)
    //
    // Fit parameters:
    // p0: Peak 1 amplitude
    // p1: Peak 1 Centroid (bin number)
    // p2: Peak sigma (in bins)
    // p3: Peak tailing amplitude
    // p4: Peak tailing sigma (in bins)
    // p5: Background offset
    // p6: Background slope
    // p7: Background Curvature

    int bin_lo, bin_hi, bin;
    int totPeaks, rndmMode = 1, lineID[2][2], elementID[2], numIterations;
    int i, j, id, n, np, m, npar, nbins, totCnts = 0, line_ID;
    int numPeaks, lo_bin, hi_bin, loBin[2][2], hiBin[2][2];
    float x0[2][2], y0[2][2], FWHM_bins[2][2];
    float x[MaxFitPoints], y[MaxFitPoints], w[MaxFitPoints];
    float h[2][MaxFitParams];
    float p[2][MaxFitParams];
    static int numFit=0;
    float y_hat[MaxFitPoints], y_hat_f[MaxFitPoints];
    float y_bgnd[MaxFitPoints], y_bgnd_f[MaxFitPoints];
    float bgndCnts, chisq, chisq_f, min_chisq_f;
    float initStepMag[MaxFitParams], maxStepMag[MaxFitParams];
    float ln2 = 0.69315f, sqrt_2ln2 = 1.1774f;
    float cent1, cent2, keV[2], binSep[2], iFac[2];
    float keV_lo, keV_hi, fitCounts, rawCounts, bgnd;

    // initial tail amplitude factor (fraction of height of gaussian)
    float tailAmpFac = 0.45f;

    // initial tail width (bins)
    float tailWidth = 6.2f;

    // increase the bin range on each side of the peaks by this factor times the peak FWHM
    float rangePadFactor = 1.5f;

    bool plotFit = false;
    bool AddTailExp = true;

    FILE *ofp;

    // fit two twin peaks
    totPeaks = 2;
    lineID[0][0] = line_ID1;
    lineID[1][0] = line_ID2;
    elementID[0] = elementID1;
    elementID[1] = elementID2;

    // fit twin peaks
    numPeaks = 2;

    // number of fit parameters
    npar = 8;

    lo_bin = numBins;
    hi_bin = 0;
    for (np = 0; np < totPeaks; np++) {

        // set peak ID's
        if (np == 0) {
            line_ID = line_ID1;
        } else {
            line_ID = line_ID2;
        }

        // if main peak is ka1/kb1 assign other peak to ka2/kb2 and vice versa
        // (higher energy peak is always first)
        if (line_ID == ka1 || line_ID == ka2) {
            lineID[np][0] = ka1;
            lineID[np][1] = ka2;
        } else if (line_ID == kb1 || line_ID == kb3) {
            lineID[np][0] = kb1;
            lineID[np][1] = kb3;
        } else if (line_ID == kb2) {
            lineID[np][0] = kb2;
            lineID[np][1] = kb1;
        }

        // line energies (higher energy peak of each twin peak)
        keV[np] = element[elementID[np]].line[lineID[np][0]].keV;

        // expected bin separation between peaks
        cent1 = (element[elementID[np]].line[lineID[np][0]].keV - k_offset) /
                k_gain;
        cent2 = (element[elementID[np]].line[lineID[np][1]].keV - k_offset) /
                k_gain;

        binSep[np] = cent1 - cent2;

        // relative intensity of peaks
        iFac[np] = element[elementID[np]].line[lineID[np][1]].intensity /
                   element[elementID[np]].line[lineID[np][0]].intensity;
        //ALOGI(" fitPeaksBgnd: %s (%s and %s): keV=%f binSep=%f iFac=%f", elementSymb[elementID[np]], lineName[lineID[np][0]], lineName[lineID[np][1]], keV[np], binSep[np], iFac[np]);

        // find bin range for fit that includes both peaks
        for (i = 0; i < numPeaks; i++) {
            id = lineID[np][i];
            x0[np][i] = element[elementID[np]].line[id].centroidBin;
            y0[np][i] = (float) cumCounts[(int) x0[np][i]];
            FWHM_bins[np][i] = element[elementID[np]].line[id].FWHM_bins;
            loBin[np][i] = element[elementID[np]].line[id].lo_bin;
            hiBin[np][i] = element[elementID[np]].line[id].hi_bin;

            // allow for one FWHM on each side of the two twin peaks
            loBin[np][i] -= rangePadFactor * (int) FWHM_bins[np][i];
            hiBin[np][i] += rangePadFactor * (int) FWHM_bins[np][i];

            if (loBin[np][i] < lo_bin) {
                lo_bin = loBin[np][i];
            }

            if (hiBin[np][i] > hi_bin) {
                hi_bin = hiBin[np][i];
            }
        }

    } // end looping over total peaks

    nbins = hi_bin - lo_bin + 1;
    //ALOG(" fitPeaksBgnd: lo_bin=%d hi_bin=%d", lo_bin, hi_bin);

    // check for array overload
    if (nbins >= MaxFitPoints) {
        ALOGI(" Too many points in fit to be stored in arrays. Increase the size of defined parameter MaxFitPoints");
        return nbins;
    }

    if (lo_bin + nbins > numBins) {
        ALOGI(" fitPeaksBgnd: Overloading counts array");
        return (lo_bin + nbins);
    }

    // background offset is just counts at lower bin limit
    p[0][5] = float(counts[lo_bin]);
    //ALOGI(" fitPeaksBgnd: initial fit bgnd = %f", p[0][5]);

    // initial background slope and curvature
    p[0][6] = (float) (counts[hi_bin] - counts[lo_bin]) / (hi_bin - lo_bin);
    p[0][7] = 0.8f; // [0.8]

    // initial Gaussian fit parameters
    for(np=0; np<totPeaks; np++) {

        // find background under peaks and subtract this from the initial peak intensities
        bgndCnts = p[0][5] + (x0[np][0] - lo_bin) * p[0][6];

        // initial best guess at fit parameters (twin Gaussians with fixed separation and equal widths)
        p[np][0] = (1.0f - tailAmpFac) * (y0[np][0] - bgndCnts);
        p[np][1] = x0[np][0] - lo_bin;
        p[np][2] = FWHM_bins[np][0] * FWHM_bins[np][0] / (8 * ln2);

        // initial tailing fit parameters
        p[np][3] = tailAmpFac; // amplitude (fraction of Gaussian amplitude)
        p[np][4] = tailWidth;  // width (bins)

    }

    // max absolute step sizes for each parameter for main Gaussian
    initStepMag[0] = p[0][0] * 0.25f;    // max change in amplitude
    initStepMag[1] = 3.0f;               // max change in centroid bin
    initStepMag[2] = p[0][2] * 0.25f;    // max change in sigma2
    initStepMag[3] = 0.5f;               // max change in tailing amplitude
    initStepMag[4] = 4.0f;               // max change in tailing width
    initStepMag[5] = p[0][5] * 0.25f;    // max change in background
    initStepMag[6] = p[0][6] * 0.10f;    // max change in background slope [0.10... 50.0 before that]
    initStepMag[7] = 0.10f;              // max change in background curvature [0.10 ... 0.50f before that]

    for (m = 0; m < nbins; m++) {
        totCnts += counts[lo_bin + m];
    }

    // intial fit values
    chisq = 0;
    for(m=0; m<nbins; m++) {
        x[m] = (float)m;                                      // x data point
        y[m] = (float)counts[lo_bin+m];              // y data point
        y_bgnd[m] = p[0][5] + p[0][6]*m + p[0][7]*m*m;        // fit a variable background along with other fit parameters
        //w[m] = y[m]/totCnts;                                  // weighting for each point
        w[m] = 1.0f;

        y_hat[m] = y_bgnd[m];
        for (np = 0; np < totPeaks; np++) {
            y_hat[m] += p[np][0] * exp(-(x[m] - p[np][1]) * (x[m] - p[np][1]) / (2 *
                                                                                 p[np][2]));                                        // initial fitted value for peak 1
            y_hat[m] += iFac[np] * p[np][0] *
                        exp(-(x[m] - (p[np][1] - binSep[np])) * (x[m] - (p[np][1] - binSep[np])) /
                            (2 * p[np][2]));     // initial fitted value for peak 2

            // add exponential tailing
            if (AddTailExp) {
                if (m < p[np][1]) {
                    y_hat[m] += p[np][3] * p[np][0] * exp(-(p[np][1] - x[m]) / p[np][4]);
                }
                if (m < (p[np][1] - binSep[np])) {
                    y_hat[m] += p[np][3] * iFac[np] * p[np][0] *
                                exp(-(p[np][1] - binSep[np] - x[m]) / p[np][4]);
                }
            }
            chisq += w[m] * (y[m] - y_hat[m]) * (y[m] -
                                                 y_hat[m]);                                                                   // chisq for initial parameter values)
        }

    }
    chisq /= nbins;

    for (np = 0; np < totPeaks; np++) {
        //ALOGI(" fitPeaksBgnd: totCnts=%d chisq=%f Initial Parameters: %f %f %f %f %f", totCnts, chisq, p[np][0], p[np][1]+lo_bin, p[np][2], p[np][3], p[np][4]);
    }

    // find bin limits for ROI under peak
    keV_lo = (float) lineKeVLo[elementID2][line_ID2];
    keV_hi = (float) lineKeVHi[elementID2][line_ID2];
    bin_lo = int((keV_lo - k_offset) / k_gain + 0.5);
    bin_hi = int((keV_hi - k_offset) / k_gain + 0.5);
    if (bin_lo < 0) bin_lo = 0;
    if (bin_hi > MaxNumBins) bin_hi = MaxNumBins - 1;

    // initialize min chisq to start chisq
    min_chisq_f = chisq;

    numIterations = NumFitIterFac*npar;

    for (j = 0; j < numIterations; j++) {

        for (n = 0; n < npar; n++) maxStepMag[n] = (1 - float(j) / numIterations) * initStepMag[n];

        // randomly switch between modes 1 (change all parameters) and 2 (change only one parameter)
        if((float)rand()/RAND_MAX<0.1) {  // 0.25
            rndmMode = 1;
        } else {
            rndmMode = 2;
        }

        // randomly determine step size for all variables
        if (rndmMode == 1) {
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    h[np][n] = 2 * ((float) rand() / RAND_MAX - 0.5f) * maxStepMag[n];
                }
            }

        } else {
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    h[np][n] = 0;
                }
            }
            n = int((float) rand() / RAND_MAX * npar);
            np = int((float) rand() / RAND_MAX * totPeaks);
            if (n >= npar) n--;
            if (np >= totPeaks) np--;
            h[np][n] = 2 * ((float) rand() / RAND_MAX - 0.5f) * maxStepMag[n];
            //ALOGI("\n Change in parameter (np=%d n=%d) = %f", np+1, n, h[np][n]);
        }

        h[0][1] = 0;
        h[1][1] = 0;

        // determine centroid of 2nd peak from the 1st peak
        //h[1][1] = ((keV[1] - spectrum.k_offset) / (keV[0]-spectrum.k_offset) * (p[0][1] + h[0][1] + lo_bin) - lo_bin) - p[1][1];

        // scale width of 2nd peak using fitted width of 1st peak
        h[1][2] = sqrt(keV[1] / keV[0]) * (p[0][2] + h[0][2]) - p[1][2];

        // determine tailing parameters of 2nd peak from 1st peak
        //h[1][3] = (p[0][3] + h[0][3]) - p[1][3];
        //h[1][4] = (p[0][4] + h[0][4]) - p[1][4];

        // check that tailing amplitude is not less than 0.4 and greater than 0.6
        for (np = 0; np < totPeaks; np++) {
            if ((p[np][3] + h[np][3]) < 0.4 || (p[np][3] + h[np][3]) > 0.6) {
                h[np][3] = 0;
            }
        }

        // check that tailing width is not less than 5 and greater than 10
        for (np = 0; np < totPeaks; np++) {
            if ((p[np][4] + h[np][4]) < 5.0 || (p[np][4] + h[np][4]) > 10.0) {
                h[np][4] = 0;
            }
        }

        // don't change background scaling
        h[0][5] = 0;

        // PJR 5/11/16 check that bin centroid is within range (otherwise don't change)
        // ...this is to prevent the calibration "walk" that resulted in some crazy wrong measurements in the field!
        for (np = 0; np < totPeaks; np++) {
            if ((p[np][1] + h[np][1]) < 0 || (p[np][1] + h[np][1] + lo_bin) > hi_bin) {
                h[np][1] = 0;
            }
        }

        // check that peak widths are in range
        //for (np=0; np<totPeaks; np++) {
        // if((p[np][2]+h[np][2])<2.0 || (p[np][2]+h[np][2])>5.0) {
        // h[np][2] = 0;
        // }
        //}

        // ensure peak and background parameters are positive
        for (n = 0; n < npar; n++) {
            for (np = 0; np < totPeaks; np++) {
                if (p[np][n] + h[np][n] <= 0) {
                    h[np][n] = 0;
                }
            }
        }

        chisq_f = 0;
        for (m = 0; m < nbins; m++) {

            // fitted value after change in parameters
            y_bgnd[m] = (p[0][5]+h[0][5]) + (p[0][6]+h[0][6])*m + (p[0][7]+h[0][7])*m*m;
            y_hat[m] = y_bgnd[m];

            for(np=0; np<totPeaks; np++) {
                y_hat[m]  += (p[np][0]+h[np][0])*exp(-(x[m]-(p[np][1]+h[np][1]))*(x[m]-(p[np][1]+h[np][1]))/(2*(p[np][2]+h[np][2])));
                y_hat[m]  += iFac[np]*(p[np][0]+h[np][0])*exp(-(x[m]-(p[np][1]+h[np][1]-binSep[np]))*(x[m]-(p[np][1]+h[np][1]-binSep[np]))/(2*(p[np][2]+h[np][2])));

                // add exponential tailing
                if(AddTailExp) {
                    if(m<(p[np][1]+h[np][1])) {
                        y_hat[m] += (p[np][3]+h[np][3])*(p[np][0]+h[np][0])*exp(-(p[np][1]+h[np][1]-x[m])/(p[np][4]+h[np][4]));
                    }
                    if(m<(p[np][1]+h[np][1]-binSep[np])) {
                        y_hat[m]  += (p[np][3]+h[np][3])*iFac[np]*(p[np][0]+h[np][0])*exp(-(p[np][1]+h[np][1]-binSep[np]-x[m])/(p[np][4]+h[np][4]));
                    }
                }
            }

            // chisq after parameters have been updated
            chisq_f += w[m]*(y[m]-y_hat[m])*(y[m]-y_hat[m]);

        }
        chisq_f /= nbins;

        if (chisq_f < min_chisq_f) {
            min_chisq_f = chisq_f;
            // update parameters
            for (n = 0; n < npar; n++) {
                for (np = 0; np < totPeaks; np++) {
                    p[np][n] += h[np][n];
                }
            }
            for(m=0; m<nbins; m++) {
                y_hat_f[m] = y_hat[m];
                y_bgnd_f[m] = y_bgnd[m];
            }
            //ALOGI(" fitPeaksBgnd: Inside Loop: best_chisq=%d Parameters: %d %f %f %f %f", (int)chisq_f, (int)p[0][0], p[0][1], p[0][2]+lo_bin, p[0][3], p[0][4]);
        }

    }  // end looping over fit iterations

    // calculate peak counts and background under 2nd peak

    // calculate counts in peak and background counts using fit
    rawCounts = 0;
    fitCounts = 0;
    bgnd = 0;
    for (m = 0; m < nbins; m++) {
        bin = m + lo_bin;
        if (bin >= bin_lo and bin <= bin_hi) {
            rawCounts += y[m];
            fitCounts += y_hat_f[m];
            bgnd += y_bgnd_f[m];
        }
    }

    // plot fit for diagnostics
    if (plotFit) {
        ofp = fopen("plotFit.txt", "a+");

        //print fit parameters in header for each peak

        for (np = 0; np < totPeaks; np++) {
            fprintf(ofp, "\n %s(%s):", elementName[elementID[np]], lineName[lineID[np][0]]);
            fprintf(ofp, "\n Gain: %6.4f MinChisq: %f", k_gain, sqrt(min_chisq_f));
            fprintf(ofp, "\n rawCnts: %d fitCnts: %d Bgnd: %d", (int) rawCounts, (int) fitCounts,
                    (int) bgnd);
            fprintf(ofp, "\n Peak1 Params: %6.1f %6.1f %6.1f %6.4f %6.4f %6.1f %6.3f %6.4f",
                    p[np][0], p[np][1], p[np][2], p[np][3], p[np][4], p[0][5], p[0][6], p[0][7]);
            fprintf(ofp, "\n Peak2 params: %6.1f %6.1f %6.1f %6.4f %6.4f", iFac[np] * p[np][0],
                    p[np][1] - binSep[np], p[np][2], p[np][3], p[np][4]);
        }
        fprintf(ofp, "\n Bin keV Cnts Fit Bgnd");

        for (m = 0; m < nbins; m++) {
            // recalculate y_hat_f (need to do this again because the very last y_hat_f from above may not be with the final fit parameters!)
            y_bgnd[m] = p[0][5] + p[0][6] * m + p[0][7] * m * m;
            y_hat_f[m] = y_bgnd[m];
            for (np = 0; np < totPeaks; np++) {
                y_hat_f[m] +=
                        p[np][0] * exp(-(x[m] - p[np][1]) * (x[m] - p[np][1]) / (2 * p[np][2]));
                y_hat_f[m] += iFac[np] * p[np][0] * exp(-(x[m] - (p[np][1] - binSep[np])) *
                                                        (x[m] - (p[np][1] - binSep[np])) /
                                                        (2 * p[np][2]));

                // add exponential tailing
                if (AddTailExp) {
                    if (m < p[np][1]) {
                        y_hat_f[m] += p[np][3] * p[np][0] * exp(-(p[np][1] - x[m]) / p[np][4]);
                    }
                    if (m < (p[np][1] - binSep[np])) {
                        y_hat_f[m] += p[np][3] * iFac[np] * p[np][0] *
                                      exp(-(p[np][1] - binSep[np] - x[m]) / p[np][4]);
                    }
                }
            }
            fprintf(ofp, "\n %d %6.2f %6.1f %6.1f %6.1f", m + lo_bin,
                    (m + lo_bin) * k_gain + k_offset, y[m], y_hat_f[m],
                    y_bgnd[m]);

        }

        fclose(ofp);

    } // end diagnostics

    // assign fit values to spectrum arrays
    element[elementID2].kAlphaRaw_fit = rawCounts;
    element[elementID2].kAlphaCnts_fit = fitCounts - bgnd;
    element[elementID2].kAlphaBgnd_fit = bgnd;

    for (np = 0; np < totPeaks; np++) {

        // return bin centroid relative to entire spectrum
        //p[np][1] += lo_bin;

        // update FWHM of peaks after fitting (p[2] is sigma squared)
        for (i = 0; i < numPeaks; i++) FWHM_bins[np][i] = 2 * sqrt_2ln2 * sqrt(p[np][2]);

        ALOGI(" fitPeaksBgnd: #%d chisq=%d Params: %d %f %f %f %f %f %f %f", np + 1,
              (int) sqrt(min_chisq_f), (int) p[np][0], p[np][1], p[np][2] + lo_bin, p[np][3],
              p[np][4], p[np][5], p[np][6], p[np][7]);
    }
    //ALOGI(" fitPeaksBgnd: PbCnts=%d rawCounts=%d fitCounts=%d bgnd=%d", (int)(rawCounts-bgnd), (int)rawCounts, (int)fitCounts, (int)bgnd);

    // increment number of times fit has been performed
    numFit++;

//    for(m=0; m<nbins; m++) {
//        ALOGI(" %d %5.1f %5.1f %5.1f", m+1, y[m], y_hat_f[m], y_bgnd_f[m]);
//    }

    // return chisq of total fit
    return (sqrt(min_chisq_f));

}

double fexp(double x) {

    // fast approximation to an exponential function

    x = 1.0 + x / 1024;
    x *= x; x *= x; x *= x; x *= x;
    x *= x; x *= x; x *= x; x *= x;
    x *= x; x *= x;
    return x;
}

float pjr_rand(void) {

    // returns a random floating point number between 0 and 1

    float rval;
    static unsigned long seed=0;

    std::mt19937 mt_rand(seed++);

    rval = (float) mt_rand() / 4294967296;

    if (rval < 0) rval = -rval;

    return (rval);

}



float Spectrum::fitTwinPeaks(int elementID, int line_ID) {
    // Fits single (twin) peaks with single (dual) Gaussians using the PJR random phase space approach
    // If lineID=ka1, secondary peak is ka2 (and vice versa)
    // If lineID=kb1, secondary peak is kb2 (and vice versa)
    //
    // Fit parameters:
    // p0: Peak 1 amplitude
    // p1: Peak 1 Centroid (bin number)
    // p2: Peak sigma (in bins)
    // p3: Flat background

    int rndmMode = 1, lineID[2], numIterations;
    int i, j, id, n, m, npar, nbins, totCnts = 0;
    int lo_bin, hi_bin, loBin[2], hiBin[2], lo_lim, hi_lim;
    int numPeaks, bin, maxSum, maxBin = 0, peakShift = 0;
    float x0[2], y0[2], FWHM_bins[2], sum;
    float x[MaxFitPoints], y[MaxFitPoints], w[MaxFitPoints];
    float h[MaxFitParams], p[MaxFitParams];
    float y_hat[MaxFitPoints], y_hat_f[MaxFitPoints], y_bgnd[MaxFitPoints];
    float chisq, chisq_f = 0.0f, min_chisq_f;
    float initStepMag[MaxFitParams], maxStepMag[MaxFitParams];
    float ln2 = 0.69315f, sqrt_2ln2 = 1.1774f;
    float cent1, cent2, binSep, iFac;

    bool plotFit = false;
    bool printFit = false;
    FILE *ofp;

    lineID[0] = line_ID;
    numPeaks = 2;
    npar = 4;

    // if main peak is ka1/kb1 assign other peak to ka2/kb2 and vice versa
    // mostly dominant, higher energy peak is always first
    if (line_ID == ka1 || line_ID == ka2) {
        lineID[0] = ka1;
        lineID[1] = ka2;
    } else if (line_ID == kb1 || line_ID == kb3) {
        lineID[0] = kb1;
        lineID[1] = kb3;
    } else if (line_ID == kb2) {
        lineID[0] = kb2;
        lineID[1] = kb1;
    }

    for (i = 0; i < numPeaks; i++) {
        id = lineID[i];
        loBin[i] = element[elementID].line[id].lo_bin;
        hiBin[i] = element[elementID].line[id].hi_bin;
        //printf("\n fitTwinPeaks: Peak %d: loBin=%d hiBin=%d", i+1, loBin[i], hiBin[i]);
    }

    // if doing energy calibration with tungsten k-alpha peak, first check if gain has drifted dramatically from assumed value
    // (tungsten k-alpha lines are most dominant lines in spectrum, so looks for the maximum counts/bin in ROI)
    if ((elementID == W || elementID == Ba) && lineID[0] == ka1) {
        // expand search area to check for gain drift
        id = lineID[0];
        maxSum = 0;
        lo_bin = int((1.0 - MaxGainDrift / 100) * element[elementID].line[id].centroidBin);
        hi_bin = int((1.0 + MaxGainDrift / 100) * element[elementID].line[id].centroidBin);

        ALOGI("fitTwinPeaks: lo_bin= %d, hi_bin= %d", lo_bin, hi_bin);

        for (i = lo_bin; i <= hi_bin; i++) {
            sum = 0;
            for (j = -2; j <= 2; j++) {
                bin = i + j;
                if (bin < 0) bin = 0;
                if (bin >= MaxNumBins) bin = MaxNumBins - 1;
                sum += cumCounts[i + j];
            }
            if (sum > maxSum) {
                maxSum = (int) sum;
                maxBin = i;
            }
        }

#ifdef PF_DEBUG
        if( maxBin > MaxNumBins || maxBin <= 0)
            maxBin = (int)element[elementID].line[id].centroidBin;
#endif

        peakShift = (int) (maxBin - element[elementID].line[id].centroidBin);
        ALOGI(" fitTwinPeaks: Peak expected at %d but max counts found at bin %d (peakShift=%d)",
              (int) element[elementID].line[id].centroidBin,
              maxBin, peakShift);
    }

    // expected bin separation between peaks
    cent1 = element[elementID].line[lineID[0]].centroidBin;
    cent2 = element[elementID].line[lineID[1]].centroidBin;
    binSep = (cent1 - cent2) * (cent1 - peakShift) / cent1;

    // relative intensity of peaks
    iFac = element[elementID].line[lineID[1]].intensity /
           element[elementID].line[lineID[0]].intensity;
    //ALOGI(" fitTwinPeaks: binSep=%f iFac=%f", binSep, iFac);

    // initial best guess at fit parameters (includes peakShift correction due to gain drift)
    for (i = 0; i < numPeaks; i++) {
        id = lineID[i];
        x0[i] = element[elementID].line[id].centroidBin + peakShift;
        y0[i] = (float) cumCounts[(int) x0[i]];
        FWHM_bins[i] = element[elementID].line[id].FWHM_bins;
        loBin[i] = element[elementID].line[id].lo_bin + peakShift;
        hiBin[i] = element[elementID].line[id].hi_bin + peakShift;
    }

    // find lo_bin and hi_bin for window to fit both peaks
    if (loBin[0] < loBin[1]) {
        lo_bin = loBin[0];
    } else {
        lo_bin = loBin[1];
    }
    if (hiBin[0] > hiBin[1]) {
        hi_bin = hiBin[0];
    } else {
        hi_bin = hiBin[1];
    }

    nbins = hi_bin - lo_bin + 1;
    if (nbins >= MaxFitPoints) {
        ALOGI(" Too many points in fit to be stored in arrays. Increase the size of defined parameter MaxFitPoints");
        return nbins;
    }

    if (lo_bin + nbins > numBins) {
        ALOGI(" curveFitTwin: Overloading counts array");
        return (lo_bin + nbins);
    }

    // fit parameter vector for twin Gaussians with fixed separation and equal widths
    p[0] = y0[0];
    p[1] = x0[0] - lo_bin;
    p[2] = FWHM_bins[0] * FWHM_bins[0] / (8 * ln2);

    // initial fit parameter for variable background (use average counts on each side of twin peaks)
    lo_lim = lo_bin - (int) (1.5 * FWHM_bins[0]);
    if (lo_lim < 0) lo_lim = 0;
    hi_lim = hi_bin + (int) (1.5 * FWHM_bins[1]);
    if (hi_lim >= numBins) hi_lim = numBins - 1;

    // only fit one background under both peaks
    p[3] = float(cumCounts[lo_lim] + cumCounts[hi_lim]) / 2;
    //ALOGI(" fitTwinPeaks: initial fit bgnd = %f", p[3]);

    // max step sizes for each parameter for main Gaussian
    initStepMag[0] = p[0] * 0.25f;    // max change in amplitude
    initStepMag[1] = 3.0f;            // max change in centroid bin
    initStepMag[2] = p[2] * 0.25f;    // max change in sigma2
    initStepMag[3] = p[3] * 0.25f;    // max change in background

    for (m = 0; m < nbins; m++) {
        totCnts += cumCounts[lo_bin + m];
    }

    chisq = 0;
    for (m = 0; m < nbins; m++) {
        x[m] = (float) m;                                       // x data point
        y[m] = (float) cumCounts[lo_bin + m];          // y data point
        y_bgnd[m] = p[3];                                       // fit a variable background along with other fit parameters
        w[m] = y[m] / totCnts;                                  // weighting for each point
        y_hat[m] = y_bgnd[m];
        y_hat[m] += p[0] * exp(-(x[m] - p[1]) * (x[m] - p[1]) / (2 * p[2]));                                // initial fitted value for peak 1
        y_hat[m] += iFac * p[0] * exp(-(x[m] - (p[1] - binSep)) * (x[m] - (p[1] - binSep)) / (2 * p[2]));   // initial fitted value for peak 2
        chisq += w[m] * (y[m] - y_hat[m]) * (y[m] - y_hat[m]);                                              // chisq for initial parameter values
    }
    //ALOGI(" fitTwinPeaks: totCnts=%d chisq=%f Initial Parameters: %f %f %f %f", totCnts, chisq, p[0], p[1]+lo_bin, p[2], p[3]);

    // initialize min chisq to start chisq
    min_chisq_f = chisq;

    // fitMode=1 is calibration mode
    if (fitMode == 1) {
        numIterations = NumPeakIterFacCalib * npar;
    } else {
        numIterations = NumPeakIterFac * npar;
    }

    for (j = 0; j < numIterations; j++) {

        for (n = 0; n < npar; n++) maxStepMag[n] = (1 - float(j) / numIterations) * initStepMag[n];

        // randomly switch between modes 1 (change all parameters) and 2 (change only one parameter)
        if ((float) rand() / RAND_MAX < 0.25) {
            rndmMode = 1;
        } else {
            rndmMode = 2;
        }

        // randomly determine step size
        if (rndmMode == 1) {
            for (n = 0; n < npar; n++) {
                h[n] = 2 * ((float) rand() / RAND_MAX - 0.5f) * maxStepMag[n];
            }
        } else {
            for (n = 0; n < npar; n++) h[n] = 0;
            n = int((float) rand() / RAND_MAX * npar);
            if (n >= npar) n--;
            h[n] = 2 * ((float) rand() / RAND_MAX - 0.5f) * maxStepMag[n];
            //ALOGI(" Change in parameter %d = %f", n, h[i][n]);
        }

        // check that centroid is within limits (otherwise don't change)
        if ((p[1] + h[1] + lo_bin) > hi_bin || (p[1] + h[1]) < 0) {
            h[1] = 0;
            //ALOG("*** Bin Centroid Out of Range (Bin=%f)", p[1] + h[1]);
        }

        // ensure peak parameters are positive (otherwise don't change)
        for (n = 0; n < npar; n++) {
            if (p[n] + h[n] < 0) {
                h[n] = 0;
            }
        }

        chisq_f = 0;
        for (m = 0; m < nbins; m++) {

            // fitted value after change in parameters
            y_bgnd[m] = p[3] + h[3];
            y_hat[m] = y_bgnd[m];
            y_hat[m] += (p[0] + h[0]) * exp(-(x[m] - (p[1] + h[1])) * (x[m] - (p[1] + h[1])) / (2 * (p[2] + h[2])));
            y_hat[m] += iFac * (p[0] + h[0]) * exp(-(x[m] - (p[1] + h[1] - binSep)) * (x[m] - (p[1] + h[1] - binSep)) / (2 * (p[2] + h[2])));

            // chisq after parameters have been updated
            chisq_f += w[m] * (y[m] - y_hat[m]) * (y[m] - y_hat[m]);

        }

        if (chisq_f < min_chisq_f) {
            min_chisq_f = chisq_f;
            // update parameters
            for (n = 0; n < npar; n++) p[n] += h[n];
            //ALOGI(" fitTwinPeaks: best_chisq=%f Parameters: %f %f %f %f %f %f", chisq_f, p[0], p[1], p[2]+lo_bin, p[3], p[4]);

            // store best fit
            for(m=0; m<nbins; m++) {
                y_hat_f[m] = y_hat[m];
            }

        }

    }  // end looping over fit iterations

    // return bin centroid relative to entire spectrum
    p[1] += lo_bin;

    ALOGI(" fitTwinPeaks: best_chisq=%f Parameters: %f %f %f %f %f", chisq_f, p[0], p[1], p[2] + lo_bin, p[3], p[4]);

    // update FWHM of peaks after fitting (p[2] is sigma squared)
    for (i = 0; i < numPeaks; i++) FWHM_bins[i] = 2 * sqrt_2ln2 * sqrt(p[2]);

    // store fit information for both fitted peaks
    for (i = 0; i < numPeaks; i++) {
        id = lineID[i];
        //ALOGI(" fitTwinPeaks: Peak=%d elemID=%d lineID=%d %s %s lo_bin=%d hi_bin=%d", i+1, elementID, id, elementName[elementID], lineName[lineID[i]], loBin[i], hiBin[i]);
        element[elementID].line[id].fitStatus = true;
        element[elementID].line[id].bgndMode = 2;
        element[elementID].line[id].lo_bin = loBin[i];
        element[elementID].line[id].hi_bin = hiBin[i];
        element[elementID].line[id].chisq = sqrt(min_chisq_f);
        element[elementID].line[id].FWHM_bins = FWHM_bins[i];
        element[elementID].line[id].FWHM_keV = FWHM_bins[i] * k_gain;
        for (n = 0; n < MaxFitParams; n++) element[elementID].line[id].fitPars[n] = p[n];
    }

    // chisq and FWHM of energy calibration peak fit
    ecalChisq = sqrt(min_chisq_f);

    // plot fit for diagnostics
    if (printFit) {
        for (m = 0; m < nbins; m++) {
            ALOG(" %d %6.2f %6.1f %6.1f", m + lo_bin, (m + lo_bin) * k_gain + k_offset, y[m], y_hat_f[m]);
        }
    }

    // return centroid bin of main Gaussian (with id=lineID)
    return (p[1]);

}

void Spectrum::printFits() {

    int eid, i, j, id;
    float lo_lim, hi_lim;
    float val[4], FWHM_bins, chisq;

    FILE *ofp;

    // open output file
    ofp = fopen("results.txt", "a+");

    ALOGI(" *********  Spectrum ***********");

    ALOGI(" Best Guess for Substrate: %s (subID=%d)", (char *) subName[subType],
          subType);

    ALOGI(" Calib Peak Details:");
    eid = PCalibElementID;
    for (j = 0; j < NumCalibPeaks; j++) {
        id = calibPeakID[j];
        ALOGI(" j=%d eid=%d id=%d", j, eid, id);
        lo_lim = element[eid].line[id].lo_lim;
        hi_lim = element[eid].line[id].hi_lim;
        chisq = element[eid].line[id].chisq;
        FWHM_bins = element[eid].line[id].FWHM_bins;
        val[0] = element[eid].line[id].fitCnts;
        val[1] = element[eid].line[id].rawCnts;
        val[2] = element[eid].line[id].bgndCnts;
        val[3] = element[eid].line[id].lineCnts;
        ALOGI(" %8s %3s: Bins %d-%d Chisq=%6.2f FWHM=%5.2f fitCnts=%5.0frawCnts=%5.0f  bgndCnts=%5.0f  lineCnts=%5.0f",
              elementName[eid], lineName[id], int(lo_lim), int(hi_lim), chisq, FWHM_bins, val[0],
              val[1], val[2], val[3]);
    }

    ALOGI(" Analysis Peak Details:");
    for (i = 0; i < MaxNumAnalyzeElements; i++) {
        eid = analyzeElementID[i];
        for (j = 0; j < numAnalyzePeaks[i]; j++) {
            if(strcmp(srcType,"Co57")==0 or strcmp(srcType,"co57")==0) {
                id = analyzePeakCo57[i][j];
            } else {
                id = analyzePeakCd109[i][j];
            }
            lo_lim = element[eid].line[id].lo_lim;
            hi_lim = element[eid].line[id].hi_lim;
            chisq = element[eid].line[id].chisq;
            FWHM_bins = element[eid].line[id].FWHM_bins;
            val[0] = element[eid].line[id].fitCnts;
            val[1] = element[eid].line[id].rawCnts;
            val[2] = element[eid].line[id].bgndCnts;
            val[3] = element[eid].line[id].lineCnts;
            ALOGI(" %8s %3s: Bins %d-%d Chisq=%6.2f FWHM=%5.2f fitCnts=%5.0f rawCnts=%5.0f  bgndCnts=%5.0f  lineCnts=%5.0f",
                  elementName[eid], lineName[id], int(lo_lim), int(hi_lim), chisq, FWHM_bins,
                  val[0], val[1], val[2], val[3]);

        }

//  if (spectrum.element[eid].kLineCnts>0) {
        if ((element[eid].kLineCnts > 0) || (eid == 0)) {
            ALOGI(" Summary for %s:", elementName[eid]);
            ALOGI("   kAlphaCnts=%5.0f kAlphRawCnts=%5.0f kAlphaRaw=%5.0f kAlphaBgnd=%5.0f LBR=%5.3f CL=%5.2f ABR=%5.2f A21R=%5.2f",
                  element[eid].kAlphaCnts,
                  element[eid].kAlphaRaw - element[eid].kAlphaBgnd,
                  element[eid].kAlphaRaw, element[eid].kAlphaBgnd,
                  element[eid].kAlphaBgndRatio, element[eid].kConfLev,
                  element[eid].alphaBetaRatio, element[eid].alpha21Ratio);
            ALOGI("   Conc(raw): %5.2fmg/cm2  Conc(fit) = %5.2fmg/cm2", element[eid].mgcm2,
                  element[eid].mgcm2);


            fprintf(ofp, "\n%5.0f %5.0f %5.0f %5.3f %5.2f %5.2f %5.2f",
                    element[eid].kAlphaCnts, element[eid].kAlphaRaw,
                    element[eid].kAlphaBgnd,
                    element[eid].kAlphaBgndRatio, element[eid].kConfLev,
                    element[eid].alphaBetaRatio, element[eid].alpha21Ratio);
        }

    }
    fclose(ofp);
}

void Spectrum::reCalibSpec(float gain) {

    // This recalculates spectrum variables that rely on the gain.
    // This should be called each time an energy calibration is done.
    // Note: This function uses the input gain value, not the value stored in spectrum.

    int i, j;

    // load line information
    for (i = 0; i < NumElements; i++) {
        for (j = 0; j < NumLinesPerElement; j++) {
            element[i].line[j].calibrate(k_offset, gain);
            element[i].line[j].centroidCnts = counts[int(element[i].line[j].centroidBin + 0.5)];
            //spectrum.element[i].line[j].approxFWHM(DefaultEnergyRes, gain);
            element[i].line[j].approxFWHM(ecalFWHM, gain);
        }
    }

    // find peak of Compton background
    //spectrum.bgndPeakCentBin = (float) findComptonPeak(spectrum);

    // set background status of bins in spectrum
    //setBinStatus(spectrum);

}

bool Spectrum::energyCalSpec(int nElem) {
    // fit k-alpha peak of selected elements
    //
    // Inputs:
    //    nElem: Number of elements to use in energy calibration
    //

    bool rstatus;
    int i, j, id, cnt, n;
    int elementID = 0;
    float keV[10] = {0};
    float sum, e_keV, FWHM_keV;
    float gain, offset;
    float kGainDrift;
    float x_av, y_av, sum_xx, sum_xy;
    float centBinFit[10] = {0};
    float avEnergyRes;

    FILE *ofp;

    initializeElements();

    // initialize ecal status to false
    ecalStatus = false;

    // fit all calibration peaks for all elements
    cnt = 0;
    for (n = 0; n < nElem; n++) {

        // determine calibration element ID
        if (n == 0) {
            elementID = element_id(PCalibElementID);
        } else {
            elementID = element_id(SCalibElementID);
        }

        // fit all peaks for element
        for (i = 0; i < NumCalibPeaks; i++) {

            id = calibPeakID[i];

            keV[cnt] = element[elementID].line[id].keV;

            // fit twin peaks
            centBinFit[cnt] = fitTwinPeaks(elementID, id);

            ALOG("energyCalSpec: Calib Peak %s %s (keV=%f centBin=%7.3f FWHM=%f bins):",
                 elementName[elementID], lineName[id], keV[cnt], centBinFit[cnt],
                 element[elementID].line[id].FWHM_bins);

            cnt++;
        }
    }

    // perform linear regression on all fitted calibration peaks to determine offset and gain
    if (cnt > 1) {
        x_av = 0;
        y_av = 0;
        for (i = 0; i < cnt; i++) {
            x_av += centBinFit[i];
            y_av += keV[i];
        }
        x_av /= cnt;
        y_av /= cnt;

        sum_xx = 0;
        sum_xy = 0;
        for (i = 0; i < cnt; i++) {
            sum_xy += (centBinFit[i] - x_av) * (keV[i] - y_av);
            sum_xx += (centBinFit[i] - x_av) * (centBinFit[i] - x_av);
        }

        // set gain and offset
        gain = sum_xy / sum_xx;
        offset = y_av - gain * x_av;

    } else {
        offset = k_offset;
        gain = (keV[0] - offset) / centBinFit[0];
    }

    // determine gain drift from default value
    if (defKGain > 0) {
        kGainDrift = fabs(gain - defKGain) / (defKGain) * 100;
    } else {
        kGainDrift = 0;
    }

    // only update gain if within drift limits from default value and set good ecal status

    if (kGainDrift < GainDriftNull) {
        rstatus = true;

        k_offset = offset;
        k_gain = gain;
        ecalStatus = true;
        //ALOG("Energy calibration is within limits (gain=%f drift=%f%%)", gain, kGainDrift);
        // otherwise, leave gain and  offset from previous calibration
    } else {
        rstatus = false;

        ALOG(" ********  Energy calibration is outside limits (gain=%f drift=%f%%) **********",
             gain, kGainDrift);
        createMessage(WarnGainDriftCode, 2, kGainDrift, 0, 0, 0);
        //if (spectrum.fitMode == 1) {
        //    ofp = fopen(CalibBadEcal, "a+");
        //    fprintf(ofp, "\n%d %f", spectrum.calibSamp, gain);
        //    fclose(ofp);
        //}
    }

    // calculate average percent energy resolution at 100keV for all calibration peaks
    sum = 0;
    for (i = 0; i < NumCalibPeaks; i++) {
        id = calibPeakID[i];
        FWHM_keV = element[elementID].line[id].FWHM_bins * k_gain;
        e_keV = element[elementID].line[id].keV;

        //ALOG("energyCalSpec: Calib. Peak %d: e_keV=%f FWHM_keV=%f FWHM_bins=%f", i+1, e_keV, FWHM_keV, FWHM_keV/gain);
        if (e_keV > 0) sum += sqrt(e_keV / 100.0f) * (FWHM_keV / e_keV);

    }

    // want average percent resolution at 100keV
    avEnergyRes = sum / NumCalibPeaks * 100;

    ALOG("energyCalSpec: #Peaks=%d Gain=%6.4f (Def=%6.4f) Offset=%6.4f (Def=%6.4f) AvEnergyRes=%5.3f%%",
         nElem * NumCalibPeaks, gain, defKGain, offset, defKOffset, avEnergyRes);

    if (avEnergyRes < DetResLoError) {
        rstatus = false;
        createMessage(ResLoErrorCode, 3, avEnergyRes, 0, 0, 0);
        avEnergyRes = DefaultEnergyRes;
    } else if (avEnergyRes > DetResHiError) {
        rstatus = false;
        createMessage(ResHiErrorCode, 3, avEnergyRes, 0, 0, 0);
        avEnergyRes = DefaultEnergyRes;
    }

    // store percent energy resolution at 100keV from fit
    ecalFWHM = avEnergyRes;
    ecalStatus = rstatus;

    // update calibration of all the K lines (added 11/29/18)
    reCalibSpec(gain);

    return (rstatus);

}

void Spectrum::plotFits() {
    // Plot fits to spectra for all peaks for spectrum

    int j, n, m, id, eid, loBin, hiBin, bgndMode;
    float p[NumFitParams];
    float x[MaxNumBins], y[MaxNumBins], keV[MaxNumBins];
    float fitCnts[MaxNumBins], peakCnts[MaxNumBins], bgndCnts[MaxNumBins];
    bool fitStatus;

    FILE *ofp;

    for (m = 0; m < numBins; m++) {
        x[m] = (float) m;
        y[m] = (float) counts[m];
        bgndCnts[m] = bgnd[m];
        peakCnts[m] = 0;
        keV[m] = k_offset + k_gain * m;
    }

    // replace raw counts with fit where peaks have been fitted
    for (eid = 0; eid < NumElements; eid++) {
        for (j = 0; j < maxLines; j++) {
            fitStatus = false;
            id = j;
            fitStatus = element[eid].line[id].fitStatus;
            bgndMode = element[eid].line[id].bgndMode;
            loBin = element[eid].line[id].lo_bin;
            hiBin = element[eid].line[id].hi_bin;
            for (n = 0; n < NumFitParams; n++) p[n] = element[eid].line[id].fitPars[n];
            //ALOGI(" plotFits: %s %s fitStatus=%d bgndMode=%d lo_bin=%d hi_bin=%d", elementName[eid], lineName[lineID], fitStatus, bgndMode, loBin, hiBin);
            if (fitStatus) {
                for (m = loBin; m <= hiBin; m++) {
                    peakCnts[m] += p[0] * exp(-(x[m] - p[1]) * (x[m] - p[1]) / (2 * p[2]));
                    // set background to zero under bgndMode=0 peaks
                    if (bgndMode == 0) bgndCnts[m] = 0;
                    // set background to p[3] under bgndMode=2 peaks
                    if (bgndMode == 2) bgndCnts[m] = p[3];
                }
            }
        }
    }

    // add peakCnts and background
    for (m = 0; m < numBins; m++) {
        fitCnts[m] = peakCnts[m] + bgndCnts[m];
    }

    ofp = fopen("plot.txt", "wb");
    if (ofp == NULL) {
        ALOGI(" Cannot open output plot file");
        return;
    }

    // calculate bin limits for plot
    loBin = int((LoPlotKeV - k_offset) / k_gain);
    hiBin = int((HiPlotKeV - k_offset) / k_gain);
    if (loBin < 0) loBin = 0;
    if (hiBin >= numBins) hiBin = numBins - 1;
    //ALOGI(" plotFits: loBin=%d hiBin=%d", loBin, hiBin);

    // write to output file
    for (m = loBin; m <= hiBin; m++) {
        fprintf(ofp, "\n%d %6.2f %6.1f %6.1f %6.1f", m, keV[m], y[m], fitCnts[m], bgndCnts[m]);
    }
    fclose(ofp);
}



int Spectrum::createMessage(int messCode, int mode, float val1, float val2, float val3, float val4) {
    //
    // Creates error or warning messages
    //
    // Mode=1: Create Status message
    // Mode=2: Create Warning message
    // Mode=3: Create Error message
    // Mode=4: Create Error message and EXIT program

    // print message start
    if (mode == 1) {
        ALOGI("STATUS");
    } else if (mode == 2) {
        ALOGI("WARNING");
    } else if (mode >= 3) {
        ALOGI("ERROR");
    }

    // print message code
    ALOGI("(%3d): ", messCode);

    switch (messCode) {

        case ResHiErrorCode:
            ALOGI("The energy resolution is too high for this substrate (%4.1f%%). Using default setting.",
                  val1);
            break;
        case ResLoErrorCode:
            ALOGI("The energy resolution is too low for this substrate (%4.1f%%). Using default setting.",
                  val1);
            break;
        case ResHiWarnCode:
            ALOGI("The energy resolution is higher than expected for this substrate (%4.1f%%).",
                  val1);
            break;
        case TempMisMatchCode:
            ALOGI("Different number of bins in template and reference template.");
            break;
        case NoSvcCalFileCode:
            ALOGI("Cannot open service calibration file - opening factory calibration file.");
            break;
        case NoCalFilesCode:
            ALOGI("Cannot open either service or calibration file.");
            break;
        case CalFacsDiffCode:
            ALOGI("Calibration samples %d and %d have substantially different calibration factors (Diff = %5.1f%%).",
                  int(val1), int(val2), val3);
            break;
        case CopySDValueCode:
            ALOGI("Template %d: Copying mgcm2SD value for concID=%d from template %d.", int(val1),
                  int(val2), (int) val3);
            break;
        case NoBlankMatchCode:
            ALOGI("No non-blank calibration samples were matched to background template %d (copying from template %d).",
                  int(val1), int(val2));
            break;
        case WarnGainDriftCode:
            ALOGI("Gain has drifted by %4.1f%% from default value.", val1);
            break;
        case TempChisqWarnCode:
            ALOGI("Poor match to available background templates (chisq=%4.3f).", val1);
            break;
        default:
            ALOGI("Invalid error code.");
            break;
    }

    return (messCode);

}