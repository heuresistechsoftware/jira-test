#ifndef HAC_HPP
#define HAC_HPP

#include "DppDefs.hpp"
#include "DppSpectrum.hpp"
#include "DppCalData.hpp"
#include "Spectrum.hpp"
#include "Calib.hpp"
#include "PeakFitResults.hpp"

bool hacInit(int mode, int analyticMode, int type);

int acquiredSpectrumCalc(const DppSpectrum dppSpectrum);
int acquiredSpectrumCalc(const DppSpectrum dppSpectrum, int analyticMode, meas_mode measMode);
int accumSpecMem(int nSpectra, Spectrum &spectrum, const DppSpectrum &dppSpec);
int accumSpecMem(int nSpectra, Spectrum &spectrum); // HBI-Pb version
void loadSpectrum(uint32_t *binCnts, int numBins, float liveTime, const DppSpectrum &dppSpec,
                  Spectrum &spectrum);

int readSysConfig(Spectrum &spectrum, int analyticMode);

int hac(Spectrum &spectrum);

int hac(Spectrum &spectrum,
        Calib &calib,
        int analyticMode,
        meas_mode measMode);

#endif // HAC_HPP
