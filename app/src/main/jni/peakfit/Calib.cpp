// Calib.cpp
//
// Compile and link code for Linux with:
// g++ -std=c++11 -lm -lstdc++ -o PeakFit PeakFit.cpp
// (Ensure that /usr/include/c++/4.8.1 is in the directory path)
//

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <fstream>
#include <sstream>
#include <sys/stat.h>

#include "Calib.hpp"
#include "PeakFit.hpp"
#include "PeakFitPrototypes.hpp"
#include "crypto.h"


//---------- Global Variables --------------------------------------------------

float avMgcm2SD[NumElements][MaxNumTemp][MaxConcLevels];
float calibRatio[NumElements][MaxNumTemp][MaxConcLevels];
//float avCalibRatio[NumElements][MaxNumTemp];
float avCalibRaw[NumElements][MaxNumTemp];

char subName[NumSubstrates][NumAltNames][25] = {
        "Thick Steel", "Thick Fe", "Steel", "steel", "na",           //  1
        "Thin Steel", "Thin Fe", "thin steel", "Thin steel", "na",   //  2
        "Thick Al", "thick al", "na", "na", "na",                    //  3
        "Thin Al", "thin al", "na", "na", "na",                      //  4
        "Thick Pop", "thick wd", "Thick Pop", "thick pop", "na",     //  5
        "Thin Pop", "thin wd", "Thin Pop", "thin pop", "na",         //  6
        "Thick SR", "Thick sheetrock", "thick sr", "na", "na",       //  7
        "Thin SR", "Thin sheetrock", "thin sr", "na", "na",          //  8
        "Brick", "brick", "Br", "br", "na",                          //  9
        "Plaster", "plaster", "pl", "na", "na",                      // 10
        "French Plaster", "french plaster", "na", "na", "na",        // 11
        "Air", "air", "na", "na", "na",                              // 12
        "Blank Thin Pop", "thin wd", "Thin Pop", "thin pop", "na"    // 13
};

//------------------------------------------------------------------------------

int readCalibParams(Calib &calib, int mode, int analyticMode) {

    // Iputs:
    //    mode=1 or 3: Reads calibration parameter text file
    //    mode=2: Reads configuration parameter text file
    //
    // Return value: number of samples read
    //
    // File format:
    //     1) Device Type
    //     2) Calibration mode
    //     3) Default livetime per sample
    //     4) Total number of calibration samples
    //     5) Number of calibration elements
    //     6) Number of calibration substrates
    //     7) Number of calibration concentration levels
    //        0.0
    //        0.7
    //        1.5 (sequential list of all levels)
    //        3.6
    //     8) Calibration sample #1 (Sample#, SampleID, Element, Substrate, ConcID, NumMeas)
    //     9) Calibration sample #2 (Sample#, SampleID, Element, Substrate, ConcID, NumMeas)
    //

    int i, n, concID, nMeas, subType, status;
    int day, month, year;
    float srcHalfLife;
    float ln2 = 0.6931472;
    char cdum[25], elem[25], sampID[25];
    char *pch, str1[25], str2[25];
    //time_t currentTime;
    FILE *ifp = NULL;
    struct stat sb;

    calib.nSamples = 0;
    calib.nNonRTA = RTATemplateNumber - 1;

    int calcElapsedDays(int, int, int, int, int, int);
    int calcElapsedDays2(int, int, int);
    std::string fileName;
    std::string modeDir;

    if (analyticMode == 3)
        modeDir = TSSoilsDir;
    else if (analyticMode == 2)
        modeDir = DustWipeDir;
    else if (analyticMode == 1)
        modeDir = ISSoilsDir;
    else
        modeDir = LeadPaintDir;

    fileName = CalibBaseDir + modeDir + CalibParamFile;
    ALOG("CalibParam file name %s", fileName.c_str());

    if ((stat(fileName.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode))) {
        fileName = CalibBaseDir;
        fileName.append(SoilsDir);
        fileName.append(CalibParamFile);
    }

    ALOG("Final CalibParam file name %s mode %d", fileName.c_str(), mode);

    // open calibration parameter file
    if (mode == 1 || mode == 3) {
        ifp = fopen(fileName.c_str(), "r");
    }

    // default source is Co57
    strcpy(calib.srcType, "Co57");

    ALOG("CalibParam file pointer %d", (long) ifp);
    // fopen returns 0 on error and file pointer on success.
    if (ifp != 0) {
        while (!feof(ifp)) {

            fscanf(ifp, "%s %s", str1, str2);
            ALOGI(" %s: %s", str1, str2);

            if (strcmp(str1, "DeviceType") == 0) calib.deviceType = atoi(str2);
            if (strcmp(str1, "CalibMode") == 0) calib.calibMode = atoi(str2);
            if (strcmp(str1, "CalibSpecDate") == 0) strcpy(calib.specDate, str2);
            if (strcmp(str1, "SourceType") == 0) strcpy(calib.srcType, str2);
            if (strcmp(str1, "CalibAssayDate") == 0) strcpy(calib.srcAssayDate, str2);
            if (strcmp(str1, "CalibAssayMCi") == 0) calib.srcAssayMCi = (float) atof(str2);
            if (strcmp(str1, "NumCalibSamples") == 0) calib.nSamples = atoi(str2);
            if (strcmp(str1, "TotNumElements") == 0) calib.nElements = atoi(str2);
            if (strcmp(str1, "TotNumSubstrates") == 0) calib.nSubstrates = atoi(str2);
            if (strcmp(str1, "TotNumNonRTA") == 0) calib.nNonRTA = atoi(str2);
            if (strcmp(str1, "TotNumInsitu") == 0) calib.nInsitu = atoi(str2);
            if (strcmp(str1, "TotNumConcLevels") == 0) {
                calib.nConcLevels = atoi(str2);
                // read concentration levels
                for (i = 0; i < calib.nConcLevels; i++) {
                    fscanf(ifp, "%s %f", cdum, &calib.concLevels[i]);
                    ALOGI(" concLev=%f", calib.concLevels[i]);
                }
                break;
            }

        }

        // get half life
        srcHalfLife = Co57HalfLife;
        if (strcmp("Co57", calib.srcType) == 0 or strcmp("co57", calib.srcType) == 0) srcHalfLife = Co57HalfLife;
        if (strcmp("Cd109", calib.srcType) == 0 or strcmp("cd109", calib.srcType) == 0) srcHalfLife = Cd109HalfLife;

        // find day, month, year for assay date
        pch = strtok(calib.srcAssayDate, " /");
        if (pch != NULL) {
            month = atoi(pch);
            pch = strtok(NULL, " /");
            day = atoi(pch);
            pch = strtok(NULL, " /");
            year = atoi(pch);
            calib.srcAssayDay = day;
            calib.srcAssayMonth = month;
            calib.srcAssayYear = year;
            ALOGI(" Calib Assay date: %2d/%2d/%4d", month, day, year);
        } else {
            ALOG("No Assay date found for calibration");
        }

        // find day, month, year for date calibration spectra were acquired
        pch = strtok(calib.specDate, " /");
        if (pch != NULL) {
            month = atoi(pch);
            pch = strtok(NULL, " /");
            day = atoi(pch);
            pch = strtok(NULL, " /");
            year = atoi(pch);
            calib.specDay = day;
            calib.specMonth = month;
            calib.specYear = year;
            ALOGI(" Calib Spectra date: %2d/%2d/%4d", month, day, year);
        } else {
            ALOG("No date found for calibration spectra");
        }

        // calculate *calibration* source strength using assay information
        calib.srcAgeDays = calcElapsedDays(calib.specMonth, calib.specDay, calib.specYear,
                                           calib.srcAssayMonth, calib.srcAssayDay,
                                           calib.srcAssayYear);
        calib.srcMCi = calib.srcAssayMCi * exp(-calib.srcAgeDays * ln2 / srcHalfLife);
        ALOGI(" Calib Source Type: %s", calib.srcType);
        ALOGI(" Calib Source Age (days): %5.1f", calib.srcAgeDays);
        ALOGI(" Calib Assay strength: %5.3f", calib.srcAssayMCi);
        ALOGI(" Current Source strength: %5.3f", calib.srcMCi);

        // read and skip header line
        fscanf(ifp, "%s %s %s %s %s %s", cdum, cdum, cdum, cdum, cdum, cdum);

        if (calib.nSamples > MaxCalibSamples) {
            ALOGW("\nToo many calibration samples in %s - increase array sizes", CalibParamFile);
            calib.nSamples = MaxCalibSamples;
        }

        // store info for each calibration sample
        calib.totMeasure = 0;
        for (n = 0; n < calib.nSamples; n++) {

            status = fscanf(ifp, "%d %s %s %d %d %d", &i, sampID, elem, &subType, &concID, &nMeas);
            if (status == 6) {
                strcpy(calib.name[n], sampID);
                calib.subType[n] = subType - 1;
                calib.concID[n] = concID;
                calib.conc[n] = calib.concLevels[concID];
                calib.nMeasure[n] = nMeas;
                calib.totMeasure += nMeas;

                // find element ID
                for (i = 0; i < NumElements; i++) {
                    if (strcmp(elem, elementSymb[i]) == 0) {
                        calib.elemID[n] = i;
                    }
                }
            } else {
                ALOGE("\nCorrupt %s file (expecting 6 entries) got %d ", CalibParamFile, status);
                break;
            }

            // break if end of file reached unexpectedly
            if (feof(ifp)) break;

        }

        fclose(ifp);
    }


    ALOGW("readCalibParam: %d samples read", calib.nSamples);

    return (calib.nSamples);
}

int readSysConfig(Spectrum &spectrum, int analyticMode) {

    // Reads the source assay date, assay source strength, gain, and offset from the system config file
    // open calibration parameter file

    int rstatus;
    int day, month, year;
    float ln2 = 0.6931472;
    //float gain, offset;
    float srcAssayMCi = 0.0f;
    char *pch, str1[25], str2[25];
    time_t currentTime;
    bool status = false;
    char srcAssayDate[25];
    char serialNumber[25];
    FILE *ifp;

    int calcElapsedDays2(int, int, int);
    std::string fileName;

    fileName = CalibBaseDir;
    fileName.append(SysConfigSvc);
    ALOG("ReadSysConfig file name %s", fileName.c_str());

    // default values if not set in config file
    spectrum.tempFitAdjust = 1.0f;
    spectrum.rtgFarFac = RTGFarFac;
    spectrum.rtgLoRate = RTGLoRate;
    spectrum.rtgHiRate = RTGHiRate;
    spectrum.rtgSurfMode = false;
    spectrum.maxCntRateSF = MaxCntRateStyroFm;
    spectrum.crudeScaleFac = MaxCrudeScaleFac;
    spectrum.soilCalOffset = 0.0f;

    spectrum.dustPullUpFac = DWipePullUpFac;
    spectrum.maxAirCntRate = MaxAirCntRate;
    spectrum.maxAirCntRateRTG = MaxAirCntRateRTG;


    // default source is Co57
    strcpy(spectrum.srcType, "Co57");

    // first try and open service system config file
    ifp = fopen(fileName.c_str(), "r");

    if (ifp == NULL) {

        fileName = CalibBaseDir;
        fileName.append(SysConfigFac);
        ALOG("ReadSysConfig file name %s", fileName.c_str());

        // otherwise, try to open factory config file
        ifp = fopen(fileName.c_str(), "r");

        if (ifp == NULL) {
            status = false;
        } else {
            ALOGI("Found SysConfigFac file");
        }
    } else {
        ALOGI("Found SysConfigSvc file");
    }

    // fopen returns 0 on error and file pointer on success.
    if (ifp != NULL) {
        while (!feof(ifp)) {

            rstatus = fscanf(ifp, "%s %s", str1, str2);

            if (rstatus == 2) {
                ALOGI(" readSysConfig: %s: %s", str1, str2);
                if (strcmp(str1, "SerialNumber") == 0) strcpy(serialNumber, str2);
                if (strcmp(str1, "SourceType") == 0) strcpy(spectrum.srcType, str2);
                if (strcmp(str1, "SrcAssayDate") == 0) strcpy(srcAssayDate, str2);
                if (strcmp(str1, "SrcAssayMCi") == 0) srcAssayMCi = (float) atof(str2);
                if (strcmp(str1, "Gain") == 0) spectrum.defKGain = (float) atof(str2);
                if (strcmp(str1, "Offset") == 0) spectrum.defKOffset = (float) atof(str2);
                if (strcmp(str1, "DWipeZeroAdjust") == 0) spectrum.dWipeZeroAdjust = (float) atof(str2);

                if (strcmp(str1, "SoilCalCoeff1") == 0) spectrum.soilCalCoeff1 = (float) atof(str2);
                if (strcmp(str1, "SoilCalCoeff2") == 0) spectrum.soilCalCoeff2 = (float) atof(str2);
                if (strcmp(str1, "SoilCalOffset") == 0) spectrum.soilCalOffset = (float) atof(str2);
                if (strcmp(str1, "SoilInsitCoeff1") == 0) spectrum.soilInsitCoeff1 = (float) atof(str2);
                if (strcmp(str1, "SoilInsitCoeff2") == 0) spectrum.soilInsitCoeff2 = (float) atof(str2);
                if (strcmp(str1, "SoilInsitOffset") == 0) spectrum.soilInsitOffset = (float) atof(str2);
                if (strcmp(str1, "DustCalCoeff1") == 0) spectrum.dustCalCoeff1 = (float) atof(str2);
                if (strcmp(str1, "DustCalCoeff2") == 0) spectrum.dustCalCoeff2 = (float) atof(str2);
                if (strcmp(str1, "DustCalOffset") == 0) spectrum.dustCalOffset = (float) atof(str2);
                if (strcmp(str1, "DustPullupFac") == 0) spectrum.dustPullUpFac = (float) atof(str2);

                if (strcmp(str1, "TempFitAdjust") == 0) spectrum.tempFitAdjust = (float) atof(str2);
                if (strcmp(str1, "CrudeScaleFac") == 0) spectrum.crudeScaleFac = (float) atof(str2);

                if (strcmp(str1, "RTAFarFac") == 0) spectrum.rtgFarFac = (float) atof(str2);
                if (strcmp(str1, "RTALoRate") == 0) spectrum.rtgLoRate = (float) atof(str2);
                if (strcmp(str1, "RTAHiRate") == 0) spectrum.rtgHiRate = (float) atof(str2);
                if (strcmp(str1, "RTASurfMode") == 0) {
                    if (strcmp(str2, "on") == 0) spectrum.rtgSurfMode = true;
                    if (strcmp(str2, "On") == 0) spectrum.rtgSurfMode = true;
                    if (strcmp(str2, "ON") == 0) spectrum.rtgSurfMode = true;
                }

                if (strcmp(str1, "MaxCntRateSF") == 0) spectrum.maxCntRateSF = (float) atof(str2);

                // obsolete (only for backward compatililty)
                if (strcmp(str1, "SoilCalSlope") == 0) spectrum.coefficient1 = (float) atof(str2);
                if (strcmp(str1, "Coefficient1") == 0) spectrum.coefficient1 = (float) atof(str2);
                if (strcmp(str1, "SoilCalOffset") == 0) spectrum.coefficient2 = (float) atof(str2);
                if (strcmp(str1, "Coefficient2") == 0) spectrum.coefficient2 = (float) atof(str2);

                if (strcmp(str1, "MaxAirCntRate") == 0) spectrum.maxAirCntRate = atoi(str2);
                if (strcmp(str1, "MaxAirCntRateRTG") == 0) spectrum.maxAirCntRateRTG = atoi(str2);

                if (strcmp(str1, "MinPbSNR") == 0) spectrum.minPbSNR = (float) atof(str2);
                if (strcmp(str1, "DefPbSNR") == 0) spectrum.defPbSNR = (float) atof(str2);
            }
        }
        fclose(ifp);

        // for now, assign gain and offset default values
        spectrum.k_gain = spectrum.defKGain;

        spectrum.k_offset = spectrum.defKOffset;

        // serial number
        spectrum.serialNumber = atoi(serialNumber);

        // get half life (default is Co57)
        spectrum.srcHalfLife = Co57HalfLife;
        if (strcmp(spectrum.srcType, "Co57") == 0 or strcmp(spectrum.srcType, "co57") == 0) spectrum.srcHalfLife = Co57HalfLife;
        if (strcmp(spectrum.srcType, "Cd109") == 0 or strcmp(spectrum.srcType, "cd109") == 0) spectrum.srcHalfLife = Cd109HalfLife;

        // calculate current *instrument* source strength using assay information
        pch = strtok(srcAssayDate, " /");
        month = atoi(pch);
        pch = strtok(NULL, " /");
        day = atoi(pch);
        pch = strtok(NULL, " /");
        year = atoi(pch);
        time(&currentTime);
        spectrum.srcAssayDay = day;
        spectrum.srcAssayMonth = month;
        spectrum.srcAssayYear = year;
        spectrum.srcAssayMCi = srcAssayMCi;
        spectrum.srcAgeDays = calcElapsedDays2(month, day, year);
        spectrum.srcMCi = srcAssayMCi * exp(-spectrum.srcAgeDays * ln2 / spectrum.srcHalfLife);

        ALOGI(" Instrument Source Type: %s", spectrum.srcType);
        ALOGI(" Instrument Source Half Life: %5.1f", spectrum.srcHalfLife);
        ALOGI(" Instrument Assay date: %2d/%2d/%4d", month, day, year);
        ALOGI(" Instrument Assay strength: %5.3f", srcAssayMCi);
        ALOGI(" Instrument Current strength: %5.3f", spectrum.srcMCi);

        status = true;
    }

    return (status);

}

void readSysCalibEnc(Calib &calib, int analyticMode) {

    std::fstream calibSTxt, calibFTxt, attemptCalibFTxt, calibEnc, calibDec, cEncrypt;
    std::string calibStr, calibEncStr, calibDecStr;
//    Crypto cryp;
    int size;
    char cdum[25];
    int i, n, elemID, tempID;
    float mgcm2SD[MaxConcLevels];
    float calibRatioFac[MaxConcLevels];
    float calibFWHM, calibRawFac;
    struct stat sb;

//    std::string fileNameEnc;
    std::string modeDir;

    if (analyticMode == 3)
        modeDir = TSSoilsDir;
    else if (analyticMode == 2)
        modeDir = DustWipeDir;
    else if (analyticMode == 1)
        modeDir = ISSoilsDir;
    else
        modeDir = LeadPaintDir;
//
//
//    fileNameEnc = CalibBaseDir + modeDir + SysCalibFacEnc;
//    ALOG("SysCalibFacEnc file name %s", fileNameEnc.c_str());
//
//    // Only go back to sl dir if is or ts don't exist for soils modes
//    // This will lead paint mode to continue to work.
//    if(analyticMode == 3 || analyticMode == 1) {
//        // If new directories don't exist use the old "sl" directory
//        if ((stat(fileNameEnc.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode))) {
//            fileNameEnc = CalibBaseDir;
//            fileNameEnc.append(SoilsDir);
//            fileNameEnc.append(SysCalibFacEnc);
//        }
//    }
//    ALOG("Final SysCalibFacEnc file name %s", fileNameEnc.c_str());
//
//	// Look for encrypted file first.
//	attemptCalibFTxt.open( fileNameEnc.c_str(), std::fstream::in | std::fstream::ate );
//	size = attemptCalibFTxt.tellg();
//
//
//	if( size < 1 ) {
//
//        // Open .txt file if .enc file does not exist
//        std::string fileName = CalibBaseDir + modeDir + SysCalibFac;
//        // Only go back to sl dir if is or ts don't exist for soils modes
//        // This will lead paint mode to continue to work.
//        if(analyticMode == 3 || analyticMode == 1) {
//            if ((stat(fileName.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode))) {
//                fileName = CalibBaseDir;
//                fileName.append(SoilsDir);
//                fileName.append(SysCalibFac);
//            }
//        }
//
//
//		// No encrypted file, open the txt file and encrypt it.
//		calibFTxt.open( fileName.c_str(), std::fstream::in | std::fstream::ate );
//		size = calibFTxt.tellg();
//
//        ALOG("SysCalibFac file name %s size %d", fileName.c_str(), size);
//
//		if( size > 0 )
//		{
//		    ALOG("SysCalibF Start encryption");
//			calibFTxt.seekg(0, std::ios::beg);
//
//            calibStr.resize((unsigned int) size);
//            calibFTxt.read((char *) calibStr.c_str(), size);
//
////            cryp.encrypt(calibStr, calibEncStr);
//
//            calibFTxt.close();
//
//            // Create the encrypted file
//            calibEnc.open(fileNameEnc.c_str(), std::fstream::out);
//            calibEnc << calibEncStr;
//            calibEnc.close();
//
//            ALOG("SysCalibF End encryption");
//
//        }
//    }
//    attemptCalibFTxt.close();
//
//    cEncrypt.open(fileNameEnc.c_str(), std::fstream::in | std::fstream::ate);
//
//    size = cEncrypt.tellg();
//    ALOG("SysCalibFacEnc open file name %s size %d", fileNameEnc.c_str(), (int)size);
//    cEncrypt.seekg(0, std::ios::beg);
//
//    calibEncStr.clear();
//    calibEncStr.resize((unsigned int) size);
//    cEncrypt.read((char *) calibEncStr.c_str(), size);
//    cEncrypt.close();
//
//    calibDecStr.clear();
//    ALOG("SysCalibFac encrypt file %s", calibEncStr.c_str());
//
////    cryp.decrypt(calibEncStr, calibDecStr);

//    ALOG("SysCalibFac decrypt file %s", calibDecStr.c_str());

    std::string fileName = CalibBaseDir + modeDir + SysCalibFac;
    // Only go back to sl dir if is or ts don't exist for soils modes
    // This will lead paint mode to continue to work.
    if (analyticMode == 3 || analyticMode == 1) {
        if ((stat(fileName.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode))) {
            fileName = CalibBaseDir;
            fileName.append(SoilsDir);
            fileName.append(SysCalibFac);
        }
    }


    // No encrypted file, open the txt file and encrypt it.
    calibFTxt.open(fileName.c_str(), std::fstream::in | std::fstream::ate);

    std::ifstream file(fileName.c_str());
    std::stringstream decCalibFStr;

    if (file) {

        decCalibFStr << file.rdbuf();

        file.close();
    }

    decCalibFStr >> cdum >> calib.calibTime;

    // read source strength at calibration time
    decCalibFStr >> cdum >> calib.srcMCi;

    for (n = 0; n < calib.nSubstrates; n++) {

        // read info (including average calibration factor over all concentrations) for each background substrate
        decCalibFStr >> elemID >> tempID >> calibFWHM >> calibRawFac;

        // read calibration ratio factor for each specific calibration concentration (except zero)
        for (i = 0; i < calib.nConcLevels; i++) {
            decCalibFStr >> calibRatioFac[i];
            //ALOGI(" readSysCalib: calibRatioFac=%f", calibRatioFac[i]);
        }

        // read SD for each calibration concentration
        for (i = 0; i < calib.nConcLevels; i++) {
            decCalibFStr >> mgcm2SD[i];
        }

        // elemID and tempID are stored in file as n+1, so must convert back
        elemID -= 1;
        tempID -= 1;

        // avoid negative index to arrays
        if ((elemID >= 0) && (tempID >= 0)) {
            calib.ecalFWHM[tempID] = calibFWHM;
            avCalibRaw[elemID][tempID] = calibRawFac;


            // store calibration ratio factor for each calibation concentration
            // NOTE: average calibration ratio factor is stored in concID=0 location
            for (i = 0; i < calib.nConcLevels; i++) {
                calibRatio[elemID][tempID][i] = calibRatioFac[i];
            }

            // store SD values for each calibration concentration
            for (i = 0; i < calib.nConcLevels; i++) {
                avMgcm2SD[elemID][tempID][i] = mgcm2SD[i];
                //calibRatio[elemID][tempID][i] = calibRatioFac[i];
            }
        }

    }

    // Read the calibration factors and apply them to the templates.
    readSysCalibCorrectionFactors(calib, analyticMode);

}

void readSysCalibCorrectionFactors(Calib &calib, int analyticMode) {

    //int n = 0; // index though number of substrates.
    int elemId, tempId;
    float corrFactor;
    std::istringstream corFactStr;
    std::string factorStr;
    std::string fileName;
    std::string modeDir;
    struct stat sb;

    if (analyticMode == 3) {
        modeDir = TSSoilsDir;
    } else if (analyticMode == 2)
        modeDir = DustWipeDir;
    else if (analyticMode == 1)
        modeDir = ISSoilsDir;
    else
        modeDir = LeadPaintDir;

    fileName = CalibBaseDir + modeDir + SysCalibCorFac;
    ALOG("SysCalibCorrectionFactors file name %s", fileName.c_str());

    if ((stat(fileName.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode))) {
        fileName = CalibBaseDir;
        fileName.append(SoilsDir);
        fileName.append(SysCalibCorFac);
    }

    std::fstream correctionFactorFile(fileName.c_str(), std::fstream::in | std::fstream::ate);
    int size = correctionFactorFile.tellg();

    ALOG("sysCalibCorrFac - size %d", size);

    if (size != -1) {
        correctionFactorFile.seekg(0);
        while (correctionFactorFile >> elemId >> tempId >> corrFactor) {

            if ((corrFactor != 0) && (tempId < calib.nSubstrates) && (elemId < 50)) {
//                ALOG("***** WARNING: Applying correction factor of %3.1f%% to substrate %d *****",
//                     corrFactor, tempId);

                // apply correction to calibration factor
                //(correction in sysCalibCorFactors file is the desired percentage change in the mgcm2 result)
                for (int i = 0; i < calib.nConcLevels; i++) {
                    calibRatio[elemId - 1][tempId - 1][i] *= 1.0 / (1.0 + corrFactor / 100);
                }
            }

        }
    }
    ALOG("Done loading correction factors");

    correctionFactorFile.close();

}

int readSysCalib(Calib &calib) {

    // Reads system calibration file to get elapsed time since calibration, calibration factors, and SD for each
    // concentration level
    //
    // Return value:
    //
    bool status = true;
    int i, n, elemID, tempID;
    float mgcm2SD[MaxConcLevels];
    float calibSrcMCi;
    float calibRatioFac[MaxConcLevels];
    float calibFWHM, calibRawFac;
    float calibCorrFac;
    time_t sysCalibTime = 0;
    char cdum[25];
    FILE *ifp;

    // first try and open service system calibration file
    ifp = fopen(SysCalibSvc, "r");


    if (ifp == NULL) {

// try to open factory calibration file
        ifp = fopen(SysCalibFac, "r");
        if (ifp == NULL) {
            status = false;
        } else {
            ALOGI("Found SysCalibFac file");
        }
    } else {
        ALOGI("Found SysCalibSvc file");
    }

    if (status) {
        // read calibration time
        fscanf(ifp, "%s %ld", cdum, &sysCalibTime);
        calib.calibTime = sysCalibTime;

        // read source strength at calibration time
        fscanf(ifp, "%s %f", cdum, &calibSrcMCi);
        calib.srcMCi = calibSrcMCi;

        //while(!feof(ifp)) {
        for (n = 0; n < calib.nSubstrates; n++) {

            // read info (including average calibration factor over all concentrations) for each background substrate
            fscanf(ifp, "%d %d %f %f", &elemID, &tempID, &calibFWHM, &calibRawFac);

            // read calibration ratio factor for each specific calibration concentration (except zero)
            for (i = 0; i < calib.nConcLevels; i++) {
                fscanf(ifp, "%f", &calibRatioFac[i]);
                //ALOGI(" readSysCalib: calibRatioFac=%f", calibRatioFac[i]);
            }

            // read SD for each calibration concentration
            for (i = 0; i < calib.nConcLevels; i++) {
                fscanf(ifp, "%f", &mgcm2SD[i]);
            }

            // elemID and tempID are stored in file as n+1, so must convert back
            elemID -= 1;
            tempID -= 1;

            // avoid negative index to arrays
            if ((elemID >= 0) && (tempID >= 0)) {
                calib.ecalFWHM[tempID] = calibFWHM;
                avCalibRaw[elemID][tempID] = calibRawFac;

                // store calibration ratio factor for each calibation concentration
                // NOTE: average calibration ratio factor is stored in concID=0 location
                for (i = 0; i < calib.nConcLevels; i++) {
                    calibRatio[elemID][tempID][i] = calibRatioFac[i];
                }

                // store SD values for each calibation concentration
                for (i = 0; i < calib.nConcLevels; i++) {
                    avMgcm2SD[elemID][tempID][i] = mgcm2SD[i];
                    //calibRatio[elemID][tempID][i] = calibRatioFac[i];
                }
            }

        }

        // read any calibration corrections (if any)
        while (!feof(ifp)) {

            if ((fscanf(ifp, "%d  %d  %f", &elemID, &tempID, &calibCorrFac)) == 3) {

                if (calibCorrFac != 0)
                    ALOG(
                            "***** WARNING: Applying correcton factor of %3.1f%% to substrate %d *****",
                            calibCorrFac, tempID);

                // apply correction to calibration factor
                //(correction in sysCalibF file is the desired percentage change in the mgcm2 result)
                for (i = 0; i < calib.nConcLevels; i++) {
                    calibRatio[elemID - 1][tempID - 1][i] *= 1.0 / (1.0 + calibCorrFac / 100);
                }
            }
        }

        fclose(ifp);
    }

    return (status);
}

float averVal(int nval, float *val) {
    // Returns the average of an array of values

    int n;
    float av = 0.0;

    for (n = 0; n < nval; n++) {
        av += val[n];
    }
    av /= nval;

    return av;
}

float median(int numValues, float *values, int mode) {

    // calculates the median value from a list of values
    //
    //  mode=1: Include all values
    //  mode=2: Ignore zero values


    int i, ibin, cnt = 0, iMin = 0, useValues = 0;
    float temp[100];
    float medianVal, minValue;
    bool valUsed[100] = {false};

    if (numValues > 100) numValues = 100;

    if (mode == 2) {
        for (i = 0; i < numValues; i++) {
            if (values[i] != 0) useValues++;
        }
    } else {
        useValues = numValues;
    }

    // order values from smallest to largest
    while (cnt < useValues) {
        minValue = 1E7;
        for (i = 0; i < numValues; i++) {
            if (mode == 2 and values[i] == 0) continue;
            if (valUsed[i]) continue;
            if (values[i] < minValue) {
                minValue = values[i];
                iMin = i;
            }
        }
        temp[cnt] = values[iMin];
        valUsed[iMin] = true;
        cnt++;
    }

    // find median value
    ibin = int(useValues / 2 + 0.5f);
    medianVal = temp[ibin];

    if (useValues == 0) medianVal = 0;

    //printf("\n Median Value = %f", medianVal);
    //for(i=0; i<numValues; i++) printf("\n Value %d = %f", i+1, temp[i]);

    return (medianVal);

}

float stanDev(int nval, float *val, int mode) {
    // Returns the standard deviation of an array of values
    // Mode=1: SD expressed as a percentage of the average value
    // Mode=2: SD value

    int n;
    float av = averVal(nval, val);
    float rval, sd = 0.0;

    for (n = 0; n < nval; n++) {
        sd += (val[n] - av) * (val[n] - av);
    }
    sd = sqrt(sd / (float) nval);

    if (mode == 1) {
        if (av != 0) {
            rval = sd / av * 100.f;
        } else {
            rval = 0;
        }
    } else {
        rval = sd;
    }

    return rval;
}

float stanDevNoZero(int nval, float *val, int mode) {
    // Returns the standard deviation of an array of values
    // Mode=1: SD expressed as a percentage of the average value
    // Mode=2: SD value

    int cnt = 0, n;
    float av = 0;
    float rval, sd = 0.0;

    for (n = 0; n < nval; n++) {
        if (val[n] != 0) {
            av += val[n];
            cnt++;
        }
    }
    av /= cnt;

    if (cnt > 0) {
        for (n = 0; n < nval; n++) {
            if (val[n] != 0) {
                sd += (val[n] - av) * (val[n] - av);
            }
        }
        sd = sqrt(sd / (float) cnt);
    }

    if (mode == 1) {
        if (av != 0) {
            rval = sd / av * 100.f;
        } else {
            rval = 0;
        }
    } else {
        rval = sd;
    }

    return rval;
}

int mostFreqInt(int nval, int *val) {

    // Returns the integer value found most frequently in the input array
    // If more than one value occurs with equal frequency as another, the first in the array is returned.

    int cnt, maxCnt = 0, i, n, m, ival = 0, nUsed = 0;
    int *usedVal;
    bool skip;

    usedVal = new int[nval];

    for (i = 0; i < nval; i++) usedVal[i] = -1;

    for (n = 0; n < nval; n++) {

        // check if value already found
        skip = false;
        for (i = 0; i < nUsed; i++) {
            if (val[n] == usedVal[i]) {
                skip = true;
                break;
            }
        }
        if (skip) continue;

        // update used values
        usedVal[nUsed] = val[n];
        nUsed++;

        cnt = 0;
        for (m = 0; m < nval; m++) {
            if (val[m] == val[n]) cnt++;
        }

        if (cnt > maxCnt) {
            ival = val[n];
            maxCnt = cnt;
        }

    }

    return (ival);

}

int readCSFile(CrossSect &crossSect) {

    // reads in cross section data from CalibCSFile

    int elemID, i, nElements, n, nBins;
    float loEnergy, hiEnergy, dEnergy, edgeEnergy;
    float e, cs;
    FILE *ifp;

    ifp = fopen(CalibCSFile, "r");
    if (ifp != NULL) {
        // read number of elements in file
        fscanf(ifp, "%d", &nElements);
        crossSect.nElements = nElements;

        for (n = 0; n < nElements; n++) {
            // read header for element
            fscanf(ifp, "%d %d %f %f %f %f", &elemID, &nBins, &edgeEnergy, &loEnergy, &hiEnergy,
                   &dEnergy);
            elemID -= 1;

            if (elemID < NumElements) {
                crossSect.dEnergy[elemID] = dEnergy;
                crossSect.loEnergy[elemID] = loEnergy;
                crossSect.hiEnergy[elemID] = hiEnergy;
                crossSect.edgeEnergy[elemID] = edgeEnergy;
                crossSect.nBins[elemID] = nBins;
            }

            // read cross section data
            for (i = 0; i < nBins; i++) {
                fscanf(ifp, "%f %f", &e, &cs);
                if (elemID < NumElements && i < NumCSBins) crossSect.cs[elemID][i] = cs;
                //ALOGI(" readCSFile: bin=%d CS=%f", i, crossSect.cs[elemID][i]);
            }

        }
    } else {
        ALOGE("Unable to open %s", CalibCSFile);
        nElements = -1;
    }

    return (nElements);

}

int calcElapsedDays(int specMonth, int specDay, int specYear, int assayMonth, int assayDay,
                    int assayYear) {

    // calculate the elapsed days since the two input dates

    int i, elapsedDays, nDays1 = 0, nDays2 = 0;


    // first calculate number of elapsed days since Jan 1st, 1970 for src assay date

    // number of days since January 1st, 1970
    for (i = 1970; i < assayYear; i++) {

        if (fmod((float) i, 4.f) == 0) {
            nDays1 += 366;
        } else {
            nDays1 += 365;
        }

    }

    // number of elapsed days for current year
    for (i = 1; i < assayMonth; i++) {

        switch (i) {
            case 1:
                nDays1 += 31;
                break;
            case 2:
                if (fmod((float) assayYear, 4) == 0) {
                    nDays1 += 29;
                } else {
                    nDays1 += 28;
                }
                break;
            case 3:
                nDays1 += 31;
                break;
            case 4:
                nDays1 += 30;
                break;
            case 5:
                nDays1 += 31;
                break;
            case 6:
                nDays1 += 30;
                break;
            case 7:
                nDays1 += 31;
                break;
            case 8:
                nDays1 += 31;
                break;
            case 9:
                nDays1 += 30;
                break;
            case 10:
                nDays1 += 31;
                break;
            case 11:
                nDays1 += 30;
                break;
            default:
                break;
        }
    }

    // add days of current month
    nDays1 += assayDay;

    // now calculate number of elapsed days since Jan 1st, 1970 for spectra acquisition date

    // number of days since January 1st, 1970
    for (i = 1970; i < specYear; i++) {

        if (fmod((float) i, 4.f) == 0) {
            nDays2 += 366;
        } else {
            nDays2 += 365;
        }

    }

    // number of elapsed days for current year
    for (i = 1; i < specMonth; i++) {

        switch (i) {
            case 1:
                nDays2 += 31;
                break;
            case 2:
                if (fmod((float) specYear, 4) == 0) {
                    nDays2 += 29;
                } else {
                    nDays2 += 28;
                }
                break;
            case 3:
                nDays2 += 31;
                break;
            case 4:
                nDays2 += 30;
                break;
            case 5:
                nDays2 += 31;
                break;
            case 6:
                nDays2 += 30;
                break;
            case 7:
                nDays2 += 31;
                break;
            case 8:
                nDays2 += 31;
                break;
            case 9:
                nDays2 += 30;
                break;
            case 10:
                nDays2 += 31;
                break;
            case 11:
                nDays2 += 30;
                break;
            default:
                break;
        }
    }

    // add days of current month
    nDays2 += specDay;

    // now calculate elapsed days
    elapsedDays = nDays2 - nDays1;

    // check for negative elapsed time
    if (elapsedDays < 0) elapsedDays = 0;

    return (elapsedDays);

}

int calcElapsedDays2(int month, int day, int year) {

    // calculate the elapsed days since the input date and the current time

    int i, elapsedDays, nDays = 0;
    time_t currentTime;

    // number of days since January 1st, 1970
    for (i = 1970; i < year; i++) {

        if (fmod((float) i, 4.f) == 0) {
            nDays += 366;
        } else {
            nDays += 365;
        }

    }

    // number of elapsed days for current year
    for (i = 1; i < month; i++) {

        switch (i) {
            case 1:
                nDays += 31;
                break;
            case 2:
                if (fmod((float) year, 4) == 0) {
                    nDays += 29;
                } else {
                    nDays += 28;
                }
                break;
            case 3:
                nDays += 31;
                break;
            case 4:
                nDays += 30;
                break;
            case 5:
                nDays += 31;
                break;
            case 6:
                nDays += 30;
                break;
            case 7:
                nDays += 31;
                break;
            case 8:
                nDays += 31;
                break;
            case 9:
                nDays += 30;
                break;
            case 10:
                nDays += 31;
                break;
            case 11:
                nDays += 30;
                break;
            default:
                break;
        }
    }

    // add days of current month
    nDays += day;

    // now calculate elapsed days
    time(&currentTime);
    elapsedDays = int(currentTime / 86400) - nDays;

    // check for negative elapsed time
    if (elapsedDays < 0) elapsedDays = 0;

    return (elapsedDays);

}

bool Calib::reset() {
    nSamples = 0;
    return true;
}