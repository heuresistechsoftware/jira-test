#ifndef ELEMENT_H
#define ELEMENT_H

#include "PeakFitPrototypes.hpp"
#include "Line.hpp"
#include "Calib.hpp"
#include "PeakFitResults.hpp"

// assign data types for elements
enum element_id {
    Pb, Cd, Au, W, Ba
};                   // elements are loaded into arrays in this order
// define the elements to analyze (called the "analysis elements")
static element_id analyzeElementID[MaxNumAnalyzeElements] = {Pb, Cd, Au, W, Ba};

static char elementName[NumElements][15];
static char elementSymb[NumElements][5];

// number of peaks to analyze for each analysis element
static int numAnalyzePeaks[MaxNumAnalyzeElements] = {1, 0, 0, 0, 0};

// define element objects
class Element {
public:

    element_id id;
    char name[15];
    int numLines;
    int numLLines;
    int numKLines;
    int numAnalyzePeaks;                // number of peaks to analyze
    int analyzePoint;            // given an element ID, points into analyzeElementID array
    float lBgndCnts;
    float lRawCnts;
    float lLineCnts;
    float lLineBgndRatio;
    float kBgndCnts;
    float kRawCnts;
    float kLineCnts;
    float kAlphaCnts;
    float kAlphaRaw;
    float kAlphaBgnd;
    float kAlphaCnts_fit;
    float kAlphaRaw_fit;
    float kAlphaBgnd_fit;
    float kBetaRaw;
    float kBetaCnts;
    float kBetaBgnd;
    float kWFlux;                           // cross section weighted flux above k-edge
    float kAlphaBgndRatio;
    float kBetaBgndRatio;
    float mgcm2;                // measured concentration using full fit to templates
    float mgcm2_quick;            // measured concentration using quick fit to templates
    float mgcm2SD;                            // SD in measured concentration (calculated during calibration from repeated measurements)
    float error;                // actual measurement error (uses mgcm2SD corrected for measurement time and source decay)
    float tempConc;                         // actual concentration of fitted background template (usually zero)
    float alpha21Ratio;
    float alphaBetaRatio;
    float lConfLev;
    float kConfLev;
    float kCalibRawFac;            // k-alpha calibration factor derived from raw counts (counts/mg/cm2/nomSec)
    float kCalibFitFac;            // k-alpha calibration factor derived from fitted counts (counts/mg/cm2/nomSec)
    float kCalibRatioFac[MaxConcLevels];    // k-alpha calibration ratio factor derived from raw counts

    Determination elemResult;

    // instantiate lines inside element
    Line line[MaxLinesPerElement];

    void setName(int);

    void calcConc(float srcMCi, float calibSrcMCi, float liveTime, int lineID, int concID);

    void calcDetermAL(float srcMCi, float calibSrcMCi, float nomSecs, float liveTime,
                      float actionLevel, float negConfLev, float posConfLev, meas_mode measMode,
                      bool RTGPresent);

    void calcDetermDL(float srcMCi, float calibSrcMCi, float nomSecs, float liveTime,
                      float detectionLevel, float negConfLev, float posConfLev, float dLNomSecs,
                      meas_mode measMode);

};

#endif // ELEMENT_H
