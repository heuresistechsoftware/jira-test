
#include <stdio.h>
#include <string.h>
#include <cmath>

#include "hac.hpp"
#include "Calib.hpp"
#include "PeakFitPrototypes.hpp"
#include "PeakFitResults.hpp"
#include "PeakFit.hpp"
#include "MessCode.hpp"

extern float positiveConfidenceLevelLimitAL;
extern float negativeConfidenceLevelLimitAL;
extern float positiveConfidenceLevelLimitDL;
extern float negativeConfidenceLevelLimitDL;

PeakFitResults results;
SampTemplate sampTemp;
Calib calib;
extern Spectrum *spectrum;
PeakFit *peakFit;


int acquiredSpectrumCalc(const DppSpectrum dppSpectrum) {

    int i, nSample, success;
    unsigned int specCnts[MaxNumBins];
    bool status = 0;

    // initialize spectrum
    spectrum->initSpectrum(dppSpectrum);

    // initialize elements (moved here from hac 11/29/18)
    spectrum->initializeElements();

    if (spectrum->performEnergyCal) {
        // accumulate spectrum counts for use in energy calibration
        int nAccSpec = accumSpecMem(NumCumSpectra, *spectrum, dppSpectrum);

        // do energy calibration
        if (nAccSpec > 0) {
            status = spectrum->energyCalSpec(1);
            ALOG(" hac: nAccSpec=%d  TotCnts=%d gain=%f offset=%f", nAccSpec, spectrum->totCnts,
                 spectrum->k_gain, spectrum->k_offset);
        }
    }

    // call hac routine
    success = hac(*spectrum);
    ALOG(" Hac called returned %d", success);

    return(success);

}

void initializeConfidenceLimits( /*PeakFitResults &results,*/ meas_mode mode) {


    switch (mode) {

        case AL:
            results.negConfLev = positiveConfidenceLevelLimitAL;
            results.posConfLev = negativeConfidenceLevelLimitAL;
            break;

        default:
            results.negConfLev = positiveConfidenceLevelLimitDL;
            results.posConfLev = negativeConfidenceLevelLimitDL;
            break;

    } //  switch (mode)

}

int hac(Spectrum &spectrum) {

    // initialize elements (commented out 11/29/18)
    // initializeElements(spectrum);

    // fit peaks of elements to analyze
    peakFit->analyzeCounts(spectrum);

    // fill arrays for normalized output spectrum
    spectrum.writeSpectrum( spectrum.k_gain, spectrum.k_offset );

    return (results.result);
}

int hac(Spectrum &spectrum,
        Calib &calib,
        int analyticMode,
        meas_mode measMode) {


    // Processes and analyzes the input spectrum.
    //
    // Return value:  0: Inconclusive
    //                1: Negative
    //                2: Positive

    // initialize elements
    spectrum.initializeElements();
    int spectrumError = 0;

    // measurement mode
    results.measMode = measMode;

    // initialize confidence limits in results array
    initializeConfidenceLimits( measMode);



    // warn if energy resolution is higher than value stored in system calibration file
    if (spectrum.ecalFWHM > (1 + DetResHiWarn / 100) * calib.ecalFWHM[results.tempType]) {
        spectrum.createMessage(ResHiWarnCode, 2, spectrum.ecalFWHM, 0, 0, 0);
    }

    // fit peaks of elements to analyze
    peakFit->analyzeCounts(spectrum, sampTemp, calib, Pb, analyticMode, measMode);

    // warn if chisquared value for template match is large (indicates poor match to available background templates)
    if ( spectrum.tempChisq > TempChisqWarn) {
        spectrumError = 1;
        spectrum.createMessage(TempChisqWarnCode, 2, spectrum.tempChisq, 0, 0, 0);
    }
    ALOG("Spectrum ChiSqWarn %f err = %d", spectrum.tempChisq, spectrumError);

    // fill arrays for normalized output spectrum
    ALOG(" HAC: Spectrum gain %f, offset %f ", spectrum.k_gain, spectrum.k_offset);
    spectrum.writeSpectrum( spectrum.k_gain, spectrum.k_offset);

    ALOG("******** Results - ConfLevel(%f) mgCm2(%6.3f) quick(%6.3f) result(%d) stdDev(%6.4f) ABRatio(%5.3f)",
         results.confLev, results.mgCm2, results.mgCm2_quick, (int) results.result, results.stdDev,
         results.alphaBetaRatio);

    if (spectrumError == 1)
        results.result = dError;

    return (results.result);

}

bool checkSubstrate(Spectrum &spectrum, bool RTGPresent) {

    // Checks for presence of substrate by comparing incremental change in counts in calibration peak with counts in
    // Compton peak region
    //
    // Return Value:
    //        True:  substrate present
    //        False: air only
    //

    int i, totCnts, lo_bin, hi_bin;
    float dLiveTime, dNomSecs, cntRate;
    float maxAirCntRate;
    bool rVal = true;
    static int airScanCount = 0; // Count how many airScan shots in a row.

    // bin limits for background Compton peak
    lo_bin = int((BgndPeakFitLoKeV - spectrum.k_offset) / spectrum.k_gain + 0.5);
    hi_bin = int((BgndPeakFitHiKeV - spectrum.k_offset) / spectrum.k_gain + 0.5);
    if (lo_bin < 0) lo_bin = 0;
    if (hi_bin >= MaxNumBins) hi_bin = MaxNumBins - 1;

    // only sum counts in low-energy side of background peak (set high bin to average of low and high bins)
    hi_bin = int(0.5*(lo_bin + hi_bin));

    totCnts = 0;
    for (i = lo_bin; i <= hi_bin; i++) {
        totCnts += spectrum.counts[i];
    }

    //ALOG("checkSubstrate: Previous values :slowCnt=%d fastCorCnt=%d accumTime=%f totCnts=%d", spectrum.prevSlowCnts, spectrum.prevFastCorrCnts, spectrum.prevAccTime, spectrum.prevTotCnts);
    //ALOG("checkSubstrate: Current  values :slowCnt=%d fastCorCnt=%d accumTime=%f totCnts=%d", spectrum.slowCnts, spectrum.fastCorrCnts, spectrum.accTime, totCnts);
    // change in live time

    dLiveTime = float(spectrum.slowCnts - spectrum.prevSlowCnts) /
                (spectrum.fastCorrCnts - spectrum.prevFastCorrCnts) *
                (spectrum.accTime - spectrum.prevAccTime);


    // change in nominal seconds
    if(strcmp(spectrum.srcType,"Co57")==0 or strcmp(spectrum.srcType,"co57")==0) {
        dNomSecs = spectrum.srcMCi / Co57NomSrcMCi * dLiveTime;
    } else {
        dNomSecs = spectrum.srcMCi / Cd109NomSrcMCi * dLiveTime;
    }

    // calculate rate of change of counts per nominal second
    cntRate = float(totCnts - spectrum.prevTotCnts) / dNomSecs;

    // update values that will be used as starting values after next incremental spectrum change
    spectrum.prevTotCnts = totCnts;
    spectrum.prevSlowCnts = spectrum.slowCnts;
    spectrum.prevFastCorrCnts = spectrum.fastCorrCnts;
    spectrum.prevAccTime = spectrum.accTime;

    ALOG("checkSubstrate: cntRate=%f totCnts=%d nomSecs=%f", cntRate, totCnts, spectrum.nomSecs);

    // max air count rate is set depending on whether RTA is present
    if(RTGPresent) {
        maxAirCntRate = spectrum.maxAirCntRateRTG;
        ALOG("checkSubstrate: RTG Present");

    } else {
        maxAirCntRate = spectrum.maxAirCntRateRTG;
        ALOG("checkSubstrate: RTG NOT Present");

    }

    // set status to false if count rate is too low (means no substrate present)
    if ((cntRate < maxAirCntRate)) {
//        airScanCount++;
//        if (airScanCount > 1) {
//            airScanCount = 0;
        rVal = false;
        ALOG("checkSubstrate: Substrate NOT Present");
        //}
        ALOG("checkSubstrate: Counts lower than threshold (airScanCount=%d)", airScanCount);

    } else {
        airScanCount = 0;
        ALOG("checkSubstrate: Substrate Present");
    }

    return (rVal);
}

int acquiredSpectrumCalc(const DppSpectrum dppSpectrum, int analyticMode, meas_mode measMode) {

    int success;
    bool status = 0;

    time_t currentTime;

    // initialize spectrum
    spectrum->initSpectrum( dppSpectrum );
    ALOG(" acquireSpectrumCalc initSpectrum done");

    // initialize nullCode
    results.nullCode = 0;

    // set flag to false for first spectrum increment
    if (dppSpectrum.nIncrement <= 1) {
        results.ALDone = false;
    }

    if (spectrum->performEnergyCal) {
        // accumulate spectrum counts for use in energy calibration
        //int nAccSpec = accumSpecFile(NumCumSpectra, spectrum, dppSpectrum);
        int nAccSpec = accumSpecMem(NumCumSpectra, *spectrum, dppSpectrum);

        // do energy calibration
        if (nAccSpec > 0) {
            status = spectrum->energyCalSpec( 1 );

            ALOG(" hac: nAccSpec=%d  TotCnts=%d gain=%f offset=%f", nAccSpec, spectrum->totCnts,
                 spectrum->k_gain, spectrum->k_offset);
        }
    }

    // success is really results.result
    success = hac( *spectrum, calib, analyticMode, measMode);
    ALOG(" Hac called returned %d", success);

    Determination determ = dNull;
    if (DoSpectrumCheck and success == determ) {
        results.result = dNull;
        results.nullCode = 1;
    }

    determ = dError;
    if(success == determ){
        results.result = dError;
        ALOG(" success is dError");
    }

    // check for presence of substrate and overide result if an air scan is detected
    if( spectrum->slowCnts <  spectrum->fastCorrCnts) {
        status = checkSubstrate(*spectrum, results.RTGPresent);
        if (!status) {
            ALOG(" checkSubstrate airscan");
            results.result = dAirScan;
        }
    }


    // check for bad template fit
    if (DoSpectrumCheck and results.tempType < 0) {


        // update previous spectrum time and tempType to current values if measurement has finished (needed for readThough routine)
        if (results.result != dInProcess and results.result != dNull and results.result != dError) {
            time(&currentTime);
            spectrum->prevSpecTime = (int) currentTime;
            spectrum->prevTempType = spectrum->tempType;
        }


        //writeLogFile(spectrum, results, 1 , 0);

        results.result = dNull;
        results.nullCode = 4;
    }

    // update previous spectrum time and tempType to current values if measurement has finished (needed for readThough routine)
    if (results.result != dInProcess and results.result != dNull  ) {
        time(&currentTime);
        spectrum->prevSpecTime = (int) currentTime;
        spectrum->prevTempType = spectrum->tempType;
    }

    //writeLogFile(spectrum, results, 1 , 0);

    //writeDebugFile(spectrum);

    ALOG(" ****!!!!**** nullCode=%d %d*****!!!!*****" , results.nullCode, results.result);

    return success;
}

// This version of hacInit is used for Pbi
bool hacInit(int mode, int analyticMode, int type) {

    // Inputs:
    //
    //    mode=1: Calibration mode
    //    mode=2: Configuration mode
    //
    //    tpe = 1 is Concentration init (Pbi)
    //    type = 2 is Detection init (HBI)

    ALOG("hacInit - System Type %d", type );
    ALOG("hacInit - Spectrum 0x%x", &spectrum );

    if( type == 1 ){

        // update gain, offset for instrument (read in from sysConfig file)
        readSysConfig( *spectrum, analyticMode );

        return(true);

    }

    // Below is the Code for type Concentration

    int nSamples;
    //bool status;
    bool retVal = false;

    // Reset all globals here, until we remove the globals.
    calib.reset();
    spectrum->reset();
    sampTemp.reset();
    results.reset();

    // tell fitTemplates to fit all background templates
    spectrum->fitMode = 2;

    // assign default gain and offset (these will be overwritten by readSysConfig if the config file exists)
    spectrum->k_gain = DefaultKGain * SpecBinMode;
    spectrum->k_offset = DefaultKOffset;

    // read calibration parameter file, if there is no calibration file or no sample in the file,
    // return a false so an error be reported to the user and not crash like it was doing before.
    nSamples = readCalibParams(calib, mode, analyticMode);
    if (nSamples > 0) {

        retVal = true;

        // default value of current instrument source strength is set to value calculated from calibParam assay info
        spectrum->srcMCi = calib.srcMCi;

        // initialize calibration gain and offset to zero
        calib.gain = 0;
        calib.offset = 0;

        ALOG("hacInit -  mode  %d", analyticMode);
        // read system calibration file to get calibration factors for each substrate
        readSysCalibEnc(calib, analyticMode);

        ALOG("hacInit - Finished readSysCalibEnc");

        // update gain, offset, and current source strength for instrument (read in from sysConfig file)
        readSysConfig(*spectrum, analyticMode);

        ALOG("hacInit - Finished readSysConfig");

        // load templates from disk
        peakFit->loadTemplates(sampTemp, 1, analyticMode);

        ALOG("hacInit - Finished loadTemplates");
    }

    return retVal;
}



void loadSpectrum(uint32_t *binCnts, int numBins, float liveTime, const DppSpectrum &dppSpec,
                  Spectrum &spectrum) {
    //
    // Stores cumulative counts in spectrum structure from the input array pointed to by binCnts with numBins
    //


    int i, totCnts = 0;

    // assign live time
    spectrum.liveTime = liveTime;

    if (numBins > MaxNumBins) {
        ALOGI("initSpectrum: Too many bins for Spectrum structure - increase size of MaxNumBins");
    }

    // assign number of bins
    if (numBins <= MaxNumBins) {
        spectrum.numBins = numBins;
    } else {
        spectrum.numBins = MaxNumBins;
    }

    // Set the pointer to the input counts
    //spectrum.counts = binCnts;

    // total counts in spectrum
    for (i = 0; i < spectrum.numBins; i++) {
        spectrum.cumCounts[i] = binCnts[i];
        totCnts += spectrum.counts[i];
    }
    spectrum.totCnts = totCnts;
    //printf("\n loadSpectrum: totCnts=%d", totCnts);

}


int accumSpecMem(int nSpectra, Spectrum &spectrum, const DppSpectrum &dppSpec) {

    // Accumulate the last set of counts from the most recent nSpectra spectra.
    // Add the most recent counts and flush out the oldest (FIFO).
    //
    // This version works by storing spectra  in memory, so it's considerably faster
    // than accumSpecFile(). It cannot be used if this system is shut down during operation,
    // so probably best for calibration mode.
    //
    // Return Value: Number of accumulated spectra
    //

    int i, n, scnt;
    static int cnt = 0;
    static time_t tStamp[NumCumSpectra] = {0};
    static int brdTemp[NumCumSpectra] = {0}, detTemp[NumCumSpectra] = {0};
    static int counts[NumCumSpectra][MaxNumBins] = {0};

    // initialize counters
    for (i = 0; i < MaxNumBins; i++) spectrum.cumCounts[i] = 0;

    // shuffle counts from each spectrum down one entry, starting with the second last entry (last entry gets flushed)
    for (n = 0; n < (nSpectra - 1); n++) {
        for (i = 0; i < spectrum.numBins; i++) {
            counts[nSpectra - 1 - n][i] = counts[nSpectra - 2 - n][i];
            tStamp[nSpectra - 1 - n] = tStamp[nSpectra - 2 - n];
            brdTemp[nSpectra - 1 - n] = brdTemp[nSpectra - 2 - n];
            detTemp[nSpectra - 1 - n] = detTemp[nSpectra - 2 - n];
        }
    }

    // put current spectrum into first entry
    time(&tStamp[0]);
    brdTemp[0] = dppSpec.boardTemp_;
    detTemp[0] = dppSpec.TECTemp_;
    for (i = 0; i < spectrum.numBins; i++) {
        counts[0][i] = spectrum.counts[i];
    }

    // sum counts in each bin
    scnt = 0;
    for (n = 0; n < nSpectra; n++) {

        if (tStamp[n] == 0) continue;

        // check time difference limits for each entry in file before including in cumulative count
        if ((float(tStamp[0] - tStamp[n]) / 60) > MaxSpecAge) {
            ALOGI(" Spectrum %d is too old (age=%4.1f mins)", n + 1, float(tStamp[0]
                    -tStamp[n])/60);
            continue;
        }

        // check board temperature difference limits for each entry in file before including in cumulative count
        if ((fabs((double) ((brdTemp[0] - brdTemp[n]) / brdTemp[0] * 100))) > MaxBrdTempDiff) {
            ALOGI(" Board temperature has varied too much for spectrum %d (%dC vs %dC)", n + 1,
                  brdTemp[n], brdTemp[0]);
            continue;
        }

        // check detector temperature difference limits for each entry in file before including in cumulative count
        if ((fabs((double) ((detTemp[0] - detTemp[n]) / detTemp[0] * 100))) > MaxDetTempDiff) {
            ALOGI(" Detector temperature has varied too much for spectrum %d (%dC vs %dC)", n + 1,
                  detTemp[n], detTemp[0]);
            continue;
        }

        for (i = 0; i < spectrum.numBins; i++) {
            spectrum.cumCounts[i] += counts[n][i];
        }

        // increment number of spectra being combined after time and temp checks
        scnt++;

    }

    cnt++;
    if (scnt > nSpectra) scnt = nSpectra;

    return scnt;
}

// New accumSpecMem for Pb dection in HBI with two params instead of three.
int accumSpecMem(int nSpectra, Spectrum &spectrum) {

    // Accumulate the last set of counts from the most recent nSpectra spectra.
    // Add the most recent counts and flush out the oldest (FIFO).
    //
    // This version works by storing spectra  in memory, so it's considerably faster
    // than accumSpecFile(). It cannot be used if this system is shut down during operation,
    // so probably best for calibration mode.
    //
    // Return Value: Total number of counts in accumulated spectra
    //

    int i, n, scnt, totCnts=0;
    static int cnt = 0;
    static int counts[NumCumSpectra][MaxNumBins] = {0};

    totCnts = 0;

    // initialize cumulative spectrum counters
    for (i = 0; i < MaxNumBins; i++) spectrum.cumCounts[i] = 0;

    // intialize counters before the first incremental spectrum
    if (spectrum.nIncrement == 1) {
        for (n = 0; n < nSpectra; n++) {
            for (i = 0; i < MaxNumBins; i++) counts[n][i] = 0;
        }
    }

    // shuffle counts from each incremental spectrum down one entry, starting with the second last entry (last entry gets flushed)
    for (n = 0; n < (nSpectra - 1); n++) {
        for (i = 0; i < spectrum.numBins; i++) {
            counts[nSpectra - 1 - n][i] = counts[nSpectra - 2 - n][i];
        }
    }

    // put current incremental spectrum into first entry
    for (i = 0; i < spectrum.numBins; i++) {
        counts[0][i] = (spectrum.counts[i] - spectrum.prevCounts[i]);
    }

    // sum counts in each bin
    scnt = 0;
    for (n = 0; n < nSpectra; n++) {

        for (i = 0; i < spectrum.numBins; i++) {
            spectrum.cumCounts[i] += counts[n][i];
            totCnts += counts[n][i];
        }

        // increment number of spectra being combined
        scnt++;

    }

    cnt++;
    if (scnt > nSpectra) scnt = nSpectra;

    return totCnts;
}


/*int accumSpecFile(int nSpectra, Spectrum &spectrum, const DppSpectrum &dppSpec) {

    // Accumulate the last set of counts from the most recent nSpectra spectra.
    // Add the most recent counts and flush out the oldest (FIFO).
    //
    // This version works using a file stored on disk, rather than in memory, so it's
    // immune to the system shutting down or crashing briefly, but much slower (about
    // 1ms to read in each stored spectrum on the Microway server).
    //
    // It is probably best to use this in peakfitting mode on the instrument, but can
    // use AccumSpecMem in calibration mode
    //
    // Return Value: Number of accumulated spectra, subject to temperature and time limits
    //

    int loBin = 0, nBins = 0, status, i, n;
    int cnt = 0, cnts, scnt;
    time_t tStamp[NumCumSpectra];
    int brdTemp[NumCumSpectra], detTemp[NumCumSpectra];
    unsigned int counts[NumCumSpectra][MaxNumBins] = {0};

    FILE *ifp;

    if (nSpectra > NumCumSpectra) nSpectra = NumCumSpectra;

    // initialize counters
    for (i = 0; i < MaxNumBins; i++) spectrum.cumCounts[i] = 0;

    // open file for reading
    ifp = fopen(AccumSpecFile, "r");

    if (ifp != NULL) {

        // Read in any existing data from file
        // first read bin number of first entry for each accumulated spectrum
        status = fscanf(ifp, "%d %d\n", &loBin, &nBins);

        cnt = 0;
        while (!feof(ifp)) {

            // read time stamp for each spectrum
            status = fscanf(ifp, "%ld %d %d", &tStamp[cnt], &brdTemp[cnt], &detTemp[cnt]);
            if (status != 3 || cnt >= NumCumSpectra) break;
            // read the counts in each bin
            for (i = 0; i < nBins; i++) {
                fscanf(ifp, "%d", &cnts);
                if ((loBin + i) < MaxNumBins) {
                    counts[cnt][loBin + i] = (unsigned int) cnts;
                }
            }

            cnt++;
        }

        ALOGI(" accSpecFile: There are currently %d entries in file", cnt);

        // shuffle counts from each spectrum down one entry, starting with the second last entry (last entry gets flushed)
        for (n = 0; n < (nSpectra - 1); n++) {
            //printf("\n accSpecFile: Shuffling entry %d down to %d", nSpectra-2-n, nSpectra-1-n);
            for (i = 0; i < nBins; i++) {
                counts[nSpectra - 1 - n][loBin + i] = counts[nSpectra - 2 - n][loBin + i];
                tStamp[nSpectra - 1 - n] = tStamp[nSpectra - 2 - n];
                brdTemp[nSpectra - 1 - n] = brdTemp[nSpectra - 2 - n];
                detTemp[nSpectra - 1 - n] = detTemp[nSpectra - 2 - n];
            }
        }

        fclose(ifp);
    }

    // lower bin limit is always zero (otherwise there would be errors when calculating this based on poor energy calibration data)
    loBin = 0;

    // number of bins
    nBins = spectrum.numBins;

    // put current spectrum into first entry
    time(&tStamp[0]);
    brdTemp[0] = dppSpec.boardTemp_;
    detTemp[0] = dppSpec.TECTemp_;
    for (i = 0; i < nBins; i++) {
        if (loBin + i >= MaxNumBins) break;
        counts[0][loBin + i] = spectrum.counts[loBin + i];
    }
    cnt++;
    if (cnt > nSpectra) cnt = nSpectra;

    // sum counts in each bin (over entire spectrum)
    // only use entries that meet the time and temperature criteria
    //for(n=0; n<nSpectra; n++) {
    scnt = 0;
    for (n = 0; n < cnt; n++) {

        if (tStamp[n] == 0) continue;

        // check time difference limits for each entry in file before including in cumulative count
        if ((float(tStamp[0] - tStamp[n]) / 60) > MaxSpecAge) {
            //ALOG(" Spectrum %d is too old (age=%4.1f mins)", n+1, float(tStamp[0]-tStamp[n])/60);
            continue;
        }

        // check board temperature difference limits for each entry in file before including in cumulative count
        if ((fabs((double) ((brdTemp[0] - brdTemp[n]) / brdTemp[0] * 100))) > MaxBrdTempDiff) {
            //ALOG(" Board temperature has varied too much for spectrum %d (%dC vs %dC)", n+1, brdTemp[n], brdTemp[0]);
            continue;
        }

        // check detector temperature difference limits for each entry in file before including in cumulative count
        if ((fabs((double) ((detTemp[0] - detTemp[n]) / detTemp[0] * 100))) > MaxDetTempDiff) {
            //ALOG(" Detector temperature has varied too much for spectrum %d (%dC vs %dC)", n+1, detTemp[n], detTemp[0]);
            continue;
        }

        for (i = 0; i < nBins; i++) {
            spectrum.cumCounts[i] += counts[n][i];
        }

        // increment number of spectra being combined after time and temp checks
        scnt++;

    }
    //printf("\naccumSpecFile:  Combined %d spectra", scnt);

    // open file for writing
    ifp = fopen(AccumSpecFile, "w");

    if (ifp != NULL) {
        // write data back to file (write over last data)
        fprintf(ifp, "%d %d", loBin, nBins);
        for (n = 0; n < cnt; n++) {
            fprintf(ifp, "\n%ld %d %d\n", tStamp[n], brdTemp[n], detTemp[n]);
            for (i = 0; i < nBins; i++) {
                fprintf(ifp, "%d ", counts[n][loBin + i]);
            }
        }
        fclose(ifp);
    }

    return scnt;
}*/

bool checkReadThrough(Spectrum &spectrum, int elemID) {

    // Checks for read through. If the background template for the current spectrum is the Read Through Gauge (RTG), then
    // compare the last previous measurement made without the RTG with the current measurement with the RTG. Read Through
    // is indicated by a substantially lower measurement with the RTG versus without the RTG. If Read Through is present,
    // calculate the near side concentration.
    //
    // Return Value: MgCm2 (If in RT mode, returns the calculated surface concentration)
    //

    float mgCm2, concRatio;
    bool RTStatus;
    time_t currentTime;

    // concentration from current measurement
    mgCm2 = spectrum.element[elemID].mgcm2;


    // get current time
    time(&currentTime);

    ALOGE("Template = %d RTGTemplate = %d", spectrum.tempType +1, calib.nNonRTA);

    // return with false status if no RTG present and store current concentration as last non-RTG concentration
    if ((spectrum.tempType + 1) <= calib.nNonRTA) {

        results.mgCm2_NoRTG = mgCm2;
        results.mgCm2_Near = mgCm2;

        results.RTGPresent = false;
        RTStatus = false;

    } else {

        // RTG is present
        results.RTGPresent = true;

        // calculate near concentration
        if (spectrum.rtgSurfMode and ((int) currentTime - spectrum.prevSpecTime) < 60 and
            (spectrum.prevTempType + 1) <= calib.nNonRTA) {
            // calculate best estimate of near-side concentration using current RTG measurement and most recent non-RTG measurement

            results.mgCm2_Near = (mgCm2 - spectrum.rtgFarFac * results.mgCm2_NoRTG) / (1 - spectrum.rtgFarFac);
            ALOG("**** Calculating near concentration using last non-RTG value");

            // assign near concentration to element concentration for display purposes and for POS/NEG decision
            spectrum.element[elemID].mgcm2 = results.mgCm2_Near;

            // for now, set RTSTatus to true if RT surface concentration is calculated (needed to work with Andy's display code)
            RTStatus = true;

        } else {

            // near-side concentration is just current RTG measurement
            results.mgCm2_Near = mgCm2;
            ALOG("**** Just using RTG value");

            //results.readThrough = false;

            RTStatus = false;

        }
        if (results.mgCm2_Near < 0) results.mgCm2_Near = 0;

        ALOG(" NoRTG=%f RTG=%f Near=%f", results.mgCm2_NoRTG, results.mgCm2, results.mgCm2_Near);

    }

    //ALOG("time=%d prevTime=%d timeDiff=%d temp=%d prevTemp=%d", (int) currentTime,
    //     spectrum.prevSpecTime, ((int) currentTime - spectrum.prevSpecTime), spectrum.tempType + 1,
    //     spectrum.prevTempType + 1);

    return (RTStatus);

}

/*int writeDebugFile(Spectrum &spectrum) {

    // write out spectrum values after each incremental update

    FILE *ofp;

    ofp = fopen("specDebug.txt", "a+");

    // totCnts, fastCorrCnts, slowCnts, accTime, liveTime
    fprintf(ofp, "\n %d %d %d %f %f ", spectrum.totCnts, spectrum.fastCorrCnts, spectrum.slowCnts,
            spectrum.accTime, spectrum.liveTime);

    fclose(ofp);

    return (1);

}*/
