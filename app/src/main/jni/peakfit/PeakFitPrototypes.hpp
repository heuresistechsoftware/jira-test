#ifndef PEAKFITPROTOTYPES_H
#define PEAKFITPROTOTYPES_H

#include <cstdint>


#ifdef PF_DEBUG
#include "android/log.h"
#define LOG_TAG "Peakfit"
#define ALOG(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define ALOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define ALOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define ALOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#else
#define ALOG(...)
#define ALOGI(...)
#define ALOGW(...)
#define ALOGE(...)
#endif



#define DefaultKGain 0.265f            // Pb200i 5x5mm gain 512 bins
#define DefaultKOffset 0.0f            // Pb200i

#define InputFileType 2                // 1: Amptek MCA file  2: Pb200i file
#define SpecBinMode 1                  // 1: No rebinning n: Combine n bins together (default gain will automatically get scaled accordingly)
#define CalibFileRoot "pb200i"         // file name root for calibration files e.g. "calib" creates the files calib00n.00m.mca
#define SuppressSuffix false           // if only one measurement for file, suppress _001 suffix on file name

// AL (Action Level) Mode parameters
#define ALConcCalcMode 2               // Mode=1: Use raw counts scaled by liveTime and source strength Mode=2: Use counts/bgnd ratio (no scaling req.) [2]
#define ALNegConfLev 3.0f               // Confidence level for -'ve result (in units of sigma eg. 2.0 means 95% conf.level)  [3.0]
#define ALPosConfLev 3.0f               // Confidence level for +'ve result (in units of sigma eg. 2.0 means 95% conf.level) [3.0]
#define ALTOutNomSecs 5.0f             // max nominal measurement time for instrument timeout in AL mode [5.0]
#define ALTOutMaxNegConc 0.05f         // Action level above which positive result is determined (e.g. 0.95 = 1.00mg/cm2 for Pb) [0.05]
#define ALTOutMaxNegConcRTG 0.05f     // Action level above which blank result is determined with RTG engaged (e.g. 0.85 = 0.90mg/cm2 for Pb) [0.05]
#define ALTOutMaxPosConc 1.05f         // percentage of action level above which positive result is forced after timeout [1.05]
#define MinNomSecs_05  5.0f          // min measurement time (nomsecs) for 0.5 action level
#define MinNomSecs_07  5.0f          // min measurement time (nomsecs) for 0.7 action level

// DL (Detection Limit) Mode results determination parameters
#define DLConcCalcMode 2               // Mode=1: Use raw counts scaled by liveTime and source strength Mode=2: Use counts/bgnd ratio (no scaling req.) [2]
#define DLTimeOut 300.0f               // DL modes: max nominal measurement time for instrument timeout
#define DLNegConfLev 3.0f               // DLStopAtSetLevel: Confidence level for -'ve result (in units of sigma eg. 2.0 means 95% conf.level)
#define DLPosConfLev 2.0f               // DLStopAtSetLevel: Confidence level for +'ve result (in units of sigma eg. 2.0 means 95% conf.level)
#define DLNomSecs 5.0f                 // DLFixedTime: nominal measurement time
#define DLAccuracy 0.03f               // DLStopAtStatistics: accuracy of instrument for infinite scan (expressed as fraction of concentration reading)

// File names

// file that contains the description of the calibration samples and calibration details
#define UserParamFile "userParam.txt"
#define CalibBaseDir "/sdcard/pf"
#define LeadPaintDir "/lp"
#define SoilsDir "/sl"
#define ISSoilsDir "/is"
#define TSSoilsDir "/ts"
#define DustWipeDir "/dw"
#define CalibParamFile "/calibParam.txt"
#define ConfigParamFile "/configParam.txt"
// file that contains the calibration factors for the device (updated each time a service calibration is performed)
#define SysCalibSvc "/sysCalibS.txt"
// file that contains the calibration factors for the device (updated each time a factory calibration is performed)
#define SysCalibFac "/sysCalibF.txt"
#define SysCalibFacEnc "/sysCalibF.enc"
#define SysConfigSvc "/sysConfigS.txt"
#define SysConfigFac "/sysConfigF.txt"
#define LogFileName "/logfile.txt"
#define TempFileRoot "/Temp"
#define RefTempRoot "/rtemp"
#define AccumSpecFile "/accumSpec.txt"   // name of file that contains rolling series of spectra (FIFO) to be used for cumulative spectra

// file name root for cumulative calibration mode spectrum files
//e.g. "cspec" creates the files cspec00n.txt
#define CalibSpecRoot "cspec"

// HBI detection parameters
#define DefPbSNR 3.0f           // 3.0
#define MinPbCounts 10.0f        // 10
#define MinSpecCounts 100.0f     // 100

// file name root for measurement mode spectrum files
// e.g. "spec" creates the files spec00n.txt
#define MeasSpecRoot "spec"
#define CalibProgress "/calibProgress.txt"
#define CalibSamplesRoot "/sdcard/CalData/"
#define CalibBadEcal "/sdcard/CalData/badEcal.txt"
#define SysCalibLog "/sysCalibLog.txt"
#define SysCalibCorFac "/sysCalibCorrFactors.txt"

// general parameters
#define NumElements  5
#define MaxNumBins 512              // Max number of energy bins in an input spectrum file
#define MaxLinesPerElement 10       // Max number of fluorescence lines per element
#define NumLinesPerElement 7        // Total number of fluorescence lines per element used
#define NumLLinesPerElement 2       // Number of L lines used
#define NumKLinesPerElement 5       // Number of K lines used

// output spectrum files
#define OutSpecNumBins 512              // Number of bins in output spectrum files
#define OutSpecLoKeV   0.0f             // Lo keV for output spectrum files
#define OutSpecHiKeV 130.0f             // Hi keV for output spectrum files
#define OutSpecBgndSubMode 1            // 1: Display background subtracted spectrum across entire energy range
// 2: Display background subtracted spectrum only in peak ROIs (set negative counts to zero)

// instrument error detection parameters
#define DoSpectrumCheck true            // check spectrum ecal and template fitting [true]

#define MaxAllowDeadTime 75.0f          // max allowed dead time for fresh source [75.0]
#define MinSkipCounts  1000             // skip spectrum in calibration mode if total spectrum counts are below this threshold
#define MaxBinCounts 1E5                // set bin counts to zero if they exceed this number (used to detect bins with abnormal counts)

// energy calibration parameters
//#if PB_SETTINGS
//#define DataSmoothSize 2              // Kernal size for smoothing input data (+/- DataSmoothSize) [2]
//#define PCalibElementID W             // Primary Element to calibrate on (for gain determination) [W]
//#define DetResLoError 0.8f            // Lower error level for measured detector resolution (percent @100keV)
//#define MaxGainDrift 3.0f             // max percentage gain drift from factory default setting for locating calibration peaks [3.0]
//#define GainDriftNull 2.5f            // max percentage gain drift from factory default setting for gain from ecal to be used
//#define DefaultEnergyRes 2.5f         // Default percent energy resolution at 100keV (0.0123)
//#define NumFitIterFac  5000           // Number of iterations used in fitPeaksBgnd routine [5000]
//#define PerformEnergyCal true         // false: use default gain/offset values true: fit calibration peaks and update energy calibration
//#define DetResHiError 10.0f              // Upper error level for measured detector resolution (percent @100keV)

//#else
//#define DataSmoothSize 0              // Kernal size for smoothing input data (+/- DataSmoothSize) [0]
//#define PCalibElementID Ba            // Primary Element to calibrate on (for gain determination) [Ba]
//#define DetResLoError 2.0f            // Lower error level for measured detector resolution (percent @100keV)
//#define MaxGainDrift 20.0f            // max percentage gain drift from factory default setting for locating calibration peaks [3.0]
//#define GainDriftNull 5.0f            // max percentage gain drift from factory default setting for gain from ecal to be used
//#define DefaultEnergyRes 3.5f         // Default percent energy resolution at 100keV (0.0123)
//#define NumFitIterFac  2500           // Number of iterations used in fitPeaksBgnd routine [5000]
//#define PerformEnergyCalHBI false     // same as above but false for HBI
//#define DetResHiError 5.0f              // Upper error level for measured detector resolution (percent @100keV)

//#endif

#define DetResHiWarn 5.0f             // Max increase in resolution over value stored in system calibration file

#define UseTemplateGain true            // false: use gain determined from energy calibration true: use gain determined from stretching fitted templates
#define NumCalibPeaks 1                 // Number of peaks to calibrate on
#define MaxCalibPeaks 10                // Max number of peaks to calibrate on
#define SCalibElementID Ba              // Secondary Element to calibrate on (for offset determination) [Ba]
#define NumCumSpectra 5                 // number of spectra (measure mode) to combine to create cumulative spectra for energy calibration [5]
#define NumCalibCumSpectra 1            // number of spectra (calib mode) to combine to create cumulative spectra for energy calibration [1]
#define MaxSpecAge 2.0                  // maximum age (in minutes) for including previous spectra in cumulative spectrum for energy calibration
#define MaxBrdTempDiff 5.0              // maximum % temp diff of DPP board for including previous spectra in cumulative spectrum for energy calibration
#define MaxDetTempDiff 5.0              // maximum % temp diff of detector for including previous spectra in cumulative spectrum for energy calibration

// operational parameters
#define ErrNumDecimalPlace 1            // number of decimal places retained for rounding error to make pos/neg decision (-1 turns off any rounding)

// analysis parameters
#define NumAnalyzeElements 1            // Number of elements to analyze
#define MaxNumAnalyzeElements 5         // Max number of elements to analyze
#define MaxNumAnalyzePeaks 4            // Max number of peaks to analyze for each analysis elements

// twin peak fit routine parameters
#define MaxFitPoints 700                // Max number of bins used to fit a peak
#define MaxFitParams 10
#define NumFitParams 6                 // Number of parameters to vary
#define NumPeakIterFac 2500            // Number of fit iterations per fit parameter being varied in fitTwinPeaks routine [1500]
#define NumPeakIterFacCalib 15000      // Number of fit iterations per fit parameter being varied [5000]

// fitPeaksBgnd fit routine parameters
#define MaxBinFitDelta 10               // Max number of bins centroid is allowed to vary from calculated centroid
#define MaxFitWidthFac 2.0             // Max multiplier factor for peak width in fit over calculated width

// parameters for fitting the Compton backgound peak
#define PeakExcludeWidth 2.00f         // Number of times wider than FWHM for excluding peaks when template fitting (used in setBinStatus) [2.00]
#define BgndPeakFitLoKeV  78.0f        // lower limit for fitting Compton background (substrate only)
#define BgndPeakFitHiKeV 115.0f        // Upper limit for fitting Compton background (substrate only)
#define NumSumBgndBins 5               // Used to define size of moving window for finding starting peak centroid
#define BgndOffsetSigmaFac 1.25f       // Determines the upper limit of the flat region used to fit the offset
#define MinDistBgndPeak 2.0f           // Min distance from Compton background peak for analysis peaks to be be analyzed correctly

// parameters for detecting absence of substrate
#define MaxAirCntRate 5500             // Max count rate in Compton peak per nominal source second for an air scan [18000]
#define MaxAirCntRateRTG 2800          // Max count rate in Compton peak per nominal source second for an air scan [18000]

// DPP parameters
#define DefFastPeakTime 100             // Default fast peak time (ns)

// parameters for plotting spectra
#define LoPlotKeV 50.0f
#define HiPlotKeV 120.0f

#endif // PEAKFITPROTOTYPES_H
