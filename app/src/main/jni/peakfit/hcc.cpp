#include "hcc.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include "Calib.hpp"
#include "PeakFit.hpp"
#include "PeakFitPrototypes.hpp"
#include "Spectrum.hpp"
#include "hac.hpp"


extern DppSpectrum dppSpec;

extern Spectrum *spectrum;
extern Calib calib;
extern PeakFit *peakFit;

bool hccInit( int mode) {
    int nSamples;
    //bool status;
    bool retVal = false;

    spectrum->reset();
    calib.reset();

    ALOGI(" hccInit: Called - %d", mode);

    // tell fitTemplates to only fit background template that matches calibration sample
    spectrum->fitMode = 1;

    // assign default gain and offset (overwritten by readSysConfig if config file exists)
    spectrum->defKGain = DefaultKGain * SpecBinMode;
    spectrum->defKOffset = DefaultKOffset;
    spectrum->k_gain = spectrum->defKGain;
    spectrum->k_offset = spectrum->defKOffset;

    // read calibration parameter file
    int analyticMode = 0; // always use lead paint calib params file.
    nSamples = readCalibParams(calib, mode, analyticMode );
    if (nSamples > 0) retVal = true;

    // default value of instrument current source strength is calculated from assay info in calibParam file
    spectrum->srcMCi = calib.srcMCi;
    spectrum->srcAssayDay = calib.srcAssayDay;
    spectrum->srcAssayMonth = calib.srcAssayMonth;
    spectrum->srcAssayYear = calib.srcAssayYear;

    // read config file if it exists, and update default values
    readSysConfig(*spectrum, analyticMode);

    // for calibration mode, reset the source strength to the calibration source strength when spectra were acquired
    spectrum->srcMCi = calib.srcMCi;

    // for calibration mode, calculate default gain and offset values and update defaults
    quickEnergyCal(*spectrum, Ba, ka1, Pb, ka1);

    return retVal;
}

void hcc(const DppSpectrum &dppSpec,
         Spectrum &spectrum,
         Calib &calib,
         int analyticMode,
         meas_mode measMode,
         int calibMode) {

    //  calibMode=1: Service calibration mode
    //  calibMode=2: Factory calibration mode
    //
    //
    //  Loop over all calibration samples and measurements
    //  Use the sample templates created by acquireCalibSpectra()
    //  Find the best fit to a blank sample template for each measurement and use for background subtraction
    //  Calculate the calibration factors and standard errors and write them out to the system calibration file
    //
    // Notes on background templates:
    //
    //      A template (temp***.txt) is created for each calibration sample. However, only templates made from blank samples are used
    //      by fitSampTemplates() to perform background subtraction. However, when the system attempts to identify which calibration
    //      sample is being used, it uses all templates to determine substrate type and element concentration level.
    //

    int i, j, m, jmax, mode, ms, n, nSamples = 0, nGood;
    int badCnt = 0, badCntList[NumSubstrates] = {0}, avCnt = 0, nAccSpec, rstatus;
    int nSubType;
    element_id elemID = Pb;
    float avVal, cfDiff, cfDiffMax;
    float totLiveTime, SD;
    float avGain = 0, avOffset = 0;

    //uint32_t binCnts[MaxNumBins];
    float totCntsAv[MaxCalibSamples];
    float kAlphaRawAv[MaxCalibSamples];
    float kAlphaRawCntAv[MaxCalibSamples];
    float kAlphaBgndAv[MaxCalibSamples];
    float kAlphaBgndRatioAv[MaxCalibSamples];
    float alphaBetaRatioAv[MaxCalibSamples];
    float kAlphaRawSD[MaxCalibSamples];
    float kAlphaRawCntSD[MaxCalibSamples];
    float kAlphaBgndSD[MaxCalibSamples];
    float alphaBetaRatioSD[MaxCalibSamples];
    float kCalibRawFacAv[MaxCalibSamples];
    float kCalibRatioFacAv[MaxCalibSamples];
    //float bgndPeakRawCntsAv[MaxCalibSamples];
    //float bgndPeakCentAv[MaxCalibSamples];
    //float bgndPeakFWHMAv[MaxCalibSamples];
    float mgcm2Av[MaxCalibSamples];
    float mgcm2SD[MaxCalibSamples];
    float error[MaxCalibSamples];
    float SDVal[NumSubstrates][MaxConcLevels];
    //float kWFluxSD[MaxCalibSamples];
    //float kWFluxAv[MaxCalibSamples];
    char fname[50];

    int tempTypeList[NumAnalyzeElements][MaxNumMeasure];
    float kAlphaRawList[NumAnalyzeElements][MaxNumMeasure];
    float kAlphaRawCntList[NumAnalyzeElements][MaxNumMeasure];
    float kAlphaBgndList[NumAnalyzeElements][MaxNumMeasure];
    float kAlphaBgndRatioList[NumAnalyzeElements][MaxNumMeasure];
    float mgcm2List[NumAnalyzeElements][MaxNumMeasure];
    float alphaBetaRatioList[NumAnalyzeElements][MaxNumMeasure];
    float kCalibRawFac;
    float totCntsList[MaxNumMeasure];

    //float kWFluxList[NumAnalyzeElements][MaxNumMeasure];
    //float bgndPeakRawCntsList[MaxNumMeasure];
    //float bgndPeakFWHMList[MaxNumMeasure];
    //float bgndPeakCentList[MaxNumMeasure];

    int cnt, cnt2[MaxConcLevels];
    float mgcm2SDAv[MaxNumTemp] = {0};
    float mgcm2SDAv2[MaxNumTemp][MaxConcLevels] = {0};
    float calibFacAv[MaxNumTemp] = {0};
    float calibFacAv2[MaxNumTemp][MaxConcLevels] = {0};
    float calibRatioFacAv[MaxNumTemp] = {0};
    float calibRatioFacAv2[MaxNumTemp][MaxConcLevels] = {0};
    float medCalibRatioFac;
    time_t currentTime;

    bool includeSamp;
    //bool goodMatch;
    bool LogEachMeas = true;

    FILE *ofp, *ofp1, *ofp2, *sdfp;

    static SampTemplate sampTemp;
    //static SampTemplate refTemp;
    //static CrossSect crossSect;

    int mostFreqInt(int, int *);
    float averVal(int, float *);
    float median(int, float *, int);
    float stanDev(int, float *, int);
    float stanDevNoZero(int, float *, int);

    // open system calibration log file
    ofp = fopen(SysCalibLog, "a");
    if (ofp != NULL) {
        // print header to log file
        fprintf(ofp, "\n Instrument SN: %d\n", spectrum.serialNumber);
        fprintf(ofp, "\n Calibration spectra that have problems:\n");

        // only one element for Pb Analyzer
        elemID = Pb;
        //elemID = Ba;

        // number of calibration samples
        nSamples = calib.nSamples;

        // load templates
        peakFit->loadTemplates(sampTemp, 1, analyticMode);

        // read in and process all calibration spectra
        for (n = 0; n < nSamples; n++) {

            ALOGI(" hcc: Analyzing calibration sample %d", n + 1);
            ALOGI("   gain=%f offset=%f", spectrum.k_gain, spectrum.k_offset);

            // initialize total live time of all measurements for the calibration sample
            totLiveTime = 0;

            // suppress file name suffix
            if (calib.nMeasure[n] == 1) {
                if (SuppressSuffix) {
                    mode = 1;
                } else {
                    mode = 2;
                }
            } else {
                mode = 2;
            }

            // store calibration sample number in spectrum array (this is used by fitTemplates to fit the correct background template)
            spectrum.calibSamp = n;
            spectrum.calibSubType = calib.subType[n];

            // loop over measurements for sample
            m = 0;
            for (ms = 0; ms < calib.nMeasure[n]; ms++) {

                // only needed for off-line testing
                // create calibration file name and read in the spectrum file
                peakFit->calibFileName(fname, n, ms, mode);
                if (InputFileType == 1) {
                    rstatus = readMCAFile(fname);
                } else {
                    rstatus = peakFit->readPb200iFile(fname, SpecBinMode);
                }
                //ALOGI("hcc: Reading spectrum %s", fname);

                // skip "bad" measurements due to shutter malfunction
                if (rstatus == 0) {
                    ALOGI(" WARNING: Acquisition failed for %s", fname);
                    badCnt++;
                    badCntList[calib.subType[n]]++;
                    fprintf(ofp, "\n %3d) %25s (#%3d): WARNING: Acquisition Failed", badCnt,
                            calib.name[spectrum.calibSamp], n + 1);
                    continue;
                }

                if (spectrum.slowCnts < (100 - MaxAllowDeadTime) / 100 * spectrum.fastCorrCnts) {
                    ALOGI(" WARNING: Dead Time is too high for %s (%3.1f%%)", fname,
                          (1.0 - (float) spectrum.slowCnts / spectrum.fastCorrCnts) * 100);
                    badCnt++;
                    badCntList[calib.subType[n]]++;
                    fprintf(ofp, "\n %3d) %25s (#%3d): WARNING: Dead Time is too high (%3.1f%%)",
                            badCnt, calib.name[spectrum.calibSamp], n + 1,
                            (1.0 - (float) spectrum.slowCnts / spectrum.fastCorrCnts) * 100);
                }


                // initialize spectrum
                spectrum.initSpectrum(dppSpec );

                // increment total livetime over all measurements for the calibration sample
                totLiveTime += spectrum.liveTime;

                // use updated energy calibration from fitting cumulative spectra for each calibration sample (done in acquireCalibSpectra)
                if (spectrum.performEnergyCal) {
                    // accumulate spectrum counts for use in energy calibration
                    nAccSpec = accumSpecMem(NumCalibCumSpectra, spectrum, dppSpec);

                    // do energy calibration
                    if (nAccSpec > 0) {
                        if (calib.subType[n] == (BlkAirSubType - 1)) {
                            spectrum.energyCalSpec(2);
                        } else {
                            spectrum.energyCalSpec(1);
                        }
                        ALOGI(" hac: nAccSpec=%d TotCnts=%d gain=%f offset=%f", nAccSpec,
                              spectrum.totCnts, spectrum.k_gain, spectrum.k_offset);

                        // calculate average gain and offset over all calibration samples
                        avGain += spectrum.k_gain;
                        avOffset += spectrum.k_offset;
                        avCnt++;

                    }
                }

                // initialize elements
                spectrum.initializeElements();

                // fit peaks of elements to analyze
                peakFit->analyzeCounts(spectrum, sampTemp, calib, elemID, analyticMode, measMode);

                // print results for each measurement to log file
                if (LogEachMeas) {
                    if (calib.conc[spectrum.calibSamp] > 0.1) {
                        kCalibRawFac = (spectrum.element[elemID].kAlphaCnts) /
                                       (calib.conc[spectrum.calibSamp] -
                                        calib.conc[spectrum.tempType]) / spectrum.nomSecs;
                    } else {
                        kCalibRawFac = 0;
                    }
                    if ((kCalibRawFac > 0 and
                         (kCalibRawFac < MinCalibRawFac or kCalibRawFac > MaxCalibRawFac)) or
                        spectrum.calibSubType != spectrum.tempType) {
                        badCnt++;
                        badCntList[calib.subType[n]]++;
                        fprintf(ofp,
                                "\n %3d) %25s (#%3d): Conc=%3.1f subType=%3d bTemp=%3d scaleFac=%5.3f kCalibRawFac=%5d tempChisq=%4d gain=%5.3f",
                                badCnt, calib.name[spectrum.calibSamp], n + 1,
                                calib.conc[spectrum.calibSamp], spectrum.calibSubType + 1,
                                spectrum.tempType + 1, spectrum.tempNormFac, (int) kCalibRawFac,
                                (int) spectrum.tempChisq, spectrum.k_gain);
                    }
                }

                // store spectrum data from this measurement for later analysis
                kAlphaRawList[elemID][m] = spectrum.element[elemID].kAlphaRaw;
                kAlphaRawCntList[elemID][m] =
                        spectrum.element[elemID].kAlphaRaw - spectrum.element[elemID].kAlphaBgnd;
                kAlphaBgndList[elemID][m] = spectrum.element[elemID].kAlphaBgnd;
                kAlphaBgndRatioList[elemID][m] = spectrum.element[elemID].kAlphaBgndRatio;
                mgcm2List[elemID][m] = spectrum.element[elemID].mgcm2;
                tempTypeList[elemID][m] = spectrum.tempType;
                alphaBetaRatioList[elemID][m] = spectrum.element[elemID].alphaBetaRatio;
                totCntsList[m] = (float) spectrum.totCnts;

                // store background peak data from this measurement for later analysis
                //bgndPeakRawCntsList[m] = spectrum.bgndPeakRawCnts;
                //bgndPeakFWHMList[m] = spectrum.bgndPeakFWHMKeV;
                //bgndPeakCentList[m] = spectrum.bgndPeakCentKeV;

                // increment "good" measurement counter
                m++;

            } // end looping over measurements for sample

            // number of good measurements for sample
            nGood = m;
            calib.nGood[n] = nGood;

            // calculate average values for calibration sample
            kAlphaRawAv[n] = averVal(nGood, kAlphaRawList[elemID]);
            kAlphaRawCntAv[n] = averVal(nGood, kAlphaRawCntList[elemID]);
            kAlphaBgndAv[n] = averVal(nGood, kAlphaBgndList[elemID]);
            kAlphaBgndRatioAv[n] = averVal(nGood, kAlphaBgndRatioList[elemID]);
            alphaBetaRatioAv[n] = averVal(nGood, alphaBetaRatioList[elemID]);
            totCntsAv[n] = averVal(nGood, totCntsList);

            // calculate average Compton peak values for calibration sample
            //bgndPeakRawCntsAv[n] = averVal(nGood, bgndPeakRawCntsList);
            //bgndPeakFWHMAv[n] = averVal(nGood, bgndPeakFWHMList);
            //bgndPeakCentAv[n] = averVal(nGood, bgndPeakCentList);

            // calculate calibration factors for sample using average values over all good measurements
            if ((calib.conc[n] - calib.conc[spectrum.tempType]) > 0 && spectrum.nomSecs > 0) {
                kCalibRawFacAv[n] = (kAlphaRawAv[n] - kAlphaBgndAv[n]) /
                                    (calib.conc[n] - calib.conc[spectrum.tempType]) /
                                    spectrum.nomSecs;
                kCalibRatioFacAv[n] =
                        kAlphaBgndRatioAv[n] / (calib.conc[n] - calib.conc[spectrum.tempType]);
            } else {
                kCalibRawFacAv[n] = 0;
                kCalibRatioFacAv[n] = 0;
            }

            // now we have the average calibration factor, calculate mgcm2 values for each measurement so average and SD can be calculated
            for (m = 0; m < nGood; m++) {

                if (kCalibRawFacAv[n] > 0 && spectrum.nomSecs > 0) {

                    if (ALConcCalcMode == 1) {
                        mgcm2List[elemID][m] =
                                kAlphaRawCntList[elemID][m] / spectrum.nomSecs / kCalibRawFacAv[n];
                    } else {
                        mgcm2List[elemID][m] = kAlphaBgndRatioList[elemID][m] / kCalibRatioFacAv[n];
                    }

                } else {
                    mgcm2List[elemID][m] = 0;
                }
            }

            // now calculate average mgcm2 value for sample
            mgcm2Av[n] = averVal(nGood, mgcm2List[elemID]);

            // calculate *absolute* standard deviations for calibration sample
            kAlphaRawSD[n] = stanDev(nGood, kAlphaRawList[elemID], 2);
            kAlphaRawCntSD[n] = stanDev(nGood, kAlphaRawCntList[elemID], 2);
            kAlphaBgndSD[n] = stanDev(nGood, kAlphaBgndList[elemID], 2);
            alphaBetaRatioSD[n] = stanDev(nGood, alphaBetaRatioList[elemID], 2);

            // calculate standard deviations for measured concentration level
            mgcm2SD[n] = stanDev(nGood, mgcm2List[elemID], 2);

            // theoretical error just for comparison
            if (nGood > 0 and kAlphaRawCntAv[n] > 0) {
                error[n] = sqrt(kAlphaRawAv[n] + kAlphaBgndAv[n] / nGood) / kAlphaRawCntAv[n] * 100;
            }
            else {
                error[n] = 0;
            }

            if (FitAllBgndTemp) {
                // most "popular" match over all measurements for background template type
                // (use this for testing non-blank fitted backgrounds)
                calib.tempType[n] = mostFreqInt(nGood, tempTypeList[elemID]);
            } else {
                // Use this if only fitting blank background templates (assign the template type to the substrate type)
                calib.tempType[n] = calib.subType[n];
            }

            ALOGI(" Actual substrate: %d  Most Freq. Fit:  %d", calib.subType[n] + 1,
                  mostFreqInt(nGood, tempTypeList[elemID]) + 1);

            // print average results for calibration sample to calibration log file
            //if(calib.conc[n]>0.1)
            //{
            //  fprintf(ofp,"\n Sample %2d: %2d %2d %5.3f (%4.2f) %6.0f (%6.2f) %6.0f (%6.2f)  %6.0f (%6.2f)  %6.0f %6.2f %6.2f",
            //	 n+1, calib.subType[n]+1, calib.tempType[n]+1, mgcm2Av[n], mgcm2SD[n], kAlphaRawCntAv[n], kAlphaRawCntSD[n], kAlphaRawAv[n],
            //		   kAlphaRawSD[n], kAlphaBgndAv[n], kAlphaBgndSD[n], bgndPeakRawCntsAv[n], bgndPeakCentAv[n], bgndPeakFWHMAv[n]);
            //}

        } // end looping over calibration samples
        //fclose(ofp);

    }

    // average gain and offset over all calibration samples
    if (avCnt > 0) {
        avGain /= avCnt;
        avOffset /= avCnt;
    }

    // flag a problem with the blank substrate spectrum if more than one non-blank spectrum for that substrate have problems
    for (n = 0; n < nSamples; n++) {
        if (calib.conc[n] < 0.1 and badCntList[calib.subType[n]] > 1) {
            badCnt++;
            fprintf(ofp, "\n %3d) %25s (#%3d): Conc=%3.1f subType=%3d", badCnt, calib.name[n],
                    n + 1, calib.conc[n], calib.subType[n] + 1);
        }
    }

    // print average gain to log file
    fprintf(ofp, "\n\n *** Average gain: %5.3f ***\n\n", avGain);
    fclose(ofp);

    // background template files have been created and calibration factors can now be calculated

    // read default SD values (by substrate type) from file to use if SD's are zero
    sdfp = fopen(DefaultSDFile, "r");
    if (sdfp != NULL) {
        while (!feof(sdfp)) {
            fscanf(sdfp, "\n %d", &nSubType);
            for (j = 0; j < calib.nConcLevels; j++) {
                fscanf(sdfp, "%f", &SDVal[nSubType - 1][j]);
            }
        }
        fclose(sdfp);
    }

    // open output system calibration file and create new updated config file
    if (calibMode == 1) {
        ofp1 = fopen(SysCalibSvc, "w");
        ofp2 = fopen(SysConfigSvc, "w");
    }
    else {
        ofp1 = fopen(SysCalibFac, "w");
        ofp2 = fopen(SysConfigFac, "w");
    }

    if (ofp1 != NULL) {
        // write current time to system calibration file header
        time(&currentTime);
        fprintf(ofp1, "CalibDate %d", (int) currentTime);

        // write current source strength to calibration file header
        fprintf(ofp1, "\nSrcMCi %5.2f", calib.srcMCi);

        // write calibration source  assay information to config file header
        fprintf(ofp2, "SerialNumber %d", spectrum.serialNumber);
        fprintf(ofp2, "\nSrcAssayDate %d/%d/%d", spectrum.srcAssayMonth, spectrum.srcAssayDay,
                spectrum.srcAssayYear);
        fprintf(ofp2, "\nSrcAssayMCi %5.2f", spectrum.srcAssayMCi);

        // write new average gain and offset to config file header
        fprintf(ofp2, "\nGain %7.5f", avGain);
        fprintf(ofp2, "\nOffset %7.5f", avOffset);

        // print out average values for each calibration sample
        for (n = 0; n < nSamples; n++) {
            ALOGI(" Sample %d (%4.1f mg %s on %s):", n + 1, calib.conc[n],
                  elementName[calib.elemID[0]], subName[calib.subType[n]][0]);
            ALOGI("  %d good measurements out of %d", calib.nGood[n], calib.nMeasure[n]);
            ALOGI("  Total counts in spectrum = %d", (int) totCntsAv[n]);
            ALOGI("  mgcm2 = %5.3f (%4.3f) [error = %4.1f]", mgcm2Av[n], mgcm2SD[n], error[n]);
            ALOGI("  kCalibRawFacAv = %6.1f", kCalibRawFacAv[n]);
            ALOGI("  kAlphaRawCntAv = %7.0f (%5.1f)", kAlphaRawCntAv[n], kAlphaRawCntSD[n]);
            ALOGI("  kAlphaRawAv = %7.0f (%5.1f)", kAlphaRawAv[n], kAlphaRawSD[n]);
            ALOGI("  kAlphaBgndAv = %7.0f (%5.1f)", kAlphaBgndAv[n], kAlphaBgndSD[n]);
            ALOGI("  alphaBetaRatioAv = %5.3f (%3.2f)", alphaBetaRatioAv[n], alphaBetaRatioSD[n]);

        }

        // for each background template type, find averages over non-blank calibration samples matched to that background template
        for (i = 0; i < calib.nTemplates; i++) {
            // skip non-blank templates
            if (!FitAllBgndTemp and sampTemp.conc[i] > 0.1) continue;

            // initialize counters and arrays
            cnt = 0;
            for (j = 0; j < calib.nConcLevels; j++) cnt2[j] = 0;

            // loop over calibration samples
            for (n = 0; n < calib.nSamples; n++) {

                if (calib.tempType[n] == i) {

                    includeSamp = false;
                    if (FitAllBgndTemp) {
                        // Use this for fitting blank and non-blank background templates
                        if (calib.conc[i] < calib.conc[n]) includeSamp = true;
                    } else {
                        // Use this for fitting only non-blank background templates
                        if (calib.conc[n] >= MinCalibConc &&
                            calib.conc[n] <= MaxCalibConc)
                            includeSamp = true;
                    }

                    // values averaged for each substrate type
                    if (includeSamp) {
                        mgcm2SDAv[i] += mgcm2SD[n];
                        calibFacAv[i] += kCalibRawFacAv[n];
                        calibRatioFacAv[i] += kCalibRatioFacAv[n];
                        cnt++;
                    }

                    // values averaged for each substrate type and concentration level
                    j = calib.concID[n];
                    mgcm2SDAv2[i][j] += mgcm2SD[n];
                    calibFacAv2[i][j] += kCalibRawFacAv[n];
                    calibRatioFacAv2[i][j] += kCalibRatioFacAv[n];
                    cnt2[j]++;
                }

            }

            // calculate averages from sums
            for (j = 0; j < calib.nConcLevels; j++) {
                if (cnt2[j] > 0) {
                    mgcm2SDAv2[i][j] /= cnt2[j];
                    calibFacAv2[i][j] /= cnt2[j];
                    calibRatioFacAv2[i][j] /= cnt2[j];
                } else {
                    mgcm2SDAv2[i][j] = 0;
                    calibFacAv2[i][j] = 0;
                    calibRatioFacAv2[i][j] = 0;
                }
            }
            if (cnt > 0) {
                mgcm2SDAv[i] /= cnt;
                calibFacAv[i] /= cnt;
                calibRatioFacAv[i] /= cnt;
            } else {
                mgcm2SDAv[i] = 0;
                calibFacAv[i] = 0;
                calibRatioFacAv[i] = 0;
            }

            if (cnt > 0) ALOGI(" %3d: %5d %5.3f %5.3f %5.3f %5.3f",
                               i + 1, (int) calibFacAv[i], calibRatioFacAv2[i][1],
                               calibRatioFacAv2[i][2], calibRatioFacAv2[i][3],
                               calibRatioFacAv2[i][4]);


        } // end looping over templates

        // print out standard deviation of calibration factors for each substrate
        ALOGI(" SD of Calibration Factors (before outlier correction):");
        for (i = 0; i < calib.nTemplates; i++) {
            SD = stanDevNoZero(calib.nConcLevels, calibRatioFacAv2[i], 1);
            if (calib.conc[i] == 0 && SD > CalibFacDiffWarn) {
                ALOGI(" Substrate %2d: %4.1f%%", i + 1, SD);
            }
        }

        // check for outlier values
        if (DoOutlierCorr) {
            for (i = 0; i < calib.nTemplates; i++) {

                cfDiffMax = 0;
                jmax = 0;

                // find concentration level with calibration factor furthest from median
                medCalibRatioFac = median(calib.nConcLevels, calibRatioFacAv2[i], 2);
                for (j = 1; j < calib.nConcLevels; j++) {
                    if ((calibRatioFacAv2[i][j]) != 0) {
                        //cfDiff = 2 * fabs(calibRatioFacAv2[i][j]-calibRatioFacAv[i]) / (calibRatioFacAv2[i][j]+calibRatioFacAv[i]) * 100;
                        cfDiff = 2 * fabs(calibRatioFacAv2[i][j] - medCalibRatioFac) /
                                 (calibRatioFacAv2[i][j] + medCalibRatioFac) * 100;
                        if (cfDiff > cfDiffMax) {
                            cfDiffMax = cfDiff;
                            jmax = j;
                        }
                    }
                }

                // replace outlier values with average of non-zero positive remaining values
                if (cfDiffMax > CalibFacDiffWarn) {
                    avVal = 0;
                    avCnt = 0;
                    for (j = 1; j < calib.nConcLevels; j++) {
                        if (j == jmax) continue;
                        if (calibRatioFacAv2[i][j] > 0) {
                            avVal += calibRatioFacAv2[i][j];
                            avCnt++;
                        }
                    }
                    if (avCnt > 0) {
                        avVal /= avCnt;
                        ALOGI(" Substrate %2d: Replacing original value %5.3f for level %d with average value %5.3f",
                              i + 1, calibRatioFacAv2[i][jmax], jmax, avVal);
                        calibRatioFacAv[i] = avVal;
                        calibRatioFacAv2[i][jmax] = avVal;
                    }
                }
            }
        }

        // print out standard deviation of calibration factors for each substrate after outlier corrections
        ALOGI(" SD of Calibration Factors (after outlier correction):");
        for (i = 0; i < calib.nTemplates; i++) {
            SD = stanDevNoZero(calib.nConcLevels, calibRatioFacAv2[i], 1);
            if (calib.conc[i] == 0 && SD > CalibFacDiffWarn / 2) {
                ALOGI(" Substrate %2d: %4.1f%%", i + 1, SD);
            }
        }

        // print out values to calibration file
        for (i = 0; i < calib.nTemplates; i++) {

            // if fitting all templates, skip templates with no non-zero calibration factors
            if (FitAllBgndTemp and calibRatioFacAv[i] == 0) continue;

            // skip non-blank templates
            if (!FitAllBgndTemp and sampTemp.conc[i] > 0.1) continue;


            // write calibration results for each blank background template type to system calibration file
            // Note: elemID and tempType are stored in sysCalib.txt as n+1 (tempType in file then matches the template files that start as bgnd001.txt)
            //  but they are converted back to n when sysCalib.txt is read and the values are stored in the arrays)
            fprintf(ofp1, "\n %2d %2d %6.3f %7.1f %7.4f", elemID + 1, i + 1, calib.ecalFWHM[i],
                    calibFacAv[i], calibRatioFacAv[i]);

            // print out average calibration ratio factors for each concentration (except for zero)
            for (j = 1; j < calib.nConcLevels; j++) {
                fprintf(ofp1, " %7.4f", calibRatioFacAv2[i][j]);
            }

            // print out average SD values for each concentration (multiplied by correction factor)
            for (j = 0; j < calib.nConcLevels; j++) {
                //if(mgcm2SDAv2[i][j]==0) mgcm2SDAv2[i][j] = SDVal[calib.subType[i]][j];
                if (mgcm2SDAv2[i][j] == 0) {
                    fprintf(ofp1, " %6.2f", DefaultMgcm2SD);
                } else {
                    fprintf(ofp1, " %6.2f", mgcm2SDAv2[i][j] * SDCorrFactor);
                }
            }

            //ALOGI(" Template %2d: Av=%5.1f (%5.1f %5.1f %5.1f %5.1f) Av=%5.2f (%5.2f %5.2f %5.2f %5.2f %5.2f)", i+1,
            //		     calibFacAv[i], calibFacAv2[i][1], calibFacAv2[i][2], calibFacAv2[i][3], calibFacAv2[i][4],
            //		     mgcm2SDAv[i], mgcm2SDAv2[i][0],mgcm2SDAv2[i][1],  mgcm2SDAv2[i][2],  mgcm2SDAv2[i][3],  mgcm2SDAv2[i][4]);


        } // end looping over blank templates (2nd loop)

        // print out average energy calibration values
        ALOGI(" Average gain:   %f", avGain);
        ALOGI(" Average offset: %f", avOffset);

        fclose(ofp1);
        fclose(ofp2);

    }
}

void outputSpectrum(unsigned int *binCnts, int numBins, int fileNum, float offset, float gain,
                    int mode) {

    //
    // Writes out a normalized spectrum using the counts stored in the array binCnts.
    //
    //     Inputs:
    //          binCnts: pointer to array that contains the counts for the output spectrum
    //          numBins: number of bins of input data
    //          fileNum: file number to be encoded into output spectrum file name
    //          totLiveTime: total livetime corresponding to the input counts
    //          offset: offset of input spectrum contained in binCnts
    //          gain: gain of input spectrum contained in binCnts
    //
    //          mode=1: File name root defined by CalibSpecRoot
    //          mode=2: File name root defined by MeasSpecRoot
    //

    int i, spec_bin, specBinInt;
    unsigned int outBinCnts[OutSpecNumBins];
    float energy[OutSpecNumBins], value;
    float specBinFrac, outSpecGain;
    char fname[50];
    char str[5];

    FILE *ofp;

    // create the output cumulative spectrum file name

    strcpy(fname, CalibSamplesRoot);
    if (mode == 1) {
        strcat(fname, CalibSpecRoot);
    } else {
        strcat(fname, MeasSpecRoot);
    }

    // have file names be numbered 1, 2, ... etc
    fileNum++;

    if (fileNum < 10) {
        strcat(fname, "00");
    } else if (fileNum < 100) {
        strcat(fname, "0");
    }
    sprintf(str, "%d", fileNum);
    strcat(fname, str);
    strcat(fname, ".txt");

    ofp = fopen(fname, "w");
    if (ofp == NULL) {
        ALOGI(" Cannot open calibration file %s", fname);
    }


    // gain of current spectrum may be different from default output file gain, so first need to rebin spectrum
    outSpecGain = (OutSpecHiKeV - OutSpecLoKeV) / OutSpecNumBins;

    // interpolate between spectrum bins to create rebinned output spectrum
    for (i = 0; i < OutSpecNumBins; i++) {

        // energy of putput spectrum bin
        energy[i] = OutSpecLoKeV + outSpecGain * i;

        // corresponding bin in current spectrum
        spec_bin = (int) ((energy[i] - offset) / gain);
        if (spec_bin < 0) spec_bin = 0;
        if (spec_bin >= MaxNumBins) spec_bin = MaxNumBins - 1;

        // integer and bin fraction for interpolation
        specBinInt = spec_bin;
        specBinFrac = spec_bin - specBinInt;

        // interpolate value between current spectrum bins
        if ((specBinInt + 1) < MaxNumBins) {
            value = (float) (((1.0 - specBinFrac) * binCnts[specBinInt] +
                                 specBinFrac * binCnts[specBinInt + 1]) * outSpecGain / gain);
        } else {
            value = 0;
        }
        //ALOGI(" specBin=%d tempBin=%f specEnergy=%f specGain=%f tempGain=%f value=%f", i, temp_bin, spec_energy, spectrum.k_gain, sampTemp.gain[n], value);
        if (i < MaxNumBins) outBinCnts[i] = (unsigned int) int(value + 0.5);

    }

    for (i = 0; i < OutSpecNumBins; i++) {
        fprintf(ofp, "\n%8.2f %d", energy[i], outBinCnts[i]);
    }

    fclose(ofp);

}

int buildTemplate(Calib &calib, int nSample, unsigned int *binCnts, int numBins, float totLiveTime,
                  float lo_keV, float hi_keV, float gain, float offset) {
    // Writes out the counts stored in the array binCnts to create a template. Templates made from blank calibration samples can be
    // be matched to a spectrum, normalized, and then subtracted
    //
    // Returns the number of templates written out so far
    //

    int cnt, i, lo_bin, hi_bin, nbins;
    int peakRawCnts;
    unsigned int sum, imax = 0, maxCnts = 0;
    float peakKeV;
    float totNomSecs;
    char fname[50];
    char str[5];

    time_t currentTime;

    cnt = nSample;

    FILE *ofp;

    // create the output background template file name
    strcpy(fname, CalibBaseDir);
    strcat(fname, TempFileRoot);

    if (cnt + 1 < 10) {
        strcat(fname, "00");
    } else if (cnt + 1 < 100) {
        strcat(fname, "0");
    }
    sprintf(str, "%d", cnt + 1);
    strcat(fname, str);
    strcat(fname, ".txt");

    ofp = fopen(fname, "w");
    ALOG(" Creating calibration file %s", fname);
    if (ofp == NULL) {
        ALOG(" Cannot open calibration file %s", fname);
    }

    totNomSecs = spectrum->liveTime;

    // find energy of Compton peak centroid for template
    sum = 0;
    lo_bin = int((BgndPeakFitLoKeV - offset) / gain + 0.5);
    hi_bin = int((BgndPeakFitHiKeV - offset) / gain + 0.5);
    for (i = lo_bin; i <= hi_bin; i++) {
        if (binCnts[i] > maxCnts) {
            maxCnts = binCnts[i];
            imax = (unsigned int) i;
        }
        sum += binCnts[i];
    }
    peakKeV = offset + imax * gain;
    peakRawCnts = int((float) sum / totNomSecs);

    // find bin limits for template
    lo_bin = int((lo_keV - offset) / gain + 0.5);
    hi_bin = int((hi_keV - offset) / gain + 0.5);

    // check bin limits are in range
    if (lo_bin < 0) {
        lo_bin = 0;
        lo_keV = offset;
    }
    if (hi_bin >= MaxNumBins) {
        hi_bin = MaxNumBins - 1;
        hi_keV = offset + hi_bin * gain;
    }

    nbins = hi_bin - lo_bin + 1;
    //ALOGI(" buildSampTemplate: Template Bin range: %d - %d", lo_bin, hi_bin);

    // formatted template files
    if (FormattedTemplates) {

        // write current time to template header
        time(&currentTime);
        fprintf(ofp, "%d", (int) currentTime);

        // write concentration to header
        fprintf(ofp, "\n%f", calib.conc[nSample]);

        // write template header
        fprintf(ofp, "\n%d %d %f %f %f %f %d %f %f %d", nbins, lo_bin, lo_keV, hi_keV, gain, offset,
                calib.subType[nSample], totNomSecs, peakKeV, peakRawCnts);

        // write bin contents
        for (i = lo_bin; i <= hi_bin; i++) {
            fprintf(ofp, "\n%d", binCnts[i]);
        }

        // binary template files
    } else {

        // write current time to template header
        time(&currentTime);
        fwrite(&currentTime, sizeof(int), 1, ofp);

        // write concentration to header
        fwrite(&calib.conc[nSample], sizeof(float), 1, ofp);

        // write template header
        fwrite(&nbins, sizeof(int), 1, ofp);
        fwrite(&lo_bin, sizeof(int), 1, ofp);
        fwrite(&lo_keV, sizeof(float), 1, ofp);
        fwrite(&hi_keV, sizeof(float), 1, ofp);
        fwrite(&gain, sizeof(float), 1, ofp);
        fwrite(&offset, sizeof(float), 1, ofp);
        fwrite(&calib.subType[nSample], sizeof(int), 1, ofp);
        fwrite(&totNomSecs, sizeof(float), 1, ofp);
        fwrite(&peakKeV, sizeof(float), 1, ofp);
        fwrite(&peakRawCnts, sizeof(int), 1, ofp);

        // write bin contents
        for (i = lo_bin; i <= hi_bin; i++) {
            fwrite(&binCnts[i], sizeof(int), 1, ofp);
        }

    }

    fclose(ofp);

    cnt++;

    return cnt;

}

int quickEnergyCal(Spectrum &spectrum, int elemID1, int lineID1, int elemID2, int lineID2) {

    // Performs a quick energy calibration using a spectrum containing
    // lineID1 of elemID1 and lineID2 of elemID2.

    int n, rstatus;
    int loBin[2], hiBin[2];
    int loPeakBin = 0, hiPeakBin = 0;
    float loKeV, hiKeV;
    float gain, offset;
    unsigned int maxCnts;

    extern double lineKeV[NumElements][NumLinesPerElement];
    //DppSpectrum dppSpec;

    rstatus = peakFit->readPb200iFile(ECalCalibSample, SpecBinMode);

    // if file exists and has good data, calulate gain and offset
    if (rstatus == 1) {

        loKeV = (float) lineKeV[elemID1][lineID1];
        hiKeV = (float) lineKeV[elemID2][lineID2];

        // find energy range for looking for low energy peak (use default gain and offset)
        loBin[0] = int((loKeV - ECalSearchRange - spectrum.defKOffset) / spectrum.defKGain + 0.5);
        loBin[1] = int((loKeV + ECalSearchRange - spectrum.defKOffset) / spectrum.defKGain + 0.5);

        // find energy range for looking for high energy peak (use default gain and offset)
        hiBin[0] = int((hiKeV - ECalSearchRange - spectrum.defKOffset) / spectrum.defKGain + 0.5);
        hiBin[1] = int((hiKeV + ECalSearchRange - spectrum.defKOffset) / spectrum.defKGain + 0.5);


        // find bin with highest counts in search range for low energy peak
        maxCnts = 0;
        for (n = 0; n < MaxNumBins; n++) {
            if (n > loBin[0] and n < loBin[1] and dppSpec.spectrum_[n] > maxCnts) {
                maxCnts = dppSpec.spectrum_[n];
                loPeakBin = n;
            }
        }

        // find bin with highest counts in search range for high energy peak
        maxCnts = 0;
        for (n = 0; n < MaxNumBins; n++) {
            if (n > hiBin[0] and n < hiBin[1] and dppSpec.spectrum_[n] > maxCnts) {
                maxCnts = dppSpec.spectrum_[n];
                hiPeakBin = n;
            }
        }

        // calculate gain and offset values
        gain = (hiKeV - loKeV) / (hiPeakBin - loPeakBin);
        offset = loKeV - loPeakBin * gain;

        // update default values
        spectrum.defKGain = gain;
        spectrum.defKOffset = offset;

        // for now, assign gain and offset default values
        spectrum.k_gain = gain;
        spectrum.k_offset = offset;

        //for(n=0; n<MaxNumBins; n++) {
        //  ALOG("Bin %d: %d", n, (int)dppSpec.spectrum_[n]);
        //}

        ALOG("quickEnergyCal: loBin1=%d loBin2=%d hiBin1=%d hiBin2=%d", loBin[0], loBin[1],
             hiBin[0], hiBin[1]);
        ALOG("quickEnergyCal: gain=%f offset = %f loKeV=%f hiKeV=%f loBin=%d hiBin=%d", gain,
             offset, loKeV, hiKeV, loPeakBin, hiPeakBin);

    }

    return (rstatus);
}

float calcFluxFactor(Spectrum &spectrum, CrossSect &crossSect) {

    // calculates the cross section weighted flux above the line edge

    int i, n;
    int hiBin, edgeBin;
    int csBin;
    float energy, wFlux = 0.0f;

    for (n = 0; n < NumElements; n++) {

        // check that gain has been set
        if (spectrum.k_gain <= 0) break;

        // spectrum bin corresponding to first bin above edge
        edgeBin = int((crossSect.edgeEnergy[n] - spectrum.k_offset) / spectrum.k_gain + 0.5);
        if (edgeBin < 0) edgeBin = 0;
        if (edgeBin >= MaxNumBins) edgeBin = MaxNumBins - 1;

        // spectrum bin corresponding to last bin in cross section file
        hiBin = int((crossSect.hiEnergy[n] - spectrum.k_offset) / spectrum.k_gain + 0.5);
        if (hiBin >= MaxNumBins) hiBin = MaxNumBins - 1;

        //ALOGI(" calcFluxFactor: edgeBin=%d hiBin=%d", edgeBin, hiBin);

        wFlux = 0;
        for (i = edgeBin; i <= hiBin; i++) {
            // find closest corresponding bin in cross section
            energy = spectrum.k_offset + i * spectrum.k_gain;
            csBin = int((energy - crossSect.loEnergy[n]) / crossSect.dEnergy[n] + 0.5);
            if (csBin < 0) csBin = 0;
            if (csBin >= crossSect.nBins[n]) csBin = crossSect.nBins[n];
            if (csBin >= NumCSBins) csBin = NumCSBins - 1;

            // calculate weighted flux
            if (energy > crossSect.edgeEnergy[n]) {
                wFlux += spectrum.counts[i] * crossSect.cs[n][csBin];
                //ALOGI(" calcFLuxFac: specBin=%d specCnts=%d energy=%f csBin=%d cs=%f wFlux=%f", i, (int)spectrum.counts[i], energy, csBin, crossSect.cs[n][csBin], wFlux);
            }

        }
        spectrum.element[n].kWFlux = wFlux / WFluxNormFac;
    }

    return wFlux;
}
