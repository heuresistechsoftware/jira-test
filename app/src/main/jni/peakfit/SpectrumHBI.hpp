//
// Created by AndyHanks on 2/1/2019.
//

#ifndef HBI120_PB_SPECTRUMHBI_H
#define HBI120_PB_SPECTRUMHBI_H

#include "Spectrum.hpp"

class SpectrumHBI : public Spectrum {
public:
    SpectrumHBI();

protected:
    void setSystemTypeConsts();
};
#endif //HBI120_PB_SPECTRUMHBI_H
