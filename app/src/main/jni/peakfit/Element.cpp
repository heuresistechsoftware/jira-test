#include "Element.hpp"

#include <cmath>
#include <cstring>

#include "PeakFitResults.hpp"


void Element::setName(int id) {
    strcpy(name, elementName[id]);
}

void Element::calcConc(float srcMCi, float calibSrcMCi, float nomSecs, int lineID, int concID) {
    // calculates measured concentration of element

    // if specific calibration factor is zero, use first non-zero value for lower concentrations
    if (kCalibRatioFac[concID] == 0 and concID > 0) {
        while (concID > 0) {
            concID--;
            if (kCalibRatioFac[concID] > 0) {
                break;
            }
        }
    }

    if (lineID==ka1 or lineID==ka2) {
        if (ALConcCalcMode == 1) {
            if (nomSecs > 0 and kCalibRawFac > 0) {
                mgcm2 = kAlphaCnts / nomSecs / kCalibRawFac + tempConc;
            } else {
                mgcm2 = 0;
            }
        } else {
            if (kCalibRatioFac[concID] > 0) {
                ALOG("calcConc Ka1 or Ka2 ConcId = %d", concID);
                mgcm2 = kAlphaBgndRatio / kCalibRatioFac[concID] + tempConc;
                ALOG("calcConc mgcm2 = %f", mgcm2);
            } else {
                mgcm2 = 0;
            }
        }
    } else if (lineID==kb1 or lineID==kb2) {
        if (ALConcCalcMode == 1) {
            if (nomSecs > 0 and kCalibRawFac > 0) {
                mgcm2 = kBetaCnts / nomSecs / kCalibRawFac + tempConc;
            } else {
                mgcm2 = 0;
            }
        } else {
            if (kCalibRatioFac[concID] > 0) {
                ALOG("calcConc Kb1 or Kb2 ConcId = %d", concID);
                mgcm2 = kBetaBgndRatio / kCalibRatioFac[concID] + tempConc;
                ALOG("calcConc mgcm2 = %f", mgcm2);
            } else {
                mgcm2 = 0;
            }
        }
    }
    //ALOG("calcConc: tempConc=%f", tempConc);
}

void Element::calcDetermAL(float srcMCi, float calibSrcMCi, float nomSecs, float liveTime,
                           float actionLevel, float negConfLev, float posConfLev,
                           meas_mode measMode, bool RTGPresent) {

    // calculate results for Action Level mode

    float measDiff, measSD, scaleTOut, negConc = 0.0f;

    // scale timeout time if RTG is present due to less flux
    if (RTGPresent) {
        scaleTOut = 2.0;
        if (actionLevel == 1.0) {
            actionLevel = 0.9;
            negConc = ALTOutMaxNegConcRTG; // adjust max neg conc for lower action level
        }
    } else {
        scaleTOut = 1.0;
        negConc = ALTOutMaxNegConc;
    }

    // reset negative confidence level if nominal time is greater than timeout time
    if (nomSecs >= scaleTOut * ALTOutNomSecs) {
        negConfLev = posConfLev;
    }

    // difference between action concentration and measurement
    measDiff = fabs(actionLevel - mgcm2);

    // scale measurement standard deviation for live time of current measurement and source decay since calibration
    // (remember that mgcm2SD contains the SD for a 1 second measurement with the source strength at the last calibration)
    if (liveTime > 0) {
        // increase SD more than Poission statistics for source aging
        measSD = mgcm2SD / sqrt(liveTime) / pow((srcMCi / calibSrcMCi), 0.75f);
    } else {
        measSD = 10.0f;
    }

    // if RTG is present, increase SD by sqrt(3)
    if (RTGPresent) measSD *= 1.73;

    // store the actual measurement SD as the error
    error = measSD;

    // confidence level of current measurement result relative to action level
    if (measSD > 0) {
        kConfLev = measDiff / measSD;
    } else {
        kConfLev = 1000;
    }

    ALOG("calcDetermAL: measSD=%f, measDiff=%f kConfLev=%f negConfLev=%f posConfLev=%f", measSD,
         measDiff, kConfLev,
         negConfLev, posConfLev);

    // check if decision has been reached or still "In Process"
    if (actionLevel == 0.5f && nomSecs < MinNomSecs_05) {

        elemResult = dInProcess;

    } else if(actionLevel == 0.7f && nomSecs < MinNomSecs_05) {

        elemResult = dInProcess;

    } else if (mgcm2 < actionLevel && kConfLev >= negConfLev) {

        elemResult = dNegative;
        ALOG("Result is Negative");



        // call positive if nominal measurement time is greater than timeout time and concentration above set level
    } else if (nomSecs >= scaleTOut * ALTOutNomSecs && mgcm2 >= ( actionLevel - negConc)) {

        ALOG("actionLevel = %f mgcm2 = %f kconf = %f posConfLev = %f", actionLevel, mgcm2, kConfLev, posConfLev);
        if ((actionLevel == 0.5f || actionLevel == 0.7f) &&
            (mgcm2 >= (actionLevel - negConc))          &&
            (mgcm2 <= (actionLevel + negConc))) {

            elemResult = dInconclusive;
            ALOG("Result is Inconclusive");

        } else {
            elemResult = dPositive;  // equal to actionLevel is positive for all other non HUD settings.
        }

        if (measMode == AL && mgcm2 < actionLevel) mgcm2 = actionLevel;

        ALOG("Timeout 1");

        // end measurement if nominal measurement time is greater than timeout time and concentration below or equal to set level
    } else if (mgcm2 >= actionLevel && kConfLev >= posConfLev) {

        elemResult = dPositive;
        ALOG("Result is Positive");

    }else if (nomSecs >= scaleTOut * ALTOutNomSecs && mgcm2 < ( actionLevel - negConc)) {

        elemResult = dNegative;

        ALOG("Timeout 2 - mgmc=%f actionLevel=%f ", mgcm2, ( actionLevel - negConc));
        // more time needed for decision
    } else {
        elemResult = dInProcess;
        ALOG("In Process");
    }

  }

//void Element::calcDetermDL(float elapsedDays, float nomSecs, float liveTime, float actionLevel, float negConfLev, float posConfLev, float dLNomSecs, meas_mode measMode)
void Element::calcDetermDL(float srcMCi, float calibSrcMCi, float nomSecs, float liveTime,
                           float actionLevel, float negConfLev, float posConfLev, float dLNomSecs,
                           meas_mode measMode) {

    // calculate results for Detection Level modes

    float measDiff, measSD;
    //float ln2=0.693147;

    // scale measurement standard deviation for live time of current measurement and source decay since calibration
    // (remember that mgcm2SD contains the SD for a 1 second measurement with the source strength at the last calibration)
    if (liveTime > 0) {
        //measSD = mgcm2SD / sqrt(liveTime * exp(- elapsedDays * ln2 /SourceHalfLife));
        measSD = mgcm2SD / sqrt(liveTime * srcMCi / calibSrcMCi);
    } else {
        measSD = 10.0f;
    }
    // store the actual measurement SD as the error
    error = measSD;

    switch (measMode) {

        case DLStopAtSetLevel:
            // DLStopAtSetLevel ("Stop at Set Level"): Measure to defined confidence level about action level (max time = DLTimeOut)

            // difference between detection concentration and measurement
            measDiff = fabs(actionLevel - mgcm2);

            // confidence level of current measurement result relative to detection level
            if (measSD > 0) {
                kConfLev = measDiff / measSD;
            } else {
                kConfLev = 1000;
            }

            // check for timeout condition when nominal measurement time is greater than timeout time
            if (nomSecs >= DLTimeOut) {

                elemResult = dDLTimedOut;

                // check if confidence level has been reached
            } else if ((mgcm2 < actionLevel and kConfLev >= negConfLev) or
                       (mgcm2 > actionLevel and kConfLev >= posConfLev)) {

                elemResult = dComplete;

                // more time needed
            } else {

                elemResult = dInProcess;

            }
            break;

        case DLFixedTime:
            // DLFixedTime: measure for set nominal source time (time = DLNomSecs, max time = DLTimeOut)

            ALOG("nomSecs = %f, dlNomSecs = %f, DLTimeout = %f", nomSecs, dLNomSecs, DLTimeOut);
            if (nomSecs >= DLTimeOut) {

                elemResult = dDLTimedOut;

            } else if (nomSecs < dLNomSecs) {

                elemResult = dInProcess;
                //ALOG("Fixed Time In Process");

            } else {

                elemResult = dComplete;
                //ALOG("Fixed Time Completed");

            }
            break;

        case DLUnlimitedTime:
            // DLUnlimitedTime: measure as long as trigger is activated (maximum time = DLTimeOut)
            if (nomSecs < DLTimeOut) {

                elemResult = dInProcess;

            } else {

                elemResult = dDLTimedOut;

            }
            break;

        case DLStopAtStatistics:
            // DLStopAtStatistics ("Stop at Statistics"): measure until error (precision) is equal to accuracy of instrument (maximum time = DLTimeOut )
            if (nomSecs >= DLTimeOut) {

                elemResult = dDLTimedOut;

            } else if (measSD < (DLAccuracy * mgcm2)) {

                elemResult = dComplete;

            } else {

                elemResult = dInProcess;

            }
            break;

        case AL:

            break;

    } // end switch (measMode)

}
