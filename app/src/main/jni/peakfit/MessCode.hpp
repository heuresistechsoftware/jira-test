#define ResLoErrorCode 100
#define ResHiErrorCode 101
#define ResHiWarnCode 102

#define TempMisMatchCode 200
#define TempChisqWarnCode 201

#define NoSvcCalFileCode 301
#define NoCalFilesCode 302
#define CalFacsDiffCode 303
#define CopySDValueCode 304
#define NoBlankMatchCode 305

#define WarnGainDriftCode 401
