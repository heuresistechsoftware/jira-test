// PeakFit.cpp
//
// Compile and link code for Linux with:
// g++ -std=c++11 -lm -lstdc++ -o PeakFit PeakFit.cpp
// (Ensure that /usr/include/c++/4.8.1 is in the directory path)
//


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <string>
#include <stdlib.h>

#include <jni.h>
#include <sys/stat.h>

#include "Element.hpp"
#include "Line.hpp"
#include "PeakFitPrototypes.hpp"
#include "Spectrum.hpp"
#include "Calib.hpp"
#include "MessCode.hpp"
#include "hcc.hpp"
#include "crypto.h"
#include "PeakFit.hpp"
#include "SpectrumHBI.hpp"
#include "hac.hpp"

extern PeakFitResults results;
extern SampTemplate sampTemp;
extern Calib calib;

//---------- Global Variables --------------------------------------------------
int matchSamp, matchMeas;

DppSpectrum dppSpec;
Spectrum *spectrum;

// AL mode action levels for each element being analyzed (mg/cm2)
float actionLevelAL[MaxNumAnalyzeElements] = {1.00, 0.00, 0.00, 0.00, 0.00};

// DL mode action levels for each element being analyzed (mg/cm2)
float actionLevelDL[MaxNumAnalyzeElements] = {0.10, 0.00, 0.00, 0.00, 0.00};

float positiveConfidenceLevelLimitAL = ALPosConfLev;
float negativeConfidenceLevelLimitAL = ALNegConfLev;
float positiveConfidenceLevelLimitDL = DLPosConfLev;
float negativeConfidenceLevelLimitDL = DLNegConfLev;
float fixedTimeNomSecs = DLNomSecs;

int overrideSmooth = -1;

// Change several #define values to parameter so they can change with soils and lead paint
int tempEcalIter;
float tempFitAdjust=1.0;
float minEnergyWeightLo;
float bgndTempLoKev;
float energyWeightPowerHi;
float maxTempEcalFac;
bool TempEnergyWeight = TempEnergyWeightOn;  //Turn on/off energy weight per mode.
float tempPullUpFac = 1.0f;

//------------------------------------------------------------------------------

bool PeakFit::calibration() {

    bool status = false;
    int mode = 3;

    meas_mode measMode = AL; // Action level mode

    // set whether in service or factory calibration mode
    int calibMode = SetCalibMode;

    // Setup hcc for processing spectra
    status = hccInit( mode );

    // first acquire all the spectra and store files and templates to disk
    acquireCalibSpectra(dppSpec);

    // now use the template files to do background subtraction and calculate calibration factors
    int analyticMode = 0;
    hcc(dppSpec, *spectrum, calib, analyticMode, measMode, calibMode);

    ALOGI(" Calibration completed");

    return status;
}

void PeakFit::loadTemplates(SampTemplate &sampTemp, int mode, int analyticMode) {

    // opens all template files and stores the data they contain in template raw count arrays
    //
    //  Inputs:
    //      sampTemp: pointer to template array to be filled
    //      calib:    pointer to calibration array
    //      gain:     gain of spectrum that template will be compared to
    //      offset:   offset of spectrum that template will be compared to
    //      livetime: livetime of spectrum that template will be compared to
    //
    //      mode=1: Open template files with root TempFileRoot
    //      mode=2: Open template file with root RefTempRoot
    //

    // readUserParams(analyticMode);

    int i, n, ncnt=0, counts;
    std::string encryptName, decryptName;

    int size;
    std::fstream attemptEncryptedTpl, encryptedTpl, decryptTpl, dest, txtSource;
    std::string encryptText;
//    Crypto cryp;
    struct stat sb;

    FILE *ifp;
    std::string fileNameRoot = CalibBaseDir, fileNameTxt;
    std::string modeDir;

    // dust mode
    if (analyticMode == 2) {
        //numExcludePeaks = numExcludePeaksLeadPaint;
        modeDir = DustWipeDir;
        fileNameRoot.append(modeDir);

        minEnergyWeightLo = MinEnergyWeightLoNonLp;
        bgndTempLoKev = BgndTempLoKeVNonLp;
        energyWeightPowerHi = EnergyWeightPowerHiNonLp;
        TempEnergyWeight = TempEnergyWeightOn;
        tempEcalIter = TempEcalIterDW;
        maxTempEcalFac = MaxTempEcalFacDW;
        tempPullUpFac = DWipePullUpFac;
        tempPullUpFac = spectrum->dustPullUpFac;
        tempFitAdjust = 1.0f;
    }
    // insitu soil mode
    else if (analyticMode == 1) {
        // numExcludePeaks = numExcludePeaksSoils;
        modeDir = ISSoilsDir;
        fileNameRoot.append(modeDir);

        // check if (ts) directory exists if not use old (sl) dir structure
        if( stat(fileNameRoot.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode) ){
            fileNameRoot = CalibBaseDir;
            fileNameRoot.append(SoilsDir);
        }

        minEnergyWeightLo = MinEnergyWeightLoNonLp;
        bgndTempLoKev = BgndTempLoKeVNonLp;
        energyWeightPowerHi = EnergyWeightPowerHiNonLp;
        TempEnergyWeight = TempEnergyWeightOff;
        tempEcalIter = TempEcalIterSL;
        maxTempEcalFac = MaxTempEcalFacSL;
        tempPullUpFac = 1.0f;
        tempFitAdjust = 1.0f;
    }
    // teststand soil mode
    else if (analyticMode == 3) {
        // numExcludePeaks = numExcludePeaksSoils;
        modeDir = TSSoilsDir;

        // check if (ts) directory exists if not use old (sl) dir structure
        if( stat(fileNameRoot.c_str(), &sb) != 0 && !S_ISDIR(sb.st_mode) ){
            fileNameRoot = CalibBaseDir;
            fileNameRoot.append(SoilsDir);
        }

        minEnergyWeightLo = MinEnergyWeightLoNonLp;
        bgndTempLoKev = BgndTempLoKeVNonLp;
        energyWeightPowerHi = EnergyWeightPowerHiNonLp;
        TempEnergyWeight = TempEnergyWeightOff;
        tempEcalIter = TempEcalIterSL;
        maxTempEcalFac = MaxTempEcalFacSL;
        tempPullUpFac = 1.0f;
        tempFitAdjust = 1.0f;
    }
    // paint mode
    else {
        //numExcludePeaks = numExcludePeaksLeadPaint;
        modeDir = LeadPaintDir;
        fileNameRoot.append(modeDir);
        minEnergyWeightLo = MinEnergyWeightLo;
        bgndTempLoKev = BgndTempLoKeV;
        energyWeightPowerHi = EnergyWeightPowerHi;
        TempEnergyWeight = TempEnergyWeightOn;
        tempEcalIter = TempEcalIterLP;
        maxTempEcalFac = MaxTempEcalFacLP;
        tempPullUpFac = 1.0f;
        tempFitAdjust = spectrum->tempFitAdjust;
    }


    if (mode == 1)
        fileNameRoot.append(TempFileRoot);
    else
        fileNameRoot.append(RefTempRoot);

    ALOG("Template root file name %s", fileNameRoot.c_str());

    for (n = 0; n < calib.nSamples; n++) {

        fileNameTxt.assign(fileNameRoot);

        if (n + 1 < 10) {
            fileNameTxt.append("00");
        } else if (n + 1 < 100) {
            fileNameTxt.append("0");
        }


        char fileNum[5];
        sprintf(fileNum, "%d", n + 1);
        fileNameTxt.append(fileNum);
        ALOG("Template file name %s", fileNameTxt.c_str());

        encryptName.assign(fileNameTxt);
        decryptName.assign(fileNameTxt);


        fileNameTxt.append(".txt");
//        encryptName.append(".tpl");  // tpl is an encrypted template file
//        decryptName.append(".dec");
//        //ALOG("Txt name : %s, EncName %s, decName %s", fileNameTxt.c_str(), encryptName.c_str(), decryptName.c_str());
//
//        attemptEncryptedTpl.open(encryptName.c_str(), std::fstream::in | std::fstream::ate);
//        int isEncrypted = attemptEncryptedTpl.tellg();
//
//        // If we can't find the encrypted file then take the txt file and encrypt it. Then fall through to process it.
//        if (isEncrypted < 1) {
//
//            attemptEncryptedTpl.close();
//
//            //Now we have extracted all the data for the app, let's convert the text file to an encrypted tpl file
//            txtSource.open(fileNameTxt.c_str(), std::fstream::in | std::fstream::ate);
//            size = txtSource.tellg(); // the output of encryption can take extra space
//            //ALOG("Test.txt size = %d", size);
//
//            if (size > 0) {
//
//                txtSource.seekg(0);
//
//                encryptText.resize((unsigned int) size);
//                // Read entire text file into buffer
//                txtSource.read((char *) encryptText.c_str(), size);
//
//                std::string encryptedStr;
//
//                //Encrypt the buffer so it can be pushed to a new file.
////                cryp.encrypt(encryptText, encryptedStr);
//
//                txtSource.close();
//
//                dest.open(encryptName.c_str(), std::fstream::out | std::fstream::ate);
//                //ALOG("Dest Size %d", (int)encryptedStr.size() );
//                dest.seekg(0);
//
//                dest << encryptedStr; //Write encrypted data out to text.enc
//                dest.flush();
//                dest.close();
//            }
//        }
//
//        encryptedTpl.open(encryptName.c_str(), std::fstream::in | std::fstream::ate);
//        size = encryptedTpl.tellg(); // the output of encryption can take extra space
//        ALOG("ENC:Test.txt size = %d", size);
//        if (size > 0) {
//            encryptedTpl.seekg(0);
//
//            encryptText.resize((unsigned int) size);
//            // Read entire encrypted file into buffer
//            encryptedTpl.read((char *) encryptText.c_str(), size);
//
//            //ALOG("ENC:Read size = %d", encryptText.size());
//            std::string decryptedStr;
//
//            // Decrypt the buffer and parse it into sampTemp struct array
////            cryp.decrypt(encryptText, decryptedStr);
//
//            // ***** Debug dump of decrypted info for debug
////		  decryptTpl.open( decryptName, std::fstream::out );
////		  decryptTpl.seekg(0);
////		  decryptTpl << decryptedStr;
////		  decryptTpl.close();
//            // ***** end debug info
//
//            // Process the string into the sampTemp struct.
//
//            //ALOG("Decrypted Template Data: ********   %d   ************", n+1);

        std::ifstream templateFile (fileNameTxt.c_str());
        if(templateFile) {
            std::stringstream decryptTemplate; //decryptedStr);

            // read template build time
            decryptTemplate << templateFile.rdbuf();

            decryptTemplate >> sampTemp.buildTime[n];
            // ALOG("buildTime : %d", sampTemp.buildTime[n]);

            // read template concentration
            decryptTemplate >> sampTemp.conc[n];
            // ALOG("Conc %f", sampTemp.conc[n]);

            // read template header
            decryptTemplate >> sampTemp.nBins[n] >> sampTemp.tLoBin[n] >> sampTemp.lo_keV[n] >>
                            sampTemp.hi_keV[n] >>
                            sampTemp.gain[n] >> sampTemp.offset[n] >> sampTemp.subType[n] >> sampTemp.nomSecs[n] >>
                            sampTemp.bgndPeakCentKeV[n] >> sampTemp.bgndPeakRawCnts[n];

            // ALOG("%d %d %f %f %f %f %d %f %f %d", sampTemp.nBins[n], sampTemp.tLoBin[n], sampTemp.lo_keV[n], sampTemp.hi_keV[n],
            //	   sampTemp.gain[n], sampTemp.offset[n], sampTemp.subType[n], sampTemp.nomSecs[n], sampTemp.bgndPeakCentKeV[n], sampTemp.bgndPeakRawCnts[n]);

            // when initially loading templates, initially set normalization factor to 1 (this will be reset in scaleTemplates())
            sampTemp.normFac[n] = 1.0f;

            // read bin counts from template file and store values in raw counts array
            sampTemp.totCnts[n] = 0;
            for (i = 0; i < sampTemp.nBins[n]; i++) {
                decryptTemplate >> counts;
                if (i < MaxTempBins) sampTemp.rCounts[n][i] = counts;
                sampTemp.totCnts[n] += counts;
            }
        }
        ALOG("Total Counts %d",sampTemp.totCnts[n] );
        //encryptedTpl.close();

        ncnt++;
    }
    sampTemp.numTemplates = ncnt;
}

void PeakFit::scaleTemplates(SampTemplate &sampTemp, Calib &calib, float gain, float offset, float nomSecs,
                    int type, int mode) {

    // Scales the raw template count data so it can be matched with the current spectrum
    //
    //  Inputs:
    //      sampTemp: pointer to template array to be filled
    //      calib:    pointer to calibration array
    //      gain:     gain of spectrum that template will be compared to
    //      offset:   offset of spectrum that template will be compared to
    //      nomSecs:  nominal source seconds of spectrum that template will be compared to
    //
    //      type=1: Only load background templates
    //      type=2: Load all templates
    //
    //      mode=1: Open template files with root TempFileRoot
    //      mode=2: Open template file with root RefTempRoot
    //

    int i, n;
    int spec_lo_bin, spec_hi_bin;
    int tempBinInt;
    float tempBinFrac;
    float value;
    float spec_energy, temp_bin;

    n = 0;
    while (n < sampTemp.numTemplates) {

        // to save time, for type=1 only rebin counts in background templates
        if (!(type == 1 and calib.conc[n] > MaxBlankConc)) {

            // calculate normalization factor using nominal source time and gain ratio
            sampTemp.normFac[n] = gain / sampTemp.gain[n] * nomSecs / sampTemp.nomSecs[n] * tempFitAdjust;

            //ALOGI(" scaleTemplates: %d:  tempConc=%f sFac=%f gain=%f tempgain=%f nomSecs=%f tempNomSecs=%f MCi=%f calibMCi=%f", n+1, calib.conc[n], sampTemp.normFac[n], gain, sampTemp.gain[n], nomSecs, sampTemp.nomSecs[n],
            //
            //       spectrum.srcMCi, calib.srcMCi);

            // read bin counts from template file and store values normalized for comparison with spectrum
            for (i = 0; i < sampTemp.nBins[n]; i++) {
                if (i < MaxTempBins)
                    sampTemp.tCounts[n][i] = sampTemp.normFac[n] * sampTemp.rCounts[n][i];
                //ALOGI(" loadSampTemplates: %d: counts=%d", n+1, counts);
            }

            // gain of current spectrum may be different from template file gain, so need to create a second rebinned array for matching
            // first calculate the number of bins in current spectrum that correspond to template
            if (gain > 0) {
                spec_lo_bin = int((sampTemp.lo_keV[n] - offset) / gain + 0.5);
                spec_hi_bin = int((sampTemp.hi_keV[n] - offset) / gain + 0.5);
            } else {
                spec_lo_bin = 0;
                spec_hi_bin = 0;
            }
            //ALOGI(" loadSampTemplate: Spectrum bin range: %d - %d", spec_lo_bin, spec_hi_bin);
            sampTemp.sNumBins[n] = spec_hi_bin - spec_lo_bin + 1;
            sampTemp.sLoBin[n] = spec_lo_bin;

            // interpolate between original template bins to create new array with same number of bins as spectrum
            for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                spec_energy = offset + gain * (spec_lo_bin + i);
                temp_bin = (spec_energy - sampTemp.lo_keV[n]) / sampTemp.gain[n];
                if (temp_bin < 0) temp_bin = 0;
                if (temp_bin >= sampTemp.nBins[n]) temp_bin = sampTemp.nBins[n] - 1;
                tempBinInt = (int) temp_bin;
                tempBinFrac = temp_bin - tempBinInt;
                if ((tempBinInt + 1) < MaxTempBins) {
                    value = (float) ((1.0 - tempBinFrac) * sampTemp.tCounts[n][tempBinInt] +
                                                tempBinFrac * sampTemp.tCounts[n][tempBinInt + 1]);
                } else {
                    value = 0;
                }
                //ALOGI(" specBin=%d tempBin=%f specEnergy=%f specGain=%f tempGain=%f value=%f", i, temp_bin, spec_energy, gain, sampTemp.gain[n], value);
                if (i < MaxNumBins) sampTemp.sCounts[n][i] = value;

            }
        }

        n++;

    } // end reading in templates

}

void PeakFit::loadSingleTemplate(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, int tempID,
                        int mode) {

    // Opens template file specified by tempID and stores the data it contains in template array
    //
    // Mode=1: Open template file with root TempFileRoot
    // Mode=2: Open template file with root RefTempRoot

    int i, n, counts;
    int spec_lo_bin, spec_hi_bin;
    int tempBinInt;
    float tempBinFrac;
    float value;
    float spec_energy, temp_bin;
    char fname[128], str[5];

    FILE *ifp;

    n = tempID;

    // create the output background template file name
    if (mode == 1) {
        strcpy(fname, TempFileRoot);
    } else {
        strcpy(fname, RefTempRoot);
    }
    if (n + 1 < 10) {
        strcat(fname, "00");
    } else if (n + 1 < 100) {
        strcat(fname, "0");
    }
    sprintf(str, "%d", n + 1);
    strcat(fname, str);
    strcat(fname, ".txt");

    //open template file
    ifp = fopen(fname, "r");
    //ALOGI(" loadSingleTemplate: Opening Template File %s", fname);

    // read template build time
    fscanf(ifp, "%d", &sampTemp.buildTime[n]);

    // read template concentration
    fscanf(ifp, "%f", &sampTemp.conc[n]);

    // read template header
    fscanf(ifp, "\n%d %d %f %f %f %f %d %f", &sampTemp.nBins[n], &sampTemp.tLoBin[n],
           &sampTemp.lo_keV[n], &sampTemp.hi_keV[n],
           &sampTemp.gain[n], &sampTemp.offset[n], &sampTemp.subType[n], &sampTemp.liveTime[n]);
    //ALOGI("loadSingleTemp: %s: %d %d %f %f %f %f %d %f %f", fname, sampTemp.nBins[n], sampTemp.tLoBin[n], sampTemp.lo_keV[n], sampTemp.hi_keV[n],
    //	   sampTemp.gain[n], sampTemp.offset[n], sampTemp.subType[n], sampTemp.conc[n], sampTemp.liveTime[n]);


    // calculate normalization factor using live time and elapsed time since calibration, and gain ratio
    sampTemp.normFac[n] =
            spectrum.k_gain / sampTemp.gain[n] * spectrum.nomSecs / sampTemp.nomSecs[n] * tempFitAdjust;

    //ALOGI(" loadSingleTemplate: SpecliveTime=%f TempLiveTime=%f normFac=%f", spectrum.liveTime, sampTemp.liveTime[n], sampTemp.normFac[n]);

    // read bin counts from template file and store values normalized for comparison with spectrum
    for (i = 0; i < sampTemp.nBins[n]; i++) {
        fscanf(ifp, "%d", &counts);
        if (i < MaxTempBins) sampTemp.tCounts[n][i] = sampTemp.normFac[n] * counts;
        //ALOGI(" loadSingleTemplate: %d: counts=%d", n+1, counts);
    }

    // gain of current spectrum may be different from template file gain, so need to create a second rebinned array for matching
    // first calculate the number of bins in current spectrum that correspond to template
    if (spectrum.k_gain > 0) {
        spec_lo_bin = int((sampTemp.lo_keV[n] - spectrum.k_offset) / spectrum.k_gain + 0.5);
        spec_hi_bin = int((sampTemp.hi_keV[n] - spectrum.k_offset) / spectrum.k_gain + 0.5);
    } else {
        spec_lo_bin = 0;
        spec_hi_bin = 0;
    }
    //ALOGI(" loadSampTemplate: Spectrum bin range: %d - %d", spec_lo_bin, spec_hi_bin);
    sampTemp.sNumBins[n] = spec_hi_bin - spec_lo_bin + 1;
    sampTemp.sLoBin[n] = spec_lo_bin;

    // interpolate between original template bins to create new array with same number of bins as spectrum
    for (i = 0; i < sampTemp.sNumBins[n]; i++) {
        spec_energy = spectrum.k_offset + spectrum.k_gain * (spec_lo_bin + i);
        temp_bin = (spec_energy - sampTemp.lo_keV[n]) / sampTemp.gain[n];
        if (temp_bin < 0) temp_bin = 0;
        if (temp_bin >= sampTemp.nBins[n]) temp_bin = sampTemp.nBins[n] - 1;
        tempBinInt = (int) temp_bin;
        tempBinFrac = temp_bin - tempBinInt;
        if ((tempBinInt + 1) < MaxTempBins) {
            value = (float) ((1.0 - tempBinFrac) * sampTemp.tCounts[n][tempBinInt] +
                                tempBinFrac * sampTemp.tCounts[n][tempBinInt + 1]);
        } else {
            value = 0;
        }
        //ALOGI(" specBin=%d tempBin=%f specEnergy=%f specGain=%f tempGain=%f value=%f", i, temp_bin, spec_energy, spectrum.k_gain, sampTemp.gain[n], value);
        if (i < MaxNumBins) sampTemp.sCounts[n][i] = value;

    }
    fclose(ifp);

}



int PeakFit::fitTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                 line_id lineID, int type) {
    // Find the best scaled template that best matches the current spectrum. (Note that this is slower than fitTemplateFast,
    // which finds the best match with an approximate scaling based on liveTime, and then finds the best scaling factor for
    // that template).
    //
    // Return the template that best matches the current spectrum
    //
    // Type=1: Only use templates created with blank calibration samples and exclude regions under element peaks
    // Type=2: Use all templates created from all calibration samples and exclude regions under the element peaks
    // Type=3: Use all templates created from all calibration samples and do not exclude regions under the element peaks
    //

    int cnt, bval, i, m, n, p, nt, nTemp, n_min = 0, sbin;
    int numScaleIter, startTempNum = 0, rtgStatus;
    float gain, kGainDrift, sval, tval, sumVal;
    float ePeak, eBin, weightFac[MaxTempBins];
    float chisq = 0.0f, chisq_min;
    float scaleFac, scaleFacMin = 1.0f, delScaleFac;
    float ecalFac, gainMin = DefaultKGain, delEcalFac;
    float c1, c2, chisq_max = (float) 1.E8;
    float localWeightPowerHi  = energyWeightPowerHi;;

    // temporary for PCS!!! 7/3/19
    float keV_lo, keV_hi, bgnd, excess; int counts, lo_bin, hi_bin;

    bool PrintDebug = false;
    bool PrintDebug2 = false;

    FILE *ofp;

    // update Compton peak information (this is also called initializeElements)
    spectrum.findComptonPeak();

    // peak of element of interest
    ePeak = (float) lineKeV[elemID][lineID];

    // find the best scaled template that matches the spectrum
    chisq_min = chisq_max;

    // reset parameters for calibration mode
    if (spectrum.fitMode == 1) {

        // only fit the correct background template
        nTemp = 1;

    // use defaults for analysis mode
    } else {
        //nTemp = sampTemp.numTemplates;

        // overide value of local energyWeightPowerHi if RTG is present
        if ((rtgStatus = checkForRTA(spectrum))==1) {
            startTempNum = calib.nNonRTA;
            nTemp = sampTemp.numTemplates;
            localWeightPowerHi = EnergyWeightPowerHiRTG;
        } else {
            startTempNum = 0;
            nTemp = calib.nNonRTA;
            localWeightPowerHi = energyWeightPowerHi;
        }

    }
    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // check for styrofoam substrate and overide values of startTempNum and nTemp
    if (spectrum.checkForStyroFm()) {
        startTempNum = 0;
        nTemp = sampTemp.numTemplates;
    }

    // pre-calculate some variables to speed up loop
    c1 = (float) ((1.0 - minEnergyWeightLo) / (ePeak - bgndTempLoKev));
    c2 = (float) ((1.0 - MinEnergyWeightHi) / (BgndTempHiKeV - ePeak));

    // iterate over the gain
    delEcalFac = 2 * maxTempEcalFac / tempEcalIter;
    ecalFac = (float) (1.0 - maxTempEcalFac);
    for (p = 0; p < tempEcalIter; p++) {

        gain = spectrum.k_gain * ecalFac;
        ALOGI(" tempEcalIter %d spectrum.k_gain=%f gain=%f", p, spectrum.k_gain, gain);

        // recalibrate spectrum variables with this gain
        //reCalibSpec(spectrum, gain);

        // scale templates with modified gain
        scaleTemplates(sampTemp, calib, gain, spectrum.k_offset, spectrum.nomSecs, type, 1);

        // in calibration mode, will only fit the correct background template
        if (spectrum.fitMode == 1) nTemp = 1;

        // loop over templates
        for (nt = startTempNum; nt < nTemp; nt++) {

            //ALOGI(" Template Loop %d of %d", nt, nTemp);
            // in calibration mode (fitMode=1), only fit the correct background template
            if (spectrum.fitMode == 1) {
                n = sampTemp.subType[spectrum.calibSamp];
            } else {
                n = nt;
            }

            // pre-calculate weighting factor (diminishes further from peak of interest)
            if (TempEnergyWeight) {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    eBin = bgndTempLoKev + i * sampTemp.gain[n];
                    if (eBin < ePeak) {
                        weightFac[i] = minEnergyWeightLo + (eBin - bgndTempLoKev) * c1;
                        weightFac[i] = pow(weightFac[i], EnergyWeightPowerLo);
                    } else {
                        weightFac[i] = MinEnergyWeightHi + (BgndTempHiKeV - eBin) * c2;
                        //weightFac[i] = pow(weightFac[i], energyWeightPowerHi);
                        weightFac[i] = pow(weightFac[i], localWeightPowerHi);
                    }
                    if (weightFac[i] < 0) weightFac[i] = 0;
                    //printf("\n Weight fac = %f", weightFac[i]);
                }
            } else {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    weightFac[i] = 1.0;
                }
            }

            // skip non-blank templates for type=1
            if (type == 1 && sampTemp.conc[n] > MaxBlankConc) continue;

            // check if normalized counts in Compton peaks are similar
            if (TempFitPeakCheck) {
                if (2 * fabs(double(spectrum.bgndPeakRawCnts - sampTemp.bgndPeakRawCnts[n])) /
                    (spectrum.bgndPeakRawCnts + sampTemp.bgndPeakRawCnts[n]) > MaxBgndPeakCntDiff)
                    continue;
            }

            //ALOGI(" Template %d has similar peak counts (%d vs %d)", n+1, sampTemp.bgndPeakRawCnts[n], (int)spectrum.bgndPeakRawCnts);

            // start with crude scaling
            delScaleFac = 2 * spectrum.crudeScaleFac / CrudeScaleIter;
            scaleFac = (float) (1.0 - spectrum.crudeScaleFac);

            // total number of crude and fine scaling iterations
            numScaleIter = CrudeScaleIter + FineScaleIter;

            for (m = 0; m < numScaleIter; m++) {
                //ALOG("Loop scaleItr %d of %d", m, numScaleIter);

                // reset at start of fine scaling
                if (m == CrudeScaleIter) {
                    if (FineScaleIter > 0) {
                        delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);
                    }
                    scaleFac = (scaleFacMin - MaxFineScaleFac);
                }

                cnt = 0;
                chisq = 0;

                // temporary for PCS!!! 7/3/19
                keV_lo = (float) lineKeVLo[elemID][lineID];
                keV_hi = (float) lineKeVHi[elemID][lineID];
                lo_bin = int((keV_lo - spectrum.k_offset) / spectrum.k_gain + 0.5);
                hi_bin = int((keV_hi - spectrum.k_offset) / spectrum.k_gain + 0.5);
                if (lo_bin < 0) lo_bin = 0;
                if (hi_bin > MaxNumBins) hi_bin = MaxNumBins - 1;
                counts = 0; bgnd = 0;

                for (i = 0; i < sampTemp.sNumBins[n]; i++) {

                    //ALOG("Loop Bins %d of %d", i, sampTemp.sNumBins[n]);

                    sbin = sampTemp.sLoBin[n] + i;

                    // temporary for PCS!!! 7/3/19
                    if(sbin>=lo_bin && sbin<hi_bin) {
                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];
                        counts += (int)sval;
                        bgnd += tval;
                    }

                    if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {

                        // for type=1 and type=2, skip bins under peaks that are not to be included for background template matching
                        if ((type == 1 or type == 2) && !spectrum.includeBgnd[sbin]) continue;

                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];

                        //ALOG(" fitTemplate: template %d: sval=%f tval=%f", n+1, sval, tval);
                        if(tval<sval) {
                            chisq += tempPullUpFac * weightFac[i] * (sval - tval) * (sval - tval);
                        } else {
                            chisq += weightFac[i] * (sval - tval) * (sval - tval);
                        }
                        cnt++;
                    }
                }

                if (cnt > 0) {
                    chisq /= cnt;
                    if (chisq < chisq_min) {
                        chisq_min = chisq;
                        n_min = n;
                        scaleFacMin = scaleFac;
                        gainMin = gain;
                        //ALOG("fitTemplate: Template %d: scaleFac=%f chisq=%f specGain=%f, gainMin=%f", n_min+1, scaleFacMin, sqrt(chisq), spectrum.k_gain, gainMin);
                    }
                }

                scaleFac += delScaleFac;

            } // end looping over scaling iterations

        }  // end looping over templates

        ecalFac += delEcalFac;

    } // end looping over ecal iterations

    // scale templates with gain from best match
    scaleTemplates(sampTemp, calib, gainMin, spectrum.k_offset, spectrum.nomSecs, type, 1);

    //ALOG("\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f", matchSamp, matchMeas, n_min+1, spectrum.calibSubType+1, scaleFacMin, sqrt(chisq));

    // print out best match to file for debugging
    if (PrintDebug) {

        ofp = fopen("'/'sdcard'/'pf'/'lp'/'tempMatch.txt", "a+");
        fprintf(ofp, "\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f",
                matchSamp, matchMeas, n_min + 1, spectrum.calibSubType + 1, scaleFacMin,
                sqrt(chisq));

        for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
            sbin = sampTemp.sLoBin[n_min] + i;
            if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {
                sval = (float) spectrum.counts[sbin];
                tval = scaleFacMin * sampTemp.sCounts[n_min][i];
                if (spectrum.includeBgnd[sbin] == 1) {
                    bval = 10;
                } else {
                    bval = 0;
                }
                fprintf(ofp, "\n%d %d %f %f %f %d", sbin, spectrum.includeBgnd[sbin],
                        (spectrum.k_offset + spectrum.k_gain * sbin), sval, tval, bval);
            }
        }
        fclose(ofp);
    }

    if (PrintDebug2) {

        ALOG("sample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f", matchSamp,
             matchMeas, n_min + 1, spectrum.calibSubType + 1, scaleFacMin, sqrt(chisq));

        for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
            sbin = sampTemp.sLoBin[n_min] + i;
            if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {
                sval = (float) spectrum.counts[sbin];
                tval = scaleFacMin * sampTemp.sCounts[n_min][i];
                if (spectrum.includeBgnd[sbin] == 1) {
                    bval = 10;
                } else {
                    bval = 0;
                }
//                ALOG("%d %d %f %f %f %d", sbin, spectrum.includeBgnd[sbin],
//                     (spectrum.k_offset + spectrum.k_gain * sbin), sval, tval, bval);
            }
        }
    }

    // store best match for substrate and template type
    spectrum.subType = sampTemp.subType[n_min];
    spectrum.tempType = n_min;

    // store bgnd for each bin in range of best matched template
    sumVal = 0;
    for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
        //ALOG("Store bckgrd %d of %d", i, sampTemp.sNumBins[n_min]);

        sbin = sampTemp.sLoBin[n_min] + i;
        if (sbin >= 0 && sbin < MaxNumBins && i < MaxTempBins) {
            tval = scaleFacMin * sampTemp.sCounts[n_min][i];
            spectrum.bgnd[sbin] = tval;
            sumVal += tval;
        }
    }

    // set total normalization factor used to scale template counts for background template matching
    spectrum.tempNormFac = scaleFacMin * sampTemp.normFac[n_min];

    // set template gain to gain determined from template stretching
    spectrum.tempGain = gainMin;

    // determine gain drift from default value
    if (spectrum.defKGain > 0) {
        kGainDrift = fabs(gainMin - spectrum.defKGain) / (spectrum.defKGain) * 100;
    } else {
        kGainDrift = 0;
    }

    // store chisq of template match
    //spectrum.tempChisq = sqrt(chisq_min);
    spectrum.tempChisq = sqrt(chisq_min) / spectrum.nomSecs;

    // set template to -1 if the chisquare value is too high
    if (chisq_min >= chisq_max) n_min = -1;

    // set template to -1 if RTA has a fault
    if (rtgStatus < 0) n_min = -1;


    ALOGI(" fitTemplate: BestTemplate Match = %d (calibSub=%d) BestScaleFac=%f SpecGain=%f BestGain=%f tempFitAdjust=%f",
          n_min + 1, spectrum.calibSubType + 1, scaleFacMin, spectrum.k_gain, gainMin, tempFitAdjust);

    // set spectrum gain to gain determined from template stretching
    if (UseTemplateGain and kGainDrift < spectrum.GainDriftNull) {
        spectrum.k_gain = gainMin;
    }

    return (n_min);

}

int PeakFit::fitTemplates2(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                  line_id lineID, int type) {
    // Find the best scaled template that best matches the current spectrum. Minimize both chisquare and the standard deviation of the deviations.
    // (NOTE: fitTemplates just minimizes chisquare).
    //
    // Return the template that best matches the current spectrum
    //
    // Type=1: Only use templates created with blank calibration samples and exclude regions under element peaks
    // Type=2: Use all templates created from all calibration samples and exclude regions under the element peaks
    // Type=3: Use all templates created from all calibration samples and do not exclude regions under the element peaks
    //

    int cnt, bval, i, m, n, p, nt, nTemp, n_min = 0, sbin;
    int numScaleIter;
    int tempEcalIter;
    float maxTempEcalFac;
    float gain, kGainDrift, sval, tval, sumVal;
    float ePeak, eBin, weightFac[MaxTempBins];
    float chisq = 0.0f, chisq_min;
    float scaleFac, scaleFacMin = 1.0f, delScaleFac;
    float ecalFac, gainMin = DefaultKGain, delEcalFac;
    float devList[MaxTempBins], devSD, devSD_min;
    float c1, c2;
    bool PrintDebug = false;
    FILE *ofp;

    float stanDev(int, float *, int);

    // update Compton peak information (this is also called initializeElements)
    spectrum.findComptonPeak();

    // peak of element of interest
    ePeak = (float) lineKeV[elemID][lineID];

    // find the best scaled template that matches the spectrum
    chisq_min = (float) 1.E8;
    devSD_min = (float) 1.E8;

    // reset parameters for calibration mode
    if (spectrum.fitMode == 1) {

        // only fit the correct background template
        nTemp = 1;
        tempEcalIter = TempEcalIter;
        maxTempEcalFac = MaxTempEcalFac;

        // no template stretching in calibration mode
        //tempEcalIter = 1;
        //maxTempEcalFac = 0;

        // use defaults for analysis mode
    } else {
        nTemp = sampTemp.numTemplates;
        tempEcalIter = TempEcalIter;
        maxTempEcalFac = MaxTempEcalFac;
    }
    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // pre-calculate some variables to speed up loop
    c1 = (float) ((1.0 - minEnergyWeightLo) / (ePeak - bgndTempLoKev));
    c2 = (float) ((1.0 - MinEnergyWeightHi) / (BgndTempHiKeV - ePeak));

    // iterate over the gain
    delEcalFac = 2 * maxTempEcalFac / tempEcalIter;
    ecalFac = (float) (1.0 - maxTempEcalFac);
    for (p = 0; p < tempEcalIter; p++) {

        gain = spectrum.k_gain * ecalFac;
        //ALOGI(" spectrum.k_gain=%f gain=%f", spectrum.k_gain, gain);

        // recalibrate spectrum variables with this gain
        //reCalibSpec(spectrum, gain);

        // scale templates with modified gain
        scaleTemplates(sampTemp, calib, gain, spectrum.k_offset, spectrum.nomSecs, type, 1);

        // in calibration mode, will only fit the correct background template
        if (spectrum.fitMode == 1) nTemp = 1;

        // loop over templates
        for (nt = 0; nt < nTemp; nt++) {

            // in calibration mode (fitMode=1), only fit the correct background template
            if (spectrum.fitMode == 1) {
                n = sampTemp.subType[spectrum.calibSamp];
            } else {
                n = nt;
            }

            // pre-calculate weighting factor (diminishes further from peak of interest)
            if (TempEnergyWeight) {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    eBin = bgndTempLoKev + i * sampTemp.gain[n];
                    if (eBin < ePeak) {
                        weightFac[i] = minEnergyWeightLo + (eBin - bgndTempLoKev) * c1;
                        weightFac[i] = pow(weightFac[i], EnergyWeightPowerLo);
                    } else {
                        weightFac[i] = MinEnergyWeightHi + (BgndTempHiKeV - eBin) * c2;
                        weightFac[i] = pow(weightFac[i], energyWeightPowerHi);
                    }
                    if (weightFac[i] < 0) weightFac[i] = 0;
                    //printf("\n Weight fac = %f", weightFac[i]);
                }
            } else {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    weightFac[i] = 1.0;
                }
            }

            // skip non-blank templates for type=1
            if (type == 1 && sampTemp.conc[n] > MaxBlankConc) continue;

            // check if normalized counts in Compton peaks are similar
            if (TempFitPeakCheck) {
                if (2 * fabs(double(spectrum.bgndPeakRawCnts - sampTemp.bgndPeakRawCnts[n])) /
                    (spectrum.bgndPeakRawCnts + sampTemp.bgndPeakRawCnts[n]) > MaxBgndPeakCntDiff)
                    continue;
            }

            //ALOGI(" Template %d has similar peak counts (%d vs %d)", n+1, sampTemp.bgndPeakRawCnts[n], (int)spectrum.bgndPeakRawCnts);

            // start with crude scaling
            delScaleFac = 2 * spectrum.crudeScaleFac / CrudeScaleIter;
            scaleFac = (float) (1.0 - spectrum.crudeScaleFac);

            // total number of crude and fine scaling iterations
            numScaleIter = CrudeScaleIter + FineScaleIter;

            for (m = 0; m < numScaleIter; m++) {

                // reset at start of fine scaling
                if (m == CrudeScaleIter) {
                    if (FineScaleIter > 0) {
                        delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);
                    }
                    scaleFac = (scaleFacMin - MaxFineScaleFac);
                }

                cnt = 0;
                chisq = 0;

                for (i = 0; i < sampTemp.sNumBins[n]; i++) {

                    sbin = sampTemp.sLoBin[n] + i;

                    if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {

                        // for type=1 and type=2, skip bins under peaks that are not to be included for background template matching
                        if ((type == 1 or type == 2) && !spectrum.includeBgnd[sbin]) continue;

                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];

                        //ALOGI(" fitTemplate: template %d: sval=%f tval=%f", n+1, sval, tval);
                        chisq += weightFac[i] * (sval - tval) * (sval - tval);
                        devList[cnt] = weightFac[i] * (sval - tval);

                        cnt++;
                    }
                }
                if (cnt > 0) {
                    chisq /= cnt;
                    if (chisq < chisq_min) {
                        devSD = stanDev(cnt, devList, 2);
                        if (devSD < devSD_min) {
                            chisq_min = chisq;
                            devSD_min = devSD;
                            n_min = n;
                            scaleFacMin = scaleFac;
                            gainMin = gain;
                            ALOG("fitTemplate2: Template %d: scaleFac=%f chisq=%f devSD=%f",
                                 n_min + 1, scaleFacMin, sqrt(chisq), devSD);
                        }
                    }
                }

                scaleFac += delScaleFac;

            } // end looping over scaling iterations

        }  // end looping over templates

        ecalFac += delEcalFac;

    } // end looping over ecal iterations

    // scale templates with gain from best match
    scaleTemplates(sampTemp, calib, gainMin, spectrum.k_offset, spectrum.nomSecs, type, 1);

    //ALOG("\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f", matchSamp, matchMeas, n_min+1, spectrum.calibSubType+1, scaleFacMin, sqrt(chisq));

    // print out best match for debugging
    if (PrintDebug) {
        ofp = fopen("tempMatch.txt", "a+");
        fprintf(ofp, "\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f",
                matchSamp, matchMeas, n_min + 1, spectrum.calibSubType + 1, scaleFacMin,
                sqrt(chisq));
        for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
            sbin = sampTemp.sLoBin[n_min] + i;
            if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {
                sval = (float) spectrum.counts[sbin];
                tval = scaleFacMin * sampTemp.sCounts[n_min][i];
                if (spectrum.includeBgnd[sbin] == 1) {
                    bval = 10;
                } else {
                    bval = 0;
                }
                fprintf(ofp, "\n%d %d %f %f %f %d", sbin, spectrum.includeBgnd[sbin],
                        (spectrum.k_offset + spectrum.k_gain * sbin), sval, tval, bval);
            }
        }
        fclose(ofp);
    }

    // store best match for substrate and template type
    spectrum.subType = sampTemp.subType[n_min];
    spectrum.tempType = n_min;

    // store bgnd for each bin in range of best matched template
    sumVal = 0;
    for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
        sbin = sampTemp.sLoBin[n_min] + i;
        if (sbin >= 0 && sbin < MaxNumBins && i < MaxTempBins) {
            tval = scaleFacMin * sampTemp.sCounts[n_min][i];
            spectrum.bgnd[sbin] = tval;
            sumVal += tval;
        }
    }

    // set total normalization factor used to scale template counts for background template matching
    spectrum.tempNormFac = scaleFacMin * sampTemp.normFac[n_min];

    // set template gain to gain determined from template stretching
    spectrum.tempGain = gainMin;

    // determine gain drift from default value
    if (spectrum.defKGain > 0) {
        kGainDrift = fabs(gainMin - spectrum.defKGain) / (spectrum.defKGain) * 100;
    } else {
        kGainDrift = 0;
    }

    // set spectrum gain to gain determined from template stretching
    if (UseTemplateGain and kGainDrift < spectrum.GainDriftNull) {
        spectrum.k_gain = gainMin;
    }

    // store chisq of template match
    spectrum.tempChisq = sqrt(chisq_min);

    ALOGI(" fitTemplate: BestTemplate Match = %d (calibSub=%d) BestScaleFac=%f SpecGain=%f BestGain=%f",
          n_min + 1, spectrum.calibSubType + 1, scaleFacMin, spectrum.k_gain, gainMin);

    return (n_min);

}

int PeakFit::quickFitTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                      line_id lineID, int type) {
    // Find the best scaled template that best matches the current spectrum in just the selected energy regions
    //
    // Return the template that best matches the current spectrum in these regions
    //
    // Type=1: Only use templates created with blank calibration samples
    // Type=2: Use all templates created from all calibration samples
    //

    int cnt, bval, i, ei, m, n, nTemp, n_min = 0, tbin, sbin;
    int concID, loBin, hiBin, numScaleIter;
    float sumCnts, sumBgnd, sval, tval;
    float eBin, mgcm2;
    float chisq = 0.0f, chisq_min;
    float scaleFac, scaleFacMin = 1.0f, delScaleFac;

    // define select regions to fit the template to spectrum
    int numFitPoints = 3;
    float eFitPoint[3][2] = {
            41.0, 43.0,
            62.5, 64.5,
            78.5, 80.5
    };

    bool includePoint;
    bool PrintDebug = false;
    FILE *ofp;

    // update Compton peak information (this is also called initializeElements)
    spectrum.findComptonPeak();

    // find the best scaled template that matches the spectrum
    chisq_min = (float) 1.E8;

    nTemp = sampTemp.numTemplates;
    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // scale templates
    scaleTemplates(sampTemp, calib, spectrum.k_gain, spectrum.k_offset, spectrum.nomSecs, type, 1);

    // loop over templates
    for (n = 0; n < nTemp; n++) {

        // skip non-blank templates for type=1
        if (type == 1 && sampTemp.conc[n] > MaxBlankConc) continue;

        // check if normalized counts in Compton peaks are similar
        if (TempFitPeakCheck) {
            if (2 * fabs(double(spectrum.bgndPeakRawCnts - sampTemp.bgndPeakRawCnts[n])) /
                (spectrum.bgndPeakRawCnts + sampTemp.bgndPeakRawCnts[n]) > MaxBgndPeakCntDiff)
                continue;
        }

        // start with crude scaling
        delScaleFac = 2 * spectrum.crudeScaleFac / CrudeScaleIter;
        scaleFac = (float) (1.0 - spectrum.crudeScaleFac);

        // total number of crude and fine scaling iterations
        numScaleIter = CrudeScaleIter + FineScaleIter;

        for (m = 0; m < numScaleIter; m++) {

            // reset at start of fine scaling
            if (m == CrudeScaleIter) {
                if (FineScaleIter > 0) delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);
                scaleFac = (scaleFacMin - MaxFineScaleFac);
            }

            cnt = 0;
            chisq = 0;

            for (i = 0; i < sampTemp.sNumBins[n]; i++) {

                sbin = sampTemp.sLoBin[n] + i;

                if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {

                    eBin = spectrum.k_offset + sbin * spectrum.k_gain;

                    // only include bins inside energy regions for the selected energy regions
                    includePoint = false;
                    for (ei = 0; ei < numFitPoints; ei++) {
                        if (eBin >= eFitPoint[ei][0] and eBin <= eFitPoint[ei][1]) {
                            includePoint = true;
                            break;
                        }
                    }

                    if (includePoint) {
                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];

                        //ALOGI(" quickFitTemplate: template %d: sval=%f tval=%f", n+1, sval, tval);
                        chisq += (sval - tval) * (sval - tval);
                        cnt++;
                    }
                }
            }
            if (cnt > 0) {
                chisq /= cnt;
                if (chisq < chisq_min) {
                    chisq_min = chisq;
                    n_min = n;
                    scaleFacMin = scaleFac;
                    // ALOG("quickFitTemplate: Template %d: scaleFac=%f chisq=%f", n_min+1, scaleFacMin, sqrt(chisq));
                }
            }

            scaleFac += delScaleFac;

        } // end looping over scaling iterations

    }  // end looping over templates

    // now sum counts minus fitted background in sum energy regions
    // bin range in spectrum
    loBin = int((lineKeVLo[elemID][lineID] - spectrum.k_offset) / spectrum.k_gain + 0.5);
    hiBin = int((lineKeVHi[elemID][lineID] - spectrum.k_offset) / spectrum.k_gain + 0.5);

    sumCnts = 0;
    sumBgnd = 0;
    for (sbin = loBin; sbin <= hiBin; sbin++) {
        tbin = sbin - sampTemp.sLoBin[n_min];
        sumBgnd += (scaleFacMin * sampTemp.sCounts[n_min][tbin]);
        sumCnts += spectrum.counts[sbin] - (scaleFacMin * sampTemp.sCounts[n_min][tbin]);
    }

    if (QuickConcCalcMode == 1) {

        mgcm2 = sumCnts / spectrum.nomSecs / avCalibRaw[elemID][n_min];
        //ALOGI(" quickFitTemplates: counts=%f mgcm2=%f", sumCnts, mgcm2);

    } else {
        // calculate approximate mgcm2 from quick template fit using average calibRatio factor
        mgcm2 = sumCnts / sumBgnd / calibRatio[elemID][n_min][0];

        // calculate more accurate mgcm2 from quick template fit using specific calibRatio factor
        concID = getConcID(calib, mgcm2);

        // if specific calibration factor is zero, use first non-zero value for lower concentrations
        if (calibRatio[elemID][n_min][concID] == 0 and concID > 0) {
            while (concID > 0) {
                concID--;
                if (calibRatio[elemID][n_min][concID] > 0) {
                    break;
                }
            }
        }
        mgcm2 = sumCnts / sumBgnd / calibRatio[elemID][n_min][concID];
        //ALOGI(" quickFitTemplates: counts=%f bgnd=%f mgcm2=%f", sumCnts, sumBgnd, mgcm2);
    }

    if (lineID == ka1 || lineID == ka2) {
        spectrum.element[elemID].mgcm2_quick = mgcm2;
    }


    // print out best match for debugging
    if (PrintDebug) {
        ofp = fopen("quickMatch.txt", "a+");
        fprintf(ofp, "\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f",
                matchSamp, matchMeas, n_min + 1, spectrum.calibSubType + 1, scaleFacMin,
                sqrt(chisq));
        for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
            sbin = sampTemp.sLoBin[n_min] + i;
            if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {
                sval = (float) spectrum.counts[sbin];
                tval = scaleFacMin * sampTemp.sCounts[n_min][i];
                if (spectrum.includeBgnd[sbin] == 1) {
                    bval = 10;
                } else {
                    bval = 0;
                }
                fprintf(ofp, "\n%d %d %f %f %f %d", sbin, spectrum.includeBgnd[sbin],
                        (spectrum.k_offset + spectrum.k_gain * sbin), sval, tval, bval);
            }
        }
        fclose(ofp);
    }

    //ALOGI(" quickFitTemplate: Quick Template Match = %d (calibSub=%d) BestScaleFac=%f", n_min+1, sampTemp.subType[n_min]+1, scaleFacMin);

    return (n_min);

}

int PeakFit::fitAllTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                    line_id lineID, int type) {
    // Find the best scaled template that best matches the current spectrum.
    //
    // Return the template that best matches the current spectrum
    //
    // Type=1: Exclude regions under element peaks
    // Type=2: Don't exclude regions under element peaks
    //

    int cnt, bval, i, j, k, m, n, p, nt, nTemp, n_min = 0, sbin;
    int id, eid, lo_bin, hi_bin;
    int sCnts, tCnts, nSelect = 0, numScaleIter;
    int tempEcalIter;
    float maxTempEcalFac, valDiff;
    float gain, kGainDrift, sval, tval, sumVal;
    float ePeak, eBin, weightFac[MaxTempBins];
    float chisq = 0.0f, chisq_min;
    float scaleFac, scaleFacMin = 1.0f, delScaleFac;
    float ecalFac, gainMin = DefaultKGain, delEcalFac;
    float c1, c2;
    bool nFnd, PrintDebug = false;
    FILE *ofp;

    // update Compton peak information (this is also called initializeElements)
    spectrum.findComptonPeak();

    // peak of element of interest
    ePeak = (float) lineKeV[elemID][lineID];

    // load templates (this should be in hacInit, but had scope issues on the instrument)
    //loadTemplates(sampTemp, 1);

    // find the best scaled template that matches the spectrum
    chisq_min = (float) 1.E8;

    // calibration mode
    if (spectrum.fitMode == 1) {

        // find the template with the same substrate but with the next lower concentration
        if (calib.concID[spectrum.calibSamp] > 0) {
            nSelect = 0;
            for (i = 1; i <= calib.concID[spectrum.calibSamp]; i++) {
                nFnd = false;
                for (n = 0; n < sampTemp.numTemplates; n++) {
                    if (calib.subType[n] == spectrum.calibSubType &&
                        calib.concID[n] == (calib.concID[spectrum.calibSamp] - i)) {
                        nSelect = n;
                        nFnd = true;
                        break;
                    }
                }
                if (nFnd) break;
            }

            // assign blank samples to itself
        } else {
            nSelect = spectrum.calibSamp;
        }

        nTemp = 1;

        tempEcalIter = TempEcalIter;
        maxTempEcalFac = MaxTempEcalFac;

        // analysis mode
    } else {
        nTemp = sampTemp.numTemplates;
        tempEcalIter = TempEcalIter;
        maxTempEcalFac = MaxTempEcalFac;
    }

    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // pre-calculate some variables to speed up loop
    c1 = (float) ((1.0 - minEnergyWeightLo) / (ePeak - bgndTempLoKev));
    c2 = (float) ((1.0 - MinEnergyWeightHi) / (BgndTempHiKeV - ePeak));

    // iterate over the gain
    delEcalFac = 2 * maxTempEcalFac / tempEcalIter;
    ecalFac = (float) (1.0 - maxTempEcalFac);
    for (p = 0; p < tempEcalIter; p++) {

        gain = spectrum.k_gain * ecalFac;
        //ALOGI(" spectrum.k_gain=%f gain=%f", spectrum.k_gain, gain);

        // scale templates with modified gain
        scaleTemplates(sampTemp, calib, gain, spectrum.k_offset, spectrum.nomSecs, 2, 1);

        // loop over templates
        for (nt = 0; nt < nTemp; nt++) {

            if (spectrum.fitMode == 1) {
                n = nSelect;
            } else {
                n = nt;
            }

            // pre-calculate weighting factor (diminishes further from peak of interest)
            if (TempEnergyWeight) {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    eBin = bgndTempLoKev + i * sampTemp.gain[n];
                    if (eBin < ePeak) {
                        weightFac[i] = minEnergyWeightLo + (eBin - bgndTempLoKev) * c1;
                        weightFac[i] = pow(weightFac[i], EnergyWeightPowerLo);
                    } else {
                        weightFac[i] = MinEnergyWeightHi + (BgndTempHiKeV - eBin) * c2;
                        weightFac[i] = pow(weightFac[i], energyWeightPowerHi);
                    }
                    if (weightFac[i] < 0) weightFac[i] = 0;
                }
            } else {
                for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                    weightFac[i] = 1.0;
                }
            }

            // skip non-blank templates with too high a concentration
            //if(sampTemp.conc[n]>MaxBlankConc and sampTemp.conc[n] >= calib.conc[spectrum.calibSamp]) continue;

            // for calibration mode, skip non-blank templates with too high a concentration
            if (spectrum.fitMode == 1 and
                sampTemp.conc[n] >= calib.conc[spectrum.calibSamp])
                continue;

            // check if normalized counts in Compton peaks are similar
            if (TempFitPeakCheck) {
                if (2 * fabs(double(spectrum.bgndPeakRawCnts - sampTemp.bgndPeakRawCnts[n])) /
                    (spectrum.bgndPeakRawCnts + sampTemp.bgndPeakRawCnts[n]) > MaxBgndPeakCntDiff)
                    continue;
            }

            //ALOGI(" Template %d has similar peak counts (%d vs %d)", n+1, sampTemp.bgndPeakRawCnts[n], (int)spectrum.bgndPeakRawCnts);

            // start with crude scaling
            delScaleFac = 2 * spectrum.crudeScaleFac / CrudeScaleIter;
            scaleFac = (float) (1.0 - spectrum.crudeScaleFac);

            // total number of crude and fine scaling iterations
            numScaleIter = CrudeScaleIter + FineScaleIter;

            for (m = 0; m < numScaleIter; m++) {

                // reset at start of fine scaling
                if (m == CrudeScaleIter) {
                    if (FineScaleIter > 0) {
                        delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);
                    }
                    scaleFac = (scaleFacMin - MaxFineScaleFac);
                }

                cnt = 0;
                chisq = 0;

                for (i = 0; i < sampTemp.sNumBins[n]; i++) {

                    sbin = sampTemp.sLoBin[n] + i;

                    if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {

                        // for type=1 skip bins under peaks that are not to be included for background template matching
                        if (type == 1 and !spectrum.includeBgnd[sbin]) continue;

                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];

                        //ALOGI(" fitTemplate: template %d: sval=%f tval=%f", n+1, sval, tval);
                        valDiff = sval - tval;
                        chisq += weightFac[i] * valDiff * valDiff;
                        cnt++;
                    }
                }
                if (cnt > 0) {
                    chisq /= cnt;
                    if (chisq < chisq_min) {

                        // check that counts are greater than background in analysis peak ROI
                        sCnts = 0;
                        tCnts = 0;
                        for (i = 0; i < MaxNumAnalyzeElements; i++) {
                            eid = analyzeElementID[i];
                            for (j = 0; j < numAnalyzePeaks[eid]; j++) {
                                id = excludePeakID[eid][j];
                                lo_bin = spectrum.element[eid].line[id].lo_bin;
                                hi_bin = spectrum.element[eid].line[id].hi_bin;
                                for (k = 0; k < sampTemp.sNumBins[n]; k++) {
                                    sbin = sampTemp.sLoBin[n] + k;
                                    if (sbin >= lo_bin and sbin <= hi_bin) {
                                        sCnts += spectrum.counts[sbin];
                                        tCnts += scaleFac * sampTemp.sCounts[n][k];
                                    }
                                }

                            }
                        }
                        //ALOG("\nfitTemplates: sCnts = %d tCnts = %d", sCnts, tCnts);

                        // only include templates which have excess counts above the template background in analysis peak ROI
                        if (sCnts > tCnts) {
                            chisq_min = chisq;
                            n_min = n;
                            scaleFacMin = scaleFac;
                            gainMin = gain;
                            //ALOG("fitTemplate: Template %d: scaleFac=%f chisq=%f", n_min+1, scaleFacMin, sqrt(chisq));
                        }

                    }
                }

                scaleFac += delScaleFac;

            } // end looping over scaling iterations

        }  // end looping over templates

        ecalFac += delEcalFac;

    } // end looping over ecal iterations

    // scale templates with gain from best match
    scaleTemplates(sampTemp, calib, gainMin, spectrum.k_offset, spectrum.nomSecs, 2, 1);

    //ALOG("\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f", matchSamp, matchMeas, n_min+1, spectrum.calibSubType+1, scaleFacMin, sqrt(chisq));

    // print out best match for debugging
    if (PrintDebug) {
        ofp = fopen("tempMatch.txt", "a+");
        fprintf(ofp, "\nsample=%d meas=%d bestMatch=%d (calibSubtype=%d): scaleFac=%f chisq=%f",
                matchSamp, matchMeas, n_min + 1, spectrum.calibSubType + 1, scaleFacMin,
                sqrt(chisq));
        for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
            sbin = sampTemp.sLoBin[n_min] + i;
            if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {
                sval = (float) spectrum.counts[sbin];
                tval = scaleFacMin * sampTemp.sCounts[n_min][i];
                if (spectrum.includeBgnd[sbin] == 1) {
                    bval = 10;
                } else {
                    bval = 0;
                }
                fprintf(ofp, "\n%d %d %f %f %f %d", sbin, spectrum.includeBgnd[sbin],
                        (spectrum.k_offset + spectrum.k_gain * sbin), sval, tval, bval);
            }
        }
        fclose(ofp);
    }

    // store best match for substrate and calibration sample type
    spectrum.subType = sampTemp.subType[n_min];
    spectrum.tempType = n_min;

    // store bgnd for each bin in range of best matched template
    sumVal = 0;
    for (i = 0; i < sampTemp.sNumBins[n_min]; i++) {
        sbin = sampTemp.sLoBin[n_min] + i;
        if (sbin >= 0 && sbin < MaxNumBins && i < MaxTempBins) {
            tval = scaleFacMin * sampTemp.sCounts[n_min][i];
            spectrum.bgnd[sbin] = tval;
            sumVal += tval;
        }
    }

    // set total normalization factor used to scale template counts for background template matching
    spectrum.tempNormFac = scaleFacMin * sampTemp.normFac[n_min];

    // determine gain drift from default value
    if (spectrum.defKGain > 0) {
        kGainDrift = fabs(gainMin - spectrum.defKGain) / (spectrum.defKGain) * 100;
    } else {
        kGainDrift = 0;
    }

    // set spectrum gain to gain determined from template stretching
    if (UseTemplateGain and kGainDrift < spectrum.GainDriftNull) {
        spectrum.k_gain = gainMin;
    }

    // set spectrum gain to gain determined from template stretching
    if (UseTemplateGain) {
        spectrum.k_gain = gainMin;
    }

    // store chisq of template match
    spectrum.tempChisq = sqrt(chisq_min);

    ALOGI(" fitTemplate: BestTemplate Match = %d (calibSub=%d) BestScaleFac=%f SpecGain=%f BestGain=%f",
          n_min + 1, spectrum.calibSubType + 1, scaleFacMin, spectrum.k_gain, gainMin);

    return (n_min);

}

int PeakFit::findTemplate(Spectrum &spectrum, int type) {

    // Find the best scaled template that best matches the current spectrum in just the selected energy regions
    //
    // Return the template that best matches the current spectrum in these regions
    //
    // Type=1: Only use templates created with blank calibration samples
    // Type=2: Use all templates created from all calibration samples
    //

    int cnt, bval, i, ei, m, n, nTemp, n_min = 0, tbin, sbin;
    int concID, loBin, hiBin, numScaleIter;
    float sumCnts, sumBgnd, sval, tval;
    float eBin, mgcm2;
    float chisq, chisq_min;
    float scaleFac, scaleFacMin = 1.0f, delScaleFac;

    // define select regions to fit the template to spectrum
//    int numFitPoints = 1;
//    float eFitPoint[3][2] = {
//            80.0, 115.0,
//            62.5,  64.5,
//            78.5,  80.5
//    };

    int numFitPoints = 3;
    float eFitPoint[3][2] = {
            0.0,  71.0,
            76.0,  83.0,
            86.0, 115.0
    };

    bool includePoint;
    bool PrintDebug = false;
    FILE *ofp;

    // update Compton peak information (this is also called initializeElements)
    spectrum.findComptonPeak();

    // find the best scaled template that matches the spectrum
    chisq_min = (float) 1.E8;

    nTemp = sampTemp.numTemplates;
    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // scale templates
    scaleTemplates(sampTemp, calib, spectrum.k_gain, spectrum.k_offset, spectrum.nomSecs, type, 1);

    // loop over templates
    for (n = 0; n < nTemp; n++) {

        // skip non-blank templates for type=1
        if (type == 1 && sampTemp.conc[n] > MaxBlankConc) continue;

        // check if normalized counts in Compton peaks are similar
        //if (TempFitPeakCheck) {
        //  if (2 * fabs(double(spectrum.bgndPeakRawCnts - sampTemp.bgndPeakRawCnts[n])) /
        //    (spectrum.bgndPeakRawCnts + sampTemp.bgndPeakRawCnts[n]) > MaxBgndPeakCntDiff)
        //  continue;
        //}

        // start with crude scaling
        delScaleFac = 2 * spectrum.crudeScaleFac / CrudeScaleIter;
        scaleFac = (float) (1.0 - spectrum.crudeScaleFac);

        // total number of crude and fine scaling iterations
        numScaleIter = CrudeScaleIter + FineScaleIter;

        for (m = 0; m < numScaleIter; m++) {

            // reset at start of fine scaling
            if (m == CrudeScaleIter) {
                if (FineScaleIter > 0) delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);
                scaleFac = (scaleFacMin - MaxFineScaleFac);
            }

            cnt = 0;
            chisq = 0;

            for (i = 0; i < sampTemp.sNumBins[n]; i++) {

                sbin = sampTemp.sLoBin[n] + i;

                if (sbin > 0 && sbin < MaxNumBins && i < MaxTempBins) {

                    eBin = spectrum.k_offset + sbin * spectrum.k_gain;

                    // only include bins inside energy regions for the selected energy regions
                    includePoint = false;
                    for (ei = 0; ei < numFitPoints; ei++) {
                        if (eBin >= eFitPoint[ei][0] and eBin <= eFitPoint[ei][1]) {
                            includePoint = true;
                            break;
                        }
                    }

                    if (includePoint) {
                        sval = (float) spectrum.counts[sbin];
                        tval = scaleFac * sampTemp.sCounts[n][i];

                        //ALOGI(" findTemplate: template %d: sval=%f tval=%f", n+1, sval, tval);
                        chisq += (sval - tval) * (sval - tval);
                        cnt++;
                    }
                }
            }
            if (cnt > 0) {
                chisq /= cnt;
                if (chisq < chisq_min) {
                    chisq_min = chisq;
                    n_min = n;
                    scaleFacMin = scaleFac;
                    // ALOG("quickFitTemplate: Template %d: scaleFac=%f chisq=%f", n_min+1, scaleFacMin, sqrt(chisq));
                }
            }

            scaleFac += delScaleFac;

        } // end looping over scaling iterations

    }  // end looping over templates


    return (n_min);

}


int PeakFit::checkForRTA(Spectrum &spectrum)
{

    int nTemp;
    int status=0;

    // Returns 1 if best template fit over Compton peak belongs to an RTA template (othherwise 0)
    nTemp = findTemplate(spectrum, 1);

    if (nTemp>=calib.nNonRTA) status = 1;

    ALOG("checkForRTA: nTemp=%d", nTemp+1);

    return(status);

}

int PeakFit::matchTemplates(SampTemplate &sampTemp, int tempID, int mode) {
    // Return the template that best matches template tempID
    // (templates being matched are within the same template  object - to
    //  match template tempID from sampTemp with reference templates from
    //  refTemp, use routine matchRefTemplates() )
    //
    // If routine is called twice, it will return 2nd best match, etc.
    //
    // Mode=1: Only match with templates created from blank calibration samples
    // Mode=2: Match with all templates created from all calibration samples
    // Mode=3: Reset function so that the next call will return best match again, etc.

    int cnt, i, n, nTemp, n_min = 0;
    float tval1, tval2;
    float chisq, chisq_min;
    bool skip = false;

    static int ntry = 0;
    static int nUsed[MaxNumTemp];

    // reset static counter
    if (mode == 3) {
        ntry = 0;
        n_min = 0;
    } else {

        nTemp = sampTemp.numTemplates;
        if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

        // find the best template match
        chisq_min = (float) 1.E8;
        for (n = 0; n < nTemp; n++) {

            // don't match template with itself
            if (n == tempID) continue;

            // skip matched templates from previous calls to the routine
            for (i = 0; i < ntry; i++) {
                if (n == nUsed[i]) {
                    skip = true;
                    break;
                }
            }
            if (skip) continue;

            // for mode=1 skip non-blank templates
            if (mode == 1 && sampTemp.conc[n] > MaxBlankConc) continue;

            cnt = 0;
            chisq = 0;
            for (i = 0; i < sampTemp.sNumBins[n]; i++) {
                if (i < MaxTempBins) {
                    tval1 = sampTemp.sCounts[tempID][i];
                    tval2 = sampTemp.sCounts[n][i];
                    chisq += (tval1 - tval2) * (tval1 - tval2);
                    cnt++;
                }
            }
            if (cnt > 0) {
                chisq /= cnt;
                if (chisq < chisq_min) {
                    chisq_min = chisq;
                    n_min = n;
                }
            }
            //ALOGI(" matchTemplates: Matching Template %d with %d: chisq=%f", tempID+1, n+1, sqrt(chisq));

            // used templates
            nUsed[ntry] = n_min;
            ntry++;

        }

    }

    return (n_min);

}

int PeakFit::matchRefTemplates(SampTemplate &sampTemp, SampTemplate &refTemp, int tempID, int mode,
                      int smooth) {
    // Return the reference template that best matches template tempID
    // (templates being matched are from different template objects - to
    //  match template tempID from sampTemp with other templates from
    //  sampTemp, use routine matchTemplates() )
    //
    // Mode=1: Only match with templates created from blank calibration samples (with scaling)
    // Mode=2: Match with all templates created from all calibration samples (with scaling)
    // Mode=3: Same as mode=1, but no scaling used during match
    // Mode=4: Same as mode=2, but no scaling used during match
    //

    int cnt, i, j, jcnt, n, numIter, m, nTemp, n_min = 0;
    float scaleFac = 0.0f, delScaleFac = 0.0f;
    float timeFac, tval1, tval2;
    float chisq, chisq_min;
    float eBin, ePeak, weightFac;
    bool PrintDebug = false;
    FILE *ofp;

    if (FineScaleIter > 0) delScaleFac = 2 * MaxFineScaleFac / (FineScaleIter + 1);

    nTemp = refTemp.numTemplates;
    if (nTemp > MaxNumTemp) nTemp = MaxNumTemp;

    // find the best scaled reference template match to template tempID
    chisq_min = (float) 1.E8;
    for (n = 0; n < nTemp; n++) {

        if ((mode == 1 || mode == 3) && refTemp.conc[n] > MaxBlankConc) continue;

        // check the templates being matched have same number of bins
        if (sampTemp.sNumBins[tempID] != refTemp.sNumBins[n]) {
            spectrum->createMessage(TempMisMatchCode, 4, 0, 0, 0, 0);
        }

        if (mode >= 3) {
            numIter = 1;
            scaleFac = 1.0f;
        } else {
            numIter = FineScaleIter;
            scaleFac = (float) (1.0 - MaxFineScaleFac);
        }

        // must also scale to account for different live times of templates
        timeFac = sampTemp.liveTime[n] / refTemp.liveTime[n];

        // peak of element of interest
        ePeak = (float) lineKeV[0][ka1];

        for (m = 0; m < numIter; m++) {

            cnt = 0;
            chisq = 0;

            for (i = 0; i < refTemp.sNumBins[n]; i++) {
                if (i < MaxTempBins) {

                    // compare smoothed values
                    if (smooth == 1) {
                        jcnt = 0;
                        tval1 = 0;
                        tval2 = 0;
                        for (j = -TempSmoothSize; j <= TempSmoothSize; j++) {
                            if ((i + j) >= 0 && (i + j) < MaxNumBins) {
                                tval1 += sampTemp.sCounts[tempID][i + j];
                                tval2 += scaleFac * timeFac * refTemp.sCounts[n][i + j];
                                jcnt++;
                            }
                        }
                        tval1 /= jcnt;
                        tval2 /= jcnt;
                        // compare raw values
                    } else {
                        tval1 = sampTemp.sCounts[tempID][i];
                        tval2 = scaleFac * timeFac * refTemp.sCounts[n][i];
                    }

                    // calculate weighting factor (diminishes further from peak of interest)
                    eBin = bgndTempLoKev +
                           i * (BgndTempHiKeV - bgndTempLoKev) / refTemp.sNumBins[n] + 1;
                    if (eBin < ePeak) {
                        weightFac = (eBin - bgndTempLoKev) / (ePeak - bgndTempLoKev);
                    } else {
                        weightFac = (BgndTempHiKeV - eBin) / (BgndTempHiKeV - ePeak);
                    }
                    if (weightFac < 0) weightFac = 0;

                    //ALOGI(" bin %d: eBin=%f weightFac=%f", i+1, eBin, weightFac);
                    chisq += weightFac * (tval1 - tval2) * (tval1 - tval2);
                    cnt++;
                }
            }
            if (cnt > 0) {
                chisq /= cnt;
                if (chisq < chisq_min) {
                    chisq_min = chisq;
                    n_min = n;
                    //scaleFacMin = scaleFac;
                }
            }

            scaleFac += delScaleFac;

        }
    }
    //ALOGI(" matchRefTemplates: Matching Template %d with Reference Template %d: scaleFac=%f chisq=%f", tempID+1, n_min+1, scaleFacMin, sqrt(chisq));

    // print out best match for debugging
    if (PrintDebug) {
        ofp = fopen("tempRMatch.txt", "a+");
        fprintf(ofp, "\n%d", tempID + 1);
        for (i = 0; i < refTemp.sNumBins[n_min]; i++) {
            if (i < MaxTempBins) {
                tval1 = sampTemp.sCounts[tempID][i];
                tval2 = scaleFac * refTemp.sCounts[n_min][i];
                fprintf(ofp, "\n%d %f %f", i, tval1, tval2);
            }
        }
        fclose(ofp);
    }

    return (n_min);

}


float PeakFit::accumAverage(int num, float value, int reset)
{

    // accumulate last num values as average and flush out oldest

    int i, avCnt, averNum;
    static int cnt;
    static float valArray[25]={0};
    float average=0;
    float avSum=0;
    float maxOutlierDiff=0.25f;

    if(reset==1) cnt = 0;

    cnt++;
    ALOG("cnt = %d, num = %d, reset = %d", cnt, num, reset );
    ALOG("Before Shuffle valArray = { %f, %f, %f, %f %f", valArray[4], valArray[3],valArray[2],valArray[1],valArray[0]);

    // shuffle down other values one place
    for(i=num-1; i>0; i--) {
        valArray[i] = valArray[i-1];
        ALOG("valArray[%d][%f] replaced with valArray[%d][%f]", i, valArray[i], i-1, valArray[i-1]);
    }

    ALOG("After Shuffle valArray = { %f, %f, %f, %f %f", valArray[4], valArray[3],valArray[2],valArray[1],valArray[0]);
    // put newest value in first location
    valArray[0] = value;
    ALOG("Replace 0th elem with %f", valArray[0]);

    if(cnt<num) {
        averNum = cnt;
    } else {
        averNum = num;
    }

    // average values
    average = 0;
    for(i=0; i<averNum; i++) {
        average += valArray[i];
    }
    if(averNum>0) {
        average /= averNum;
        ALOG("Average of valArray = %f number avged %d", average, averNum);
    }

    // now remove outliers
    avCnt=0;
    avSum=0;
    for(i=0; i<averNum; i++) {
        if(fabs(valArray[i]-average)/average<maxOutlierDiff) {
            avSum += valArray[i];
            avCnt++;
        }
    }
    if(avCnt>0) {
        average = avSum / avCnt;
        ALOG("Remove outliers Average of valArray = %f", average);
    }

    return(average);
}

void PeakFit::analyzeCounts(Spectrum& spectrum) {

    int i, j, n, nROI, cent_bin[2], lo_bin, hi_bin, maxCnts[2] = {0}, winWidth, totCnts, nSpectra;
    float keV_cent[2][2], keV_lo[2], sig[2], keV_hi[2], bgndCnts[2], counts[2], bgnd[2], slope, inter;
    float SNR1=0, SNR2=0, maxSNR=0;

    element_id elemID = Pb;

    spectrum.nIncrement++;
    ALOG("nIncrement=%d", spectrum.nIncrement);

    // fit k-alpha and k-beta peaks for incremental spectrum
    nSpectra = 2;
    totCnts = accumSpecMem(nSpectra, spectrum);
    SNR1 = spectrum.fitPeaksBgnd(elemID, ka1, elemID, kb1, 3);


    // fit k-alpha and k-beta peaks for cumulative spectrum
//    if(spectrum.nIncrement>1) {
//        SNR2 = spectrum.fitPeaksBgnd(elemID, ka1, elemID, kb1, 2) / sqrt(spectrum.nIncrement);
//    }

    // find maximum SNR from incremental and cumulative spectra
    if(SNR1>SNR2) {
        maxSNR = SNR1;
    } else {
        maxSNR = SNR2;
    }

    // default detection level
    spectrum.PbLevel = 0;

    // set result flag using highest SNR (check for Null spectrum first)
    ALOG(" ");
    ALOG(" ");
    ALOG(" ");
    results.SNR = maxSNR;
    if(spectrum.totCnts==0) {
        results.result = dNull;
        ALOG("Blank Spectrum - NULL Reading");
    } else if(maxSNR>spectrum.minPbSNR) {
        //results.result = dPositive;
        ALOG(" ***** LEAD DETECTED *****");

        // set detection level
        if(maxSNR<spectrum.defPbSNR) {
            //spectrum.PbLevel = 1;
            results.result = dInconclusive;
        } else {
            //spectrum.PbLevel = 2;
            results.result = dPositive;
        }

    } else {
        results.result = dNegative;
        ALOG(" ***** NO LEAD Found *****");
    }
    ALOG(" SNR1=%f SNR2=%f (Min = %f)", SNR1, SNR2, spectrum.minPbSNR);
    ALOG(" ");
    ALOG(" ");
    ALOG(" ");

}


void PeakFit::analyzeCounts(Spectrum& spectrum, SampTemplate& sampTemp, Calib& calib, /*PeakFitResults& results,*/ element_id elemID, int analyticMode, meas_mode measMode) {

    // analyze the counts under the peak above the background in the ROI defined for the peak
    //
    // Input variables:
    //    measMode: Selects whether in Action level (AL) or Detection Level (DL) mode
    //    subType:  <0:  All background templates can be fitted to the spectrum
    //              >=0: Only fit background templates with substrate type subType
    //

    int i, n, lo_bin, hi_bin;
    int concID, counts;
    float bgnd;
    float keV_lo, keV_hi;
    float mgcm2;
    float conc, concLo, concHi, concMin, concMax, w1 = 0.0f, w2 = 0.0f;
    float ratio, ratioMax = 0.0f, calcConc = 0.0f;
    time_t currentTime;
    int reset = 0;

    line_id lineID = analyzePeakCo57[0][0];

    bool checkReadThrough(Spectrum &, int elemID);

    float SoilCalCoeff1 = spectrum.coefficient1;
    float SoilCalCoeff2 = spectrum.coefficient2;

    // loop over all peaks to be analyzed for element elemID
    for(n=0; n<numAnalyzePeaks[elemID]; n++) {

        // loop over all peaks to be analyzed for element elemID
        for (n = 0; n < numAnalyzePeaks[elemID]; n++) {

            // check gain has been set
            if (spectrum.k_gain <= 0) break;

            // get line to analyze, depending on element and source type
            if(strcmp(spectrum.srcType,"Co57")==0 or strcmp(spectrum.srcType,"co57")==0) {
                lineID = analyzePeakCo57[elemID][n];
            } else {
                lineID = analyzePeakCd109[elemID][n];
            }

            // fit background templates to spectrum
            if (FitAllBgndTemp) {
                // find best match to blank and non-blank background templates and store spectrum background
                results.tempType = fitAllTemplates(spectrum, sampTemp, calib, elemID, lineID, 1);
            } else {
                // find best match to blank background templates and store spectrum background
                results.tempType = fitTemplates(spectrum, sampTemp, calib, elemID, lineID, 1);

            }

            // first find bin limits for ROI under peak
            keV_lo = (float) lineKeVLo[elemID][lineID];
            keV_hi = (float) lineKeVHi[elemID][lineID];
            lo_bin = int((keV_lo - spectrum.k_offset) / spectrum.k_gain + 0.5);
            hi_bin = int((keV_hi - spectrum.k_offset) / spectrum.k_gain + 0.5);
            if (lo_bin < 0) lo_bin = 0;
            if (hi_bin > MaxNumBins) hi_bin = MaxNumBins - 1;

            counts = 0;
            bgnd = 0;
            for (i = lo_bin; i < hi_bin; i++) {
                counts += spectrum.counts[i];
                bgnd += spectrum.bgnd[i];
            }

            ALOG("analyzeCounts: peak=%d lo_bin=%d hi_bin=%d Raw = %d Bgnd = %d Cnts = %d", n + 1,
                 lo_bin, hi_bin, counts, (int) bgnd, counts - (int) bgnd);

            // store values in spectrum array
            if (lineID == ka1 || lineID == ka2) {
                spectrum.element[elemID].kAlphaRaw = counts;
                spectrum.element[elemID].kAlphaBgnd = bgnd;
                spectrum.element[elemID].kAlphaCnts = counts - bgnd;
                if (bgnd > 0) {
                    spectrum.element[elemID].kAlphaBgndRatio = (counts - bgnd) / bgnd;
                } else {
                    spectrum.element[elemID].kAlphaBgndRatio = 0;
                }
            } else if (lineID == kb1 || lineID == kb2 || lineID == kb3) {
                spectrum.element[elemID].kBetaRaw = counts;
                spectrum.element[elemID].kBetaBgnd = bgnd;
                spectrum.element[elemID].kBetaCnts = counts - bgnd;
                if (bgnd > 0) {
                    spectrum.element[elemID].kBetaBgndRatio = (counts - bgnd) / bgnd;
                } else {
                    spectrum.element[elemID].kBetaBgndRatio = 0;
                }
                // calculate alpha/beta ratio
                if (spectrum.element[elemID].kAlphaCnts > 0 &&
                    spectrum.element[elemID].kBetaCnts > 0) {
                    spectrum.element[elemID].alphaBetaRatio =
                            spectrum.element[elemID].kBetaCnts /
                            spectrum.element[elemID].kAlphaCnts;
                } else {
                    spectrum.element[elemID].alphaBetaRatio = 0;
                }

                // ALOG("\nanalyzeCounts: kAlphaCnts=%d kBetaCnts=%d ratio=%f", (int)spectrum.element[elemID].kAlphaCnts,
                //	     (int)spectrum.element[elemID].kBetaCnts, spectrum.element[elemID].alphaBetaRatio);
            }

        }

        // set raw calibration factor depending on element and matched background template
        spectrum.element[elemID].kCalibRawFac = avCalibRaw[elemID][spectrum.tempType];

        // set calibration ratio factors depending on element and matched background template
        for (i = 0; i < calib.nConcLevels; i++) {
            spectrum.element[elemID].kCalibRatioFac[i] = calibRatio[elemID][spectrum.tempType][i];
        }

        // concentration of matched background template (usually zero)
        spectrum.element[elemID].tempConc = calib.conc[spectrum.tempType];

        // first calculate approximate measured concentration using average calibration factor
        spectrum.element[elemID].calcConc(spectrum.srcMCi, calib.srcMCi, spectrum.nomSecs, lineID, 0);

        // preliminary values using average calibration factor
        mgcm2 = spectrum.element[elemID].mgcm2;
        concID = getConcID(calib, mgcm2);
        ALOGI(" analyzeCounts: Preliminary mgcm2 = %f", mgcm2);

        // calculate measured concentration using specific calibration factor for concID
        spectrum.element[elemID].calcConc(spectrum.srcMCi, calib.srcMCi, spectrum.nomSecs, lineID, concID);

        ALOGI(" analyticMode=%d", analyticMode);

        // Soil In-Situ Analysis (full peak fitting - templates only used to determine whether test stand or insitu mode is being used)
        if (analyticMode==1) {

          spectrum.fitPeaksBgnd(W, kb2, elemID, ka1);

          ratio = spectrum.element[elemID].kAlphaCnts_fit / spectrum.element[elemID].kAlphaBgnd_fit;

          calcConc = (spectrum.soilInsitCoeff2 * ratio * ratio + spectrum.soilInsitCoeff1 * ratio + spectrum.soilInsitOffset) / 1000;

          if(spectrum.nomSecs<10) {
              reset = 1;
          } else {
              reset = 0;
          }
          spectrum.element[elemID].mgcm2 = accumAverage(5, calcConc, reset);
          ALOG("Coeff2 = %f, Coeff1 = %f, ratio = %f, avgMgCm2 = %f, calconc = %f", spectrum.soilInsitCoeff2, spectrum.soilInsitCoeff1,ratio,  spectrum.element[elemID].mgcm2, calcConc);

          results.tempType = 999;  // HACK prevent null reading for soils

        // Soils TestStand mode
        }else if (analyticMode==3) {

          spectrum.fitPeaksBgnd(W, kb2, elemID, ka1);

          ratio = spectrum.element[elemID].kAlphaCnts_fit / spectrum.element[elemID].kAlphaBgnd_fit;

          calcConc = (spectrum.soilCalCoeff2 * ratio * ratio + spectrum.soilCalCoeff1 * ratio + spectrum.soilCalOffset) / 1000;

          if(spectrum.nomSecs<10) {
              reset = 1;
          } else {
              reset = 0;
          }
          spectrum.element[elemID].mgcm2 = accumAverage(5, calcConc, reset);
          ALOG("Coeff2 = %f, Coeff1 = %f, ratio = %f, avgMgCm2 = %f, calconc = %f", spectrum.soilCalCoeff2, spectrum.soilCalCoeff1,ratio,  spectrum.element[elemID].mgcm2, calcConc);

          // round negative values up to zero
          if(spectrum.element[elemID].mgcm2<0) spectrum.element[elemID].mgcm2 = 0;

          results.tempType = 999;  // HACK prevent null reading for soils

          // Dust wipes analysis (fit a blank template, but use a polynomial fit rather than calibration factors from sysCalibF to determine concentration)
        }else if(analyticMode==2) {

          ratio = (spectrum.element[elemID].kAlphaRaw - spectrum.element[elemID].kAlphaBgnd) / spectrum.element[elemID].kAlphaBgnd;

          // if curvature is negative (dustCalCoeff2<0) then find maximum of polynomial fit curve
          if(spectrum.dustCalCoeff2<0) {
              ratioMax = (float) (-0.5 * spectrum.dustCalCoeff1 / spectrum.dustCalCoeff2);
              concMax = spectrum.dustCalCoeff2 * ratioMax * ratioMax + spectrum.dustCalCoeff1 * ratioMax + spectrum.dustCalOffset;
          }

          // use polynomial fit if ratio is below ratioMax or for positive curvature, otherwise use linear extrapolation from concMax
          if(spectrum.dustCalCoeff2>0 or ratio<=ratioMax) {
              spectrum.element[elemID].mgcm2 = spectrum.dustCalCoeff2 * ratio * ratio + spectrum.dustCalCoeff1 * ratio + spectrum.dustCalOffset;
          } else {
              spectrum.element[elemID].mgcm2 = ratio/ratioMax * concMax;
          }

          ALOGI("ratio=%f (ratioMax=%f) conc=%f (concMax=%f)", ratio, ratioMax, spectrum.element[elemID].mgcm2*1000, concMax*1000);

          // subtract offset
          //spectrum.element[elemID].mgcm2 += spectrum.dustCalOffset;

          // round negative values up to zero
          if(spectrum.element[elemID].mgcm2<0) spectrum.element[elemID].mgcm2 = 0;

          ALOGI(" #### Template Chisq = %f ####", spectrum.tempChisq);

        }
    }
    // updated values
    mgcm2 =  spectrum.element[elemID].mgcm2;
    concID = getConcID(calib, mgcm2);
    //ALOGI(" analyzeCounts: Secondary mgcm2 = %f", mgcm2);

    ALOG("analyzeCounts: concID(%d) elemID(%d) spectrum.tempType(%d)", concID, elemID,
         spectrum.tempType);

    // set SD in measurement depending on element, matched background template, and measured concentration
    // (interpolate between concentration values)
    conc = calib.concLevels[concID];
    concMin = calib.concLevels[1];
    concMax = calib.concLevels[calib.nConcLevels - 1];

    if (mgcm2 < concMin) {
        spectrum.element[elemID].mgcm2SD = avMgcm2SD[elemID][spectrum.tempType][1];
    } else if (mgcm2 > concMax) {
        spectrum.element[elemID].mgcm2SD = avMgcm2SD[elemID][spectrum.tempType][calib.nConcLevels -
                                                                                1];
    } else if (concID > 0 && mgcm2 < conc) {
        concLo = calib.concLevels[concID - 1];
        concHi = calib.concLevels[concID];
        w1 = (concHi - mgcm2) / (concHi - concLo);
        w2 = (mgcm2 - concLo) / (concHi - concLo);
        spectrum.element[elemID].mgcm2SD = w1 * avMgcm2SD[elemID][spectrum.tempType][concID - 1] +
                                           w2 * avMgcm2SD[elemID][spectrum.tempType][concID];
        //ALOGI(" analyzeCounts 1: mgcm2=%f conc=%f concID=%d concLo=%f concHi=%f w1=%f w2=%f",  mgcm2, conc, concID, concLo, concHi, w1, w2);
    } else if (concID < calib.nConcLevels - 1 && mgcm2 > conc) {
        concLo = calib.concLevels[concID];
        concHi = calib.concLevels[concID + 1];
        if (concHi > concLo) {
            w1 = (concHi - mgcm2) / (concHi - concLo);
            w2 = (mgcm2 - concLo) / (concHi - concLo);
        }
        spectrum.element[elemID].mgcm2SD = w1 * avMgcm2SD[elemID][spectrum.tempType][concID] +
                                           w2 * avMgcm2SD[elemID][spectrum.tempType][concID + 1];
        //ALOGI(" analyzeCounts 2: mgcm2=%f conc=%f concID=%d concLo=%f concHi=%f w1=%f w2=%f",  mgcm2, conc, concID, concLo, concHi, w1, w2);
    }

    // check for read through (moved this from hac 8/13/16)
    results.readThrough = checkReadThrough(spectrum, elemID);

    // calculate results and/or determine if measurement should continue
    if (results.measMode == AL) {

        // calculate positive/negative result for Action Level (AL) mode
        spectrum.element[elemID].calcDetermAL(spectrum.srcMCi, calib.srcMCi, spectrum.nomSecs,
                                              spectrum.liveTime, actionLevelAL[elemID],
                                              results.negConfLev, results.posConfLev,
                                              results.measMode, results.RTGPresent);
        ALOG("++++++++ CalcDetermAL Done Result = %d", spectrum.element[elemID].elemResult);

    } else {

        // calculate result for Detection Level modes
        spectrum.element[elemID].calcDetermDL(spectrum.srcMCi, calib.srcMCi, spectrum.nomSecs,
                                              spectrum.liveTime, actionLevelDL[elemID],
                                              results.negConfLev, results.posConfLev,
                                              fixedTimeNomSecs, measMode);

        // at beginning of measurement calculate AL result just for display purposes while in Detection Level modes
        if (!results.ALDone) {
            //spectrum.element[elemID].calcDetermAL(spectrum.srcMCi, calib.srcMCi, spectrum.nomSecs, spectrum.liveTime, actionLevelAL[elemID], results.negConfLev, results.posConfLev, results.measMode, results.RTGPresent);
            results.ALmgCm2 = spectrum.element[elemID].mgcm2;
            results.ALerror = spectrum.element[elemID].error;
            results.ALResult = spectrum.element[elemID].elemResult;

            if (results.ALResult != dInProcess) {
                results.ALDone = true;
            }
            ALOG("Extended Mode: ALResult=%d ALmgCm2=%f ****", results.ALResult, results.ALmgCm2);
        }
    }

    // separation of Compton peaks in spectrum and matched template
    spectrum.bgndPeakSepKeV =
            spectrum.bgndPeakCentKeV - sampTemp.bgndPeakCentKeV[spectrum.tempType];

    results.result = spectrum.element[elemID].elemResult;
    results.mgCm2 = spectrum.element[elemID].mgcm2;
    results.mgCm2_quick = spectrum.element[elemID].mgcm2_quick;
    results.confLev = spectrum.element[elemID].kConfLev;
    results.stdDev = spectrum.element[elemID].error;
    results.alphaBetaRatio = spectrum.element[elemID].alphaBetaRatio;
    results.tempChisq = spectrum.tempChisq;
    results.tempNormFactor = spectrum.tempNormFac;

    // override result if best-matched template flipped during last incremental spectrum
    if(TempFitCheck && spectrum.tempType!=spectrum.prevTempType) {
        results.result = dInProcess;
    }
    spectrum.prevTempType = spectrum.tempType;

}

int PeakFit::getConcID(Calib &calib, float conc) {

    // Returns the closest concentration ID (as defined in calibParam) for the measured concentration

    int i, imin = 0;
    float diff, diff_min = 1.E4;

    for (i = 0; i < calib.nConcLevels; i++) {
        diff = fabs(conc - calib.concLevels[i]);
        if (diff < diff_min) {
            diff_min = diff;
            imin = i;
        }

    }

    return imin;

}

int PeakFit::readPb200iFile(const char *fname, int binMode) {

    // Reads Heuresis Pb200i spectrum file called fname into spectrum array
    //  Inputs:
    //           fname: pointer to input file name
    //
    //  Return Value: Read Status (<0: File not found 0: Too few counts, 1: Good status)
    //

    int i, j, nbins, totCnt = 0, rval, sum;
    int TECTemp = 0, hiVolt = 0, boardTemp = 0;
    int slowCnt = 0, fastCnt = 0, fastCntCorr = 0;
    int fastPeakTime = 0;
    int avCnt, avVal, tmpCnts[MaxNumBins];
    float slowPeakTime = 0;
    float accTime = 0, liveTime = 0, realTime = 0;
    char str[1024];
    bool PrintParams = false;
    FILE *ifp;

    // open spectrum file
    ifp = fopen(fname, "r");
    if (ifp == NULL) {
        ALOGI(" Cannot open input spectrum file %s", fname);
        return -666;
    } else {

        // read file header information
        while (!feof(ifp)) {

            fscanf(ifp, "%[^,\n]%*c", str);
            //ALOGI(" str=%s", str);

            if (strstr(str, "Board Temp") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                boardTemp = atoi(str);
            }

            if (strstr(str, "Detector Temp") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                TECTemp = atoi(str);
            }

            if (strstr(str, "Fast Count") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                fastCnt = atoi(str);
            }

            if (strstr(str, "Slow Count") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                slowCnt = atoi(str);
            }

            if (strstr(str, "Real Time") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                realTime = (float) atof(str);
            }

            if (strstr(str, "Accum Time") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                accTime = (float) atof(str);
            }

            if (strstr(str, "Bias Voltage") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                hiVolt = atoi(str);
            }

            if (strstr(str, "TPEA") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                slowPeakTime = (float) atof(str);
            }

            if (strstr(str, "TPFA") != NULL) {
                fscanf(ifp, "%[^,\n]%*c", str);
                fastPeakTime = (int) atof(str);
            }

            // look for start of data
            //if(strstr(str,"spectrum")!=NULL) break;
            if (strstr(str, "<<Data>>") != NULL) break;

        }

        // now read data
        nbins = 0;
        totCnt = 0;
        while (!feof(ifp)) {
            fscanf(ifp, "%[^,\n]%*c", str);
            if (strstr(str, "<<End>>") != NULL) break;
            if (nbins >= MaxNumBins) {
                break;
            }
            dppSpec.spectrum_[nbins] = (uint32_t) atoi(str);

            // check that there are not excessive counts in the bin
            if (dppSpec.spectrum_[nbins] > MaxBinCounts) dppSpec.spectrum_[nbins] = 0;

            //ALOGI(" readPb200iFile: %d: counts=%d", nbins+1, dppSpec.spectrum_[nbins]);
            totCnt += dppSpec.spectrum_[nbins];
            nbins++;
        }

        fclose(ifp);

        // rebin the data if necessary
        if (binMode > 1) {
            sum = 0;
            j = 0;
            for (i = 0; i < nbins; i++) {
                sum += dppSpec.spectrum_[i];
                if (i > 0 && fmod((float) i, (float) binMode) == 0) {
                    dppSpec.spectrum_[j] = (uint32_t) sum;
                    sum = 0;
                    j++;
                }
            }
            // new number of bins
            nbins /= binMode;
        }

        // smooth the data if requested
        if (spectrum->DataSmoothSize > 0) {
            for (i = 0; i < nbins; i++) {
                // first copy to temporary array
                tmpCnts[i] = dppSpec.spectrum_[i];
            }
            for (i = spectrum->DataSmoothSize; i < nbins - spectrum->DataSmoothSize; i++) {
                avCnt = 0;
                avVal = 0;
                for (j = -spectrum->DataSmoothSize; j <= spectrum->DataSmoothSize; j++) {
                    avVal += tmpCnts[i + j];
                    avCnt++;
                }
                dppSpec.spectrum_[i] = (uint32_t) (avVal / avCnt);
            }
        }

        // use default peaking time if not set from the file header
        if (fastPeakTime == 0) {
            fastPeakTime = DefFastPeakTime;
        }

        // calculate live time and dead time
        if ( fastCnt > 0 && slowCnt < fastCnt ) {

            // first correct the fast count for deadtime in the fast channel
            fastCntCorr = int(fastCnt * accTime / (accTime - fastCnt * fastPeakTime * 1.E-9));

            //liveTime = (float)slowCnt/fastCnt * accTime;
            liveTime = (float) slowCnt / fastCntCorr * accTime;

        } else {
            liveTime = accTime;
            ALOGI(" readPb200iFile: WARNING: Fast count is zero, so using accumulated time for live time");
        }

        dppSpec.numBins_ = (uint16_t) nbins;
        dppSpec.accTime_ = accTime;
        dppSpec.fastCount_ = (uint32_t) fastCnt;
        dppSpec.fastCntCorr_ = (uint32_t) fastCntCorr;
        dppSpec.slowCount_ = (uint32_t) slowCnt;
        dppSpec.TECTemp_ = (uint32_t) TECTemp;
        dppSpec.boardTemp_ = (uint32_t) boardTemp;
        dppSpec.liveTime_ = liveTime;

        if (PrintParams) {
            ALOGI(" Number bins = %d", nbins);
            ALOGI(" Fast Peak Time = %d", fastPeakTime);
            ALOGI(" Slow Peak Time = %f", slowPeakTime);
            ALOGI(" High Volt.  = %d V", hiVolt);
            ALOGI(" TEC Temp.   = %d K", TECTemp);
            ALOGI(" Board Temp. = %d C", boardTemp);
            ALOGI(" Fast Count = %d", fastCnt);
            ALOGI(" Fast Count (corr) = %d", fastCntCorr);
            ALOGI(" Slow Count = %d", slowCnt);
            ALOGI(" Acc. Time  = %f", accTime);
            ALOGI(" Real Time  = %f", realTime);
            ALOGI(" Live Time  = %5.1f%%", liveTime / accTime * 100);
        }


        if (fastCnt > slowCnt && totCnt > MinSkipCounts) {
            rval = 1;
        } else {
            rval = 0;
        }

    }

    return (rval);
}

int PeakFit::getFileName(char *fname, Calib &calib) {

    // Creates sequential file names
    //
    // return value: Sample number for current file
    // zero if end reached

    int nval;
    static int n = 0, m = 0;

    char str[5];

    strcpy(fname, calib.name[n]);
    if (m + 1 < 10) {
        strcat(fname, "_00");
    } else if (m + 1 < 100) {
        strcat(fname, "_0");
    } else {
        strcat(fname, "_");
    }
    sprintf(str, "%d", m + 1);
    strcat(fname, str);
    strcat(fname, ".csv");

    // sample number for current file
    nval = n + 1;

    // increment measurement counter
    m++;

    // increment sample counter
    if (m >= calib.nMeasure[n]) {
        m = 0;
        n++;
    }

    return (nval);

}

int PeakFit::readFileName(char *fname, int nStart, int nStop) {

    // Creates sequential reading* file names
    //
    // return value: File number for current file
    //               <0 if end reached

    int nval;
    static int n = 0;

    if (n == 0) n = nStart;

    char str[5];

    strcpy(fname, "reading");
    if (n < 10) {
        strcat(fname, "00");
    } else if (n < 100) {
        strcat(fname, "0");
    }
    sprintf(str, "%d", n);
    strcat(fname, str);
    strcat(fname, ".csv");

    // current file number
    nval = n;
    if (n > nStop) nval = -1;

    n++;

    return (nval);

}

int PeakFit::calibFileName(char *fname, int n, int m, int mode) {

    // Creates file name for nth calibration sample and mth measurement
    // mode=1: Only one measurement per calibration sample, so don't add _xxx to end of file
    // mode=2: Multiple measurements per calibration sample, so add _xxx to end of file

    char str[5];

    strcpy(fname, CalibSamplesRoot);
    strcat(fname, calib.name[n]);

    if (mode == 2) {
        if (m + 1 < 10) {
            strcat(fname, "_00");
        } else if (m + 1 < 100) {
            strcat(fname, "_0");
        } else {
            strcat(fname, "_");
        }
        sprintf(str, "%d", m + 1);
        strcat(fname, str);
    }
    strcat(fname, ".csv");

    ALOG("fname = %s", fname);
    return (n + 1);

}

void PeakFit::calibFileNameOld(char *fname, int n, int m) {

    // creates file name for nth calibration sample and mth measurement

    // Create calibration file name to read in (calib01 (1), ...calib01 (100), ...calib99 (100)... etc)
    // First number is sample number, second number is measurement number

    char str[5];

    strcpy(fname, CalibFileRoot);
    if (n + 1 < 10) {
        strcat(fname, "0");
    }
    sprintf(str, "%d", n + 1);
    strcat(fname, str);
    strcat(fname, " (");

    sprintf(str, "%d", m + 1);
    strcat(fname, str);
    if (InputFileType == 1) {
        strcat(fname, ").mca");
    } else {
        strcat(fname, ").csv");
    }

}

void PeakFit::acquireCalibSpectra(DppSpectrum dppSpec) {

    // Acquires calibration spectra. Uses a temporary file to continue where it left off in case of a system crash

    int i, n = 0, m, mode, rstatus;
    uint32_t cumCnts[MaxCalibSamples][MaxNumBins];
    float totLiveTime;
    char fname[50];
    float kGainCum[MaxCalibSamples];
    float kOffsetCum[MaxCalibSamples];

    int buildTemplate(Calib &calib, int nSample, unsigned int *binCnts, int numBins,
                      float totLiveTime, float lo_keV, float hi_keV, float gain, float offset);
    void outputSpectrum(unsigned int *binCnts, int numBins, int fileNum, float offset, float gain,
                        int mode);

    // initialize cumulative counts array
    for (n = 0; n < calib.nSamples; n++) {
        for (i = 0; i < MaxNumBins; i++) {
            cumCnts[n][i] = 0;
        }
        kGainCum[n] = DefaultKGain * SpecBinMode;
        kOffsetCum[n] = DefaultKOffset;
    }

    ALOG("acquireCalibSpectra nSamples = %d", calib.nSamples);
    // acquire spectra
    n = 0;
    while (n < calib.nSamples) {

        if (n >= calib.nSamples) break;

        totLiveTime = 0;

        if (calib.nMeasure[n] == 1) {
            if (SuppressSuffix) {
                mode = 1;
            } else {
                mode = 2;
            }
        } else {
            mode = 2;
        }

        ALOG("acquireCalibSpectra nMeasure = %d", calib.nMeasure[n]);
        for (m = 0; m < calib.nMeasure[n]; m++) {

            // read spectra from disk
            if (InputFileType == 1) {
                calibFileNameOld(fname, n, m);
                rstatus = readMCAFile(fname);
            } else {
                calibFileName(fname, n, m, mode);
                rstatus = readPb200iFile(fname, SpecBinMode);
            }
            ALOGI(" acquireCalibSPectra: Acquiring spectrum file %s", fname);

            // skip spectra that have too few counts (usually due to shutter mafunction)
            if (rstatus == 0) {
                ALOGI(" WARNING: Too few counts in input spectrum %s", fname);
                continue;
            }

            // initialize spectrum
            spectrum->initSpectrum(dppSpec);

            // initialize elements
            spectrum->initializeElements();

            // increment total livetime over all measurements for the calibration sample
            totLiveTime += spectrum->liveTime;

            // fill cumulative bin counts array for cmode==1
            for (i = 0; i < spectrum->numBins; i++) {
                cumCnts[n][i] += spectrum->counts[i];
            }

        } // end looping over measurements for sample

        if (spectrum->performEnergyCal) {
            loadSpectrum(cumCnts[n], spectrum->numBins, totLiveTime, dppSpec, *spectrum);

            if (calib.subType[n] == (BlkAirSubType - 1)) {
                spectrum->energyCalSpec( 2);
            } else {
                spectrum->energyCalSpec( 1);
            }
            calib.ecalFWHM[n] = spectrum->ecalFWHM;
        }

        // write cumulative spectrum for sample over all measurements to spectrum file (mode=1 for calibration)
        outputSpectrum(cumCnts[n], spectrum->numBins, n, spectrum->k_offset, spectrum->k_gain, 1);

        ALOG("acquireCalibSpectra Done outputSpectrum");

        // create template output file from cumulative spectrum for calibration sample (uses updated gain and offset)
        calib.nTemplates = buildTemplate(calib, n, cumCnts[n], spectrum->numBins, totLiveTime,
                                         bgndTempLoKev, BgndTempHiKeV, spectrum->k_gain,
                                         spectrum->k_offset);

        ALOG("acquireCalibSpectra Done buildTemplate");
        // only needed for offline testing
        n++;

    }
}

/*******************************************************************************************************************************
 *
 * EXTERN C function below needed for JNI access from Java classes
 *
 ******************************************************************************************************************************/
extern "C"
{
// Get the number of bin to smooth either the default or override if it exists.
int getDataSmoothSize() {
    int smoothBins = spectrum->DataSmoothSize;

    if ((overrideSmooth > 0) && (overrideSmooth < MaxNumBins)) {
        smoothBins = overrideSmooth;
    }

    return (smoothBins);
}

int setSpectrumSmoothing(int smoothingBins) {

    if ((smoothingBins > 0) && (smoothingBins < MaxNumBins))
        overrideSmooth = smoothingBins;

    ALOG("OverrideSmooth = %d", overrideSmooth);

    return overrideSmooth;
}


bool SetActionLevel(/*JNIEnv *env, jobject obj,*/ float fActionLevel, int elemID) {
    // Set action level for element elemID

    actionLevelAL[elemID] = fActionLevel;

    ALOG("SetActionLevel");
    return true;
}

bool SetActionLevelDL(JNIEnv *env, jobject obj, float fActionLevelDL, float SDAboveActionLevelDL,
                      float SDBelowActionLevelDL, int elemID) {
    // Set action level for element elemID

    actionLevelDL[elemID] = fActionLevelDL;
    positiveConfidenceLevelLimitDL = SDAboveActionLevelDL;
    negativeConfidenceLevelLimitDL = SDBelowActionLevelDL;

    ALOG("SetActionLevelDL");
    return true;
}

bool SetActionLevelAL(JNIEnv *env, jobject obj, float fActionLevelAL, float SDAboveActionLevelAL,
                      float SDBelowActionLevelAL, int elemID) {
    // Set action level for element elemID

    actionLevelAL[elemID] = fActionLevelAL;
    positiveConfidenceLevelLimitAL = SDAboveActionLevelAL;
    negativeConfidenceLevelLimitAL = SDBelowActionLevelAL;

    ALOG("SetActionLevelAL %f %f %f", fActionLevelAL, SDAboveActionLevelAL, SDBelowActionLevelAL);
    return true;
}

bool SetDetectionLevel(JNIEnv *env, jobject obj, float detectLevel, int elemID) {
    // Set detection level for element elemID

    actionLevelDL[elemID] = detectLevel;

    return true;
}

float SetNomSecsDL(JNIEnv *env, jobject obj, float nomSecs) {

    // This call to nomSeconds is necessary to set the exact nominal seconds.
    fixedTimeNomSecs = nomSecs;

    ALOG("SetNomSecsDL: %2.2f %2.2f", nomSecs, fixedTimeNomSecs);

    return fixedTimeNomSecs;
}

bool setPbSensitivity( float snr ) {
    spectrum->minPbSNR = snr;
    return true;
}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_SetActionLevel(
        JNIEnv *env, jclass obj, float fActionLevel, int elemID) {

    bool res = SetActionLevel(/*env, obj,*/ fActionLevel, elemID);
    ALOG("SetActionLevel %f, %d %d", fActionLevel, elemID, res);

    return (jboolean) res;
}


JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_SetActionLevelDL(
        JNIEnv *env,
        jobject obj,
        float fActionLevelDL,
        float SDAboveActionLevelDL,
        float SDBelowActionLevelDL,
        int elemID) {

    return (jboolean) SetActionLevelDL(env, obj, fActionLevelDL, SDAboveActionLevelDL,
                                       SDBelowActionLevelDL,
                                       elemID);

}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_SetActionLevelAL(
        JNIEnv *env,
        jobject obj,
        float fActionLevelAL,
        float SDAboveActionLevelAL,
        float SDBelowActionLevelAL,
        int elemID) {

    return (jboolean) SetActionLevelAL(env, obj, fActionLevelAL, SDAboveActionLevelAL,
                                       SDBelowActionLevelAL,
                                       elemID);

}

JNIEXPORT jfloat JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_SetNomSecsDL(JNIEnv *env,
                                                                                       jobject obj,
                                                                                       float fnomSecs) {

    return SetNomSecsDL(env, obj, fnomSecs);

}


JNIEXPORT jint JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_sourceCorrectSeconds(
        JNIEnv *env,
        jobject obj,
        int realMilliseconds) {
    // return corrected millisecond to account for source decay.
    //return spectrum->sourceCorrectMS( realMilliseconds );
    return (jint) spectrum->realSeconds(realMilliseconds);

}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_calibration(JNIEnv *env,
                                                                                        jobject obj) {
    return (jboolean) 0; //calibration();

}

JNIEXPORT jint JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_setSpectrumSmoothing(
        JNIEnv *env,
        jobject obj,
        int smoothingBins) {
    // Override the smoothing setting for the spectrum.
    return setSpectrumSmoothing(smoothingBins);

}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_hacInit(JNIEnv *env,
                                                                                    jobject obj,
                                                                                    int mode,
                                                                                    int analyticalMode,
                                                                                    int type ) {
    // Override the smoothing setting for the spectrum.
    return (jboolean) hacInit(mode, analyticalMode, type);

}

JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_PeakFitWrapper_createSpectrum(JNIEnv *env,
                                                                                    jobject obj,
                                                                                    int type) {
    // Override the smoothing setting for the spectrum.
    if (type == 0)
        spectrum = new Spectrum();
    else  if (type == 1)
        spectrum = new SpectrumHBI();

    ALOG("Spectrum Created Type %d, Spectrum 0x%x", type, &spectrum);

    return (jboolean) (spectrum == NULL);

}

JNIEXPORT jboolean JNICALL
Java_com_heuresistech_xrfservices_PeakFitWrapper_setPbSensitivity(JNIEnv *env, jobject instance,
                                                                  jfloat snr) {
    ALOG("Pb Sensitivity set to %f", snr);
    return (jboolean) setPbSensitivity( snr );

}

}
