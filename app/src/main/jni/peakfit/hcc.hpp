#ifndef HCC_HPP
#define HCC_HPP

#include "Spectrum.hpp"
#include "DppDefs.hpp"
#include "DppSpectrum.hpp"
#include "DppCalData.hpp"
#include "Calib.hpp"
#include "PeakFitResults.hpp"

/** Heuresis Calibration Code.  This function processes the supplied spectra to
 *  create calibration factors
 *
 * \param [in]  dppSpec  Spectrum and other supporting data from the DPP
 * \param [in]  calData  Calibration data - for future use
 */
bool hccInit( int mode);

void hcc(const DppSpectrum &dppSpec,
         Spectrum &spectrum,
         Calib &calib,
         int analyticMode,
         meas_mode measMode,
         int calibMode);

int readMCAFile(const char *fname);
int quickEnergyCal(Spectrum &spectrum, int elemID1, int lineID1, int elemID2, int lineID2);

#endif // HCC_HPP
