#ifndef PEAKFIT_H
#define PEAKFIT_H

#include <cstdint>

#include "DppSpectrum.hpp"
#include "PeakFitResults.hpp"

#include "PeakFitPrototypes.hpp"
#include "Line.hpp"
#include "Element.hpp"
#include "Spectrum.hpp"

class PeakFit {
public:
    PeakFit();

    bool calibration( ) ;

    int checkForRTA(Spectrum &spectrum);

    void analyzeCounts(Spectrum &spectrum);

    void analyzeCounts(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib,
                        element_id elemID, int analyticMode,
                        meas_mode measMode);



    void loadTemplates(SampTemplate &sampTemp, int mode, int analyticMode);

    void scaleTemplates(SampTemplate &sampTemp, Calib &calib, float gain, float offset, float liveTime,
                   int type, int mode);

    void loadSingleTemplate(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, int tempID,
                            int mode);

    int fitTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                     line_id lineID, int mode);

    int fitTemplates2(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                               line_id lineID, int type);

    int quickFitTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                                   line_id lineID, int type);

    int fitAllTemplates(Spectrum &spectrum, SampTemplate &sampTemp, Calib &calib, element_id elemID,
                                 line_id lineID, int type);

    int findTemplate(Spectrum &spectrum, int type);

    int matchTemplates(SampTemplate &sampTemp, int tempID, int mode);

    int matchRefTemplates(SampTemplate &sampTemp, SampTemplate &refTemp, int tempID, int mode,
                          int smooth);


    float accumAverage(int num, float value, int reset);

    int getConcID(Calib &calib, float conc);

    int readPb200iFile(const char *fname, int binMode);

    int getFileName(char *fname, Calib &calib);

    int readFileName(char *fname, int nStart, int nStop);

    int calibFileName(char *fname, int n, int m, int mode);

    void calibFileNameOld(char *fname, int n, int m);

    void acquireCalibSpectra(DppSpectrum dppSpec);

};

extern "C"{
    int getDataSmoothSize();
    int setSpectrumSmoothing(int smoothingBins);
}

#endif
