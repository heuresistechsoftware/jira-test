#include "Line.hpp"

#include <cstdio>
#include <cmath>

#include "PeakFit.hpp"

void Line::calibrate(float offset, float gain) {
    centroidBin = (keV - offset) / gain;
};


void Line::approxFWHM(float energyRes, float gain) {

    //FWHM_bins = (float)sqrt(keV/100.f) * (energyRes*100.f)/gain;
    FWHM_bins = sqrt(keV / 100.f) * energyRes / gain;
    lo_bin = int(centroidBin - (PeakExcludeWidth * FWHM_bins) / 2);
    hi_bin = int(centroidBin + (PeakExcludeWidth * FWHM_bins) / 2);
    //printf("\n approxFWHM: %s keV=%f FWHM_bins=%f centroidBin=%d centroidCnts=%d centroidBgnd=%d lo_bin=%d hi_bin=%d",
    //	lineName[id], keV, FWHM_bins, centroidBin, centroidCnts, int(centroidBgnd), lo_bin, hi_bin);
};


void Line::calcFWHM(float energyRes, float gain) {

    float dx, c = 0.6006f;  //c = 1/(2*sqrt(ln2))

    FWHM_keV = sqrt(keV / 100.f) * (energyRes / 100.f) * keV;
    FWHM_bins = FWHM_keV / gain;

    // determine bin limits for line (dx is #bins from centroid for [centroidCnts-bgndCnts] to decline to 0.1 counts)
    if (centroidCnts > centroidBgnd + 1) {
        dx = c * (float) sqrt(log(10 * double(centroidCnts - centroidBgnd))) * FWHM_bins;
    } else {
        dx = FWHM_bins / 2;
    }

    // round to closest bin and add 1 to account for errors in centroid
    dx = int(dx + 0.5) + 1.0f;

    lo_bin = int(centroidBin - dx);
    hi_bin = int(centroidBin + dx);

    printf("\n calcFWHM: keV=%f FWHM_bins=%f centroidBin=%d centroidCnts=%d centroidBgnd=%d 2dx=%d lo_bin=%d (%5.2f keV) hi_bin=%d (%5.2f keV)",
           keV, FWHM_bins, (int) centroidBin, centroidCnts, int(centroidBgnd), int(2 * dx), lo_bin,
           lo_bin * gain, hi_bin, hi_bin * gain);

};
