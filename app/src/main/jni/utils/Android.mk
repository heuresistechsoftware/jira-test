LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE           := crypto
LOCAL_SRC_FILES        := ../pre-compiled/libcrypto.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := utils-jni

LOCAL_C_INCLUDES := \
	$(TOP_PATH)/include 

LOCAL_SRC_FILES := \
	gpio.cpp  \
#	crypto.cpp

#-march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9
LOCAL_CPPFLAGS := -std=c++11 -Ofast  -mfpu=vfpv3-fp16 \
                  -mfloat-abi=softfp

LOCAL_LDLIBS := -L$(SYSROOT)/../usr/lib -llog
#LOCAL_SHARED_LIBRARIES := crypto
#LOCAL_LDLIBS := -L$(LOCAL_PATH)/pre-compiled -lcrypto
#LOCAL_LDLIBS += -llog 

include $(BUILD_SHARED_LIBRARY)
