
#include <string>
#include <openssl/aes.h>
#include "android/log.h"
#define CRYPTO_LOG_TAG "Crypto"
#define CRYPT_LOG(...) __android_log_print(ANDROID_LOG_INFO, CRYPTO_LOG_TAG, __VA_ARGS__)


class Crypto{
public:
	Crypto();
	~Crypto();

	int encrypt( std::string &inBuf, std::string &outBuf );
	int decrypt( std::string &encrypted, std::string &decrypt );


private:

	void hex_print(const void* pv, size_t len);

	unsigned char aes_key[16];
	unsigned char iv[AES_BLOCK_SIZE];
	AES_KEY enc_key, dec_key;
};

extern int cryptoTest();
