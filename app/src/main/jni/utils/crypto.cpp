
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "crypto.h"

unsigned char keys[] = {0x35, 0x23, 0x74, 0x25, 0x32, 0x73, 0x56, 0x6e, 0x6a, 0x4e, 0x56, 0x4e, 0x39, 0x37, 0x24, 0x37 };

Crypto::Crypto() {
	memcpy(aes_key, keys, 16*sizeof(unsigned char));
}

Crypto::~Crypto(){

}

int Crypto::encrypt(  std::string &inBuf,  std::string &outBuf ){

	//! Set the encryption key
	AES_set_encrypt_key(aes_key, sizeof(aes_key)*8, &enc_key);

	//! Clear the initialization vector
	memset(iv, 0x00, 16);

	// round size up to next AES_BLOCK size
	int size = inBuf.size()  ;

	//CRYPT_LOG("Encrypt %d bytes", size );

	int outSize = size + AES_BLOCK_SIZE - 1 - (size -1) % AES_BLOCK_SIZE;
	outBuf.resize((unsigned int) outSize);

	//! Encrypt the raw text string.
	AES_cbc_encrypt((unsigned char *)inBuf.c_str(), (unsigned char *)outBuf.c_str(), (size_t) size, &enc_key, iv, AES_ENCRYPT);

	//! Sent the encrypted buffer in the outBuf
	//CRYPT_LOG("OutBuf size %d", outBuf.size());

	return 0;
}

int Crypto::decrypt( std::string &encrypted, std::string &decrypt ){

	//! Set the decryption key
	AES_set_decrypt_key(aes_key, 128, &dec_key); // Size of key is in bits

	//! Clear the IV (initialization vector)
	memset(iv, 0x00, 16);

	// Set up for decrypting input stream
	int size = encrypted.size();

	decrypt.resize((unsigned int) size);

	//! Decrypt the incoming string.
	AES_cbc_encrypt((unsigned char *)encrypted.c_str(), (unsigned char *)decrypt.c_str(),
                    (size_t) size, &dec_key, iv, AES_DECRYPT);

	return 0;
}

int cryptoTest( )
{
	int size;
	std::fstream  source, dest, decode;
	Crypto cryp;
	std::string rawText;

	char sourceFile[] = "/sdcard/test.txt";

//// Open a plain text file and encrypt it.
	source.open( sourceFile, std::fstream::in | std::fstream::ate);
	size = source.tellg(); // the output of encryption can take extra space
	CRYPT_LOG("Test.txt size = %d", size);
	source.seekg(0);

	char *inText = new char[size];
	//rawText.resize(size);
	source.readsome( inText, size );  // Read raw text into buffer
	source.close();

	std::fstream dumpOut;
	dumpOut.open("/sdcard/dumpOut.txt", std::fstream::out | std::fstream::ate);
	dumpOut.seekg(0);
	dumpOut << inText;
	dumpOut.close();

	rawText.insert(0, inText);
	std::string encryptedStr ;
	CRYPT_LOG("rawText size %d", rawText.size() );

	cryp.encrypt( rawText, encryptedStr );

	std::string destFile = "/sdcard/test.enc";
	dest.open( destFile.c_str(), std::fstream::out | std::fstream::ate );
	CRYPT_LOG("Dest Size %d", (int)encryptedStr.size() );
	dest.seekg(0);

	dest << encryptedStr; //Write encrypted data out to text.enc
	dest.close();
/// Decode the encryption and store the decoded file
	decode.open( "/sdcard/test.dec", std::fstream::out );
	decode.seekg(0);

	std::string decryptedStr;

	cryp.decrypt( encryptedStr, decryptedStr );

	CRYPT_LOG(" Decrypted: encrypt size %d, decrypt size %d", encryptedStr.size(), decryptedStr.size() );

	decode << decryptedStr;
	decode.close();

	CRYPT_LOG("Finished Crypto");

	return 0;
}

