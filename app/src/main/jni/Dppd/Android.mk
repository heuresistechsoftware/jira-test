LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Dppd-jni

LOCAL_C_INCLUDES := \
	$(TOP_PATH)/libusb \
	$(TOP_PATH)/peakfit \
	$(TOP_PATH)/utils

LOCAL_SRC_FILES := \
	ConsoleHelper.cpp \
    DP5Protocol.cpp \
    DP5Status.cpp \
    DppLibUsb.cpp \
    DppUtilities.cpp \
    ParsePacket.cpp \
    SendCommand.cpp \
    stringex.cpp \
    Dppd.cpp 


#LOCAL_CPPFLAGS := -std=c++11 -O2 -DNDEBUG -g -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9
LOCAL_CPPFLAGS := -std=c++11 -Ofast   \
                    -mfpu=vfpv3-fp16  -mfloat-abi=softfp -DDPP_DEBUG -g

LOCAL_LDLIBS := -llog

LOCAL_SHARED_LIBRARIES := Usb-jni \
			  libPeakfit-jni      \
			  utils-jni
			  
include $(BUILD_SHARED_LIBRARY)



