/**
 * dpp daemon - provides the entry point to the dpp daemon 
 * process that served as an interface to the dpp hardware via 
 * libusb and the maestro daemon via UDP socket. 
 * Most of this code comes directly from the code provided by 
 * Amptek as a unit test program. 
 *  
 */
#include <iostream>
#include <string>
#include <errno.h>
#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>
#include <jni.h>
#include <math.h>
#include "fcntl.h"

#include "ConsoleHelper.h"
#include "stringex.h"

#include "DppCalData.hpp"
#include "DppSpectrum.hpp"
#include "Dppd.h"
#include "hac.hpp"
#include "PeakFit.hpp"
#include "PeakFitPrototypes.hpp"
#include "gpio.h"

#include "android/log.h"
#ifdef DPP_DEBUG
#define LOG_TAG_DPP "Dpp-jni"
#define DLOG(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG_DPP, __VA_ARGS__)
#else
#define DLOG(...)
#endif

using namespace std;

extern PeakFitResults 	results;
extern Spectrum		*spectrum;




#define DPP_CONFIG_FILE "/sdcard/pf/DP5_Config.txt"
#define DPP_FILE_PATH   "/sdcard/pf/"

#define DPP_ENABLE  36  // GPIO 2-4 = pin 36 enables the DPP power
#define DPP_USB_ENABLE 95  // GPIO 3-31 enable usb power to DPP
#define DPP_USB_OVER_CURRENT 94 // GPIO 3-30 indicates USB over current detected

#define MAXBUFLEN 100

Dppd dppd;
DppSpectrum dppSpec;
DppCalData calData;

CConsoleHelper chdpp;                   // Wraps libusb commands for use by DPP


bool bRunSpectrumTest = false;          // run spectrum test
bool bRunConfigurationTest = false;     // run configuration test
bool bHaveStatusResponse = false;       // have status response
bool isConfigFromHW = false;         // have configuration from hardware

pthread_mutex_t lck;
bool spectrumCollectionStopped = true;


bool ReadAndSetConfigFile(  );
bool ConnectToDefaultDPP(int analyticMode);

bool DppInit()
{
	bool results = false;

	// This does not work. It conflicts with kernel boot up
//	gpio_set_dir(DPP_ENABLE, true); // Make enable output
//	gpio_set_value(DPP_ENABLE, true);
//
//	gpio_set_dir(DPP_USB_ENABLE, true);  // Make usb enable output
//	gpio_set_value(DPP_USB_ENABLE, true); //turn on usb power to DPP
//

	return results;
}

//**************************************************************************
//
//! ConnectToDefaultDPP finds all connected DPPs and connected to the default one.
//!
//! The DPP needs to be connected to the libusb stack in order to make any
//! calls to the DPP. The DPP is used to get/set DPP configuration, get
//! spectrum for detector and report status of the DPP.
//!
//!  /return A true(1) is returned if a connection occurs, else a false(0).
//
//**************************************************************************
bool ConnectToDefaultDPP(int analyticMode)
{
   bool status = false;

	DLOG("CALLED -- ConnectToDefaultDPP ");
    if (pthread_mutex_init(&lck, NULL) != 0)
    {
        DLOG("\n mutex init failed\n");
    }

    if(chdpp.LibUsb_Connect_Default_DPP())
	{
	    status = ReadAndSetConfigFile();
        DLOG("CALLED -- ConnectToDefaultDPP %d", status);
	}

    return status;
}

//**************************************************************************
//
//! GetDPPStatus pulls the status info from DPP into memory structure
//!
//! This call extracts the status from the DPP and makes it available
//! to process_status command that actually populates the DPPStatus class
//! and generates a status-string.
//!
//!  /return A true(1) is status was obtained otherwise false(0).
//
//**************************************************************************
bool GetDppStatus( )
{
	bool retStatus = false;
	//DLOG("GetDppStatus start ");


    if (chdpp.LibUsb_isConnected)
    {
        if (chdpp.LibUsb_SendCommand(XMTPT_SEND_STATUS))
        {
            if (chdpp.LibUsb_ReceiveData())
            {
            	//DLOG("STATUS is ready in DP5Status class ");
                bRunSpectrumTest = true;
                bHaveStatusResponse = true;
                bRunConfigurationTest = true;
                retStatus = true;
            }
        }else{
            DLOG("SetDPPStatus - send command status failed");
            pthread_mutex_unlock( &lck );
            return retStatus;  // return false if SEND_STATUS failed.
        }

        if (chdpp.LibUsb_SendCommand(XMTPT_SEND_SPECTRUM_STATUS)) {
            if (chdpp.LibUsb_ReceiveData()) {

                dppSpec.accTime_ = (float) chdpp.DP5Stat.m_DP5_Status.AccumulationTime;
                dppSpec.biasVolts_ = (uint32_t) chdpp.DP5Stat.m_DP5_Status.HV; // was VREF_IN
                dppSpec.boardTemp_ = (uint32_t) chdpp.DP5Stat.m_DP5_Status.DP5_TEMP;
                dppSpec.fastCount_ = (uint32_t) chdpp.DP5Stat.m_DP5_Status.FastCount;
                dppSpec.fpgaVersion_ = chdpp.DP5Stat.m_DP5_Status.FPGA;
                dppSpec.realTime_ = (float) chdpp.DP5Stat.m_DP5_Status.RealTime;
                dppSpec.slowCount_ = (uint32_t) chdpp.DP5Stat.m_DP5_Status.SlowCount;
                dppSpec.startTime_ = time(NULL);
                dppSpec.TECTemp_ = (uint32_t) chdpp.DP5Stat.m_DP5_Status.DET_TEMP;
                dppSpec.magic_ = CSpectrumMagic;
                dppSpec.version_ = CSpectrumVersion;
                retStatus = true;
//                DLOG("GetDppStatus - Receive Data Det Temp = %d  boardTemp = %d" + dppSpec.TECTemp_, dppSpec.boardTemp_);

            }else{
                DLOG("GetDppStatus - Receive Data Failed");
                retStatus = false;
            }
        } else  {
            DLOG("GetDppStatus - Send Command Failed");
            retStatus = false;
            //acquiredSpec = false;
        }
    }
    //DLOG("GetDPPStatus done");

    return retStatus;
}

//**************************************************************************
//*
//* Gets the detector temp, bias voltage, and board temp from DPP and returns
//* them to the caller. The calling class (Java) must have public int variables
//* named biasVolts, boardTemp, and TECTemp.
//*
//**************************************************************************
bool getDppSysMonitor( JNIEnv * env, jobject  obj ){

    // If we are acquiring spectrum don't access the DPP to get status
    ///if( inAcquireSpectrum == true )
    //    return false;
    pthread_mutex_lock( &lck );

    //DLOG("CALLED -- getDppSysMonitor ");

    // Get a reference to this object's class
    jclass thisClass = env->GetObjectClass(obj);

    jfieldID fidbiasVoltage = env->GetFieldID(thisClass, "biasVolts", "I");

    jfieldID fidboardTemp = env->GetFieldID(thisClass, "boardTemp", "I");

    jfieldID fidTECTemp = env->GetFieldID(thisClass, "TECTemp", "I");


    GetDppStatus();  // get params into dppSpec structure.


    env->SetIntField(obj, fidbiasVoltage, dppSpec.biasVolts_);

    env->SetIntField(obj, fidboardTemp, dppSpec.boardTemp_);

    env->SetIntField(obj, fidTECTemp, dppSpec.TECTemp_);
    pthread_mutex_unlock( &lck );
    //DLOG("END -- getDppSysMonitor ");
    return true;

}

//**************************************************************************
//
//! ReadDppConfigurationFromHardware pulls the config from DPP into a class
//!
//! This function will also load presets into the class.
//!
//! \return True(1) is returned if configuration is read from DPP else false(0).
//
//**************************************************************************
bool ReadDppConfigurationFromHardware()
{
    CONFIG_OPTIONS CfgOptions;
    DLOG("ATTEMPT -- ReadDppConfigurationFramHardware %d %d ",bHaveStatusResponse, bRunConfigurationTest );
    if (bHaveStatusResponse && bRunConfigurationTest)
    {
    	DLOG("CALLED -- ReadDppConfigurationFramHardware ");
       // pthread_mutex_lock( &lck );

        // Set options for XMTPT_FULL_READ_CONFIG_PACKET
        chdpp.CreateConfigOptions(&CfgOptions, "", chdpp.DP5Stat, false);

        // clear all flags, set flags only for specific readback properties
        chdpp.ClearConfigReadFormatFlags();

        //chdpp.DisplayCfg = false;	// DisplayCfg format overrides general readback format

        chdpp.CfgReadBack = true;   // requesting general readback format
        if (chdpp.LibUsb_SendCommand_Config(XMTPT_FULL_READ_CONFIG_PACKET, CfgOptions))
        {
            if (chdpp.LibUsb_ReceiveData())
            {
                if (chdpp.HwCfgReady)
                {
                    isConfigFromHW = true;
                    DLOG("Full HW config read from DPP " );
                    //to access config use this string  chdpp.HwCfgDP5
                }
                else
                {
                	isConfigFromHW = false;
                }
            }
            else
			{
				isConfigFromHW = false;
			}
        }
        else
		{
			isConfigFromHW = false;
		}
       // pthread_mutex_unlock( &lck );
    }
    return isConfigFromHW;
}

//**************************************************************************
//
//! returnDppConfig builds string with the config info and returns it.
//!
//! \return A large string with the full DPP configuration is returned.
//
//**************************************************************************
char *RetrunDppConfig( )
{
	char * retStr = NULL;
	DLOG("CALLED -- ReturnDppConfig ");

	// make sure the chdpp has been updated
	if (isConfigFromHW)
	{
		retStr = (char*)chdpp.HwCfgDP5.c_str();
	}

	return retStr;
}

//**************************************************************************
//
//! returnPresets builds string with the preset info and returns it.
//!
//! \return A string with the preset command and values is returned.
//
//**************************************************************************
jstring ReturnPresets( JNIEnv * env, jobject  obj )
{
	jstring retStr = NULL;

	DLOG("CALLED -- ReturnPresets ");
	// make sure the chdpp has been updated
    if (isConfigFromHW)
    {
    	string output = chdpp.strPresetCmd + "\n" + chdpp.strPresetVal;
    	retStr = env->NewStringUTF( output.c_str() );
    }

    return retStr;
}

//**************************************************************************
//
//! SendPresetAcquisitionTime sets the amount of time the DPP will accumulate spectrum.
//!
//! \return A true(1) is returned if the command is sent to the DPP, else false(0).
//
//**************************************************************************

bool SendPresetAcquisitionTime( string strPRET )
{
	bool status = false;
    CONFIG_OPTIONS CfgOptions;
    chdpp.CreateConfigOptions(&CfgOptions, "", chdpp.DP5Stat, false);

    CfgOptions.HwCfgDP5Out = strPRET;
    DLOG("PresetAcqu : %s", CfgOptions.HwCfgDP5Out.c_str());

    pthread_mutex_lock( &lck );

    // send PresetAcquisitionTime string, bypass any filters, read back the mode and settings
    if (chdpp.LibUsb_SendCommand_Config(XMTPT_SEND_CONFIG_PACKET_EX, CfgOptions)) {
    	//ReadDppConfigurationFromHardware();    // read setting back
    	DLOG("Set value PresetAcqu : %s", CfgOptions.HwCfgDP5Out.c_str());
        status = true;
    }

    pthread_mutex_unlock( &lck );
    return status;
}

//**************************************************************************
//
//! ClearSpectrum causes the DPP to reset the counts from the DPP.
//!
//! A GetDPPStatus is required before ClearSpectrum will work. This is needed
//! to setup the dp5Status structure used in the spectrum processing.
//!
//! \return A true(1) is returned if the spectrum was cleared else false(0).
//
//**************************************************************************
bool ClearSpectrum( )
{
    int i;
	bool results = false;

    pthread_mutex_lock( &lck );

	 //! update values that will be used after next incremental spectrum change
	 spectrum->prevTotCnts = 0;
	 spectrum->prevSlowCnts = 0;
	 spectrum->prevFastCorrCnts = 0;
	 spectrum->prevAccTime = 0;

	 // clear previous counts
     for(i=0; i<MaxNumBins; i++) spectrum->prevCounts[i] = 0;

	 //! In order to clear a spectrum a disable, clear, .
	 results = chdpp.LibUsb_SendCommand(XMTPT_DISABLE_MCA_MCS);
	 results &= chdpp.LibUsb_SendCommand(XMTPT_SEND_CLEAR_SPECTRUM_STATUS);
	 //results &= chdpp.LibUsb_SendCommand(XMTPT_ENABLE_MCA_MCS);

    // reset increment counters
    spectrum->nIncrement = 0;
    dppSpec.nIncrement = 0;

	 DLOG("CALLED -- DPP ClearSpectrum res = %d" , results);

    pthread_mutex_unlock( &lck );

	 return results;
}

//**************************************************************************
//
//! StartSpectrum causes the DPP to start accumulating spectrum.
//!
//! A GetDPPStatus is required before StartSpectrum will work. This is needed
//! to setup the dp5Status structure used in the spectrum processing.
//!
//! \return A true(1) is returned if the accumulation has started else false(0).
//
//**************************************************************************
bool StartSpectrum( )
{
	bool results = false;

    pthread_mutex_lock( &lck );



	 //! In order to continue acquiring a  spectrum an enable sequence is used.
	 //results = chdpp.LibUsb_SendCommand(XMTPT_DISABLE_MCA_MCS);
	 //results &= chdpp.LibUsb_SendCommand(XMTPT_SEND_CLEAR_SPECTRUM_STATUS);

	 results = chdpp.LibUsb_SendCommand(XMTPT_ENABLE_MCA_MCS);
     spectrumCollectionStopped = false;
	 DLOG("CALLED -- DPP StartSpectrum res = %d" , results);

    pthread_mutex_unlock( &lck );

	 return results;
}

//**************************************************************************
//
//! StopSpectrum causes the DPP to stop accumulating spectrum.
//!
//! \return A true(1) is returned if the stop command succeeds, else false(0).
//
//**************************************************************************
bool StopSpectrum(  )
{
    bool results;


    pthread_mutex_lock( &lck );
	results = chdpp.LibUsb_SendCommand(XMTPT_DISABLE_MCA_MCS);
    spectrumCollectionStopped = true;
    DLOG("CALLED -- DPP StopSpectrum result %d", results);
    pthread_mutex_unlock( &lck );

    return results;
}

std::string getAnalyticModeString(int analyticMode){
    std::string modeName;

    switch (analyticMode){
        case 0:
            modeName = "Lead Paint";
            break;
        case 1:
            modeName = "Soils";
            break;
        case 2:
            modeName = "Dust Wipes";
            break;
        default:
            modeName = "Unknown";
    }

    return modeName;

}

//**************************************************************************
//
//! AcquireSpectrum grabs a snapshot of the spectrum and sends it off to be processed.
//!
//! Once the DPP has been started it will continuously accumulate spectrum. During this
//! accumulation snapshots of the spectrum will be sent to the peakfit code for processing.
//! The peakfit code will report back to the app if it has finished with this reading. If
//! it has then stopSpectrum will be called to stop the hardware from accumulating more
//! spectrum.
//!
//! Requires that StartSpectrum be called first.
//! Requires that StopSpectrum be called when no more spectrum are needed.
//!
//! \return A true(1) is returned is a spectrum was retrieved from hardware else a false(0).
//
//**************************************************************************
bool AcquireSpectrum( JNIEnv * env, jobject  obj, int analyticMode, int receivedMeasMode, bool processSpectrum )
{
	bool acquiredSpec = false;
	int specSum = 0;
	meas_mode measure_mode;

	jintArray rawSpectrum_array;
	jintArray normSubSpectrum_array;
	jintArray normSpectrum_array;

	int *rawSpectrum = NULL;
	int *normSubSpectrum = NULL;
	int *normSpectrum = NULL;

    DLOG("AcquireSpectrum - Started ");


    pthread_mutex_lock( &lck );
	// Instead of passing enums, I pass int.  Error check here.
	// And using correct type to be able to pass to function.
	switch (receivedMeasMode) {
	default:
	case 0:
		measure_mode = AL;
		break;

	case 1:
		measure_mode = DLStopAtSetLevel;
		break;

	case 2:
		measure_mode = DLFixedTime;
		break;

	case 3:
		measure_mode = DLUnlimitedTime;
		break;

	case 4:
		measure_mode = DLStopAtStatistics;
		break;
	}

	// Get a reference to this object's class
	jclass thisClass = env->GetObjectClass(obj);


	jfieldID fidresult = env->GetFieldID(thisClass, "result", "I");
	
	jfieldID fidmgCm2 = env->GetFieldID(thisClass, "mgCm2", "F");

	jfieldID fidconfLev = env->GetFieldID(thisClass, "confLev", "F");
	
	jfieldID fidstdDev = env->GetFieldID(thisClass, "stdDev", "F");
	
	jfieldID fidRawSpectrum = env->GetFieldID(thisClass, "rawSpectrum", "[I");
	jfieldID fidNormSubSpectrum = env->GetFieldID(thisClass, "normSubSpectrum", "[I");
	jfieldID fidNormSpectrum = env->GetFieldID(thisClass, "normSpectrum", "[I");

	jfieldID fidaccTime = env->GetFieldID(thisClass, "accTime", "F");

	jfieldID fidbiasVoltage = env->GetFieldID(thisClass, "biasVolts", "I");

	jfieldID fidboardTemp = env->GetFieldID(thisClass, "boardTemp", "I");

	jfieldID fidfastCount = env->GetFieldID(thisClass, "fastCount", "I");

	jfieldID fidslowCount = env->GetFieldID(thisClass, "slowCount", "I");

	jfieldID fidrealTime = env->GetFieldID(thisClass, "realTime", "F");

	jfieldID fidTECTemp = env->GetFieldID(thisClass, "TECTemp", "I");

	jfieldID fidmsgString = env->GetFieldID(thisClass, "dppHwConfigStr", "Ljava/lang/String;");

	jfieldID fidtempType = env->GetFieldID(thisClass, "tempType", "I");

	jfieldID fidnomSecs = env->GetFieldID(thisClass, "nomSecs", "F");

	jfieldID fidnumBins = env->GetFieldID(thisClass, "numBins", "I");

	jfieldID fidk_gain = env->GetFieldID(thisClass, "k_gain", "F");

	jfieldID fidk_offset = env->GetFieldID(thisClass, "k_offset", "F");

	jfieldID fidlive_time = env->GetFieldID(thisClass, "live_time", "F");

    jfieldID fidanalytic_mode = env->GetFieldID(thisClass, "analyticMode", "I");
    jfieldID fidmode = env->GetFieldID( thisClass, "mode", "I");

    jfieldID fidtemp_chisq = env->GetFieldID(thisClass, "tempChisq", "F");

    jfieldID fidtemp_norm_factor = env->GetFieldID(thisClass, "tempNormFactor", "F");

    jfieldID fidmgCm2_noRTG = env->GetFieldID(thisClass, "mgCm2_NoRTG", "F");
    jfieldID fidmgCm2_Near = env->GetFieldID(thisClass, "mgCm2_Near", "F");
    jfieldID fidisReadThrough = env->GetFieldID(thisClass, "isReadThrough", "Z");
    jfieldID fidRTGPresent = env->GetFieldID(thisClass, "RTGPresent", "Z");

	jfieldID fidNullCode = env->GetFieldID(thisClass, "nullCode", "I");

    jfieldID fidALResult = env->GetFieldID(thisClass, "ALResult", "I");
    jfieldID fidALmgCm2 = env->GetFieldID(thisClass, "ALmgCm2", "F");
    jfieldID fidALerror = env->GetFieldID(thisClass, "ALerror", "F");
    jfieldID fidALDone = env->GetFieldID(thisClass, "ALDone", "Z");
    jfieldID fidsourceMCi = env->GetFieldID(thisClass, "srcMCi", "F");

    // GetDppStatus check the DPP Status and gets Spectrum Status filling dppSpec from chdpp.
    if ( GetDppStatus() ) {
        ReadDppConfigurationFromHardware();

//        DLOG("Receive Data Det Temp = %d  boardTemp = %d", dppSpec.TECTemp_, dppSpec.boardTemp_);

        float fastCnt = dppSpec.fastCount_;
        float accTime = dppSpec.accTime_;
        float slowCnt = dppSpec.slowCount_;
        float fastPeakTime = chdpp.fastPeakTime;

        if (fastCnt > 0) {

            // first correct the fast count for deadtime in the fast channel
            dppSpec.fastCntCorr_ = uint32_t(
                    fastCnt * accTime / (accTime - fastCnt * fastPeakTime * 1.E-9));

            if (slowCnt > dppSpec.fastCntCorr_) {
                // this is an error case adjusting for DPP error.
                dppSpec.liveTime_ = accTime;
            } else {
                // Normal operation case.
                dppSpec.liveTime_ = slowCnt / dppSpec.fastCntCorr_ * accTime;
            }

        } else {
            dppSpec.liveTime_ = accTime;
        }

        DLOG("FastCnt %f  SlowCnt %f  fastCntCorr %d  acctTime %f, presetAcq %f ", fastCnt, slowCnt,
             dppSpec.fastCntCorr_, accTime, (float) chdpp.PresetAcq);

        unsigned int highPeakValue = 0;
        int highPeakBin = 0;
        unsigned int mycounts = 0;
        for (int s = 0; s < CSpectrumNumBins; s++) {
            dppSpec.spectrum_[s] = (uint32_t) chdpp.DP5Proto.SPECTRUM.DATA[s];
            mycounts += dppSpec.spectrum_[s];
            if( dppSpec.spectrum_[s] >= highPeakValue){
                highPeakValue = dppSpec.spectrum_[s];
                highPeakBin = s;
            }

        }
        DLOG("Peak Bin %d value %d", highPeakBin, highPeakValue);

        // rebin spectrum data
        int i, j = 0, icnt = 0, rebinNumBins, sumCnt = 0;
        int rebinSpec[MaxNumBins];

        for (i = 0; i < CSpectrumNumBins; i++) {
            sumCnt += chdpp.DP5Proto.SPECTRUM.DATA[i];
            j++;
            if (j == SpecBinMode) {
                rebinSpec[icnt] = sumCnt;
                icnt++;
                sumCnt = 0;
                j = 0;
            }
        }

        rebinNumBins = icnt;

        // smooth spectrum data if DataSmoothSize>0
        int kSize = getDataSmoothSize();
        for (i = 0; i < rebinNumBins; i++) {

            if (kSize > 0 && (i >= kSize && i < (rebinNumBins - kSize))) {
                sumCnt = 0;
                for (j = -kSize; j <= kSize; j++) {
                    sumCnt += rebinSpec[i + j];
                }
                dppSpec.spectrum_[i] = (uint32_t) (sumCnt / (2 * kSize + 1));
            } else {
                dppSpec.spectrum_[i] = (uint32_t) rebinSpec[i];
            }
            specSum += rebinSpec[i];

        }

        if (SpecBinMode > 1)
            dppSpec.numBins_ = (uint16_t) rebinNumBins;
        else
            dppSpec.numBins_ = CSpectrumNumBins;

        DLOG("Acquired Spectrum : Sum bins(%d) accTime(%f) LiveTime(%f) fastPeak %f ps %d", specSum, dppSpec.accTime_, dppSpec.liveTime_, fastPeakTime, processSpectrum);

        // processSpectrum = true; // PJR temporary !!force spectrum to be processed
        if( processSpectrum ){
            acquiredSpectrumCalc(dppSpec, analyticMode, measure_mode);
        }else{
            acquiredSpectrumCalc(dppSpec);
        }


        // Let the first call to acquireSpectrumCalc get the 0 nIncrement so it reset the ALDone flag.
        dppSpec.nIncrement++;

        // Allocate only space we need for the spectrum's that we are
        // sending back.
        rawSpectrum = new int [dppSpec.numBins_];
        normSpectrum = new int [dppSpec.numBins_];
        normSubSpectrum = new int [dppSpec.numBins_];

        // Grab all three spectrum types
        for (int idx = 0; idx < dppSpec.numBins_; idx++) {
            //if( processSpectrum ) {
                //DLOG("Getting processed spectrum");
                rawSpectrum[idx] =  dppSpec.spectrum_[idx];
                normSubSpectrum[idx] =  spectrum->normCountsBSub[idx];
                normSpectrum[idx] =  spectrum->normCounts[idx];

//            }else{
//                //DLOG("Getting raw spectrum");
//                // subtracted spectrum is not used in calibration but fill it in anyway
//                rawSpectrum[idx] = (long) dppSpec.spectrum_[idx];
//                normSubSpectrum[idx] = (long) dppSpec.spectrum_[idx];
//                normSpectrum[idx] = (long) dppSpec.spectrum_[idx];
//            }
        }

        env->SetIntField(obj, fidresult, results.result);

        env->SetFloatField(obj, fidmgCm2, results.mgCm2);

        env->SetFloatField(obj, fidconfLev, results.confLev);

        env->SetFloatField(obj, fidstdDev, results.stdDev);

        env->SetFloatField(obj, fidaccTime, dppSpec.accTime_);

        env->SetIntField(obj, fidbiasVoltage, dppSpec.biasVolts_);

        env->SetIntField(obj, fidboardTemp, dppSpec.boardTemp_);

        env->SetIntField(obj, fidfastCount, dppSpec.fastCount_);

        env->SetIntField(obj, fidslowCount, dppSpec.slowCount_);

        env->SetFloatField(obj, fidrealTime, dppSpec.realTime_);

        env->SetIntField(obj, fidTECTemp, dppSpec.TECTemp_);

        rawSpectrum_array = (jintArray) env->GetObjectField(obj, fidRawSpectrum);
        env->SetIntArrayRegion(rawSpectrum_array, 0, dppSpec.numBins_,
                               (const int *) rawSpectrum );

        normSpectrum_array = (jintArray) env->GetObjectField(obj, fidNormSpectrum);
        env->SetIntArrayRegion(normSpectrum_array, 0, dppSpec.numBins_,
                               (const int *) normSpectrum );

        normSubSpectrum_array = (jintArray) env->GetObjectField(obj, fidNormSubSpectrum);
        env->SetIntArrayRegion(normSubSpectrum_array, 0, dppSpec.numBins_,
                               (const int *) normSubSpectrum );

        jstring dppHwConfigStr = env->NewStringUTF(chdpp.HwCfgDP5.c_str());

        env->SetObjectField(obj, fidmsgString, dppHwConfigStr);

        env->SetIntField(obj, fidtempType, results.tempType);

       env->SetFloatField(obj, fidnomSecs, spectrum->nomSecs);

        env->SetIntField(obj, fidnumBins, dppSpec.numBins_);

        env->SetFloatField(obj, fidk_gain, spectrum->k_gain);

        env->SetFloatField(obj, fidk_offset, spectrum->k_offset);

        env->SetFloatField(obj, fidlive_time, dppSpec.liveTime_);

        env->SetIntField(obj, fidanalytic_mode, analyticMode);
        env->SetIntField(obj, fidmode, measure_mode);

        env->SetFloatField(obj, fidtemp_chisq, results.tempChisq);
        env->SetFloatField(obj, fidtemp_norm_factor, results.tempNormFactor);

        env->SetFloatField(obj, fidmgCm2_noRTG, results.mgCm2_NoRTG);
        env->SetFloatField(obj, fidmgCm2_Near, results.mgCm2_Near);
        env->SetBooleanField(obj, fidisReadThrough, (jboolean) results.readThrough);
        env->SetBooleanField(obj, fidRTGPresent, (jboolean) results.RTGPresent);
        env->SetIntField(obj, fidNullCode, results.nullCode);

        env->SetIntField( obj, fidALResult, results.ALResult);
        env->SetFloatField( obj, fidALmgCm2, results.ALmgCm2);
        env->SetFloatField( obj, fidALerror, results.ALerror);
        env->SetBooleanField(obj, fidALDone, (jboolean) results.ALDone);
        env->SetFloatField( obj, fidsourceMCi, spectrum->srcMCi );

        // Clean up now that we've used it.
        delete[] normSubSpectrum;
        delete[] rawSpectrum;
        delete[] normSpectrum;
        DLOG("Deleted -- norm, raw, sub spectrum");

        // true because we ran thru this code.
        acquiredSpec = true;
        
    }
    else {
       DLOG("AquireSpectrum - Failed to receive USB Data");
       acquiredSpec = false;
    }
    pthread_mutex_unlock( &lck );
    DLOG("AquireSpectrum - Done!");
    return acquiredSpec;
}

//**************************************************************************
//
//! CloseConnections close the connection to the DPP.
//!
//! \return A true(1) is returned if the stop command succeeds, else false(0).
//
//**************************************************************************
bool CloseConnection(  )
{
	DLOG("CALLED -- CloseConnection ");
    chdpp.LibUsb_Close_Connection();
    return chdpp.LibUsb_isConnected;
}


//**************************************************************************
//
//! ReadAndSetConfigFile reads the text file and pushes it to the DPP.
//!
//! \return A zero(0) is returned if the config is successfully loaded, else (1).
//
//**************************************************************************

bool ReadAndSetConfigFile(  )
{
    CONFIG_OPTIONS CfgOptions;
    struct stat st;

    if ( stat(DPP_CONFIG_FILE, &st) == -1 )
    {
    	DLOG( "DPP Set Config Failed" );
        return(0);
    }

    DLOG("CALLED -- ReadAndSetConfigFile ");

    pthread_mutex_lock( &lck );

    chdpp.CreateConfigOptions( &CfgOptions, "", chdpp.DP5Stat, false );
    CfgOptions.HwCfgDP5Out = chdpp.SndCmd.GetDP5CfgStr( DPP_CONFIG_FILE );

    if ( chdpp.LibUsb_SendCommand_Config(XMTPT_SEND_CONFIG_PACKET_EX, CfgOptions ))
    {
    	ReadDppConfigurationFromHardware( );
        DLOG("DPP Initialized");
        pthread_mutex_unlock( &lck );
        return(1);
    }
    else
    {
    	DLOG( "DPP Initialization Failed" );
        pthread_mutex_unlock( &lck );
        return(0);
    }
}

// read an I/O line to determine if battery is powering the system.
// a 1 is returned if battery is powering the system a 0 if hot swap
// is powering the system.
bool isBatteryPower(  )
{
	unsigned int value;

	// make battery power an input
	gpio_set_dir( 93, 0);

	// 93 is the I/O number for GPIO3_29 labeled as on_int_pwr_n is schematics
	gpio_get_value( 93, &value );
	//DLOG("Battery Power = %d", value);

	return (bool) value;
}

// required by JNI interface
extern "C"
{
//-------------------------------------------------------------------------------------------------
//
//  JNI Wrapper CALLS Need to add a line for each Java APP that is calling these common functions
//
//-------------------------------------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_ConnectToDefaultDPP( JNIEnv * env, jobject  obj, int analyticMode )
{
    DLOG( "DPP ConnectToDefaultDPP mode = %d", analyticMode );
   return (jboolean) ConnectToDefaultDPP(analyticMode);
}

/***********************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_ReadingData_AcquireSpectrum( JNIEnv * env, jobject  obj, int analytic_mode, int measure_mode, bool cal )
{
    return (jboolean)AcquireSpectrum( env,  obj, analytic_mode, measure_mode, cal);
}

/***********************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_StopSpectrum( JNIEnv * env, jobject  obj )
{
    DLOG( "DPP Stop Spectrum" );
   return (jboolean) StopSpectrum();
}

/***********************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_ClearSpectrum( JNIEnv * env, jobject  obj )
{
    DLOG( "DPP Clear Spectrum" );
   return (jboolean) ClearSpectrum();
}

/*****************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_StartSpectrum( JNIEnv * env, jobject  obj )
{
    DLOG( "DPP Start Spectrum" );
   return (jboolean) StartSpectrum();
}

/*****************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_Disconnect( JNIEnv * env, jobject  obj )
{
   return (jboolean) CloseConnection();
}

/*****************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_SendPresetAcquisitionTime( JNIEnv * env, jobject  obj, float pret )
{
	char strPRET[6];
	sprintf(strPRET, "%1.1f", pret);
	string presetTime = "PRET=";
	presetTime += strPRET;
	presetTime += ";";
	DLOG("Preset time %s", presetTime.c_str());
	return (jboolean) SendPresetAcquisitionTime(presetTime);
}

/*****************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_isBatteryPower( JNIEnv * env, jobject  obj )
{
	return (jboolean) isBatteryPower();
}

/*****************************************************************************************/
JNIEXPORT jboolean JNICALL Java_com_heuresistech_xrfservices_DppService_getDppSysMonitor( JNIEnv * env, jobject  obj ){

    return (jboolean) getDppSysMonitor(env, obj);
}


}   // end extern "C"
Dppd::Dppd(void) {}
Dppd::~Dppd(void){}
