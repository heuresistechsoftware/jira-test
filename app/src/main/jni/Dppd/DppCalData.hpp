/** \file DppCalData.hpp
 *  \brief DPP Calibration Data File Format
 *  \copyright 2014 by Heuresis
 */
#ifndef DPPCALDATA_HPP
#define DPPCALDATA_HPP

#include <cstdint>
#include <ctime>

#include "DppDefs.hpp"

const uint32_t CCalDataMagic = 0x1eadca1d; // Lead calibration data
const uint32_t CCalDataVersion = 1;

/** Calibration Data for a Bin.
 */
struct BinCalData
{
    uint32_t number_; ///< Bin number
    float energy_;    ///< Bin energy
};

/** DPP CalData File Format.  This struct defines the layout of the CalData
 * data files that are stored by the DPP Manager, and passed to the Heuresis
 * Analysis Code (HAC) for processing, and to the Android App for display.
 */
struct DppCalData
{
  // Header
  uint32_t magic_;                    ///< Must equal CCalDataMagic
  uint32_t version_;                  ///< Must equal CCalDataVersion

  // General info
  time_t calTime_;                    ///< Time of calibration

  // Calibration info
  char energyUnits_[4];               ///< Just a guess right now
  BinCalData bins_[CSpectrumNumBins]; ///< Calibration data per bin
};

#endif // DPPCALDATA_HPP
