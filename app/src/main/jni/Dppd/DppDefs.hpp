/** \file DppDefs.hpp
 *  \brief DPP Definitions
 *  \author Robert S. Grimes
 *  \copyright 2014 by Heuresis
 */
#ifndef DPPDEFS_HPP
#define DPPDEFS_HPP

#include <cstdint>
#include <ctime>

const uint16_t CSpectrumNumBins = 512;

#endif // DPPDEFS_HPP
