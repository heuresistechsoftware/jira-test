/** \file DppSpectrum.hpp
 *  \brief DPP Spectrum File Format
 *  \author Robert S. Grimes
 *  \copyright 2014 by Heuresis
 */
#ifndef DPPSPECTRUM_HPP
#define DPPSPECTRUM_HPP

#include <cstdint>
#include <ctime>

#include "DppDefs.hpp"

const uint32_t CSpectrumMagic = 0xdead1ead;
const uint32_t CSpectrumVersion = 1;

/** DPP Spectrum File Format.  This struct defines the layout of the Spectrum
 * data files that are stored by the DPP Manager, and passed to the Heuresis
 * Analysis Code (HAC) for processing, and to the Android App for display.
 *
 * With the exception of the first three fields, the DPP Status and Spectrum
 * data are all obtained from the DPP Response Packet: "4096-channel spectrum
 * plus Status", PID1 = 0x81, PID2 = 0x0a.  See the Amptek "DP5 Programmer's
 * Guide" for more information about these fields.
 */
struct DppSpectrum
{
	 // Header
	  uint32_t magic_;      ///< Must equal CSpectrumMagic
	  uint32_t version_;    ///< Must equal CSpectrumVersion

	  // General info
	  time_t startTime_;  ///< Start time of the acquisition of this spectrum

	  int nIncrement;

	  // PJR added 9/4/14
	  float liveTime_;

	  // DPP Status data
	  uint32_t fpgaVersion_;  ///< Status[25]
	  uint32_t fastCount_;  ///< Status[3:0]
	  uint32_t slowCount_;  ///< Status[7:4]
	  float accTime_;       ///< Status[15:12]
	  float realTime_;      ///< Status[23:20]
	  uint32_t biasVolts_;  ///< Status[30:31]
	  uint32_t TECTemp_;    ///< Status[32:33]
	  uint32_t boardTemp_;  ///< Status[34]
	  uint32_t fastCntCorr_; /// calculated in acquireSpectrum

	  // DPP Spectrum data
	  uint16_t numBins_;    ///< Maximum CSpectrumNumBins
	  uint32_t spectrum_[CSpectrumNumBins]; ///< Spectrum data
};

#endif // DPPSPECTRUM_HPP
