/** 
 *  header file for the dpp daemon
 * 
 * 
 */

#ifndef _DPPD_H
#define _DPPD_H

class Dppd 
{
public:
	Dppd(void);
	~Dppd(void);
     
	void ProcessCommand(char *buf, char* response);

};

#endif /** _DPPD_H */
