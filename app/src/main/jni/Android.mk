TOP_PATH := $(call my-dir)

include $(CLEAR_VARS)

include $(call all-subdir-makefiles)

include app/src/main/jni/Smd/Android.mk
include app/src/main/jni/peakfit/Android.mk
include app/src/main/jni/libusb/Android.mk
include app/src/main/jni/Dppd/Android.mk
include app/src/main/jni/utils/Android.mk
