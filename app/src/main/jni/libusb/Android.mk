# OpenNI 2.x Android makefile. 
# Copyright (C) 2012 PrimeSense Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 

# *** 
# *** Note: This module will only get built if compiled via the NDK! ***
# *** 

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := Usb-jni
LOCAL_SRC_FILES := core.c descriptor.c io.c sync.c os/linux_usbfs.c


LOCAL_CPPFLAGS := -std=c++11 -Ofast -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9 -mfpu=vfpv3-fp16  -mfloat-abi=softfp

#LOCAL_CPPFLAGS := -std=c++11 -02 -DNDEBUG -g -DLIBUSB_DEBUG
 
LOCAL_LDLIBS    := -llog

include $(BUILD_SHARED_LIBRARY)

