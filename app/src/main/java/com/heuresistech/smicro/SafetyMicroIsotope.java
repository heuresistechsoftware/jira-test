package com.heuresistech.smicro;

//! \file SafetyMicroIsotope.java
//! \brief This file contains the Safety Microcontroller interface and controller.

import android.util.Log;

//! \brief safetyMicroIsotope is responsible for handling the control and status for the Safety
//!     Microcontroller.
//!
//! The SM is responsible for all of the safety features related to the radioacrive isotope. This
//! includes the shutter control, LED indicators, trigger, proximity button, and SPI communication
//! to the Processor module. This class also handles the LED on the LCD board. It acts as a
//! secondary radiation LED.
public class SafetyMicroIsotope{

    // Native code declarations.

    //! \brief Call to SM and retrieve the status word indicating a fault, shutter status, etc.
    public native int getSystemStatus();
    //! \brief Retrieve the fault status from the SM
    public native int getFaultStatus();
    //! \brief Initalize SM for Pb200i use, sets up SPI for communication.
    public native int InitializeSMD();
    //! \brief Disable motor so shutter cannot be open.
    public native boolean closeSMD();
    //! \brief Sets the varialbe hold time for the shutter being held open.
    public native int setShutterVarHoldTime (short vhtime);
    //! \brief Arm the SM to allow shutter control to occur.
    public native int armShutter();
    //! \brief disable shutter control
    public native int disarmShutter();
    //! \brief closes the shutter by turning off the motor.
    public native int closeShutter();
    //! \brief Controls the red LED on the LCD module
    public native boolean setRedLed( boolean on );
    //! \brief Controls the green LED
    public native boolean setGrnLed( boolean on );
    //! \brief Turns on both red and green LEDs to get yellow LED
    public native boolean setYellowLed( boolean on );
    //! \brief Turns off all LEDs
    public native boolean setOffLed();
    //! \brief Allows the proximity safety feature to be bypassed.
    public native boolean setSurfaceBypass( boolean bypass);
    //! \brief Forces shutter open without regard to safety interlocks
    public native int openShutterNoInterlocks( );
    //! \brief Enables the ability to override the safety interlocks
    public native int enableInterlockOverride( );
    //! \brief passing a True enabled momentary trigger, false enables normal trigger
    public native boolean setMomentaryTrig( boolean momentaryTrig );
    //! \brief Reset the SM (pull power and reapply power)
    public native boolean commsReset();
    //! set the timeout in milliseconds for trigger fault timeout.
    public native boolean setTimeout( long trgiTimeout );
    //! set the timeout in milliseconds for surface fault timeout.
    public native boolean setSurfaceTimeout( long proximityTimeout);
    //! Get the version of firmware from the Safety Module
    public native short getFirmwareVer( );


    //! Make sure there is only one instance of this class.
    private static SafetyMicroIsotope instance = null;
    //! Reference to the thread that monitors the state of the SM controller
    private Thread	safetyMicroListeningThread = null;
    //! Used to signal if thread is running or not.
    private boolean smPaused = true;  //Start paused until thread is run.

    //! Contains a bit representation of faults detected by the SM.
    private boolean	smFault	= false;

    //! Indicates the state of the proximity button. True = pressed, False = not
    private boolean	proxPressed	 = false;

    //! Indicates the trigger was engaged, true = trigger, false = no trigger active
    private boolean	triggerPulled = false;

    //! Indicates the shutter is closed. No radiation
    private boolean shutterClosed = true;

    //! Indicates the shutter is open and emitting X-Rays.
    private boolean shutterOpen = false;

    //! THIS IS NOT SUPPOSED TO BE HERE, WAS PART OF TestActivity.
    private boolean stopReading = false;

    //! A text message representing the SM fault.
    private CharSequence faultText;

    private int pollingCount = 0;

    private static int faultCount;

    //! Load the native libraries. SMD only for this class.
    static {
        System.loadLibrary("Smd-jni");
    }

    //! Straight up singleton instance retrieval function. There should only be one
    //! of these. Requires the constructor to be private and only called from here.
    public static SafetyMicroIsotope getInstance(){
        if( instance == null ) {
            instance = new SafetyMicroIsotope();

        }
        return instance;
    }

    //! Constructor initialized the class and kicks off monitoring thread.
    private SafetyMicroIsotope(){

        InitializeSMD();
        setOffLed();
        faultText = "No Safety Faults";
        getSMVersion(); // flush the version so it works on subsequent calls.
        setShutterVarHoldTime( (short)29990 );


       // smResume();
    }

    public String getSMVersion(){
        short ver = getFirmwareVer();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Integer.toHexString(ver);
    }

    //! Stops the monitoring thread and clears the instnace so a new instance can be started.
    public void smStop(){
        //! Allow getInstance to create a new smlistenthread.
        instance = null;

        //! This should cause the thread to stop executing since safetyMicroListeningThread !=
        //! thisThread which is checked by the while loop to keep executing the thread.
        safetyMicroListeningThread = null;
        smPaused = true; // stop is like paused, needed to restart properly.
    }

    public void smPause(){
        setOffLed();
        //safetyMicroListeningThread = null;
        smPaused = true;

        //Reset state of SM to defaults since we aren't talking to hardware anymore
        smFault	= false;
        proxPressed	 = false;
        triggerPulled = false;
        shutterClosed = true;
        shutterOpen = false;
    }

    public void smResume(){
        if( smPaused ){
            runSafetyMicroListeningThread();
            smPaused = false;
        }
    }


    //! \brief Check to see if there is a fault condition in the SM.
    //! \return True if there is a fault and False if no faults exist.
    public boolean isSmFault(){
        return smFault;
    }

    //! \brief Check if the proximity button is engaged or not.
    //!
    //! In some cases the proximity is not used as a safety feature. This function will still
    //! return the true state of the proximity button, unless the proximity override is set. If
    //! it is set then this function always return true.
    //!
    //! \return True if the proximity sensor is engaged and false otherwise.
    public boolean isProxPressed(){
        return proxPressed;
    }

    //! \brief Indicates if the trigger is being pressed.
    //! \return True if the trigger is being pulled, false otherwise.
    public boolean isTriggerPulled() {
        return this.triggerPulled;
    }

    //! \brief Check if the shutter is closed or open.
    //! \return True is the shutter is close (no Xrays), false if it is not closed.
    public boolean isShutterClosed(){
        return this.shutterClosed;
    }

    //! \brief Force the shutter closed if for instance the measurement is complete but trigger is pressed.
    //! \return True if the shutter closed, false if not.
    public int shutterClose(){
        return closeShutter();
    }

    //! \brief Check to see if the shutter is open.
    //!
    //! The definition of open is all of the way open. The open sensor needs to be trigger in
    //! order for this to report open. It is possible for the shutter to be not open and not close.
    //! This state is an error and will be reported.
    //!
    //! \return True if the open sensor is active, false otherwise
    public boolean isShutterOpen(){
        return shutterOpen;
    }

    //! \brief Returns the text string with the current SM fault.
    //! \return a string with 'No SM Fault' or a msg with fault.
    public String getFaultText(){
        return faultText.toString();
    }

    //! \brief Bypasses the proximity button and always return true for isProxPressed.
    //! \return True if the prox bypass was successful, false otherwise.
    public boolean setProximityBypass( boolean bypass ){
        return setSurfaceBypass(bypass);
    }

    //! \brief Control the yellow LED.
    //!
    //! \param ledOn - true turns the yellow led on, false turns it off.
    //!
    public void yellowLed( boolean ledOn) {
        setYellowLed(ledOn);
    }

    //! \brief Control the red LED
    //!
    //! \param ledOn - true turns the red LED on, false turns it off.
    public void redLed( boolean ledOn ) {
        setRedLed(ledOn);
    }

    //! \brief Control the green LED
    //!
    //! \param ledOn - true turns the green LED on, false turns it off.
    public void greenLed( boolean ledOn ){
        setGrnLed(ledOn);
    }

    //! \brief Allows the shutter to be open. Arms the software lock on the shutter.
    public void enableShutter(){
        armShutter();
    }

    //! \brief Disallows control over the shutter. Disable the software shutter lock.
    public void disableShutter(){
        disarmShutter();
    }

    //! \brief Disables the shutter motor so it remains closed.
    public void disableSMicro(){
        closeSMD();
    }

    //! \brief Bypass shutter controls. Allow the shuter to open with no other interlocks needed.
    //!
    //! Requires enableInterlockBypass to be called be this.
    //!
    //! \return True is shutter is opened. false otherwise.
    public int openShutterInterlockBypass( )
    {
        return openShutterNoInterlocks();
    }

    //! \brief Allow the shutter control to bypass interlocks like trigger and proximity.
    //! return true if the override was successful, false otherwise.
    public int enableInterlockBypass( ){ return  enableInterlockOverride( ); }

    private boolean resetSMComms(){
        return commsReset();
    }

    public boolean setTriggerStyle( boolean MomentaryTrig ){
        return setMomentaryTrig( MomentaryTrig );
    }

    public boolean setSafetyTimeout( long safetyTimeout ){
        return setTimeout( safetyTimeout );
    }

    //! \brief Communicate with the SM and retrieve the status and fault state.
    //!
    //! The thread monitors the Safety Micro and determines if any faults exist.
    //! It also sets the LCD LED to the state indicating the state of the SM.
    //! Red means the shutter is open and Xrays are present. Yellow is an SM fault.
    //! Green means everything is OK and the shutter is ready to be opened.
    //!
    //! This thread also keeps the heartbeat alive. If the SM does not get regular heartbeat
    //! messages it will close the shutter and not allow it to open. This thread will send
    //! heartbeat messages every 50ms to ensure the heartbeat doesn't timeout.
    public void runSafetyMicroListeningThread() {

        safetyMicroListeningThread = new Thread() {
            public void run() {

                // Status bit fields
                final int SHUTTER_OPEN   = 0x01;
                final int SHUTTER_CLOSED = 0x02;
                final int TRIGGER        = 0x04;
                final int PROX           = 0x08;
                final int FAULT          = 0x10;
                // if needed uncomment: final int SAFETY_LEDS    = 0x20;

                // Fault bit fields
                final int F_MOTOR		 = 0x01;
                final int F_LED			 = 0x02;
                final int F_TRIG		 = 0x04;
                final int F_SHUTTER_DRV  = 0x08;
                final int F_HEARTBEAT    = 0x10;
                final int F_FIRMWARE	 = 0x20;
                final int F_PROX		 = 0x40;
                final int F_SHUTTER_SNS  = 0x80;
                final int F_BAD_SM_STATE = 0xFF;

                final int HEARTBEAT_REFRESH_TIME = 25; // call getSystemStatus to reset heart beat timer
                final int FAULT_DETECT_LIMIT = 0;

                Thread thisThread = Thread.currentThread();
                //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(context, TestActivity.class));

                resetSMComms();
                try {
                    Thread.sleep(700);
                }catch( InterruptedException e){
                    e.printStackTrace();
                }
                smPaused = false;

                while (safetyMicroListeningThread == thisThread) {
                    try {
                        if(!smPaused) {
                            int faultStatus;
                            ++pollingCount;

                            // This needs to be insured that it is called all the time ...
                            // Its almost like a watchdog.
                            int sysStatus = getSystemStatus();


                            //;// ;// Log.i ("sysStatus","sysStatus = " + sysStatus );
                            //sysStatus = 0x3;
                            // at startup the sysStatus comes up 0 which looks like open shutter.
                            // If this is the case loop around and get better data.
                            if ((sysStatus == 0) ||
                                    (sysStatus == 0xff)/* ||
                            ( sysStatus & 0x03 ) == 0x03*/) {
                                ++faultCount;
                                ;// ;// Log.e("sysStatus", " SysStatus = " + Integer.toString(sysStatus, 16));

                                // Failure must occur 5 times in a row to be called failure
                                if (faultCount > FAULT_DETECT_LIMIT) {
                                    ;// ;// Log.e("sysStatus", "sysStatus = " + sysStatus );
                                    sysStatus |= 0xF0;  // Force the fault bit so it is detected below
                                    faultCount = 0; // no faults clear count
                                }

                            }

                            if ((sysStatus & FAULT) != 0) {

                                if ((sysStatus & 0xF0) == 0xF0) {
                                    faultStatus = F_BAD_SM_STATE;
                                } else {
                                    faultStatus = getFaultStatus(); // & ~F_HEARTBEAT;

                                }
//                                Log.e("sysStatus", "FaultStatus = " + Integer.toString(faultStatus, 16) +
//                                       " SysStatus = " + Integer.toString(sysStatus, 16));
                                if (faultStatus != 0) {

                                    smFault = true;

                                    switch (faultStatus) {
                                        case F_MOTOR:
                                            faultText = "Motor Supply Fault";
                                            break;
                                        case F_LED:
                                            faultText = "Safety LED Fault";
                                            break;
                                        case F_TRIG:
                                            faultText = "Trigger Stuck on Fault";
                                            break;
                                        case F_SHUTTER_DRV:
                                            faultText = "Shutter Drive Fault";
                                            break;
                                        case F_HEARTBEAT:
                                            faultText = "Establishing comms with SM";
                                            break;
                                        case F_FIRMWARE:
                                            faultText = "Firmware Mismatch Fault";
                                            break;
                                        case F_PROX:
                                            faultText = "Prox stuck on Fault";
                                            break;
                                        case F_SHUTTER_SNS:
                                            faultText = "Shutter sensor Fault";
                                            break;
                                        case F_BAD_SM_STATE:
                                            faultText = "SM Bad State";
                                            //smFault = false;
                                            resetSMComms();
                                            break;
                                        default:
                                            faultText = "No SM communications"; // null;
                                            //smFault = false;
                                            resetSMComms();
                                            break;
                                    }

                                    if( faultText != null ){
                                        // Leave for Debug
                                        Log.w("SMListeningThread", faultText.toString());
                                    }
                                } else {
                                    smFault = false;
                                }

                            } else { //No faults process normal actions

                                faultCount = 0; // no faults clear count
                                smFault = false;

                                triggerPulled = ((0 != (sysStatus & TRIGGER)));

                                proxPressed = ((0 != (sysStatus & PROX)));

                                shutterClosed = ((0 != (sysStatus & SHUTTER_CLOSED)));
                                shutterOpen = ((0 != (sysStatus & SHUTTER_OPEN)));
                            }

                            //**************************************
                            // Process LED on the LCD based on status and fault state.
                            //**************************************

//                        ;// ;// Log.e("sysStatus", "FaultStatus = " + Integer.toString(faultStatus, 16) +
//                                " SysStatus = " + Integer.toString(sysStatus, 16));

                            if (!shutterClosed) {
                                // Shutter is open, turn red for xray emitting
                                setGrnLed(false);
                                setRedLed(true);
                            } else {

                                // If the shutter closed during the reading, we need to
                                // stop the reading.  In turn current results get stored.
                                //stopTakingReading();
                                stopReading = true;

                                if (smFault) {
                                    // Shutter is closed if fault turn yellow
                                    setYellowLed(true);
                                } else {
                                    // Shutter is closed, no fault follow proximity state
                                    if (proxPressed) {
                                        setRedLed(false);
                                        setGrnLed(true);
                                    } else {
                                        setOffLed();
                                    }
                                }
                            }
                        }
                        // Measurements are currently being taken.
                        // All is good during a measurement, as long as
                        // we are hitting this thread.
                        // This time refreshes the heartbeat to the SM. 50ms is about as high
                        // as it can go and still keep heartbeat from timing out.
                        Thread.sleep( HEARTBEAT_REFRESH_TIME );

                    } catch( InterruptedException e){
                        e.printStackTrace();
                    }

                } // while

            } // void run
        }; // new Thread

        safetyMicroListeningThread.setName("safetyMicroListeningThread");
        safetyMicroListeningThread.setDaemon(true);
        safetyMicroListeningThread.start();

    } //  runSafetyMicroListeningThread
}
