package com.heuresistech.xrfservices;

//! \file ReadingData.java
//! \brief This file contains the definition of ReadingData class.

import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.heuresistech.xrfservices.Constants;


//! \brief ReadingData represents all data associated with result data
//!
//! ReadingData represents the set of data that describes a reading result. Each and
//! every reading is defined by this data set. It includes DPP settings, several spectra,
//!	 system stats, and user settings. This class is used by ReadingDB to store the results
//! into the readingsDB.
//!
//! Note: This class is responsible for pulling data directly from the DPP into this class,
//!         This is done in the populateFromDPP function.
public class ReadingData {

    //! \brief Native function to acquire all the data from the DPP including spectrum.
    //!
    //! All the data from the DPP is acqiured in this call, this information gets passed onto
    //! the peakFit algorithm for analysis the results are returned through a broadcast intent.
    //!
    //! \param measure_mode is used by the peakfit code to adjust determination of results. PCS
    //!         Actionl level is 0; StopAtLevel = 1; FixedTime =2; UnlimitedTime=3; StopOnStat=4
    //!
    //! \return True if spectrum was aquired, false otherwise. If this fails there is a problem
    //!         with communication with the DPP board, this is an internal USB connection.
	public native boolean AcquireSpectrum(int analytic_mode, int measure_mode, boolean processSpectrum);

    private final double percisionCutoff = 29.995;

    private NumberFormat format0decimal;
    private NumberFormat format1decimal;
    private NumberFormat format2decimal;
    private NumberFormat format4decimals;
    private NumberFormat format6decimals;

    //! Load the Native C library to access the DPP
    static {
        System.loadLibrary("Dppd-jni");
    }
	
	//!  Class constructor set defaults to zeros.
	public ReadingData () {

        rdgDataLookup = new HashMap<>();

        format0decimal = new DecimalFormat("#0");
        format1decimal = new DecimalFormat("0.0");
        format2decimal = new DecimalFormat("0.00");
        format4decimals = new DecimalFormat("0.0000");
        format6decimals = new DecimalFormat("0.000000");
	
		readingNumber = 0;
		rawSpectrum	    = new int[Constants.SPECTRA_SIZE_RETRIEVE];
		normSubSpectrum = new int[Constants.SPECTRA_SIZE_RETRIEVE];
        normSpectrum    = new int[Constants.SPECTRA_SIZE_RETRIEVE];

		mgCm2		= (float) 0.0;
		confLev		= (float) 0.0;
		stdDev		= (float) 0.0;
		//date;
		//time;
		accTime		= (float) 0.0;
		biasVolts	= 0;
		boardTemp	= 0;
		fastCount	= 0;
		slowCount	= 0;
		realTime	= (float)0.0;
		TECTemp		= 0;
		accumCounts = 0;
		//dppHwConfigStr;
		dppSetting	= new ArrayList<>();
		dppValues	= new ArrayList<>();
		tempType	= 0;
		nomSecs		= (float) 0.0;
		
		k_gain      = 0;
		k_offset    = 0;
		live_time   = 0.0f;
		numBins     = 0;
		
		user            = "";
        resultString    = "";
        mode            = 0;
        action_level    = (float) 0.0;
        nullCode        = 0;
        setMgCm2_NoRTG(0.0f);
        setMgCm2_Near (0.0f);
        setIsReadThrough (false);
        setIsRTGPresent (false);

        setALResult (0);
        setALmgCm2 (0.0f);
        setALerror  (0.0f);
        setALDone (false);
        jobId = "";
        resultFound = false;
        isCalRdg  = false;
        result = 0;

	}

    //! \brief Pull data from DPP into ReadingData class.
    //!
    //! \param measureMode is used to tell PeakFit how to handle the data, either action level
    //!         or run to limit.
    //!
    public boolean populateFromDPP( int analytic_mode, int measureMode, boolean processSpectrum ){
        boolean acq = AcquireSpectrum( analytic_mode, measureMode, processSpectrum );
        return acq;
    }

    //! \brief Retreive the total accumulation counts from the raw spectrum
    //! \return number of accumulated counts.
	public int getAccumCounts() {
		return accumCounts;
	}
	public void setAccumCounts(int accumCounts) {
		this.accumCounts = accumCounts;
	}

    //! \brief Retreive raw spectrum accumulation time.
    //! \return raw spectrum accumulation time in milliseconds.
	public float getAccTime() {
		return accTime;
	}
	public void setAccTime(float accTime) {
		this.accTime = accTime;
        rdgDataLookup.put( "Accum Time", format2decimal.format(accTime));

	}

    //! \brief Access the detector Bias voltage
    //! \return the measured value of the detector bias voltage.
	public int getBiasVolts() {
		return biasVolts;
	}
	public void setBiasVolts(int biasVolts) {
		this.biasVolts = biasVolts;
        rdgDataLookup.put( "Bias Volts", String.valueOf( biasVolts ));
	}

    //! \brief Get DPP board temp in 1/10th of degrees Celcius.
    //! \return board temperature in 1/10th of degree C.
	public int getBoardTemp() {
		return boardTemp;
	}
	public void setBoardTemp(int boardTemp) {
		this.boardTemp = boardTemp;
        rdgDataLookup.put( "Board Temp", String.valueOf( boardTemp ));
	}

    //! \brief Get the fast counts from the DPP
	public int getFastCount() {
		return fastCount;
	}
	public void setFastCount(int fastCount) {
		this.fastCount = fastCount;
        rdgDataLookup.put( "Fast Count", String.valueOf( fastCount ));
	}

    //! \brief Get the slow counts from the DPP
	public int getSlowCount() {
		return slowCount;
	}
	public void setSlowCount(int slowCount) {
		this.slowCount = slowCount;
        rdgDataLookup.put( "Slow Count", String.valueOf(slowCount));
	}

    //! \brief Get the actual wall time duration of the spectrum collection.
	public float getRealTime() {
		return realTime;
	}
	public void setRealTime(float realTime) {
		this.realTime = realTime;
        rdgDataLookup.put( "Real Time", format2decimal.format(realTime));
	}

    //! \brief Get detector cooling temperature in degrees Kelvin
	public int getTECTemp() {
		return TECTemp;
	}
	public void setTECTemp(int tECTemp) {
		TECTemp = tECTemp;
        rdgDataLookup.put( "Detector Temp", String.valueOf( TECTemp ));
	}

    //! \brief Get the reading number associated with this data set.
    //! \return a linear incrementing reading number that increatses with eash reading taken.
	public long getReadingNumber() {
		return readingNumber;
	}
	public void setReadingNumber(long readingNumber) {
		this.readingNumber = readingNumber;
        rdgDataLookup.put("Reading #", String.valueOf(readingNumber));
	}

    //! Retrieve the raw spectrum which comes from the DPP before peakFit processing.
    //! \return spectrum is returned in an array of ints that contain counts in each bin.
	public int[] getRawSpectrum() {
		return this.rawSpectrum;
	}
	public void setRawSpectrum(int[] spectrum) {
		this.rawSpectrum = spectrum;
	}

    //! \brief Retrieve the energy normalized and subtracted from best fit calibration template spectrum.
    //! \return spectrum is returned in an array of ints that contain counts in each bin.
	public int[] getNormSubSpectrum() {
		return this.normSubSpectrum;
	}
	public void setNormSubSpectrum(int[] spectrum) {
		this.normSubSpectrum = spectrum;
	}

    //! \brief Retrieve the energy normalized spectrum.
    //! \return spectrum is returned in an array of ints that contain counts in each bin.
    public int[] getNormSpectrum() {
        return this.normSpectrum;
    }
    public void setNormSpectrum(int[] spectrum) {
        this.normSpectrum = spectrum;
    }

    //! \brief Get the results of measurement as calculated by peakFit algos
    //! \return a int representing Posative, Negative, Null, ect.
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
        rdgDataLookup.put( "Result Index" , String.valueOf(result));
        //Log.d("RdgData", "Results set to " + getResult());
	}

    //! \brief Get the concentration as determined by peakFit algo
    //! \return the concentration value
	public float getConcentration() {
		return mgCm2;
	}
	public void setConcentration(float mgCm2) {
		this.mgCm2 = mgCm2;
        if( this.mgCm2 < percisionCutoff ) {
            if( getMode() == 0 ) {  // lead paint mode
                rdgDataLookup.put("Concentration", format1decimal.format(mgCm2));
            }else{
                rdgDataLookup.put("Concentration", format2decimal.format(mgCm2));
            }
        }else{
            rdgDataLookup.put("Concentration", format0decimal.format(mgCm2));
        }
	}

    //! \brief Get the confidence level of the results provided by peakFit.
    //! \return a float value representing the percent confidence in the results.
	public float getConfLev() {
		return confLev;
	}
	public void setConfLev(float confLev) {
		this.confLev = confLev;
        rdgDataLookup.put( "Conf Level", format2decimal.format( confLev ));
	}

    //! \brief Gets the standard deviation (error) in the concentration value.
    //! \return the error +/- in the concentration value.
	public float getStdDev() {
		return stdDev;
	}
	public void setStdDev(float stdDev) {
		this.stdDev = stdDev;
        if( this.mgCm2 < percisionCutoff ) { // change formatting based on concentration value
            if( this.mode == 0 ) {  // lead paint mode
                rdgDataLookup.put("3 SD", format1decimal.format(this.stdDev));
            }else{
                rdgDataLookup.put("3 SD", format2decimal.format(this.stdDev));
            }
        }else{
            rdgDataLookup.put("3 SD", format1decimal.format(this.stdDev));
        }
	}

    //! \brief time of day the reading was taken in HH:mm:ss
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
        rdgDataLookup.put( "Time", this.time );
	}

    //! \brief date the reading was taken
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
        rdgDataLookup.put( "Date", date);
	}

    //! \brief obtain the template type used in the reading as a spectrum baseline
    //! \return the one based index of the template used in this reading
    public int getTempType() {
        return tempType;
    }
    public void setTempType(int tempType) {
    	this.tempType = tempType;
        rdgDataLookup.put( "Template Index", String.valueOf( tempType ));
    }

    //! \brief the config string for the DPP settings
    //! \return the string in a space seperated list of DPP settings.
    public String getDppHwConfigStr() {
		return dppHwConfigStr;
	}
	public void setDppHwConfigStr(String dppHwConfig) {

		this.dppHwConfigStr = dppHwConfig;

		if( this.dppHwConfigStr != null ){
		
	        String tempStr1 = this.dppHwConfigStr.replace("\r", "");
	        String tempStr2 = tempStr1.replace("\r", "");
	        String tempStr3 = tempStr2.replace("\n", "");
            dppHwConfigStr = tempStr3.replace(";", " ");
	        String delims = " ";
			String[] tokens = this.dppHwConfigStr.split(delims);

			dppSetting.clear();
			dppValues.clear();

            for( String token : tokens ) {
                delims = "=";
                String[] tokenSplit = token.split(delims);
                dppSetting.add(tokenSplit[0]);
                dppValues.add(tokenSplit[1]);
            }
		}

	}  //  setDppHwConfigStr(String dppHwConfigStr)

//	public ArrayList<String>  getDppHwConfigStrArraySettings() {
//
//		return dppSetting;
//	}
//	public ArrayList<String>  getDppHwConfigStrArrayValues() {
//
//		return dppValues;
//	}

    //! \brief the nominal seconds (source decay & dead time corrected) of the reading
    public float getnomSecs() {
        return nomSecs;
    }
    public void setnomSecs (float nomSecs) {
        this.nomSecs = nomSecs;
        rdgDataLookup.put( "NomSecs", format2decimal.format( nomSecs ));
    }

    //! \brief the gain of the system used to normalize the spectrum
    public float getKGain (){
    	return this.k_gain;
    }
    public void setKGain (float gain){
    	this.k_gain = gain;
        rdgDataLookup.put( "K Gain", format6decimals.format( k_gain));
    }

    //! \brief the offset of the system is a +/- shift in the spectrum.
    public float getKOffset (){
    	return this.k_offset;
    }
    public void setKOffset (float offset){
    	this.k_offset = offset;
        rdgDataLookup.put( "K Offset", format6decimals.format( k_offset ));
    }

    //! \brief Returns the live time of the reading, the amount of time the detector was active.
    //! \return live time is the oposite of dead time. together they add up to accum time.
    public float getlive_time(){
        return live_time;
    }
    public void setlive_time( float liveTime ){
        this.live_time = liveTime;
        rdgDataLookup.put( "Live Time", format2decimal.format(live_time));
    }

    //! \brief The number of bins in the spectrum, currently 512 bins but could change.
    public int getNumBins (){
    	return this.numBins;
    }
    public void setNumBins (int numBins){
    	this.numBins = numBins;
        rdgDataLookup.put( "Num Bins", String.valueOf(numBins));
    }

    //! \brief The user that is logged in when the reading was taken.
    public String getUser() {
        return user;
	}
	public void setUser(String user) {
	    this.user = user;
        rdgDataLookup.put( "User", user);
	}

    //! \brief The string representation of the results as in Positive, Negative
	public void setResultString (String resultString) {
	    this.resultString = resultString;
        rdgDataLookup.put( "Result", resultString);
	}
	public String getResultString () {
	        return resultString;
	}

    //! \brief The mode used to take the reaidngs, Action Leve (PCS), Extended stoponTime, etc
    public int getMode() {
        return mode;
    }
    public void setMode(int mode) {
        this.mode = mode;
        rdgDataLookup.put( "Mode", String.valueOf(mode));
    }

    //! \brief get/set the action level for which level of lead is positive.
	public void setActionLevel (float action_level) {
	    this.action_level = action_level;
        rdgDataLookup.put( "Action Level", format2decimal.format(action_level));
	}
	public float getActionLevel () {
	        return action_level;
	}

    public int getAnalyticMode() {
        return analyticMode;
    }
    public void setAnalyticMode(int analytic_mode) {
        this.analyticMode = analytic_mode;
        rdgDataLookup.put( "Analytic Mode", String.valueOf(analyticMode));
    }

    public float getTempChisq() {
        return tempChisq;
    }
    public void setTempChisq(float tempChisq) {
        this.tempChisq = tempChisq;
        rdgDataLookup.put( "Template Chisq", format4decimals.format( tempChisq ));
    }

    public float getTempNormFactor() {
        return tempNormFactor;
    }
    public void setTempNormFactor(float tempNormFactor) {
        this.tempNormFactor = tempNormFactor;
        rdgDataLookup.put( "Template Norm Factor", format6decimals.format(tempNormFactor));
    }

    public float getMgCm2_Near() {
        return mgCm2_Near;
    }
    public void setMgCm2_Near(float mgCm2_Near) {
        this.mgCm2_Near = mgCm2_Near;
        if( this.mgCm2 < percisionCutoff ) { // change formatting based on concentration value
            if( this.mode == 0 ) {  // lead paint mode
                rdgDataLookup.put("MgCm2 Near", format1decimal.format(this.mgCm2_Near));
            }else{
                rdgDataLookup.put("MgCm2 Near", format2decimal.format(this.mgCm2_Near));
            }
        }else{
            rdgDataLookup.put("MgCm2 Near", format1decimal.format(this.mgCm2_Near));
        }
    }

    public float getMgCm2_NoRTG() {
        return mgCm2_NoRTG;
    }
    public void setMgCm2_NoRTG(float mgCm2_NoRTG) {
        this.mgCm2_NoRTG = mgCm2_NoRTG;
        if( this.mgCm2 < percisionCutoff ) { // change formatting based on concentration value
            if( this.mode == 0 ) {  // lead paint mode
                rdgDataLookup.put("MgCm2 NoRTG", format1decimal.format(this.mgCm2_NoRTG));
            }else{
                rdgDataLookup.put("MgCm2 NoRTG", format2decimal.format(this.mgCm2_NoRTG));
            }
        }else{
            rdgDataLookup.put("MgCm2 NoRTG", format1decimal.format(this.mgCm2_NoRTG));
        }
    }

    public boolean isReadThrough() {
        return isReadThrough;
    }
    public void setIsReadThrough(boolean isReadThrough) {
        this.isReadThrough = isReadThrough;
        rdgDataLookup.put( "Read Through", String.valueOf(isReadThrough));
    }

    public boolean isRTAPresent() {
        return RTGPresent;
    }
    public void setIsRTGPresent(boolean rtg_present) {
        this.RTGPresent = rtg_present;
        rdgDataLookup.put( "RTA Present", String.valueOf( RTGPresent ));
    }

    public int getNullCode() {
        return nullCode;
    }
    public void setNullCode(int nullCode) {
        this.nullCode = nullCode;
        rdgDataLookup.put( "Null Code", String.valueOf(nullCode));
    }

    public int getALResult() {
        return ALResult;
    }
    public void setALResult(int ALResult) {
        this.ALResult = ALResult;
        rdgDataLookup.put( "AL Result", String.valueOf(ALResult));
    }

    public float getALmgCm2() {
        return ALmgCm2;
    }
    public void setALmgCm2(float ALmgCm2) {
        this.ALmgCm2 = ALmgCm2;
        if( this.mgCm2 < percisionCutoff ) { // change formatting based on concentration value
            if( this.mode == 0 ) {  // lead paint mode
                rdgDataLookup.put("AL MgCm2", format1decimal.format(this.ALmgCm2));
            }else{
                rdgDataLookup.put("AL MgCm2", format4decimals.format(this.ALmgCm2));
            }
        }else{
            rdgDataLookup.put("AL MgCm2", format4decimals.format(this.ALmgCm2));
        }
    }

    public float getALerror() {
        return ALerror;
    }
    public void setALerror(float ALerror) {
        this.ALerror = ALerror;
        if( this.mgCm2 < percisionCutoff ) { // change formatting based on concentration value
            if( this.mode == 0 ) {  // lead paint mode
                rdgDataLookup.put("AL Error", format1decimal.format(this.ALerror));
            }else{
                rdgDataLookup.put("AL Error", format2decimal.format(this.ALerror));
            }
        }else{
            rdgDataLookup.put("AL Error", format2decimal.format(this.ALerror));
        }
    }

    public boolean isALDone() {
        return ALDone;
    }
    public void setALDone(boolean ALDone) {
        this.ALDone = ALDone;
        rdgDataLookup.put( "AL Done", String.valueOf(ALDone));
    }

    public int getUnits() {
        return units;
    }
    public void setUnits(int units) {
        this.units = units;
        rdgDataLookup.put( "Units", String.valueOf(units));
    }

    public float getSrcMCi() {
        return srcMCi;
    }
    public void setSrcMCi(float src_mci) {
        this.srcMCi = src_mci;
        rdgDataLookup.put( "Src MCi", format2decimal.format(src_mci));
    }

    public String getJobId() {
        return jobId;
    }
    public void setJobId(String job_id) {
        this.jobId = job_id;
        rdgDataLookup.put( "Job Id", this.jobId);
    }

    public boolean getResultFound(){ return resultFound; }
    public void setResultFound( boolean found ){
        this.resultFound = found;
        rdgDataLookup.put( "Result Found", String.valueOf(this.resultFound) );
    }

    public boolean isNullReading( ){ return isNullReading; }
    public void setIsNullReading( boolean isNull ){
        this.isNullReading = isNull;
        rdgDataLookup.put( "Null Reading", String.valueOf(this.isNullReading));
    }

    public boolean isCalibrationRdg(){ return isCalRdg; }
    public void setCalibrationRdg( boolean isCal ){
        this.isCalRdg = isCal;
        rdgDataLookup.put( "Calibration Reading", String.valueOf(this.isCalRdg));
    }

    public long getSequenceNum(){ return jobSeqNum; }
    public void setJobIdSeqNum( long seqNum ){
        this.jobSeqNum = seqNum;
        rdgDataLookup.put("Job Sequence Num", String.valueOf(jobSeqNum) );
    }

    private long	readingNumber;
    private int		result;
    private float	mgCm2;
    private float	confLev;
    private float	stdDev;
    private String  date;
    private String  time;
    private float	accTime;
    private int		biasVolts;
    private int		boardTemp;
    private int		fastCount;
    private int		slowCount;
    private float	realTime;
    private int		TECTemp;
    private int		accumCounts;
    private String	dppHwConfigStr;

    private int		tempType;
    private float	nomSecs;
    private float   k_gain;
    private float   k_offset;
    private float   live_time;
    private int     numBins;
    private String  user;
    private String  resultString;
    private int     mode;
    private float   action_level;
    private int     nullCode;

    private int  analyticMode;
    private float   tempChisq;
    private float   tempNormFactor;

    private int units;

    private float mgCm2_NoRTG;
    private float mgCm2_Near;
    private boolean isReadThrough;
    private boolean RTGPresent;

    private int ALResult;
    private float ALmgCm2;
    private float ALerror;
    private boolean ALDone;
    private float srcMCi;

    private ArrayList<String> dppSetting;
    private ArrayList<String> dppValues;

    private int[]	rawSpectrum;
    private int[]   normSubSpectrum;
    private int[]   normSpectrum;

    private String jobId; // MMDDYYHHmm-SN default format for ID
    private boolean resultFound;
    private boolean isNullReading;
    private boolean isCalRdg;

    private long jobSeqNum;

    public Map<String,String> rdgDataLookup;
}  //  class ReadingData
