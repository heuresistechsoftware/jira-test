package com.heuresistech.xrfservices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

public class SpectrumCollection extends SpectrumCollectionBase {

    private ReadingData rdg;
    private boolean leadFound = false;
    private Context context;
    //private SpectrumReceiver specRecvr;

    public SpectrumCollection( Context ctx ){
        context = ctx;
        //specRecvr = new SpectrumReceiver();
    }

    public boolean detectLead() {
        boolean isLead = false;

        if (rdg.getResult() == 2) {
            isLead = true;
        }

        return isLead;
    }

    //public ReadingData getRdg() {
//        return rdg;
//    }

//    public class SpectrumReceiver extends BroadcastReceiver {
//        public SpectrumReceiver() {
//            IntentFilter filter = new IntentFilter(DppService.ACTION_RESP);
//            context.registerReceiver(this, filter);
//        }
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // This method is called when this BroadcastReceiver receives an Intent broadcast.
//            Log.i( "SpectrumReceiver", "Action: " + intent.getAction());
//
//            Bundle extras = intent.getExtras();
//
//            if (extras != null) {
//
//                rdg.setResult(extras.getInt("result"));
//
//                rdg.setConcentration(extras.getFloat("mgCm2"));
//                rdg.setConfLev(extras.getFloat("confLev"));
//                rdg.setStdDev(extras.getFloat("stdDev"));
//                rdg.setRawSpectrum(extras.getIntArray("spectrum"));
//                rdg.setNormSpectrum(extras.getIntArray("bgnd_spectrum"));
//                rdg.setAccTime(extras.getFloat("accTime"));
////                broadcastIntent.putExtra ("biasVolts", rdg.getBiasVolts());
////                broadcastIntent.putExtra ("boardTemp", rdg.getBoardTemp());
////                broadcastIntent.putExtra ("fastCount", rdg.getFastCount());
////                broadcastIntent.putExtra ("slowCount", rdg.getSlowCount());
////                broadcastIntent.putExtra ("realTime", rdg.getRealTime());
////                broadcastIntent.putExtra ("TECTemp", rdg.getTECTemp());
////                broadcastIntent.putExtra ("dppHwConfig", rdg.getDppHwConfigStr());
////                broadcastIntent.putExtra ("tempType", rdg.getTempType());
////                broadcastIntent.putExtra ("nomSecs", rdg.getnomSecs());
////                broadcastIntent.putExtra ("numBins", rdg.getNumBins());
////                broadcastIntent.putExtra ("k_gain", rdg.getKGain());
////                broadcastIntent.putExtra ("k_offset", rdg.getKOffset());
//
//            } else {
//                rdg.setResult(0);
//            }
//
//            leadFound = detectLead();
//            Log.d("SpectrumReceiver", "Found Lead " + leadFound);
//        }
//    }
}
