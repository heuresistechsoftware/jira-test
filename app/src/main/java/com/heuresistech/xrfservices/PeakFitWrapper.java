package com.heuresistech.xrfservices;

import android.util.Log;

/**
 * Created by a on 7/8/16.
 */
public class PeakFitWrapper {

    public enum system {Concentration, Detection}

    public native float SetNomSecsDL ( float nomSecs );
    public static native boolean SetActionLevel ( float fActionLevelAL, int elemID );
    public native boolean SetActionLevelAL ( float fActionLevelAL, float SDAboveActionLevelAL,
                                              float SDBelowActionLevelAL, int elemID );
    public native boolean SetActionLevelDL ( float fActionLevelDL, float SDAboveActionLevelDL,
                                              float SDBelowActionLevelDL, int elemID );
    public native int sourceCorrectSeconds( int realMilliseconds );
    public native int setSpectrumSmoothing( int smoothBinCnt );
    public native boolean hacInit( int mode, int analyticalMode, int type );

    public native boolean calibration();
    public native boolean createSpectrum(int type);
    public native boolean setPbSensitivity( float snr );

    private static PeakFitWrapper instance = null;

    public static PeakFitWrapper getInstance(system type ){
        if( instance == null ) {
            instance = new PeakFitWrapper( type );
        }
        return instance;
    }

    public boolean Calibration(){

        return calibration();
    }

    public int getSourceCorrectedMS(int realMS){
        return sourceCorrectSeconds(realMS);
    }

    public float setNominalSeconds (float nomSecs){
        return SetNomSecsDL( nomSecs );
    }

    public boolean setActionLevel( float actionLevel, int elementId ){

        return SetActionLevel( actionLevel, elementId );
    }

//    public boolean setActionLevelAL( float actionLevel, float SDAboveActionLevel, float SDBelowActionLevel, int elemID ){
//        return SetActionLevelAL( actionLevel, SDAboveActionLevel, SDBelowActionLevel, elemID);
//    }

    public boolean setActionLevelDL( float actionLevel, float SDAboveActionLevel, float SDBelowActionLevel, int elemID ){
        return SetActionLevelDL( actionLevel, SDAboveActionLevel, SDBelowActionLevel, elemID);
    }

    public int setNumSpectrumSmoothingBins( int numBins ){
        return setSpectrumSmoothing( numBins );
    }

    public boolean loadCalibrations( int analyticalMode ){

        boolean success;
        final int CALIBRATION_MODE = 1;

        success = hacInit( CALIBRATION_MODE, analyticalMode, sysType.ordinal() );

        return success;
    }

    private PeakFitWrapper(system type){

        sysType = type;
        Log.i("PeakFitWrapper", "System type = " + type.ordinal());
        createSpectrum( sysType.ordinal() );
    }

    public boolean setPbDetectionSensitivity( float snr ){

        return setPbSensitivity( snr );
    }


    static {
        System.loadLibrary("Peakfit-jni");
    }

    private system sysType;

}
