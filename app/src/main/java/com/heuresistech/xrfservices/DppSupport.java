package com.heuresistech.xrfservices;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

public class DppSupport {

    private static DppService dpp;
    public  static boolean dppLoaded;
    public static BroadcastReceiver usbDppReceiver;

    private static Activity callingActivity = null;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private static String initilizing_hardware_message;

    public static void getDppPermission(Activity myActivity,
//                                        final Runnable sim,
//                                        final Runnable norm,
                                        final String initializingHardwareMessage) {

        callingActivity = myActivity;

        initilizing_hardware_message = initializingHardwareMessage;

        final UsbManager manager = (UsbManager) callingActivity.getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<String> it = deviceList.keySet().iterator();
        boolean foundIt = false;
        while (it.hasNext()) {
            String deviceName = it.next();
            final UsbDevice device = deviceList.get(deviceName);
            String VID = Integer.toHexString(device.getVendorId()).toUpperCase();
            String PID = Integer.toHexString(device.getProductId()).toUpperCase();
            String Class = Integer.toHexString(device.getDeviceClass());
            String name = device.getManufacturerName();
            //Integer numInterfaces = device.getInterfaceCount();
            //UsbInterface usbInterface = device.getInterface(0);

            Log.d("loadDPP", "USB name: " + name);
            String manufName = "Amptek Inc";
            if (name.equals(manufName)) {
                foundIt = true;
                if (!manager.hasPermission(device)) {
                     usbDppReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();
                            if (ACTION_USB_PERMISSION.equals(action)) {
                                synchronized (this) {
                                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                                        dppLoaded = loadDPP( );
                                    }
                                }
                            }
                        }
                    };
                    PendingIntent pi = PendingIntent.getBroadcast(callingActivity, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                    callingActivity.registerReceiver(usbDppReceiver, filter);
                    manager.requestPermission(device, pi);
                } else {
                    dppLoaded = loadDPP();
                }
            }
        }
        if (!foundIt) {
            loadDPPSim();
        }
    }

    public static boolean loadDPP( ){
        Log.d("loadDPP", "Starting DPP!");

        boolean success = false;

        // Start up the DPP service
        dpp = DppService.getInstance(callingActivity);

        if ( dpp != null ){
           success = dpp.connect(0,0  );
        }

        return success;
    }

    private static void loadDPPSim(){
        Log.d("loadDPPSIM", "Starting simulated DPP!");
    }
}