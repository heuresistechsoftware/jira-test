package com.heuresistech.xrfservices;

//! \file Constants.java
//! \brief This file contains constants defined for processing XRF.

//! \class Constants
//! \brief Constants used by general XRF processing.
public class Constants {

	//! Number of Bins in the spectrum aa delivered from the DPP pulse processor
	public static final int SPECTRA_SIZE_RETRIEVE = 512;

    //! Enum position for action level mode (PCS mode)
    // public static final int ACTION_LEVEL_MODE = 0;

    //! \enum for measurement modes Action Level (PCS) and detection limit
    //public enum eActionLevelSettings { ACTION_LEVEL, DETECTION_LIMIT }

    //! \enum Activities that configure user settings
    public enum eWhichUserActivity {
        PERMISSIONS,
        CHANGE_PASSWORD,
        CHANGE_USERNAME,
        CHANGE_SETTINGS
    }

    //! Default Action Level for determining Positive or Negative results.
    public static final float[] PCS_ACTION_LEVEL = {1.0f, 0.7f, 0.5f};

    //! Positive results string for non-PCS readings
    public static String NON_PCS_POSITIVE = "POS";
    //! Negative results string for non-PCS readings
    public static String NON_PCS_NEGATIVE = "NEG";

    //! Region settings for configuring default safety rules like proximity detection or two
    //! handed operation. us = USA.
    public static String US_Region = "us";

    //! Region settings for configuring default safety rules like proximity detection or two
    //! handed operation. fr= france
    public static String FR_Region = "fr";

    //! Region settings for configuring default safety rules like proximity detection or two
    //! handed operation.  reg1 is intended for swiss.
    public static String REG1_Region = "reg1";

//    public static final int ModeLeadPaint = 0;
//    public static final int ModeSoils = 1;
//    public static final int ModeDustWipes = 2;

    //! Used to index the elem array, for now we only support Pb(lead) so it is index 0
    //! If there are other eleme support they should be indexed here.
    public static final int PbElemIndex = 0;

    public static final int PPT_TO_PPM = 1000;

}

//! \mainpage XRFServices Library
//!
//! XRFServices are a set of common classes, threads, and tools used by the Pb200i app and
//! the Factory and Service apps.
//! ________________
//!
//! \ref Constants "Constants used in XRFServices"<BR>
//! \ref ReadingData "Reading Data class"<BR>
//! \ref SafetyMicroIsotope "Safety manager and monitor"<BR>
