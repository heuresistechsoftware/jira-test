package com.heuresistech.xrfservices;

import android.content.Context;
import android.util.Log;

import com.heuresistech.smicro.SafetyMicroIsotope;

public class AcquireSpectrum {

    private ReadingData readingData;
    private DppService dpp;
    private Thread threadSpectrumCollector = null;

    public AcquireSpectrum(Context ctx) {
        // Start up the DPP service
        dpp = DppService.getInstance(ctx);

        boolean dppSts = dpp.connect(0, 0);

        ;// Log.w("StartSystemServices", "DPP connected " + dppSts);
        if (dppSts)
            dpp.launchDppService();
    }

    /**************************************************************************
     *	This thread is simply used as a trigger to call the service that
     *	acquires a reading data along with the spectrum.  (AcquireSpectrum ())
     */
    public void runSpectrumCollector(final int milliSecs, final boolean processSpectrum) {

        final int SHUTTER_SETTLE_TIME = 150;     // 150 ms to let shutter fully open

        threadSpectrumCollector = new Thread() {

            public void run() {
                Thread thisThread = Thread.currentThread();

                Log.i("SpecCollector", "Thread spec collector started " + milliSecs + "ms");

                try {

                    SafetyMicroIsotope.getInstance().enableInterlockBypass();
                    SafetyMicroIsotope.getInstance().openShutterNoInterlocks();
                    Thread.sleep(SHUTTER_SETTLE_TIME);

                    Log.i("SpecCollector", "Trig pulled get a spectrum");
                    dpp.runAcquireSpectrumThread(milliSecs, processSpectrum);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        threadSpectrumCollector.start();
    }

    public void stopSpectrumCollector() {
        threadSpectrumCollector = null;
        dpp.stopSpectrumCollection();
        dpp.clearSpectrum();

    }

}
