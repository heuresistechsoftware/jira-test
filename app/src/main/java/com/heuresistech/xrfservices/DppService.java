package com.heuresistech.xrfservices;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.heuresistech.smicro.SafetyMicroIsotope;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


//! \brief DppService is responsible for interfacing with DPP (Digital Pulse Processor)
//!
//! The DPP is collects the spectrum from the detector and bins along with all
//! the relevant data, like acquisition time, fast counts and slow counts.
//!
public class DppService {

    public static final String BROADCAST_ACTION =
            "com.heuresistech.dppservice.BROADCAST";
    public static final String ACTION_RESP =
            "com.heuresistech.xrfservices.RESULTS_MESSAGE_PROCESSED";
    public static final String BIAS_VOLTS = "Bias";
    public static final String BIAS_VOLTS_INSPEC = "BiasInSpec";
    public static final String BOARD_TEMP = "BoardTemp";
    public static final String BOARD_TEMP_INSPEC = "BoardTempInSpec";
    public static final String TEC_TEMP = "TECTemp";

    //private Intent ResultServiceIntent = new Intent(this, ResultsService.class);
    public static final String TEC_TEMP_INSPEC = "TECTempInSpec";
    //! Make sure there is only one instance of this class.
    private static DppService instance = null;
    private static Thread dppMonitorThread = null;

    private final Context callingCtx;
    private int mode, aMode;
    private Thread acquireSpectrumThread = null;

    public native boolean ConnectToDefaultDPP(int analyticMode);

    public native boolean Disconnect();

    public native boolean ClearSpectrum();

    public native boolean StartSpectrum();

    public native boolean StopSpectrum();

    public native boolean SendPresetAcquisitionTime(float seconds);

    public native boolean getDppSysMonitor();

    public static DppService getInstance(Context ctx) {
        if (instance == null) {
            instance = new DppService(ctx);
        }

        return instance;
    }

    //! Load the Native C library to access the DPP
    static {
        System.loadLibrary("Dppd-jni");
    }

    private int biasVolts;
    private boolean biasVoltsInSpec;
    private int boardTemp;
    private boolean boardTempinSpec;
    private int TECTemp;
    private boolean TECTempInSpec;


    private DppService(Context ctx) {
        biasVolts = 0;
        boardTemp = 0;
        TECTemp = 0;
        mode = aMode = 0;
        callingCtx = ctx.getApplicationContext();
    }

    public DppService() {
        callingCtx = null;
    }


    //! \brief connect sets up a connection over USB from CPU module to the DPP.
    public boolean connect(int mode_, int analyticMode) {

        aMode = analyticMode;
        mode = mode_;
        return ConnectToDefaultDPP(analyticMode);
    }

    //! \brief clearSpectrum Sets the DPP spectrum to zero so collection can start over.
    public boolean clearSpectrum() {
        return ClearSpectrum();
    }

    //! \brief startSpectrumCollection starts the spectrum collection process in the DPP
    public boolean startSpectrumCollection() {
        return StartSpectrum();
    }


    //! \brief stopSpectrumCollection stops the DPP from accumulating but does not clear the spectrum
    public boolean stopSpectrumCollection(){
        StopSpectrum();
        acquireSpectrumThread = null; // stop collection thread
        return true;
    }

    public void stopDppService(){
        dppMonitorThread = null;
    }


    public void launchDppService(){

        biasVoltsInSpec = false;
        boardTempinSpec = false;
        TECTempInSpec = false;

        final int MONITOR_REFRESH_TIME = 5000; // refresh every 5 seconds

        dppMonitorThread = new Thread() {
            public void run() {

                Thread thisThread = Thread.currentThread();

                while (dppMonitorThread == thisThread) {
                    try {

                        //;// Log.i("DppService", "Calling SysMonitor");

                        // First we check the DPP to get detector temp and DPP board temp.
                        // This should load local variable biasVolts, boardTemp, TECTemp.
                        getDppSysMonitor();
                        Thread.sleep(500);
                        getDppSysMonitor();


                        biasVoltsInSpec = isBiasVoltsInRange();

                        boardTempinSpec = isBoardTempInRange();

                        TECTempInSpec = isTECTTempInRange();

                        broadcastDppStatus(callingCtx);

                        //;// Log.i("DppService", "Done SysMonitor");

                        //Refresh monitored values every 5 seconds.
                        Thread.sleep(MONITOR_REFRESH_TIME);


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } // while
            } // void run
        }; // new Thread

        dppMonitorThread.setName("DppMonitorThread");
        dppMonitorThread.start();
    }

    private void broadcastDppStatus(Context ctx) {

        Intent broadcastIntent = new Intent(BROADCAST_ACTION);

        broadcastIntent.putExtra(BIAS_VOLTS, biasVolts);
        broadcastIntent.putExtra(BIAS_VOLTS_INSPEC, biasVoltsInSpec);
        broadcastIntent.putExtra(BOARD_TEMP, boardTemp);
        broadcastIntent.putExtra(BOARD_TEMP_INSPEC, boardTempinSpec);
        broadcastIntent.putExtra(TEC_TEMP, TECTemp);
        broadcastIntent.putExtra(TEC_TEMP_INSPEC, TECTempInSpec);

        if (ctx != null) ctx.sendBroadcast(broadcastIntent);
    }

    private boolean isBiasVoltsInRange() {

        final int BIAS_VOLTS_MAX_THRESHOLD = 660;
        final int BIAS_VOLTS_MIN_THRESHOLD = 540;

        return !(biasVolts > BIAS_VOLTS_MAX_THRESHOLD || biasVolts < BIAS_VOLTS_MIN_THRESHOLD);
    }

    private boolean isBoardTempInRange() {

        final int DPP_BOARD_TEMP_MAX = 85;
        return (boardTemp) <= DPP_BOARD_TEMP_MAX;
    }

    private boolean isTECTTempInRange() {

        final int TEC_TEMP_MAX = 255;
        //final int TEC_TEMP_MIN = 248;

        return TECTemp <= TEC_TEMP_MAX;
    }

    //*******************************************************
    //
    // The single purpose for this thread is to gather the spectrum
    // and send it to callReadingService.
    //
    //	This needs to be called once per spectrum collection.
    public void runAcquireSpectrumThread(final int collectionTime, final boolean processSpectrum) {

        acquireSpectrumThread = new Thread() {
            public void run() {

                Thread thisThread = Thread.currentThread();

                startSpectrumCollection();
                while (acquireSpectrumThread == thisThread) {

                    try {

                    // Sleep while the DPP collects the spectrum. In Factory Cal case
                    // we get one spectrum at the end of the collection. We are using
                    // a 1 second reading so we wait 1.2 seconds before we go get the
                    // spectrum.
                    Log.d("AcquireSpec", "CollectTime " + collectionTime);

                    Thread.sleep( collectionTime ) ;

                    processDpp(processSpectrum);


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            } // void run
        }; // new Thread

        acquireSpectrumThread.setName("acquireSpectrumThread");
        acquireSpectrumThread.start();

    } // runAcquireSpectrumThread

    private void processDpp(boolean processSpectrum ) {

        ReadingData rdg = new ReadingData();

        rdg.setRawSpectrum(new int[Constants.SPECTRA_SIZE_RETRIEVE]);
        rdg.setNormSpectrum(new int[Constants.SPECTRA_SIZE_RETRIEVE]);

        // For this FactoryCal application, only one measurement mode exists.
        // Not switchable.
        boolean success = rdg.populateFromDPP(aMode, mode, processSpectrum);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(ACTION_RESP);
        //broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);

        if (success) {
            // These were populated in Native code.
            // In AcquireSpectrum() call.
            broadcastIntent.putExtra("result", rdg.getResult());
            broadcastIntent.putExtra( "resultString", rdg.getResultString());

            broadcastIntent.putExtra("mgCm2", rdg.getConcentration());
            broadcastIntent.putExtra("confLev", rdg.getConfLev());
            broadcastIntent.putExtra("stdDev", rdg.getStdDev());
            broadcastIntent.putExtra("rawSpectrum", rdg.getRawSpectrum());
            broadcastIntent.putExtra("normSpectrum", rdg.getNormSpectrum());
            broadcastIntent.putExtra("accTime", rdg.getAccTime());
            broadcastIntent.putExtra("biasVolts", rdg.getBiasVolts());
            broadcastIntent.putExtra("boardTemp", rdg.getBoardTemp());
            broadcastIntent.putExtra("fastCount", rdg.getFastCount());
            broadcastIntent.putExtra("slowCount", rdg.getSlowCount());
            broadcastIntent.putExtra("realTime", rdg.getRealTime());
            broadcastIntent.putExtra("TECTemp", rdg.getTECTemp());
            broadcastIntent.putExtra("dppHwConfig", rdg.getDppHwConfigStr());
            broadcastIntent.putExtra("tempType", rdg.getTempType());
            broadcastIntent.putExtra("nomSecs", rdg.getnomSecs());
            broadcastIntent.putExtra("numBins", rdg.getNumBins());
            broadcastIntent.putExtra("k_gain", rdg.getKGain());
            broadcastIntent.putExtra("k_offset", rdg.getKOffset());

            callingCtx.sendBroadcast(broadcastIntent);
            Log.d("ProcessDPP", "Sent Broadcast DPP data");

            // Leave this here for debugging Pb detection spectrum. On HBI or Pb200i
            writeRawSpecToFile(rdg);
        }

    }

    private void writeRawSpecToFile(ReadingData rdg) {

        StringBuilder output = new StringBuilder(4096);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("MM-dd-yy", Locale.US);
        String formatDate = date.format(c.getTime());
        String filepath = Environment.getExternalStorageDirectory().getPath() + "/spectrum/debug/" + formatDate;
        File dir = new File(filepath);
        dir.mkdirs();
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss", Locale.US);
        String formattedTime = time.format(c.getTime()); // eg. 08:16:19
        String filename = "spectrum-" + formattedTime + ".csv";
        File file = new File(dir, filename);

        boolean fileExists = file.exists();

        // TODO Error handler for if the file can not be created.
        try {

            //FileOutputStream fileout = new FileOutputStream (file, bAppendToFile);
            //OutputStreamWriter outputWriter=new OutputStreamWriter (fileout);
            BufferedWriter outputWriter = new BufferedWriter(new FileWriter(file, false));
            //output.append( String.format(loc, "%d%s", rdg.getReadingNumber(), ","));
            //outputWriter.write(output.toString());
            int size = rdg.getRawSpectrum().length;
            int[] rawSpec;
            rawSpec = rdg.getRawSpectrum();

            // format the spectrum into a long string of numbers, separated by commas.
            //spectrum_out.append("spectrum\n");
            for (int i = 0; i < size; i++) {

                output.append(rawSpec[i] + ",");

            }

            // Now write the spectrum out to the file.
            //outputWriter.write(spectrum_out.toString());
            output.append("\n");

            // Write the reading data and spectrum if enabled
            outputWriter.write(output.toString());

            outputWriter.flush();
            outputWriter.close();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        callingCtx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));

    }


}
