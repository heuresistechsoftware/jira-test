package com.heuresistech.xrfservices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public abstract class SpectrumCollectionBase {

    DppService dpp;

    public void start( int secs, boolean processSpectrum, Context ctx){

        dpp =  DppService.getInstance(ctx);

        if (dpp != null) {
            dpp.StopSpectrum();
            dpp.ClearSpectrum();
            dpp.runAcquireSpectrumThread(secs * 1000, processSpectrum);
        }

    }

    public void stop(){

        if( dpp != null) {
            dpp.stopSpectrumCollection();
        }

    }

}